//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.common
 * File: Constants.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides globally accessible constant values.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 17, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.common;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.commons.lang3.tuple.ImmutablePair;

public class Constants
{
    /**
     * List of acceptable Bibliographic Identifiers. These can be used by the Bibliographic Import tool to search for
     * records.
     */
    public static final List<String> BIBLIO_ID_TYPES = Collections.unmodifiableList( Arrays.asList( new String[] { "ISBN", "ISSN", "EAN" } ) );

    /**
     * Aether's data storage directory.
     */
    public static final Path DIRECTORY_AETHER_STORAGE = Paths.get( System.getProperty( "user.home" ), "Aether" );

    /**
     * Directory where Lucene will store its index files.
     */
    public static final Path DIRECTORY_INDEX_STORAGE = Paths.get( DIRECTORY_AETHER_STORAGE.toString(), "SearchIndex" );

    /**
     * Directory where Aether will store its image uploads.
     */
    public static final Path DIRECTORY_SYSTEM_RESOURCES = Paths.get( DIRECTORY_AETHER_STORAGE.toString(), "Resources" );
    
    /**
     * Directory where thumbnails of image resources are stored.
     */
    public static final Path DIRECTORY_SYSTEM_RESOURCES_THUMBNAILS = Paths.get( DIRECTORY_SYSTEM_RESOURCES.toString(), "Thumbnails" );

    /**
     * String array of acceptable image extensions.
     */
    public static final String[] IMAGE_FILE_EXTENSIONS = new String[] { "jpg", "jpeg", "gif", "bmp", "png" };

    static
    {
        // Sort the array to allow it to be binary-searched
        Arrays.sort( IMAGE_FILE_EXTENSIONS );
    }

    /**
     * File names for system managed uploads.
     */
    public static final String FILE_NAME_CUSTOM_LOGO = "custom_logo.png";

    /**
     * Extracts a four-digit year.
     */
    public static final Pattern PATTERN_FOUR_DIGIT_YEAR = Pattern.compile( "(\\d{4})" );

    /**
     * Extracts either 10 or 13 digits.
     */
    public static final Pattern PATTERN_ISBN = Pattern.compile( "(\\d{10}|\\d{13})" );

    /**
     * Extracts two groups of 4 digits each separated by a hyphen.
     */
    public static final Pattern PATTERN_ISSN = Pattern.compile( "(\\d{4}-\\d{4})" );

    /**
     * Extracts 13 digits.
     */
    public static final Pattern PATTERN_EAN = Pattern.compile( "(\\d{13})" );

    /**
     * Extracts a variable-length group of 1 or more digits.
     */
    public static final Pattern PATTERN_LCCN = Pattern.compile( "(\\d+)" );

    /**
     * Extracts the primary title of, potentially, the subtitle of a bibliographic entry.
     */
    public static final Pattern PATTERN_COMPOUND_TITLE = Pattern.compile( "([^:/]+)(?:\\s*:\\s*)?([^:/]*?)(?:\\s*)/?" );

    /**
     * Default locale.
     */
    public static final String DEFAULT_LOCALE = "en_US";

    /**
     * List of item types used by Aether and their corresponding character code (MARC Leader, Position 06).
     */
    //@formatter:off
    public static final List<ImmutablePair<Character, String>> marcBiblioRecordTypes = Collections.unmodifiableList(
        Arrays.asList( 
            new ImmutablePair<Character, String>( 'a', "language_material" ),
            new ImmutablePair<Character, String>( 'c', "notated_music" ),
            new ImmutablePair<Character, String>( 'd', "manuscript_music" ),
            new ImmutablePair<Character, String>( 'e', "maps" ),
            new ImmutablePair<Character, String>( 'f', "manuscript_maps" ),
            new ImmutablePair<Character, String>( 'g', "projected_media" ),
            new ImmutablePair<Character, String>( 'i', "recorded_sound" ),
            new ImmutablePair<Character, String>( 'j', "recorded_music" ),
            new ImmutablePair<Character, String>( 'k', "2d_graphics" ),
            new ImmutablePair<Character, String>( 'm', "computer_file" ),
            new ImmutablePair<Character, String>( 'o', "kits" ),
            new ImmutablePair<Character, String>( 'p', "mixed_materials" ),
            new ImmutablePair<Character, String>( 'r', "3d_artifacts" ),
            new ImmutablePair<Character, String>( 't', "manuscripts" )
        )
    );
    //@formatter:on

    /**
     * Mapping of item type character codes (MARC Leader, Position 06) to Material Design Icons.
     */
    //@formatter:off
    public static final Map<Character, String> marcBiblioRecordTypeIcons = Collections.unmodifiableMap(
        new HashMap<Character, String>() {
            private static final long serialVersionUID = -837168619214962846L;
            {
                put( 'a', "library_books" );
                put( 'c', "queue_music" );
                put( 'd', "queue_music" );
                put( 'e', "map" );
                put( 'f', "map" );
                put( 'g', "local_movies" ); 
                put( 'i', "volume_up" );
                put( 'j', "library_music" );
                put( 'k', "photo" );
                put( 'm', "computer" );
                put( 'o', "shopping_basket" );            
                put( 'p', "collections" );
                put( 'r', "language" );
                put( 't', "subject" );      
            }
        }
    );
    //@formatter:on
}