//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.common
 * File: MarcUtilities.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 21, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.marc4j.marc.ControlField;
import org.marc4j.marc.DataField;
import org.marc4j.marc.Record;
import org.marc4j.marc.Subfield;
import org.marc4j.marc.VariableField;

public class MarcUtilities
{
    private static Logger log = LogManager.getLogger( MarcUtilities.class );

    private MarcUtilities()
    {
    }

    public static String getRecordType( Record record )
    {
        if( record.getLeader() != null )
        {
            return String.valueOf( record.getLeader().getTypeOfRecord() );
        }
        return "";
    }

    public static String getRecordTypeName( Record record )
    {
        if( record.getLeader() != null )
        {
            char recordType = record.getLeader().getTypeOfRecord();
            switch ( recordType )
            {
                case 'a':
                    return "language_material";
                case 'c':
                    return "notated_music";
                case 'd':
                    return "manuscript_music";
                case 'e':
                    return "maps";
                case 'f':
                    return "manuscript_maps";
                case 'g':
                    return "projected_media";
                case 'i':
                    return "recorded_sound";
                case 'j':
                    return "recorded_music";
                case 'k':
                    return "2d_graphics";
                case 'm':
                    return "computer_file";
                case 'o':
                    return "kits";
                case 'p':
                    return "mixed_materials";
                case 'r':
                    return "3d_artifacts";
                case 't':
                    return "manuscripts";
            }
        }
        return "";
    }

    public static String getControlNumber( Record record )
    {
        ControlField controlField = record.getControlNumberField();
        if( controlField != null )
        {
            return controlField.getData();
        }
        return "";
    }

    public static String getLCCN( Record record )
    {
        DataField lccnField = (DataField) record.getVariableField( "010" );
        if( lccnField != null && lccnField.getSubfield( 'a' ) != null )
        {
            return lccnField.getSubfield( 'a' ).getData().trim();
        }
        return "";
    }

    public static String getTitle( Record record )
    {
        String[] titleFields = new String[] { "240", "245" };
        List<VariableField> fields = record.getVariableFields( titleFields );
        StringBuffer title = new StringBuffer();

        for ( VariableField nextField : fields )
        {
            DataField dataField = (DataField) nextField;
            if( dataField.getSubfield( 'a' ) != null )
            {
                title.append( dataField.getSubfield( 'a' ).getData().trim() );
            }
            if( dataField.getSubfield( 'b' ) != null )
            {
                title.append( dataField.getSubfield( 'b' ).getData().trim() );
            }

            if( title.length() > 0 ) break;
        }

        return title.toString();
    }

    public static String getCreators( Record record )
    {
        StringBuffer authorBuffer = new StringBuffer();

        // First, check the 100 field
        DataField dataField = (DataField) record.getVariableField( "100" );
        if( dataField != null )
        {
            LinkedList<String> authors = new LinkedList<String>();
            for ( Subfield nextSubfield : dataField.getSubfields() )
            {
                // Generational Identifier needs to be separated from name by a space
                if( nextSubfield.getCode() == 'b' )
                {
                    authorBuffer.append( " " + nextSubfield.getData() );
                }
                else
                {
                    authorBuffer.append( " " + nextSubfield.getData() );
                }
            }

            authors.add( authorBuffer.toString() );
            authorBuffer = new StringBuffer();

            // In cases where there are 2 or 3 authors for a work, the second
            // and third authors will be listed in 700$a added entry fields
            List<VariableField> fields = record.getVariableFields( "700" );
            for ( VariableField nextField : fields )
            {
                DataField addedEntryField = (DataField) nextField;
                for ( Subfield nextSubfield : addedEntryField.getSubfields() )
                {
                    if( nextSubfield.getCode() == 'b' )
                    {
                        authorBuffer.append( " " + nextSubfield.getData() );
                    }
                    else
                    {
                        authorBuffer.append( " " + nextSubfield.getData() );
                    }
                }

                authors.add( authorBuffer.toString() );
                authorBuffer = new StringBuffer();
            }
            return String.join( "\n", authors );
        }

        // If no results found, check the 700$a added entry field.
        // In cases where there are more than 3 authors, only the
        // first (i.e. primary) author will be listed and will be
        // a 700$a field, not the 100$a field.
        dataField = (DataField) record.getVariableField( "700" );
        if( dataField != null )
        {
            for ( Subfield nextSubfield : dataField.getSubfields() )
            {
                if( nextSubfield.getCode() == 'b' )
                {
                    authorBuffer.append( " " + nextSubfield.getData() );
                }
                else
                {
                    authorBuffer.append( " " + nextSubfield.getData() );
                }
            }
            return authorBuffer.toString();
        }

        return "";
    }

    public static String getPublicationYear( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "260" );
        if( dataField != null && dataField.getSubfield( 'c' ) != null )
        {
            Matcher m = Constants.PATTERN_FOUR_DIGIT_YEAR.matcher( dataField.getSubfield( 'c' ).getData().trim() );
            if( m.matches() )
            {
                return m.group( 1 );
            }
            return dataField.getSubfield( 'c' ).getData().trim();
        }
        return "";
    }

    public static String getPublisherName( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "260" );
        if( dataField != null && dataField.getSubfield( 'b' ) != null )
        {
            return dataField.getSubfield( 'b' ).getData().trim();
        }
        return "";
    }

    public static String getPublicationLocation( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "260" );
        if( dataField != null && dataField.getSubfield( 'a' ) != null )
        {
            return dataField.getSubfield( 'a' ).getData().trim();
        }
        return "";
    }

    public static String getEditionStatement( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "250" );
        if( dataField != null && dataField.getSubfield( 'a' ) != null )
        {
            return dataField.getSubfield( 'a' ).getData().trim();
        }
        return "";
    }

    public static String getEditionResponsibility( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "250" );
        if( dataField != null && dataField.getSubfield( 'b' ) != null )
        {
            return dataField.getSubfield( 'b' ).getData().trim();
        }
        return "";
    }

    public static String getBiblioIds( Record record )
    {
        String biblioIds = "";

        biblioIds = getISBNs( record );
        if( biblioIds.length() > 0 ) return biblioIds;

        biblioIds = getISSNs( record );
        if( biblioIds.length() > 0 ) return biblioIds;

        biblioIds = getEANs( record );
        if( biblioIds.length() > 0 ) return biblioIds;

        biblioIds = getLCCN( record );
        if( biblioIds.length() > 0 ) return biblioIds;

        return "";
    }

    public static String getISBNs( Record record )
    {
        StringBuffer isbns = new StringBuffer();

        List<VariableField> fields = record.getVariableFields( "020" );
        for ( VariableField nextField : fields )
        {
            DataField dataField = (DataField) nextField;
            if( isbns.length() > 0 ) isbns.append( "\n" );
            if( dataField.getSubfield( 'a' ) != null ) isbns.append( dataField.getSubfield( 'a' ).getData().trim() );
        }

        return isbns.toString();
    }

    public static String getISSNs( Record record )
    {
        StringBuffer issns = new StringBuffer();

        List<VariableField> fields = record.getVariableFields( "022" );
        for ( VariableField nextField : fields )
        {
            DataField dataField = (DataField) nextField;
            if( issns.length() > 0 ) issns.append( "\n" );
            if( dataField.getSubfield( 'a' ) != null ) issns.append( dataField.getSubfield( 'a' ).getData().trim() );
        }

        return issns.toString();
    }

    public static String getEANs( Record record )
    {
        StringBuffer eans = new StringBuffer();

        List<VariableField> fields = record.getVariableFields( "024" );
        for ( VariableField nextField : fields )
        {
            DataField dataField = (DataField) nextField;
            if( eans.length() > 0 ) eans.append( "\n" );
            if( dataField.getIndicator1() == '3' && dataField.getSubfield( 'a' ) != null ) eans.append( dataField.getSubfield( 'a' ).getData().trim() );
        }

        return eans.toString();
    }

    public static Boolean isSerial( Record record )
    {
        char biblioLevel = record.getLeader().getImplDefined1()[0];
        return Boolean.valueOf( biblioLevel == 's' );
    }

    public static Date getCopyrightDate( Record record )
    {
        // @formatter:off
        //
        // ALOGRITHM:
        // (1) Prefer 264 #4$c (Copyright Date); otherwise,
        // (2) Pick the first date provided by 264$c (Production, Publication, Distribution, Manufacture, Copyright, etc.); otherwise,
        // (3) Pick the first date provided by 260$c (Function not specified).
        // (4) If a date was found, parse it into a java.util.Date and return that.
        // (5) If no date was found, return null.
        //
        // @formatter:on

        List<VariableField> fields264 = record.getVariableFields( "264" );
        List<VariableField> fields260 = record.getVariableFields( "260" );
        String copyrightNotice = null;

        try
        {
            // (1 - 2) Search for 264$c Subfields
            if( fields264 != null && fields264.size() > 0 )
            {
                for ( VariableField nextField : fields264 )
                {
                    DataField nextDataField = (DataField) nextField;
                    if( nextDataField.getSubfield( 'c' ) != null )
                    {
                        // (1) Prefer 264$c where Indicator 2 is '4'
                        if( nextDataField.getIndicator2() == '4' )
                        {
                            copyrightNotice = nextDataField.getSubfield( 'c' ).getData().trim();
                            break;
                        }
                        // (2) Fall-back to any 264$c
                        else if( copyrightNotice == null )
                        {
                            copyrightNotice = nextDataField.getSubfield( 'c' ).getData().trim();
                        }
                    }
                }
            }

            // (3) If no 264$c was found, fall-back to 260$c
            if( copyrightNotice == null && fields260 != null && fields260.size() > 0 )
            {
                for ( VariableField nextField : fields260 )
                {
                    DataField nextDataField = (DataField) nextField;
                    if( nextDataField.getSubfield( 'c' ) != null )
                    {
                        copyrightNotice = nextDataField.getSubfield( 'c' ).getData().trim();
                        break;
                    }
                }
            }

            // (4) If a date was found, parse it into a java.util.Date and return that.
            if( copyrightNotice != null )
            {
                Matcher m = Constants.PATTERN_FOUR_DIGIT_YEAR.matcher( copyrightNotice );
                if( m.matches() )
                {
                    return ( new SimpleDateFormat( "yyyy" ).parse( m.group( 1 ) ) );
                }
            }
        }
        catch ( Exception e )
        {
            MarcUtilities.log.error( e );
        }

        // (5) If no date was found, return null.
        return null;
    }

    public static String getVolumeInformation( Record record )
    {
        //@formatter:off
        //
        // ALGORITHM:
        // (1) Collect all 363 fields:
        //     a. Check for existence of $i field
        //     b. If at least $i is present, record each 363 
        //        field on a new line in the format $a.$b ($j $k, $i)
        //     c. Note that fields are: 
        //          i) $a = 1st level of enumeration (e.g. volume)
        //         ii) $b = 2nd level of enumeration (e.g. issue/number)
        //        iii) $i = year
        //         iv) $j = month
        //          v) $k = day
        // (2) If no satisfactory 363 fields are found, collect all 362 fields:
        //     a. Check for the existence of $a field
        //     b. Record all $a fields as they are with each on its own line
        // (3) If no satisfactory fields are found, return empty string.
        //
        //@formatter:on

        StringBuffer volumeInformation = new StringBuffer();
        List<VariableField> fields362 = record.getVariableFields( "362" );
        List<VariableField> fields363 = record.getVariableFields( "363" );

        try
        {
            // (1) Collect all 363 fields
            for ( VariableField nextField : fields363 )
            {
                DataField nextDataField = (DataField) nextField;

                // (1)a. Check for the existence of $i field
                if( nextDataField.getSubfield( 'i' ) != null )
                {
                    // (1)b. Format as $a.$b ($j $k, $i) with each 363 on a new line
                    if( volumeInformation.length() > 0 ) volumeInformation.append( "\n" );

                    if( nextDataField.getSubfield( 'a' ) != null )
                    {
                        volumeInformation.append( nextDataField.getSubfield( 'a' ).getData().trim() );
                        if( nextDataField.getSubfield( 'b' ) != null )
                        {
                            volumeInformation.append( "." + nextDataField.getSubfield( 'b' ).getData().trim() );
                        }
                        volumeInformation.append( " (" );
                    }

                    if( nextDataField.getSubfield( 'j' ) != null )
                    {
                        volumeInformation.append( nextDataField.getSubfield( 'j' ).getData().trim() + " " );
                        if( nextDataField.getSubfield( 'k' ) != null )
                        {
                            volumeInformation.append( nextDataField.getSubfield( 'k' ).getData().trim() + ", " );
                        }
                        volumeInformation.append( nextDataField.getSubfield( 'i' ).getData().trim() );
                    }

                    if( nextDataField.getSubfield( 'a' ) != null )
                    {
                        volumeInformation.append( ")" );
                    }
                }
            }

            // (2) If no satisfactory 363 fields are found, collect all 362 fields
            if( volumeInformation.length() < 1 )
            {
                for ( VariableField nextField : fields362 )
                {
                    DataField nextDataField = (DataField) nextField;

                    // (2)a. Check for the existence of $a field
                    if( nextDataField.getSubfield( 'a' ) != null )
                    {
                        // (2)b. Record all $a fields as they are with each on its own line
                        if( volumeInformation.length() > 0 ) volumeInformation.append( "\n" );
                        volumeInformation.append( nextDataField.getSubfield( 'a' ).getData().trim() );
                    }
                }
            }
        }
        catch ( Exception e )
        {
            MarcUtilities.log.error( e );
        }

        // (3) If no satisfactory fields were found, return empty string
        return volumeInformation.toString();
    }

    public static String getSeriesTitle( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "490" );
        if( dataField != null && dataField.getSubfield( 'a' ) != null )
        {
            return dataField.getSubfield( 'a' ).getData().trim();
        }
        return "";
    }

    public static String getSeriesVolume( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "490" );
        if( dataField != null && dataField.getSubfield( 'v' ) != null )
        {
            return dataField.getSubfield( 'v' ).getData().trim();
        }
        return "";
    }

    public static String getSeriesIssn( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "490" );
        if( dataField != null && dataField.getSubfield( 'x' ) != null )
        {
            return dataField.getSubfield( 'x' ).getData().trim();
        }
        return "";
    }

    public static String getAbstractText( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "520" );
        if( dataField != null && dataField.getSubfield( 'a' ) != null )
        {
            return dataField.getSubfield( 'a' ).getData().trim();
        }
        return "";
    }

    // Physical characteristics such as illustrative matter, coloration, playing speed,
    // groove characteristics, presence and kind of sound, number of channels, motion
    // picture presentation format, etc.
    public static String getPhysicalOther( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "300" );
        if( dataField != null && dataField.getSubfield( 'b' ) != null )
        {
            return dataField.getSubfield( 'b' ).getData().trim();
        }
        return "";
    }

    // Number of physical pages, volumes, cassettes, total playing time, etc.
    public static String getPhysicalExtent( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "300" );
        if( dataField != null && dataField.getSubfield( 'a' ) != null )
        {
            return dataField.getSubfield( 'a' ).getData().trim();
        }
        return "";
    }

    // Expressed in centimeters, millimeters, or inches; may include a parenthetical
    // qualifier giving the format of the item (e.g., (fol.), (8vo)).
    public static String getPhysicalDimensions( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "300" );
        if( dataField != null && dataField.getSubfield( 'c' ) != null )
        {
            return dataField.getSubfield( 'c' ).getData().trim();
        }
        return "";
    }

    public static String getUrl( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "856" );
        if( dataField != null && dataField.getSubfield( 'u' ) != null )
        {
            return dataField.getSubfield( 'u' ).getData().trim();
        }
        return "";
    }

    public static String getTargetAudience( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "521" );
        if( dataField != null && dataField.getSubfield( 'a' ) != null )
        {
            return dataField.getSubfield( 'a' ).getData().trim();
        }
        return "";
    }

    public static String getCallNumber( Record record )
    {
        StringBuffer callNumber = new StringBuffer();
        DataField dataField = (DataField) record.getVariableField( "050" );
        if( dataField != null && dataField.getSubfield( 'a' ) != null )
        {
            callNumber.append( dataField.getSubfield( 'a' ).getData().trim() );
            if( dataField.getSubfield( 'b' ) != null )
            {
                callNumber.append( " " );
                callNumber.append( dataField.getSubfield( 'b' ).getData().trim() );
            }
        }
        return callNumber.toString();
    }

    public static String getDeweyDecimal( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "082" );
        if( dataField != null && dataField.getSubfield( 'a' ) != null )
        {
            return dataField.getSubfield( 'a' ).getData().trim();
        }
        return "";
    }

    public static String getSubjects( Record record )
    {
        List<VariableField> subjectFields = record.getVariableFields( new String[] { "600", "610", "611", "630", "648", "650", "650", "651" } );
        List<String> subjects = new ArrayList<String>( subjectFields.size() );

        for ( VariableField nextField : subjectFields )
        {
            DataField nextSubjectDataField = (DataField) nextField;
            if( nextSubjectDataField.getSubfield( 'a' ) != null )
            {
                subjects.add( nextSubjectDataField.getSubfield( 'a' ).getData().trim() );
            }
        }

        if( subjects.size() > 0 )
        {
            return StringUtils.join( subjects.toArray(), "\n" );
        }

        return "";
    }

    public static String getResponsibilityStatement( Record record )
    {
        DataField dataField = (DataField) record.getVariableField( "245" );
        if( dataField != null && dataField.getSubfield( 'c' ) != null )
        {
            return dataField.getSubfield( 'c' ).getData().trim();
        }
        return "";
    }
}