//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.srumarc4j
 * File: SruMarcXmlReader.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * MARC4J MarcXmlReader that parses a MARC XML collection out of an SRU response.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 21, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.srumarc4j;

import java.io.StringReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.marc4j.MarcException;
import org.xml.sax.InputSource;
import com.aether.ejb.viewmodels.SruResponseRecordSet;

public class SruMarcXmlReader
{
    private static Logger log = LogManager.getLogger( SruMarcXmlReader.class );

    private SruResponseRecordSet recordSet = new SruResponseRecordSet();
    private InputSource in = null;

    public SruMarcXmlReader(StringReader sr)
    {
        in = new InputSource( sr );
    }

    public SruResponseRecordSet parse()
    {
        try
        {
            SruPackageHandler handler = new SruPackageHandler( recordSet );
            SruMarcXmlParser parser = new SruMarcXmlParser( handler );
            parser.parse( in );
        }
        catch ( MarcException me )
        {
            recordSet.setParserError( Boolean.TRUE );
            recordSet.setMarcParserError( me );
            SruMarcXmlReader.log.error( me );
        }

        return recordSet;
    }
}