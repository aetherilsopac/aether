//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.srumarc4j
 * File: SruMarcXmlParser.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Handles executing the SAX parsing of an SRU Response that carries a MARC XML payload.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 21, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.srumarc4j;

import javax.xml.parsers.SAXParserFactory;
import org.marc4j.MarcException;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public class SruMarcXmlParser
{
    private ContentHandler handler = null;

    public SruMarcXmlParser(SruPackageHandler handler)
    {
        this.handler = handler;
    }

    public void parse( InputSource input )
    {
        parse( handler, input );
    }

    private void parse( ContentHandler handler, InputSource input )
    {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        XMLReader reader = null;
        try
        {
            reader = spf.newSAXParser().getXMLReader();
            reader.setFeature( "http://xml.org/sax/features/namespaces", true );
            reader.setFeature( "http://xml.org/sax/features/namespace-prefixes", true );
            reader.setContentHandler( handler );
            reader.parse( input );
        }
        catch ( Exception e )
        {
            throw new MarcException( "Unable to parse input", e );
        }
    }

}
