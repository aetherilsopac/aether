//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: 
 * File: SruPackageHandler.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Unwraps the payload of the SRU response and returns an SruQueryRecordSet object with version, total records available, and raw MARC XML populated.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 19, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.srumarc4j;

import java.util.HashMap;
import org.apache.commons.lang3.StringEscapeUtils;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import com.aether.ejb.viewmodels.SruResponseRecordSet;

public class SruPackageHandler implements ContentHandler
{
    private StringBuffer rawMarcXml;
    private StringBuffer sb;
    private int totalRecordsReturned = 0;
    private SruResponseRecordSet sruRecordSet = null;
    private boolean writeToRawMarcXml = false;

    /** Constants representing each valid tag type */
    private static final int SEARCH_RETRIEVE_RESPONSE_ID = 1;

    private static final int VERSION_ID = 2;

    private static final int NUMBER_OF_RECORDS_ID = 3;

    private static final int RECORDS_ID = 4;

    private static final int SRU_RECORD_ID = 5;

    private static final int RECORD_SCHEMA_ID = 6;

    private static final int RECORD_PACKING_ID = 7;

    private static final int RECORD_DATA_ID = 8;

    private static final int RECORD_ID = 9;

    private static final int CONTROL_FIELD_ID = 10;

    /** XML Namespace attribute */
    private static final String XMLNS_ATTR = "xmlns";

    /** Hashset for mapping of element strings to constants (Integer) */
    private static final HashMap<String, Integer> elementMap;

    static
    {
        elementMap = new HashMap<String, Integer>();

        elementMap.put( "searchRetrieveResponse", new Integer( SEARCH_RETRIEVE_RESPONSE_ID ) );
        elementMap.put( "version", new Integer( VERSION_ID ) );
        elementMap.put( "numberOfRecords", new Integer( NUMBER_OF_RECORDS_ID ) );
        elementMap.put( "records", new Integer( RECORDS_ID ) );
        elementMap.put( "sruRecord", SRU_RECORD_ID );
        elementMap.put( "recordSchema", RECORD_SCHEMA_ID );
        elementMap.put( "recordPacking", RECORD_PACKING_ID );
        elementMap.put( "recordData", RECORD_DATA_ID );
        elementMap.put( "record", RECORD_ID );
        elementMap.put( "controlfield", CONTROL_FIELD_ID );
    }

    public SruPackageHandler(SruResponseRecordSet recordSet)
    {
        this.sruRecordSet = recordSet;
    }

    @Override
    public void startDocument() throws SAXException
    {
    }

    @Override
    public void startElement( String uri, String name, String qName, Attributes atts ) throws SAXException
    {

        String realname = ( name.length() == 0 ) ? qName : name;
        Integer elementType = null;

        if( realname.equalsIgnoreCase( "record" ) )
        {
            if( atts.getValue( XMLNS_ATTR ) != null )
            {
                elementType = elementMap.get( "record" );
            }
            else
            {
                elementType = elementMap.get( "sruRecord" );
            }
        }
        else
        {
            elementType = elementMap.get( realname );
        }

        elementType = ( elementType != null ) ? elementType : -1;

        switch ( elementType.intValue() )
        {
            case SEARCH_RETRIEVE_RESPONSE_ID:
                break;
            case VERSION_ID:
                sb = new StringBuffer();
                break;
            case NUMBER_OF_RECORDS_ID:
                sb = new StringBuffer();
                break;
            case RECORDS_ID:
                rawMarcXml = new StringBuffer();
                rawMarcXml.append( "<collection>" );
                break;
            case SRU_RECORD_ID:
                break;
            case RECORD_SCHEMA_ID:
                sb = new StringBuffer();
                break;
            case RECORD_PACKING_ID:
                sb = new StringBuffer();
                break;
            case RECORD_DATA_ID:
                break;
            case RECORD_ID:
                writeToRawMarcXml = true;
                totalRecordsReturned++;
            default:
                if( writeToRawMarcXml ) createStartTag( qName, atts );
        }
    }

    @Override
    public void characters( char[] ch, int start, int length ) throws SAXException
    {
        // I stumbled across a few MARC records from the Library of Congress
        // that have the <, >, &, and other characters in data fields (e.g. 310$b of ISSN 0278-6648).
        // This causes the SAX parser to error out, so I am escaping these characters here.
        String input = String.copyValueOf( ch, start, length );
        input = StringEscapeUtils.escapeXml11( input );

        if( writeToRawMarcXml )
        {
            rawMarcXml.append( input );
        }
        if( sb != null ) sb.append( input );
    }

    @Override
    public void endElement( String uri, String name, String qName ) throws SAXException
    {

        String realname = ( name.length() == 0 ) ? qName : name;
        Integer elementType = null;

        if( realname.equalsIgnoreCase( "record" ) )
        {
            if( writeToRawMarcXml )
            {
                elementType = elementMap.get( "record" );
            }
            else
            {
                elementType = elementMap.get( "sruRecord" );
            }
        }
        else
        {
            elementType = elementMap.get( realname );
        }

        elementType = ( elementType != null ) ? elementType : -1;

        switch ( elementType.intValue() )
        {
            case SEARCH_RETRIEVE_RESPONSE_ID:
                break;
            case VERSION_ID:
                sruRecordSet.setSruVersion( sb.toString() );
                sb = null;
                break;
            case NUMBER_OF_RECORDS_ID:
                sruRecordSet.setTotalRecordsAvailable( Integer.parseInt( sb.toString() ) );
                sb = null;
                break;
            case RECORDS_ID:
                rawMarcXml.append( "</collection>" );
                sruRecordSet.setRawMarcXml( rawMarcXml.toString() );
                rawMarcXml = null;
                sruRecordSet.setTotalRecordsReturned( totalRecordsReturned );
                break;
            case SRU_RECORD_ID:
                break;
            case RECORD_SCHEMA_ID:
                if( sb.indexOf( "marcxml" ) < 0 ) throw new SAXException( "SRU record schema not identified as \"marcxml\"." );
                sb = null;
                break;
            case RECORD_PACKING_ID:
                if( sb.indexOf( "xml" ) < 0 ) throw new SAXException( "SRU record packing not identified as \"xml\"." );
                sb = null;
                break;
            case RECORD_DATA_ID:
                break;
            case RECORD_ID:
                createEndTag( name );
                writeToRawMarcXml = false;
            default:
                if( writeToRawMarcXml ) createEndTag( name );
        }

    }

    private void createStartTag( String qName, Attributes atts )
    {
        if( rawMarcXml != null )
        {
            rawMarcXml.append( "<" + qName );
            writeAttributes( atts );
            rawMarcXml.append( ">" );
        }
    }

    private void writeAttributes( Attributes atts )
    {
        if( rawMarcXml != null )
        {
            for ( int i = 0; i < atts.getLength(); i++ )
            {
                rawMarcXml.append( " " + atts.getQName( i ) + "=\"" + atts.getValue( i ) + "\"" );
            }
        }
    }

    private void createEndTag( String qName )
    {
        if( rawMarcXml != null )
        {
            rawMarcXml.append( "</" + qName + ">" );
        }
    }

    @Override
    public void endDocument() throws SAXException
    {
        // not implemented
    }

    @Override
    public void ignorableWhitespace( char[] data, int offset, int length ) throws SAXException
    {
        // not implemented
    }

    @Override
    public void endPrefixMapping( String prefix ) throws SAXException
    {
        // not implemented
    }

    @Override
    public void skippedEntity( String name ) throws SAXException
    {
        // not implemented
    }

    @Override
    public void setDocumentLocator( Locator locator )
    {
        // not implemented
    }

    @Override
    public void processingInstruction( String target, String data ) throws SAXException
    {
        // not implemented
    }

    @Override
    public void startPrefixMapping( String prefix, String uri ) throws SAXException
    {
        // not implemented
    }

}