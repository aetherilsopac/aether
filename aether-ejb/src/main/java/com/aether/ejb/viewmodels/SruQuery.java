//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.web.viewmodels
 * File: BiblioImportQuery.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Shuttles an SRU query from the JavaScript front-end to the back-end.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 17, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.viewmodels;

import java.io.Serializable;
import com.aether.ejb.models.SruSource;

public class SruQuery implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = -1865269911188629335L;

    private String biblioId;
    private String biblioIdType;
    private SruSource sruSource;
    private int pageSize;
    private int page;

    public String getBiblioId()
    {
        return biblioId;
    }

    public String getBiblioIdType()
    {
        return biblioIdType;
    }

    public SruSource getSruSource()
    {
        return sruSource;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public int getPage()
    {
        return page;
    }

    public void setBiblioId( String biblioId )
    {
        this.biblioId = biblioId;
    }

    public void setBiblioIdType( String biblioIdType )
    {
        this.biblioIdType = biblioIdType;
    }

    public void setSruSource( SruSource sruSource )
    {
        this.sruSource = sruSource;
    }

    public void setPageSize( int pageSize )
    {
        this.pageSize = pageSize;
    }

    public void setPage( int page )
    {
        this.page = page;
    }
}
