//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.viewmodels
 * File: IndexQuery.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Transforms a catalog/biblio query into a format easily executed against the Lucene index.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Oct 11, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.viewmodels;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.aether.ejb.enums.IndexField;

public class IndexQuery implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = -6507118281954121349L;

    /**
     * Specifies what kind of query will be constructed from the parameters.
     * - SINGLE_FIELD: Only a single field (specified in the IndexQuery constructor) will be queried on.
     * - MULTI_FIELD: All queryable fields with valid values will be queried on.
     * - WHOLISTIC: The query string will be parsed using StandardAnalyzer and queries against the "ALL" field.
     */
    public enum QueryType
    {
        SINGLE_FIELD, MULTI_FIELD, WHOLISTIC
    }

    private QueryType queryType = null;
    private HashMap<IndexField, Object> queryValues = new HashMap<IndexField, Object>();
    private int pageSize;
    private int page;

    /**
     * Query for biblios on all fields with valid values. The fields that may be queried on are listed in
     * {@link IndexField}.
     * 
     * @param biblioQuery
     *            BiblioObject specifying what field(s) are to be queried on and their corresponding query values.
     */
    public IndexQuery(BiblioQuery biblioQuery)
    {
        page = biblioQuery.getPage();
        pageSize = biblioQuery.getPageSize();

        if( biblioQuery.getField() == IndexField.FIELD_BIBLIO_ALL )
        {
            queryType = QueryType.WHOLISTIC;
            if( biblioQuery.getQuery() != null )
            {
                queryValues.put( IndexField.FIELD_BIBLIO_ALL, biblioQuery.getQuery() );
            }
        }
        else
        {
            if( biblioQuery.getField() == IndexField.FIELD_BIBLIO_MULTI )
            {
                queryType = QueryType.MULTI_FIELD;
            }
            else
            {
                queryType = QueryType.SINGLE_FIELD;
            }

            // QUERY
            if( biblioQuery.getQuery() != null )
            {
                queryValues.put( biblioQuery.getField(), biblioQuery.getQuery() );
            }

            // ID
            if( biblioQuery.getId() != null )
            {
                queryValues.put( IndexField.FIELD_BIBLIO_ID, biblioQuery.getId() );
            }

            // TITLE
            if( biblioQuery.getTitle() != null && biblioQuery.getTitle().length() > 0 )
            {
                queryValues.put( IndexField.FIELD_BIBLIO_TITLE, biblioQuery.getTitle() );
            }

            // ISBN
            if( biblioQuery.getIsbn() != null && biblioQuery.getIsbn().length() > 0 )
            {
                queryValues.put( IndexField.FIELD_BIBLIO_ISBN, biblioQuery.getIsbn() );
            }

            // ISSN
            if( biblioQuery.getIssn() != null && biblioQuery.getIssn().length() > 0 )
            {
                queryValues.put( IndexField.FIELD_BIBLIO_ISSN, biblioQuery.getIssn() );
            }

            // EAN
            if( biblioQuery.getEan() != null && biblioQuery.getEan().length() > 0 )
            {
                queryValues.put( IndexField.FIELD_BIBLIO_EAN, biblioQuery.getEan() );
            }

            // LCCN
            if( biblioQuery.getLccn() != null && biblioQuery.getLccn().length() > 0 )
            {
                queryValues.put( IndexField.FIELD_BIBLIO_LCCN, biblioQuery.getLccn() );
            }

            // DEWEY DECIMAL
            if( biblioQuery.getDeweyDecimal() != null && biblioQuery.getDeweyDecimal().length() > 0 )
            {
                queryValues.put( IndexField.FIELD_BIBLIO_DEWEY_DECIMAL, biblioQuery.getDeweyDecimal() );
            }

            // SUBJECTS
            if( biblioQuery.getSubjects() != null && biblioQuery.getSubjects().length() > 0 )
            {
                queryValues.put( IndexField.FIELD_BIBLIO_SUBJECTS, biblioQuery.getSubjects() );
            }

            // CALL NUMBER
            if( biblioQuery.getCallNumber() != null && biblioQuery.getCallNumber().length() > 0 )
            {
                queryValues.put( IndexField.FIELD_BIBLIO_CALL_NUMBER, biblioQuery.getCallNumber() );
            }
        }
    }

    /**
     * The type of query this IndexQuery represents.
     */
    public QueryType getQueryType()
    {
        return queryType;
    }

    /**
     * A list of IndexField values mapped to the actual query value for those fields.
     */
    public Map<IndexField, Object> getQueryValues()
    {
        return queryValues;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public int getPage()
    {
        return page;
    }

    public void setPageSize( int pageSize )
    {
        this.pageSize = pageSize;
    }

    public void setPage( int page )
    {
        this.page = page;
    }
}
