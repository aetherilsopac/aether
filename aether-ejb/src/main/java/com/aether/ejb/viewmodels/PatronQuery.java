//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.web.viewmodels
 * File: PatronQuery.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Shuttles a patron query from the JavaScript front-end to the back-end.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 7, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.viewmodels;

import java.io.Serializable;

public class PatronQuery implements Serializable
{
    /**
     * GUID for Serializable
     */
    private static final long serialVersionUID = -2659003148754160671L;

    private String cardNumber;
    private String lastName;
    private String firstName;
    private String userName;
    private int pageSize;
    private int page;

    public String getCardNumber()
    {
        return cardNumber;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getUserName()
    {
        return userName;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public int getPage()
    {
        return page;
    }

    public void setPageSize( int pageSize )
    {
        this.pageSize = pageSize;
    }

    public void setPage( int page )
    {
        this.page = page;
    }

    public void setCardNumber( String cardNumber )
    {
        this.cardNumber = cardNumber;
    }

    public void setLastName( String lastName )
    {
        this.lastName = lastName;
    }

    public void setFirstName( String firstName )
    {
        this.firstName = firstName;
    }

    public void setUserName( String userName )
    {
        this.userName = userName;
    }

}
