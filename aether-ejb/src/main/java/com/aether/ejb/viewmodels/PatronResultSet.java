//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.web.viewmodels
 * File: PatronResultSet.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Shuttles the results of a patron query from the back-end to the JavaScript front-end.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 7, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.viewmodels;

import java.io.Serializable;
import java.util.List;
import com.aether.ejb.models.minimized.UserMinimized;

public class PatronResultSet implements Serializable
{
    /**
     * GUID for Serializable
     */
    private static final long serialVersionUID = 9062990940485899784L;

    private List<UserMinimized> patrons;
    private long totalResults;

    public PatronResultSet()
    {
    }

    public PatronResultSet(List<UserMinimized> patrons, long totalPatrons)
    {
        this.patrons = patrons;
        this.totalResults = totalPatrons;
    }

    public List<UserMinimized> getPatrons()
    {
        return patrons;
    }

    public long getTotalResults()
    {
        return totalResults;
    }

    public void setPatrons( List<UserMinimized> patrons )
    {
        this.patrons = patrons;
    }

    public void setTotalResults( long totalResults )
    {
        this.totalResults = totalResults;
    }
}
