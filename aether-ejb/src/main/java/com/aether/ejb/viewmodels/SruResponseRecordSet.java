//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.viewmodels
 * File: SruQueryRecordSet.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Shuttles the results of an SRU query from the back-end to the JavaScript front-end.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 18, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.viewmodels;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.marc4j.MarcException;
import com.aether.ejb.models.Biblio;

public class SruResponseRecordSet implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = -2850969463844221286L;

    private String sruVersion;
    private int totalRecordsAvailable;
    private int totalRecordsReturned;
    private List<Biblio> records = new LinkedList<Biblio>();
    private String rawMarcXml = "";
    private Boolean parserError = Boolean.FALSE;
    private MarcException marcParserError = null;

    public String getSruVersion()
    {
        return sruVersion;
    }

    public int getTotalRecordsAvailable()
    {
        return totalRecordsAvailable;
    }

    public int getTotalRecordsReturned()
    {
        return totalRecordsReturned;
    }

    public List<Biblio> getRecords()
    {
        return records;
    }

    public String getRawMarcXml()
    {
        return rawMarcXml;
    }

    public Boolean isParserError()
    {
        return parserError;
    }

    public MarcException getMarcParserError()
    {
        return marcParserError;
    }

    public void setSruVersion( String sruVersion )
    {
        this.sruVersion = sruVersion;
    }

    public void setTotalRecordsAvailable( int totalRecordsAvailable )
    {
        this.totalRecordsAvailable = totalRecordsAvailable;
    }

    public void setTotalRecordsReturned( int totalRecordsReturned )
    {
        this.totalRecordsReturned = totalRecordsReturned;
    }

    public void setRecords( List<Biblio> records )
    {
        this.records = records;
    }

    public void setRawMarcXml( String rawMarcXml )
    {
        this.rawMarcXml = rawMarcXml;
    }

    public void setParserError( Boolean parserError )
    {
        this.parserError = parserError;
    }

    public void setMarcParserError( MarcException marcParserError )
    {
        this.marcParserError = marcParserError;
    }
}
