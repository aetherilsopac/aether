//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.web.viewmodels
 * File: PatronQuery.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Shuttles a catalog query from the JavaScript front-end to the back-end.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 7, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.viewmodels;

import java.io.Serializable;
import com.aether.ejb.enums.IndexField;

public class BiblioQuery implements Serializable
{
    /**
     * GUID for Serializable
     */
    private static final long serialVersionUID = 5074879865046617153L;

    private Integer id;
    private String title;
    private String isbn;
    private String issn;
    private String ean;
    private String lccn;
    private String deweyDecimal;
    private String subjects;
    private String callNumber;
    private String query;
    private int pageSize;
    private int page;
    private IndexField field;

    public Integer getId()
    {
        return id;
    }

    public String getTitle()
    {
        return title;
    }

    public String getIsbn()
    {
        return isbn;
    }

    public String getIssn()
    {
        return issn;
    }

    public String getEan()
    {
        return ean;
    }

    public String getLccn()
    {
        return lccn;
    }

    public String getDeweyDecimal()
    {
        return deweyDecimal;
    }

    public String getSubjects()
    {
        return subjects;
    }

    public String getCallNumber()
    {
        return callNumber;
    }

    public String getQuery()
    {
        return query;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public int getPage()
    {
        return page;
    }

    public IndexField getField()
    {
        return field;
    }

    public void setId( Integer id )
    {
        this.id = id;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }

    public void setIsbn( String isbn )
    {
        this.isbn = isbn;
    }

    public void setIssn( String issn )
    {
        this.issn = issn;
    }

    public void setEan( String ean )
    {
        this.ean = ean;
    }

    public void setLccn( String lccn )
    {
        this.lccn = lccn;
    }

    public void setDeweyDecimal( String deweyDecimal )
    {
        this.deweyDecimal = deweyDecimal;
    }

    public void setSubjects( String subjects )
    {
        this.subjects = subjects;
    }

    public void setCallNumber( String callNumber )
    {
        this.callNumber = callNumber;
    }

    public void setQuery( String query )
    {
        this.query = query;
    }

    public void setPageSize( int pageSize )
    {
        this.pageSize = pageSize;
    }

    public void setPage( int page )
    {
        this.page = page;
    }

    public void setField( IndexField field )
    {
        this.field = field;
    }

}
