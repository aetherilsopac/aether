//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.indexing
 * File: Reindexer.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 12, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.indexing;

import java.time.Instant;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.aether.ejb.beans.interfaces.BiblioBusinessLocal;
import com.aether.ejb.beans.interfaces.SystemSettingsBusinessLocal;
import com.aether.ejb.enums.SystemSettingName;
import com.aether.ejb.models.Biblio;
import com.aether.ejb.models.SystemSetting;

public class Reindexer extends Observable implements Runnable
{
    private static Logger log = LogManager.getLogger( Reindexer.class );

    private AtomicBoolean keepRunning = new AtomicBoolean( true );
    private IndexManager indexManager;
    private BiblioBusinessLocal biblioEjb;
    private SystemSettingsBusinessLocal systemSettingsEjb;
    private Long totalBiblios = 0L;
    private int firstBiblio = 0;
    private int percentComplete = 0;
    private static final int BATCH_SIZE = 10_000;
    private List<Biblio> biblioBatch;

    public Reindexer(BiblioBusinessLocal biblioEjb, SystemSettingsBusinessLocal systemSettingsEjb, IndexManager indexManager)
    {
        this.biblioEjb = biblioEjb;
        this.systemSettingsEjb = systemSettingsEjb;
        this.indexManager = indexManager;
    }

    @Override
    public void run()
    {
        try
        {
            totalBiblios = biblioEjb.getTotal();

            while ( keepRunning.get() && firstBiblio < totalBiblios )
            {
                biblioBatch = biblioEjb.getBatch( firstBiblio, Reindexer.BATCH_SIZE );
                firstBiblio += biblioBatch.size();

                for ( Biblio nextBiblio : biblioBatch )
                {
                    indexManager.addDocument( nextBiblio );
                }

                updatePercentComplete( (int) ( (double) firstBiblio / totalBiblios * 100.0d ) );

                try
                {
                    Thread.sleep( 200 );
                }
                catch ( InterruptedException e )
                {
                    indexManager.setReindexing( false );
                    keepRunning.set( false );
                }
            }

            // Update audit data in the System Settings table
            SystemSetting lastReindexDate = systemSettingsEjb.getSetting( SystemSettingName.LAST_REINDEX_DATE.name() );
            lastReindexDate.setValue( Instant.now().toString() );
            systemSettingsEjb.saveSetting( lastReindexDate );

            SystemSetting lastReindexRecordCount = systemSettingsEjb.getSetting( SystemSettingName.LAST_REINDEX_RECORD_COUNT.name() );
            lastReindexRecordCount.setValue( totalBiblios.toString() );
            systemSettingsEjb.saveSetting( lastReindexRecordCount );
        }
        catch ( Exception e )
        {
            indexManager.setReindexing( false );
            Reindexer.log.error( "Exception encountered while re-indexing", e );
        }
    }

    private void updatePercentComplete( int percentComplete )
    {
        if( this.percentComplete != percentComplete )
        {
            setChanged();
            notifyObservers( new Integer( percentComplete ) );
            this.percentComplete = percentComplete;
        }
    }
}
