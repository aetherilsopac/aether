//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.indexing
 * File: IndexManager.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Oct 9, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.indexing;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.ControlledRealTimeReopenThread;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.SearcherFactory;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import com.aether.ejb.beans.interfaces.BiblioBusinessLocal;
import com.aether.ejb.beans.interfaces.SystemSettingsBusinessLocal;
import com.aether.ejb.common.Constants;
import com.aether.ejb.enums.IndexField;
import com.aether.ejb.models.Biblio;
import com.aether.ejb.viewmodels.IndexQuery;
import com.aether.ejb.viewmodels.IndexResultSet;

@Startup
@Singleton( mappedName = "ejb/IndexManager" )
public class IndexManager implements IndexManagerLocal
{
	private static Logger log = LogManager.getLogger( IndexManager.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/BiblioBusiness" )
	private BiblioBusinessLocal biblioEjb;
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/SystemSettingsBusiness" )
	private SystemSettingsBusinessLocal systemSettingsEjb;
	
	private static final String FIELD_BIBLIO_ALL = "all";
	private static final String FIELD_BIBLIO_UPDATE_DATE = "update_date";
	
	private AtomicBoolean isReindexing = new AtomicBoolean( false );
	private AtomicBoolean isSearchable = new AtomicBoolean( false );
	private AtomicInteger reindexProgress = new AtomicInteger( 0 );
	private Reindexer reindexer = null;
	private Thread reindexThread = null;
	
	private PerFieldAnalyzerWrapper analyzer = null;
	private Directory indexDirectory = null;
	private IndexWriterConfig indexWriterConfig = null;
	private IndexWriter indexWriter = null;
	private SearcherManager searcherManager = null;
	private ControlledRealTimeReopenThread<IndexSearcher> nrtReopener = null;
	
	private final int MAX_RESULTS = 10_000;
	
	@PostConstruct
	public void init()
	{
		initializeLucene( false, true );
	}
	
	private void initializeLucene( boolean create, boolean searchable )
	{
		try
		{
			setSearchable( searchable );
			
			HashMap<String, Analyzer> fieldAnalyzerMap = new HashMap<String, Analyzer>();
			fieldAnalyzerMap.put( FIELD_BIBLIO_ALL, new StandardAnalyzer() );
			fieldAnalyzerMap.put( FIELD_BIBLIO_UPDATE_DATE, new KeywordAnalyzer() );
			fieldAnalyzerMap.put( IndexField.FIELD_BIBLIO_ID.getFieldId(), new KeywordAnalyzer() );
			fieldAnalyzerMap.put( IndexField.FIELD_BIBLIO_TITLE.getFieldId(), new StandardAnalyzer() );
			fieldAnalyzerMap.put( IndexField.FIELD_BIBLIO_CREATOR.getFieldId(), new StandardAnalyzer() );
			fieldAnalyzerMap.put( IndexField.FIELD_BIBLIO_ISBN.getFieldId(), new KeywordAnalyzer() );
			fieldAnalyzerMap.put( IndexField.FIELD_BIBLIO_ISSN.getFieldId(), new KeywordAnalyzer() );
			fieldAnalyzerMap.put( IndexField.FIELD_BIBLIO_EAN.getFieldId(), new KeywordAnalyzer() );
			fieldAnalyzerMap.put( IndexField.FIELD_BIBLIO_LCCN.getFieldId(), new KeywordAnalyzer() );
			fieldAnalyzerMap.put( IndexField.FIELD_BIBLIO_DEWEY_DECIMAL.getFieldId(), new KeywordAnalyzer() );
			fieldAnalyzerMap.put( IndexField.FIELD_BIBLIO_SUBJECTS.getFieldId(), new StandardAnalyzer() );
			fieldAnalyzerMap.put( IndexField.FIELD_BIBLIO_CALL_NUMBER.getFieldId(), new KeywordAnalyzer() );
			
			analyzer = new PerFieldAnalyzerWrapper( new StandardAnalyzer(), fieldAnalyzerMap );
			
			indexDirectory = FSDirectory.open( Constants.DIRECTORY_INDEX_STORAGE );
			indexWriterConfig = new IndexWriterConfig( analyzer );
			
			if ( create ) indexWriterConfig.setOpenMode( OpenMode.CREATE );
			
			indexWriter = new IndexWriter( indexDirectory, indexWriterConfig );
			searcherManager = new SearcherManager( indexWriter, new SearcherFactory() );
			
			nrtReopener = new ControlledRealTimeReopenThread<IndexSearcher>( indexWriter, searcherManager, 1.0, 0.1 );
			nrtReopener.setName( "Aether Lucene NRT Reopen thread." );
			nrtReopener.setDaemon( true );
			nrtReopener.setPriority( Thread.MAX_PRIORITY );
			
			nrtReopener.start();
		}
		catch ( Exception e )
		{
			IndexManager.log.error( "Failed to initialize IndexManager. Index and Search facilities will be unavailable!", e );
		}
	}
	
	/**
	 * Flag indicating if the index is available and safe for searching.
	 */
	@Override
	public boolean isSearchable()
	{
		return isSearchable.get();
	}
	
	private void setSearchable( boolean isSearchable )
	{
		this.isSearchable.getAndSet( isSearchable );
	}
	
	@Override
	public boolean addDocument( Biblio biblio )
	{
		try
		{
			//@formatter:off
            StringBuffer allFields = new StringBuffer();
            Document doc = new Document();
            /* Date Updated */  doc.add( new StringField( FIELD_BIBLIO_UPDATE_DATE, DateTools.dateToString( biblio.getDateUpdated(), DateTools.Resolution.SECOND ), Store.YES ) );
            
            /* Biblio ID */     doc.add( new StringField( IndexField.FIELD_BIBLIO_ID.getFieldId(), biblio.getId().toString(), Store.YES ) );
                                allFields.append( biblio.getId().toString() + " " );
            
            /* Title */         doc.add( new TextField( IndexField.FIELD_BIBLIO_TITLE.getFieldId(), biblio.getTitle(), Store.NO ) );
                                allFields.append( biblio.getTitle() + " " );
                                
            /* Creator */       for(String nextCreator : biblio.getAuthor().split( System.lineSeparator() ))
                                {
                                    doc.add( new TextField( IndexField.FIELD_BIBLIO_CREATOR.getFieldId(), nextCreator, Store.NO ) );
                                    allFields.append( nextCreator + " " );
                                }
                                
            /* ISBN */          if(biblio.getIsbn() != null && biblio.getIsbn().length() > 0) 
                                {
                                    Matcher m = Constants.PATTERN_ISBN.matcher( biblio.getIsbn() );
                                    while(m.find())
                                    {
                                        doc.add( new StringField( IndexField.FIELD_BIBLIO_ISBN.getFieldId(), m.group( 1 ), Store.NO ) );
                                        allFields.append( m.group( 1 ) + " " );
                                    }
                                }
            
            /* ISSN */          if(biblio.getIssn() != null && biblio.getIssn().length() > 0) 
                                {
                                    Matcher m = Constants.PATTERN_ISSN.matcher( biblio.getIssn() );
                                    while(m.find())
                                    {
                                        doc.add( new StringField( IndexField.FIELD_BIBLIO_ISSN.getFieldId(), m.group( 1 ), Store.NO ) );
                                        allFields.append( m.group( 1 ) + " " );
                                    }
                                }
            
            /* EAN */           if(biblio.getEan() != null && biblio.getEan().length() > 0)
                                {
                                    Matcher m = Constants.PATTERN_EAN.matcher( biblio.getEan() );
                                    while(m.find())
                                    {
                                        doc.add( new StringField( IndexField.FIELD_BIBLIO_EAN.getFieldId(), m.group( 1 ), Store.NO ) );
                                        allFields.append( m.group( 1 ) + " " );
                                    }
                                }
            
            /* LCCN */          if(biblio.getLccn() != null && biblio.getLccn().length() > 0)
                                {
                                    Matcher m = Constants.PATTERN_LCCN.matcher( biblio.getLccn() );
                                    while(m.find())
                                    {
                                        doc.add( new StringField( IndexField.FIELD_BIBLIO_LCCN.getFieldId(), m.group( 1 ), Store.NO) );
                                        allFields.append( m.group( 1 ) + " " );
                                    }
                                }
            
            /* Dewey Decimal */ if(biblio.getDeweyDecimal() != null && biblio.getDeweyDecimal().length() > 0) 
                                {
                                    doc.add( new StringField( IndexField.FIELD_BIBLIO_DEWEY_DECIMAL.getFieldId(), biblio.getDeweyDecimal(), Store.NO ) );
                                    allFields.append( biblio.getDeweyDecimal() + " " );
                                }
            
            /* Subjects */      if(biblio.getSubjects() != null && biblio.getSubjects().length() > 0) 
                                {
                                    doc.add( new TextField( IndexField.FIELD_BIBLIO_SUBJECTS.getFieldId(), biblio.getSubjects(), Store.NO ) );
                                    allFields.append( biblio.getSubjects() + " " );
                                }
            
            /* Call Number */   if(biblio.getCallNumber() != null && biblio.getCallNumber().length() > 0) 
                                {
                                    doc.add( new StringField( IndexField.FIELD_BIBLIO_CALL_NUMBER.getFieldId(), biblio.getCallNumber(), Store.NO ) );
                                    allFields.append( biblio.getCallNumber() + " " );
                                }
            
            /* Abstract */      if(biblio.getAbstractText() != null && biblio.getAbstractText().length() > 0)
                                {
                                    allFields.append( biblio.getAbstractText() );
                                }
            
            /* All Fields */    doc.add( new TextField( IndexField.FIELD_BIBLIO_ALL.getFieldId(), allFields.toString(), Store.NO ) );
            //@formatter:on
			
			indexWriter.addDocument( doc );
			searcherManager.maybeRefresh();
			indexWriter.commit();
		}
		catch ( Exception e )
		{
			IndexManager.log.error( "Error indexing Bibliographic record with ID [" + biblio.getId() + "].", e );
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean updateDocument( Biblio biblio )
	{
		try
		{
			TermQuery query = new TermQuery( new Term( IndexField.FIELD_BIBLIO_ID.getFieldId(), biblio.getId().toString() ) );
			indexWriter.deleteDocuments( query );
			searcherManager.maybeRefresh();
			indexWriter.commit();
		}
		catch ( Exception e )
		{
			IndexManager.log.error( "Error delete Bibliographic record with ID [" + biblio.getId() + "] from index.", e );
			return false;
		}
		
		return addDocument( biblio );
	}
	
	@Override
	public boolean deleteDocument( Biblio biblio )
	{
		try
		{
			TermQuery query = new TermQuery( new Term( IndexField.FIELD_BIBLIO_ID.getFieldId(), biblio.getId().toString() ) );
			indexWriter.deleteDocuments( query );
			searcherManager.maybeRefresh();
			indexWriter.commit();
		}
		catch ( Exception e )
		{
			IndexManager.log.error( "Error delete Bibliographic record with ID [" + biblio.getId() + "] from index.", e );
			return false;
		}
		
		return true;
	}
	
	/**
	 * !!! WARNING !!! This method deletes the ENTIRE index and rebuilds it. Not
	 * only could this be dangerous, but it could be extremely resource hungry
	 * and time consuming.
	 */
	@Override
	public void reindex()
	{
		// If it's already running, do nothing
		if ( reindexThread != null && reindexThread.isAlive() || isReindexing.get() )
		{
			IndexManager.log.warn( "An attempt was made to re-index the search index while a re-index was already under way." );
			return;
		}
		
		shutdown();
		initializeLucene( true, false );
		reindexProgress.set( 0 );
		
		reindexer = new Reindexer( biblioEjb, systemSettingsEjb, this );
		reindexer.addObserver( this );
		reindexThread = new Thread( reindexer );
		setReindexing( true );
		reindexThread.start();
	}
	
	@Override
	public IndexResultSet queryBiblios( IndexQuery query )
	{
		IndexResultSet resultSet = new IndexResultSet();
		IndexSearcher searcher = null;
		
		if ( !isSearchable() )
		{
			IndexManager.log.warn( "An attempt was made to execute a query while the index was not in a searchable state." );
			return null;
		}
		
		try
		{
			final IndexSearcher blockScopeSearcher = searcher = searcherManager.acquire();
			BooleanQuery.Builder builder = new BooleanQuery.Builder();
			
			switch ( query.getQueryType() )
			{
				case WHOLISTIC:
				case SINGLE_FIELD:
				{
					buildQueryForSingleField( query, builder );
					break;
				}
				
				case MULTI_FIELD:
				{
					buildQueryForMultipleFields( query, builder );
					break;
				}
			}
			
			TopScoreDocCollector collector = TopScoreDocCollector.create( MAX_RESULTS );
			int startIndex = (query.getPage() - 1) * query.getPageSize();
			blockScopeSearcher.search( builder.build(), collector );
			
			TopDocs hits = collector.topDocs( startIndex, query.getPageSize() );
			
			//@formatter:off
            List<Integer> resultIds =   Stream.of( hits.scoreDocs ).map( 
                                            ( d ) -> {
                                                try
                                                {
                                                    return Integer.parseInt( 
                                                        blockScopeSearcher
                                                        .doc( d.doc )
                                                        .get( IndexField.FIELD_BIBLIO_ID.getFieldId() )
                                                    );
                                                }
                                                catch ( Exception e )
                                                {
                                                    IndexManager.log.error( "Document ID could not be converted to Integer.", e );
                                                }
                                                return null;
                                            } 
                                        ).collect( Collectors.toList() );
            //@formatter:on
			
			resultSet.setTotalResults( hits.totalHits );
			resultSet.setResults( resultIds );
		}
		catch ( Exception e )
		{
			IndexManager.log.error( "Exception encountered while trying to query for Biblios.", e );
		}
		finally
		{
			if ( searcher != null )
			{
				try
				{
					searcherManager.release( searcher );
				}
				catch ( IOException e )
				{
					IndexManager.log.error( "Exception occurred while releasing lock on SearcherManager.", e );
				}
			}
		}
		
		return resultSet;
	}
	
	private void buildQueryForSingleField( IndexQuery query, BooleanQuery.Builder builder )
	{
		try
		{
			IndexField singleField = query.getQueryValues().keySet().iterator().next();
			
			switch ( singleField )
			{
				case FIELD_BIBLIO_ID:
				case FIELD_BIBLIO_CALL_NUMBER:
				case FIELD_BIBLIO_EAN:
				case FIELD_BIBLIO_ISBN:
				case FIELD_BIBLIO_ISSN:
				case FIELD_BIBLIO_LCCN:
				{
					StandardTokenizer tokenizer = new StandardTokenizer();
					tokenizer.setReader( new StringReader( query.getQueryValues().get( singleField ).toString() ) );
					tokenizer.reset();
					while ( tokenizer.incrementToken() )
					{
						builder.add( new TermQuery( new Term( singleField.getFieldId(), tokenizer.getAttribute( CharTermAttribute.class ).toString() ) ), Occur.MUST );
					}
					tokenizer.end();
					tokenizer.close();
					break;
				}
				
				case FIELD_BIBLIO_ALL:
				case FIELD_BIBLIO_DEWEY_DECIMAL:
				case FIELD_BIBLIO_SUBJECTS:
				case FIELD_BIBLIO_TITLE:
				case FIELD_BIBLIO_CREATOR:
				{
					builder.add( new PhraseQuery( singleField.getFieldId(), query.getQueryValues().get( singleField ).toString() ), Occur.MUST );
					break;
				}
				
				default:
					throw new Exception( "Attempt was made to build a query for a single field with an invalid field type [" + singleField.getFieldId() + "]." );
			}
		}
		catch ( Exception e )
		{
			IndexManager.log.error( "Error occured while building query for single field.", e );
		}
	}
	
	private void buildQueryForMultipleFields( IndexQuery query, BooleanQuery.Builder builder )
	{
		try
		{
			for ( IndexField nextField : query.getQueryValues().keySet() )
			{
				Object nextValue = query.getQueryValues().get( nextField );
				
				switch ( nextField )
				{
					case FIELD_BIBLIO_ID:
					case FIELD_BIBLIO_CALL_NUMBER:
					case FIELD_BIBLIO_EAN:
					case FIELD_BIBLIO_ISBN:
					case FIELD_BIBLIO_ISSN:
					case FIELD_BIBLIO_LCCN:
					{
						StandardTokenizer tokenizer = new StandardTokenizer();
						tokenizer.setReader( new StringReader( nextValue.toString() ) );
						tokenizer.reset();
						while ( tokenizer.incrementToken() )
						{
							builder.add( new TermQuery( new Term( nextField.getFieldId(), tokenizer.getAttribute( CharTermAttribute.class ).toString() ) ), Occur.MUST );
						}
						tokenizer.end();
						tokenizer.close();
						break;
					}
					
					case FIELD_BIBLIO_ALL:
					case FIELD_BIBLIO_DEWEY_DECIMAL:
					case FIELD_BIBLIO_SUBJECTS:
					case FIELD_BIBLIO_TITLE:
					case FIELD_BIBLIO_CREATOR:
					{
						builder.add( new PhraseQuery( nextField.getFieldId(), nextValue.toString() ), Occur.SHOULD );
						break;
					}
					
					default:
						throw new Exception( "Attempt was made to build a query for multiple fields with an invalid field type [" + nextField.getFieldId() + "]." );
				}
			}
		}
		catch ( Exception e )
		{
			IndexManager.log.error( "Error occured while building query for multiple fields.", e );
		}
	}
	
	@PreDestroy
	private void shutdown()
	{
		try
		{
			setSearchable( false );
			
			nrtReopener.close();
			searcherManager.close();
			indexWriter.close();
			indexDirectory.close();
			analyzer.close();
		}
		catch ( Exception e )
		{
			IndexManager.log.error( "Error occured while building query for multiple fields.", e );
		}
	}
	
	@Override
	public void update( Observable o, Object arg )
	{
		if ( arg != null && Integer.class.isAssignableFrom( arg.getClass() ) )
		{
			reindexProgress.set( ((Integer) arg).intValue() );
			
			if ( reindexProgress.get() == 100 )
			{
				reindexThread.interrupt();
				reindexThread = null;
				reindexer = null;
				setSearchable( true );
				setReindexing( false );
			}
		}
	}
	
	@Override
	public boolean isReindexing()
	{
		return isReindexing.get();
	}
	
	public void setReindexing( boolean isReindexing )
	{
		this.isReindexing.set( isReindexing );
	}
	
	@Override
	public int getReindexProgress()
	{
		return reindexProgress.get();
	}
}
