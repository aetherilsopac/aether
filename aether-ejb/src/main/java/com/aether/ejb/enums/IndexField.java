//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.enums
 * File: IndexFields.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Specifies a list of fields in the Lucene index that can be queried on.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Oct 11, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.enums;

/**
 * Enumeration of Biblio fields that may be indexed and queried on.
 */
public enum IndexField
{
    //@formatter:off
    FIELD_BIBLIO_ALL("all"),
    
    /** DO NOT USE THIS OPTION. IT IS FOR INTERNAL USE ONLY! */
    FIELD_BIBLIO_MULTI("multi"),
    
    /** ID of a Biblio object. */
    FIELD_BIBLIO_ID("id"),
    
    /** Title of a Biblio entry. */
    FIELD_BIBLIO_TITLE("title"),
    
    /** Creators of a Biblio entry. */
    FIELD_BIBLIO_CREATOR("creator"),
    
    /** ISBNs associated with a Biblio entry. */
    FIELD_BIBLIO_ISBN("isbn"),
    
    /** ISSNs associated with a Biblio entry. */
    FIELD_BIBLIO_ISSN("issn"),
    
    /** EANs associated with a Biblio entry. */
    FIELD_BIBLIO_EAN("ean"),
    
    /** Library of Congress Control Number assigned to a Biblio entry. */
    FIELD_BIBLIO_LCCN("lccn"),
    
    /** Dewey Decimal code assigned to a Biblio entry. */
    FIELD_BIBLIO_DEWEY_DECIMAL("dewey_decimal"),
    
    /** Subjects assigned to a Biblio entry. */
    FIELD_BIBLIO_SUBJECTS("subjects"),
    
    /** Call Number assigned to a Biblio entry. */
    FIELD_BIBLIO_CALL_NUMBER("call_number");
    //@formatter:on

    private String fieldId = null;

    IndexField(String fieldId)
    {
        this.fieldId = fieldId;
    }

    public String getFieldId()
    {
        return fieldId;
    }
}