//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.enums
 * File: SystemSetting.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Nov 15, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.enums;

public enum SystemSettingName
{
    /**
     * API Key provided by https://console.developers.google.com/ for obtaining a Google Books API query quota.
     * For more on Google Books APIs see https://developers.google.com/books/docs/overview.
     */
    GOOGLE_API_KEY,

    /**
     * System Title/Name show in the nav bar at the top of the screen.
     */
    SYSTEM_TITLE,

    /**
     * Logo shown next to the System Title.
     */
    CUSTOM_LOGO,

    /**
     * Flag that determines if the system allows user login/access.
     */
    SYSTEM_AVAILABLE,

    /**
     * Message shown to users when the system is unavailable.
     */
    SYSTEM_UNAVAILABLE_MESSAGE,

    /**
     * Date the database was last reindexed.
     */
    LAST_REINDEX_DATE,

    /**
     * Number of records indexed by the last reindexing.
     */
    LAST_REINDEX_RECORD_COUNT
}
