//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: Help.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 23, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.aether.ejb.enums.WebDocumentType;

@Entity
@Table( name = "web_documents" )
public class WebDocument extends AuditableEntity implements Serializable
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = 5531148304678970494L;
	
	private WebDocumentType documentType;
	private String friendlyUrl;
	private String title;
	private String content;
	private Boolean permanent;
	private String locale;
	
	@Enumerated( EnumType.STRING )
	@Column( name = "document_type", length = 1 )
	public WebDocumentType getDocumentType()
	{
		return documentType;
	}
	
	@Column( name = "friendly_url", length = 250, unique = true )
	public String getFriendlyUrl()
	{
		return friendlyUrl;
	}
	
	@Column( name = "title", length = 250, nullable = false )
	public String getTitle()
	{
		return title;
	}
	
	@Column( name = "content", length = 16777215, columnDefinition = "MEDIUMTEXT" )
	public String getContent()
	{
		return content;
	}
	
	@Column( name = "permanent", nullable = false )
	public Boolean getPermanent()
	{
		return permanent;
	}
	
	@Column( name = "locale", length = 20, nullable = false )
	public String getLocale()
	{
		return locale;
	}
	
	public void setDocumentType( WebDocumentType documentType )
	{
		this.documentType = documentType;
	}
	
	public void setFriendlyUrl( String friendlyUrl )
	{
		this.friendlyUrl = friendlyUrl;
	}
	
	public void setTitle( String title )
	{
		this.title = title;
	}
	
	public void setContent( String content )
	{
		this.content = content;
	}
	
	public void setPermanent( Boolean permanent )
	{
		this.permanent = permanent;
	}
	
	public void setLocale( String locale )
	{
		this.locale = locale;
	}
}
