package com.aether.ejb.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@MappedSuperclass
public abstract class AuditableEntity
{
	protected Integer id;
	protected Date dateCreated;
	protected User createdBy;
	protected Date dateUpdated;
	protected User updatedBy;
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "id" )
	public Integer getId()
	{
		return id;
	}
	
	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "date_created", nullable = false )
	public Date getDateCreated()
	{
		return dateCreated;
	}
	
	@JsonIgnore
	@ManyToOne( optional = false, fetch = FetchType.EAGER )
	@JoinColumn( name = "created_by", nullable = false )
	public User getCreatedBy()
	{
		return createdBy;
	}
	
	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "date_updated", nullable = false )
	public Date getDateUpdated()
	{
		return dateUpdated;
	}
	
	@JsonIgnore
	@ManyToOne( optional = false, fetch = FetchType.EAGER )
	@JoinColumn( name = "updated_by", nullable = false )
	public User getUpdatedBy()
	{
		return updatedBy;
	}
	
	public void setId( Integer id )
	{
		this.id = id;
	}
	
	public void setDateCreated( Date dateCreated )
	{
		this.dateCreated = dateCreated;
	}
	
	public void setCreatedBy( User createdBy )
	{
		this.createdBy = createdBy;
	}
	
	public void setDateUpdated( Date dateUpdated )
	{
		this.dateUpdated = dateUpdated;
	}
	
	public void setUpdatedBy( User updatedBy )
	{
		this.updatedBy = updatedBy;
	}
}
