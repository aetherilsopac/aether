//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.ejb.models.minimized
 * File: ShelvingLocationMinimized.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Minimized version of ShelvingLocation entity for quick transport.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Feb 09, 2017, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models.minimized;

import java.io.Serializable;

import com.aether.ejb.models.ShelvingLocation;

public class ShelvingLocationMinimized implements Serializable
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -805820144967354196L;
	
	private Integer id;
	private String name;
	private String description;
	private Boolean active;
	
	public ShelvingLocationMinimized( ShelvingLocation location )
	{
		this.id = location.getId();
		this.name = location.getName();
		this.description = location.getDescription();
		this.active = location.getActive();
	}
	
	public Integer getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public Boolean getActive()
	{
		return active;
	}
	
	public void setId( Integer id )
	{
		this.id = id;
	}
	
	public void setName( String name )
	{
		this.name = name;
	}
	
	public void setDescription( String description )
	{
		this.description = description;
	}
	
	public void setActive( Boolean active )
	{
		this.active = active;
	}
	
}
