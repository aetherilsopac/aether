//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: Biblio.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Maps to the Biblio table to represent a bibliographic entry. This does not represent an actual physical/owned item, only the bibliographic
 * information about items that may or may not be owned by the organization. For example, a bibliographic entry about Harry Potter would describe
 * a Harry Potter book, but not represent a copy of a Harry Potter book. An "Item" entry that references its associated bibliographic entry would
 * be required to represent an actual copy of a Harry Potter book.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 16, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;
import java.util.Date;
import java.util.regex.Matcher;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.marc4j.marc.Record;

import com.aether.ejb.common.Constants;
import com.aether.ejb.common.MarcUtilities;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table( name = "biblios" )
public class Biblio extends AuditableEntity implements Serializable
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -970251691590718494L;
	
	private String author;
	private String title;
	private String notes;
	private Boolean serial;
	private Date copyrightDate;
	private String volumeInformation;
	private ItemType itemType;
	private String isbn;
	private String issn;
	private String ean;
	private String publicationYear;
	private String publisherName;
	private String publicationLocation;
	private String seriesTitle;
	private String seriesIssn;
	private String seriesVolume;
	private String editionStatement;
	private String editionResponsibility;
	private String abstractText;
	private String physicalExtent;
	private String physicalOther;
	private String physicalDimensions;
	private String lccn;
	private String controlNumber;
	private String marc;
	private String url;
	private String targetAudience;
	private Integer totalIssues = 0;
	private String recordType;
	private String callNumber;
	private String deweyDecimal;
	private String subjects;
	private String responsibilityStatement;
	
	public Biblio()
	{
	}
	
	public Biblio( Record marcRecord )
	{
		this( marcRecord, null, null );
	}
	
	public Biblio( Record marcRecord, String marcXml )
	{
		this( marcRecord, null, marcXml );
	}
	
	public Biblio( Record marcRecord, ItemType itemType )
	{
		this( marcRecord, itemType, null );
	}
	
	public Biblio( Record marcRecord, ItemType itemType, String marcXml )
	{
		setAuthor( MarcUtilities.getCreators( marcRecord ) );
		setTitle( MarcUtilities.getTitle( marcRecord ) );
		setSerial( MarcUtilities.isSerial( marcRecord ) );
		setCopyrightDate( MarcUtilities.getCopyrightDate( marcRecord ) );
		setVolumeInformation( MarcUtilities.getVolumeInformation( marcRecord ) );
		setItemType( itemType );
		setIsbn( MarcUtilities.getISBNs( marcRecord ) );
		setEan( MarcUtilities.getEANs( marcRecord ) );
		setPublicationYear( MarcUtilities.getPublicationYear( marcRecord ) );
		setPublisherName( MarcUtilities.getPublisherName( marcRecord ) );
		setPublicationLocation( MarcUtilities.getPublicationLocation( marcRecord ) );
		
		if ( getSerial() )
		{
			setIssn( MarcUtilities.getISSNs( marcRecord ) );
			setSeriesTitle( MarcUtilities.getSeriesTitle( marcRecord ) );
			setSeriesIssn( MarcUtilities.getSeriesIssn( marcRecord ) );
			setSeriesVolume( MarcUtilities.getSeriesVolume( marcRecord ) );
		}
		
		setEditionStatement( MarcUtilities.getEditionStatement( marcRecord ) );
		setEditionResponsibility( MarcUtilities.getEditionResponsibility( marcRecord ) );
		setAbstractText( MarcUtilities.getAbstractText( marcRecord ) );
		setPhysicalOther( MarcUtilities.getPhysicalOther( marcRecord ) );
		setPhysicalExtent( MarcUtilities.getPhysicalExtent( marcRecord ) );
		setPhysicalDimensions( MarcUtilities.getPhysicalDimensions( marcRecord ) );
		setLccn( MarcUtilities.getLCCN( marcRecord ) );
		setControlNumber( MarcUtilities.getControlNumber( marcRecord ) );
		setUrl( MarcUtilities.getUrl( marcRecord ) );
		setTargetAudience( MarcUtilities.getTargetAudience( marcRecord ) );
		setRecordType( MarcUtilities.getRecordType( marcRecord ) );
		setRecordTypeName( MarcUtilities.getRecordTypeName( marcRecord ) );
		setCallNumber( MarcUtilities.getCallNumber( marcRecord ) );
		setDeweyDecimal( MarcUtilities.getDeweyDecimal( marcRecord ) );
		setSubjects( MarcUtilities.getSubjects( marcRecord ) );
		setResponsibilityStatement( MarcUtilities.getResponsibilityStatement( marcRecord ) );
		
		setMarc( marc );
	}
	
	@Column( name = "author", length = 65535, columnDefinition = "TEXT" )
	public String getAuthor()
	{
		return author;
	}
	
	@JsonProperty( value = "primaryAuthor", access = Access.READ_ONLY )
	@Transient
	public String getPrimaryAuthor()
	{
		String[] authors = getAuthor().split( "\\n" );
		if ( authors.length > 0 ) return authors[0];
		return "";
	}
	
	@Column( name = "title", length = 65535, columnDefinition = "TEXT" )
	public String getTitle()
	{
		return title;
	}
	
	/**
	 * The title formatted by removing any trailing ISBD (International Standard
	 * Bibliographic Description) standard characters and normalizing white
	 * space around a colon separating the main and sub titles. In addtion, if a
	 * statement of responsiblity exists, it is appended to the end, separated
	 * by a forward slash.
	 */
	@JsonIgnore
	private String fullTitleFormatted;
	
	@JsonProperty( value = "fullTitleFormatted", access = Access.READ_ONLY )
	@Transient
	public String getFullTitleFormatted()
	{
		Matcher m = Constants.PATTERN_COMPOUND_TITLE.matcher( getTitle() );
		StringBuffer formattedTitle = new StringBuffer();
		if ( m.matches() )
		{
			formattedTitle.append( m.group( 1 ).trim() );
			if ( m.group( 2 ).length() > 0 )
			{
				formattedTitle.append( " : " + m.group( 2 ).trim() );
			}
			if ( getResponsibilityStatement() != null && getResponsibilityStatement().length() > 0 )
			{
				formattedTitle.append( " / " + getResponsibilityStatement() );
			}
			return formattedTitle.toString();
		}
		return getTitle();
	}
	
	@Column( name = "notes", length = 65535, columnDefinition = "TEXT" )
	public String getNotes()
	{
		return notes;
	}
	
	@Column( name = "serial", nullable = false )
	public Boolean getSerial()
	{
		return serial;
	}
	
	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "copyright_date" )
	public Date getCopyrightDate()
	{
		return copyrightDate;
	}
	
	@Column( name = "volume_info", length = 65535, columnDefinition = "TEXT" )
	public String getVolumeInformation()
	{
		return volumeInformation;
	}
	
	@ManyToOne( optional = true, fetch = FetchType.EAGER )
	@JoinColumn( name = "item_type_id" )
	public ItemType getItemType()
	{
		return itemType;
	}
	
	@Column( name = "isbn", length = 13 )
	public String getIsbn()
	{
		return isbn;
	}
	
	@Column( name = "issn", length = 8 )
	public String getIssn()
	{
		return issn;
	}
	
	@Column( name = "ean", length = 13 )
	public String getEan()
	{
		return ean;
	}
	
	@JsonIgnore
	private String biblioId;
	
	@JsonProperty( value = "biblioId", access = Access.READ_ONLY )
	@Transient
	public String getBiblioId()
	{
		if ( getIsbn() != null && getIsbn().length() > 0 ) return getIsbn();
		if ( getIssn() != null && getIssn().length() > 0 ) return getIssn();
		if ( getEan() != null && getEan().length() > 0 ) return getEan();
		
		return "";
	}
	
	@Column( name = "publication_year", length = 65535, columnDefinition = "TEXT" )
	public String getPublicationYear()
	{
		return publicationYear;
	}
	
	/**
	 * The 4 digits of the year extracted from any other superfluous characters
	 * (e.g. "c2016." becomes "2016").
	 */
	@JsonIgnore
	private String publicationYearFormatted;
	
	@JsonProperty( value = "publicationYearFormatted", access = Access.READ_ONLY )
	@Transient
	public String getPublicationYearFormatted()
	{
		Matcher m = Constants.PATTERN_FOUR_DIGIT_YEAR.matcher( getPublicationYear() );
		if ( m.find() )
		{
			return m.group( 1 );
		}
		return getPublicationYear();
	}
	
	@Column( name = "publisher_name", length = 65535, columnDefinition = "TEXT" )
	public String getPublisherName()
	{
		return publisherName;
	}
	
	@Column( name = "publication_location", length = 65535, columnDefinition = "TEXT" )
	public String getPublicationLocation()
	{
		return publicationLocation;
	}
	
	@Column( name = "series_title", length = 65535, columnDefinition = "TEXT" )
	public String getSeriesTitle()
	{
		return seriesTitle;
	}
	
	@Column( name = "series_issn", length = 8 )
	public String getSeriesIssn()
	{
		return seriesIssn;
	}
	
	@Column( name = "series_volume", length = 65535, columnDefinition = "TEXT" )
	public String getSeriesVolume()
	{
		return seriesVolume;
	}
	
	@Column( name = "edition_statement", length = 65535, columnDefinition = "TEXT" )
	public String getEditionStatement()
	{
		return editionStatement;
	}
	
	@Column( name = "edition_responsibility", length = 65535, columnDefinition = "TEXT" )
	public String getEditionResponsibility()
	{
		return editionResponsibility;
	}
	
	@Column( name = "abstract", length = 65535, columnDefinition = "TEXT" )
	public String getAbstractText()
	{
		return abstractText;
	}
	
	@Column( name = "physical_extent", length = 65535, columnDefinition = "TEXT" )
	public String getPhysicalExtent()
	{
		return physicalExtent;
	}
	
	@Column( name = "physical_other", length = 65535, columnDefinition = "TEXT" )
	public String getPhysicalOther()
	{
		return physicalOther;
	}
	
	@Column( name = "physical_dimensions", length = 65535, columnDefinition = "TEXT" )
	public String getPhysicalDimensions()
	{
		return physicalDimensions;
	}
	
	@Column( name = "lccn", length = 25 )
	public String getLccn()
	{
		return lccn;
	}
	
	@Column( name = "control_number", length = 25 )
	public String getControlNumber()
	{
		return controlNumber;
	}
	
	@Column( name = "marc", length = 65535, columnDefinition = "TEXT" )
	public String getMarc()
	{
		return marc;
	}
	
	@Column( name = "url", length = 65535, columnDefinition = "TEXT" )
	public String getUrl()
	{
		return url;
	}
	
	@Column( name = "target_audience", length = 65535, columnDefinition = "TEXT" )
	public String getTargetAudience()
	{
		return targetAudience;
	}
	
	@Column( name = "total_issues" )
	public Integer getTotalIssues()
	{
		return totalIssues;
	}
	
	@Column( name = "record_type", length = 1 )
	public String getRecordType()
	{
		return recordType;
	}
	
	/**
	 * This field is only used to display the MARC Bibliographic Record Type to
	 * users when they are searching for an SRU import. It simply stores a short
	 * textual description of the record type.
	 */
	@JsonIgnore
	private String recordTypeName;
	
	@JsonProperty( value = "recordTypeName", access = Access.READ_ONLY )
	@Transient
	public String getRecordTypeName()
	{
		return recordTypeName;
	}
	
	@Column( name = "call_number", length = 100 )
	public String getCallNumber()
	{
		return callNumber;
	}
	
	@Column( name = "dewey_decimal", length = 7 )
	public String getDeweyDecimal()
	{
		return deweyDecimal;
	}
	
	@Column( name = "subjects", length = 65535, columnDefinition = "TEXT" )
	public String getSubjects()
	{
		return subjects;
	}
	
	@Column( name = "responsibility_statement", length = 65535, columnDefinition = "TEXT" )
	public String getResponsibilityStatement()
	{
		return responsibilityStatement;
	}
	
	public void setAuthor( String author )
	{
		this.author = author;
	}
	
	public void setTitle( String title )
	{
		this.title = title;
	}
	
	public void setNotes( String notes )
	{
		this.notes = notes;
	}
	
	public void setSerial( Boolean serial )
	{
		this.serial = serial;
	}
	
	public void setCopyrightDate( Date copyrightDate )
	{
		this.copyrightDate = copyrightDate;
	}
	
	public void setVolumeInformation( String volumeInformation )
	{
		this.volumeInformation = volumeInformation;
	}
	
	public void setItemType( ItemType itemType )
	{
		this.itemType = itemType;
	}
	
	public void setIsbn( String isbn )
	{
		this.isbn = isbn;
	}
	
	public void setIssn( String issn )
	{
		this.issn = issn;
	}
	
	public void setEan( String ean )
	{
		this.ean = ean;
	}
	
	public void setPublicationYear( String publicationYear )
	{
		this.publicationYear = publicationYear;
	}
	
	public void setPublisherName( String publisherName )
	{
		this.publisherName = publisherName;
	}
	
	public void setPublicationLocation( String publicationLocation )
	{
		this.publicationLocation = publicationLocation;
	}
	
	public void setSeriesTitle( String seriesTitle )
	{
		this.seriesTitle = seriesTitle;
	}
	
	public void setSeriesIssn( String seriesIssn )
	{
		this.seriesIssn = seriesIssn;
	}
	
	public void setSeriesVolume( String seriesVolume )
	{
		this.seriesVolume = seriesVolume;
	}
	
	public void setEditionStatement( String editionStatement )
	{
		this.editionStatement = editionStatement;
	}
	
	public void setEditionResponsibility( String editionResponsibility )
	{
		this.editionResponsibility = editionResponsibility;
	}
	
	public void setAbstractText( String abstractText )
	{
		this.abstractText = abstractText;
	}
	
	public void setPhysicalExtent( String physicalExtent )
	{
		this.physicalExtent = physicalExtent;
	}
	
	public void setPhysicalOther( String physicalOther )
	{
		this.physicalOther = physicalOther;
	}
	
	public void setPhysicalDimensions( String physicalDimensions )
	{
		this.physicalDimensions = physicalDimensions;
	}
	
	public void setLccn( String lccn )
	{
		this.lccn = lccn;
	}
	
	public void setControlNumber( String controlNumber )
	{
		this.controlNumber = controlNumber;
	}
	
	public void setMarc( String marc )
	{
		this.marc = marc;
	}
	
	public void setUrl( String url )
	{
		this.url = url;
	}
	
	public void setTargetAudience( String targetAudience )
	{
		this.targetAudience = targetAudience;
	}
	
	public void setTotalIssues( Integer totalIssues )
	{
		this.totalIssues = totalIssues;
	}
	
	public void setRecordType( String recordType )
	{
		this.recordType = recordType;
	}
	
	public void setRecordTypeName( String recordTypeName )
	{
		this.recordTypeName = recordTypeName;
	}
	
	public void setCallNumber( String callNumber )
	{
		this.callNumber = callNumber;
	}
	
	public void setDeweyDecimal( String deweyDecimal )
	{
		this.deweyDecimal = deweyDecimal;
	}
	
	public void setSubjects( String subjects )
	{
		this.subjects = subjects;
	}
	
	public void setResponsibilityStatement( String responsibilityStatement )
	{
		this.responsibilityStatement = responsibilityStatement;
	}
}
