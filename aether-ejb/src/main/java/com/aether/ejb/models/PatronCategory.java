//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: PatronCategory.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jul 29, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;
import com.aether.ejb.enums.PatronType;

@Entity
@Table(name = "patron_categories")
public class PatronCategory implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = 8590837194719218738L;

    private Integer id;
    private String categoryName;
    private String description;
    private Short enrollmentPeriod;
    private Date enrollmentPeriodDate;
    private Short minimumAge;
    private Short maximumAge;
    private Short maximumIssues;
    private Short maximumOnsiteIssues;
    private PatronType patronType;
    private Boolean active;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId()
    {
        return id;
    }

    @Column(name = "category_name", length = 50)
    public String getCategoryName()
    {
        return categoryName;
    }

    @Lob
    @Column(name = "description", columnDefinition = "MEDIUMTEXT")
    public String getDescription()
    {
        return description;
    }

    @Column(name = "enrollmentperiod")
    public Short getEnrollmentPeriod()
    {
        return enrollmentPeriod;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "enrollmentperioddate")
    public Date getEnrollmentPeriodDate()
    {
        return enrollmentPeriodDate;
    }

    @Column(name = "minimumage")
    public Short getMinimumAge()
    {
        return minimumAge;
    }

    @Column(name = "maximumage")
    public Short getMaximumAge()
    {
        return maximumAge;
    }

    @Column(name = "max_issues")
    public Short getMaximumIssues()
    {
        return maximumIssues;
    }

    @Column(name = "max_onsite_issues")
    public Short getMaximumOnsiteIssues()
    {
        return maximumOnsiteIssues;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "patron_type", length = 1)
    public PatronType getPatronType()
    {
        return patronType;
    }

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = "active")
    public Boolean getActive()
    {
        return active;
    }

    public void setId( Integer id )
    {
        this.id = id;
    }

    public void setCategoryName( String categoryName )
    {
        this.categoryName = categoryName;
    }

    public void setDescription( String description )
    {
        this.description = description;
    }

    public void setEnrollmentPeriod( Short enrollmentPeriod )
    {
        this.enrollmentPeriod = enrollmentPeriod;
    }

    public void setEnrollmentPeriodDate( Date enrollmentPeriodDate )
    {
        this.enrollmentPeriodDate = enrollmentPeriodDate;
    }

    public void setMinimumAge( Short minimumAge )
    {
        this.minimumAge = minimumAge;
    }

    public void setMaximumAge( Short maximumAge )
    {
        this.maximumAge = maximumAge;
    }

    public void setMaximumIssues( Short maximumIssues )
    {
        this.maximumIssues = maximumIssues;
    }

    public void setMaximumOnsiteIssues( Short maximumOnsiteIssues )
    {
        this.maximumOnsiteIssues = maximumOnsiteIssues;
    }

    public void setPatronType( PatronType patronType )
    {
        this.patronType = patronType;
    }

    public void setActive( Boolean active )
    {
        this.active = active;
    }

}
