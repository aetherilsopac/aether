//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: SruSource.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Models records describing available SRU (Search/Retrieval via URL) interfaces.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 17, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table( name = "sru_sources" )
public class SruSource extends AuditableEntity implements Serializable
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -6143108700123410908L;
	
	private String organization;
	private String databaseName;
	private String url;
	private String sruProfile;
	private String sruIndices;
	private String description;
	private Boolean active;
	
	@Column( name = "organization", length = 50, nullable = false )
	public String getOrganization()
	{
		return organization;
	}
	
	@Column( name = "database_name", length = 50 )
	public String getDatabaseName()
	{
		return databaseName;
	}
	
	@Column( name = "url", length = 250, nullable = false )
	public String getUrl()
	{
		return url;
	}
	
	@Column( name = "sru_profile", length = 50, nullable = false )
	public String getSruProfile()
	{
		return sruProfile;
	}
	
	@Column( name = "sru_indices", length = 65535, columnDefinition = "TEXT", nullable = false )
	public String getSruIndices()
	{
		return sruIndices;
	}
	
	@Column( name = "description", length = 65535, columnDefinition = "TEXT" )
	public String getDescription()
	{
		return description;
	}
	
	@Column( name = "active", nullable = false )
	public Boolean getActive()
	{
		return active;
	}
	
	public void setOrganization( String organization )
	{
		this.organization = organization;
	}
	
	public void setDatabaseName( String databaseName )
	{
		this.databaseName = databaseName;
	}
	
	public void setUrl( String url )
	{
		this.url = url;
	}
	
	public void setSruProfile( String sruProfile )
	{
		this.sruProfile = sruProfile;
	}
	
	public void setSruIndices( String sruIndices )
	{
		this.sruIndices = sruIndices;
	}
	
	public void setDescription( String description )
	{
		this.description = description;
	}
	
	public void setActive( Boolean active )
	{
		this.active = active;
	}
}
