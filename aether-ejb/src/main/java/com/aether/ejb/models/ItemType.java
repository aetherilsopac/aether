//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: ItemType.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * To allow librarians to assign bibliographic entries to groups so they may control circulation procedures and characteristics of all
 * items belonging to each group.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 16, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table( name = "item_types" )
public class ItemType extends AuditableEntity implements Serializable
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = 1293799890763929433L;
	
	private String description;
	private Boolean loanable;
	private Byte[] icon;
	private String checkinMessage;
	private String checkinMessageStyle;
	private Boolean opacSearchable;
	private Boolean active;
	private Boolean defaultType;
	private List<ItemTypeField> fields;
	
	@Column( name = "description", length = 250 )
	public String getDescription()
	{
		return description;
	}
	
	@Column( name = "loanable", nullable = false )
	public Boolean getLoanable()
	{
		return loanable;
	}
	
	@Column( name = "icon" )
	public Byte[] getIcon()
	{
		return icon;
	}
	
	@Column( name = "checkin_message", length = 250 )
	public String getCheckinMessage()
	{
		return checkinMessage;
	}
	
	@Column( name = "checkin_message_style", length = 50 )
	public String getCheckinMessageStyle()
	{
		return checkinMessageStyle;
	}
	
	@Column( name = "opac_searchable", nullable = false )
	public Boolean getOpacSearchable()
	{
		return opacSearchable;
	}
	
	@Column( name = "active", nullable = false )
	public Boolean isActive()
	{
		return active;
	}
	
	@Column( name = "default", nullable = false )
	public Boolean isDefault()
	{
		return defaultType;
	}
	
	@JsonIgnore
	@OneToMany( fetch = FetchType.LAZY, mappedBy = "itemType" )
	public List<ItemTypeField> getFields()
	{
		return fields;
	}
	
	public void setDescription( String description )
	{
		this.description = description;
	}
	
	public void setLoanable( Boolean loanable )
	{
		this.loanable = loanable;
	}
	
	public void setIcon( Byte[] icon )
	{
		this.icon = icon;
	}
	
	public void setCheckinMessage( String checkinMessage )
	{
		this.checkinMessage = checkinMessage;
	}
	
	public void setCheckinMessageStyle( String checkinMessageStyle )
	{
		this.checkinMessageStyle = checkinMessageStyle;
	}
	
	public void setOpacSearchable( Boolean opacSearchable )
	{
		this.opacSearchable = opacSearchable;
	}
	
	public void setActive( Boolean active )
	{
		this.active = active;
	}
	
	public void setDefault( Boolean defaultType )
	{
		this.defaultType = defaultType;
	}
	
	public void setFields( List<ItemTypeField> fields )
	{
		this.fields = fields;
	}
}
