//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: ItemTypeField.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * To allow Bibliographic entry/item fields to be tailored to a specific Item Type. Certain fields may be applicable to one Item Type
 * and not to others. Abstracting them into this class with a "visible" attribute allows for this customization. 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 29, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.aether.ejb.models.compositekeys.ItemTypeFieldId;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "item_type_fields")
public class ItemTypeField implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = 8575031459785297940L;

    private ItemTypeFieldId id;
    private String biblioColumnName;
    private ItemType itemType;
    private Boolean visible;
    private List<ItemTypeLabel> labels;

    public ItemTypeField()
    {
    }

    public ItemTypeField(ItemType itemType, String biblioColumnName, Boolean visible)
    {
        this.setId( new ItemTypeFieldId( itemType, biblioColumnName ) );
        this.setVisible( visible );
    }

    @EmbeddedId
    public ItemTypeFieldId getId()
    {
        return id;
    }

    @MapsId("itemTypeId")
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_type_id", referencedColumnName = "id", insertable = false, updatable = false)
    public ItemType getItemType()
    {
        return itemType;
    }

    @Column(name = "biblio_column_name", insertable = false, updatable = false)
    public String getBiblioColumnName()
    {
        return biblioColumnName;
    }

    @Column(name = "is_visible", nullable = false)
    public Boolean isVisible()
    {
        return visible;
    }

    //@formatter:off
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({ 
        @JoinColumn(name = "item_type_id", referencedColumnName = "item_type_id"),
        @JoinColumn(name = "biblio_column_name", referencedColumnName = "biblio_column_name") 
    })
    //@formatter:on
    public List<ItemTypeLabel> getLabels()
    {
        return labels;
    }

    public void setId( ItemTypeFieldId id )
    {
        this.id = id;
    }

    public void setItemType( ItemType itemType )
    {
        this.itemType = itemType;
    }

    public void setBiblioColumnName( String biblioColumnName )
    {
        this.biblioColumnName = biblioColumnName;
    }

    public void setVisible( Boolean visible )
    {
        this.visible = visible;
    }

    public void setLabels( List<ItemTypeLabel> labels )
    {
        this.labels = labels;
    }
}
