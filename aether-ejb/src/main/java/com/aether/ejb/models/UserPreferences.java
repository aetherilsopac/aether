//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: UserPreference.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Models the USER_PREFERENCES table.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 11, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.aether.ejb.converters.YesNoToBooleanConverter;

@Entity
@Table( name = "user_preferences" )
public class UserPreferences implements Serializable
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -7222331504837444964L;
	
	private Integer id;
	private Boolean navSidebarExpanded;
	
	@Id
	@Column( name = "user_id" )
	public Integer getId()
	{
		return id;
	}
	
	@Convert( converter = YesNoToBooleanConverter.class )
	@Column( name = "is_nav_sidebar_expanded" )
	public Boolean getNavSidebarExpanded()
	{
		return navSidebarExpanded;
	}
	
	public void setId( int id )
	{
		this.id = id;
	}
	
	public void setNavSidebarExpanded( Boolean navSidebarExpanded )
	{
		this.navSidebarExpanded = navSidebarExpanded;
	}
}
