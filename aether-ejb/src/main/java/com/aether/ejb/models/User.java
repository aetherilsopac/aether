//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: User.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Represents the USERS table.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Apr 3, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.aether.ejb.enums.UserStatus;
import com.aether.ejb.enums.UserType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

//@formatter:off
@JsonTypeInfo(
    include = As.EXISTING_PROPERTY, 
    use = JsonTypeInfo.Id.NAME, 
    property = "userType", 
    visible = true
)
@JsonSubTypes(value = {
    @JsonSubTypes.Type( name = "EMPLOYEE", value = EmployeeUser.class ),
    @JsonSubTypes.Type( name = "PATRON", value = PatronUser.class )
})
//@formatter:on
@Entity
@Inheritance( strategy = InheritanceType.JOINED )
@Table( name = "users" )
public abstract class User extends AuditableEntity implements Serializable
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = 7170830106110903446L;
	
	private String userName;
	private String firstName;
	private String middleInitial;
	private String lastName;
	private String email;
	private String email2;
	private String phone;
	private String phone2;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String zip;
	private String country;
	private Date dob;
	private Branch branch;
	private UserType userType;
	private String passwordHash;
	private String password;
	private UserStatus status;
	private Role role;
	private List<Permission> permissions;
	private UserPreferences preferences;
	
	@Column( name = "uname", length = 100, nullable = false, unique = true )
	public String getUserName()
	{
		return userName;
	}
	
	@Column( name = "fname", length = 100, nullable = false )
	public String getFirstName()
	{
		return firstName;
	}
	
	@Column( name = "mi", length = 1 )
	public String getMiddleInitial()
	{
		return middleInitial;
	}
	
	@Column( name = "lname", length = 100, nullable = false )
	public String getLastName()
	{
		return lastName;
	}
	
	@JsonProperty( value = "friendlyName", access = Access.READ_ONLY )
	@Transient
	public String getFriendlyName()
	{
		return getFirstName() + getLastName();
	}
	
	@JsonProperty( value = "formalName", access = Access.READ_ONLY )
	@Transient
	public String getFormalName()
	{
		String formalName = getLastName() + ", " + getFirstName();
		
		if ( getMiddleInitial() != null && getMiddleInitial().length() > 0 )
		{
			formalName += " " + getMiddleInitial() + ".";
		}
		
		return formalName;
	}
	
	@Column( name = "email", length = 100 )
	public String getEmail()
	{
		return email;
	}
	
	@Column( name = "email2", length = 100 )
	public String getEmail2()
	{
		return email2;
	}
	
	@Column( name = "phone", length = 50 )
	public String getPhone()
	{
		return phone;
	}
	
	@Column( name = "phone2", length = 50 )
	public String getPhone2()
	{
		return phone2;
	}
	
	@Column( name = "address_line1", length = 100 )
	public String getAddressLine1()
	{
		return addressLine1;
	}
	
	@Column( name = "address_line2", length = 100 )
	public String getAddressLine2()
	{
		return addressLine2;
	}
	
	@Column( name = "city", length = 50 )
	public String getCity()
	{
		return city;
	}
	
	@Column( name = "state", length = 50 )
	public String getState()
	{
		return state;
	}
	
	@Column( name = "zip", length = 10 )
	public String getZip()
	{
		return zip;
	}
	
	@Column( name = "country", length = 100 )
	public String getCountry()
	{
		return country;
	}
	
	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "dob", nullable = false )
	public Date getDob()
	{
		return dob;
	}
	
	@ManyToOne( optional = false, fetch = FetchType.EAGER )
	@JoinColumn( name = "branch_id", nullable = false )
	public Branch getBranch()
	{
		return branch;
	}
	
	@Enumerated( EnumType.STRING )
	@Column( name = "utype", length = 1, nullable = false )
	public UserType getUserType()
	{
		return userType;
	}
	
	@JsonIgnore
	@Column( name = "password_hash" )
	public String getPasswordHash()
	{
		return passwordHash;
	}
	
	@Transient
	public String getPassword()
	{
		return password;
	}
	
	@Enumerated( EnumType.STRING )
	@Column( name = "status", length = 10, nullable = false )
	public UserStatus getStatus()
	{
		return status;
	}
	
	@ManyToOne( optional = false )
	@JoinColumn( name = "role_id", nullable = false )
	public Role getRole()
	{
		return role;
	}
	
	//@formatter:off
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_permissions",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "permission_id", referencedColumnName = "id")}
    )
    public List<Permission> getPermissions()
    {
        return permissions;
    }
    //@formatter:on
	
	@OneToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL )
	@PrimaryKeyJoinColumn
	public UserPreferences getPreferences()
	{
		return preferences;
	}
	
	public void setUserName( String userName )
	{
		this.userName = userName;
	}
	
	public void setFirstName( String firstName )
	{
		this.firstName = firstName;
	}
	
	public void setMiddleInitial( String middleInitial )
	{
		this.middleInitial = middleInitial;
	}
	
	public void setLastName( String lastName )
	{
		this.lastName = lastName;
	}
	
	public void setEmail( String email )
	{
		this.email = email;
	}
	
	public void setEmail2( String email2 )
	{
		this.email2 = email2;
	}
	
	public void setPhone( String phone )
	{
		this.phone = phone;
	}
	
	public void setPhone2( String phone2 )
	{
		this.phone2 = phone2;
	}
	
	public void setAddressLine1( String addressLine1 )
	{
		this.addressLine1 = addressLine1;
	}
	
	public void setAddressLine2( String addressLine2 )
	{
		this.addressLine2 = addressLine2;
	}
	
	public void setCity( String city )
	{
		this.city = city;
	}
	
	public void setState( String state )
	{
		this.state = state;
	}
	
	public void setZip( String zip )
	{
		this.zip = zip;
	}
	
	public void setCountry( String country )
	{
		this.country = country;
	}
	
	public void setDob( Date dob )
	{
		this.dob = dob;
	}
	
	public void setBranch( Branch branch )
	{
		this.branch = branch;
	}
	
	public void setUserType( UserType userType )
	{
		this.userType = userType;
	}
	
	public void setPasswordHash( String passwordHash )
	{
		this.passwordHash = passwordHash;
	}
	
	public void setPassword( String password )
	{
		this.password = password;
	}
	
	public void setStatus( UserStatus status )
	{
		this.status = status;
	}
	
	public void setRole( Role role )
	{
		this.role = role;
	}
	
	public void setPermissions( List<Permission> permissions )
	{
		this.permissions = permissions;
	}
	
	public void setPreferences( UserPreferences preferences )
	{
		this.preferences = preferences;
	}
	
	// Methods for making conversion to Spring Security models easier
	@JsonIgnore
	@Transient
	public boolean isAccountNonExpired()
	{
		return true;
	}
	
	@JsonIgnore
	@Transient
	public boolean isAccountNonLocked()
	{
		return true;
	}
	
	@JsonIgnore
	@Transient
	public boolean isCredentialsNonExpired()
	{
		return true;
	}
	
	@JsonIgnore
	@Transient
	public boolean isEnabled()
	{
		return getStatus() == UserStatus.ACTIVE;
	}
}
