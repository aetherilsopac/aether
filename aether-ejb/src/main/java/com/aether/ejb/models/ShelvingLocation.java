package com.aether.ejb.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table( name = "shelving_locations" )
public class ShelvingLocation extends AuditableEntity implements Serializable
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -1440738176033840618L;
	
	private String name;
	private String description;
	private Boolean active;
	
	public ShelvingLocation()
	{
		setActive( Boolean.TRUE );
	}
	
	@Column( name = "name", length = 100, nullable = false, unique = true )
	public String getName()
	{
		return name;
	}
	
	@Column( name = "description", length = 500 )
	public String getDescription()
	{
		return description;
	}
	
	@Column( name = "active", nullable = false )
	public Boolean getActive()
	{
		return active;
	}
	
	public void setName( String name )
	{
		this.name = name;
	}
	
	public void setDescription( String description )
	{
		this.description = description;
	}
	
	public void setActive( Boolean active )
	{
		this.active = active;
	}
}
