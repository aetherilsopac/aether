//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models.minimized
 * File: UserMinimized.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 26, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models.minimized;

import java.io.Serializable;
import java.util.Date;

import com.aether.ejb.enums.UserStatus;
import com.aether.ejb.enums.UserType;
import com.aether.ejb.models.Branch;
import com.aether.ejb.models.Role;
import com.aether.ejb.models.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

public class UserMinimized implements Serializable
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -4965406778945836670L;
	
	private Integer id;
	private String userName;
	private String firstName;
	private String middleInitial;
	private String lastName;
	private String friendlyName;
	private String formalName;
	private String email;
	private String phone;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String zip;
	private String country;
	private Date dob;
	private Branch branch;
	private UserType userType;
	private UserStatus status;
	private Role role;
	
	public UserMinimized()
	{
	}
	
	public UserMinimized( User user )
	{
		this.id = user.getId();
		this.userName = user.getUserName();
		this.firstName = user.getFirstName();
		this.middleInitial = user.getMiddleInitial();
		this.lastName = user.getLastName();
		this.friendlyName = user.getFriendlyName();
		this.formalName = user.getFormalName();
		this.email = user.getEmail();
		this.phone = user.getPhone();
		this.addressLine1 = user.getAddressLine1();
		this.addressLine2 = user.getAddressLine2();
		this.city = user.getCity();
		this.state = user.getState();
		this.zip = user.getZip();
		this.country = user.getCountry();
		this.dob = user.getDob();
		this.branch = user.getBranch();
		this.userType = user.getUserType();
		this.status = user.getStatus();
		this.role = user.getRole();
	}
	
	public Integer getId()
	{
		return id;
	}
	
	public String getUserName()
	{
		return userName;
	}
	
	public String getFirstName()
	{
		return firstName;
	}
	
	public String getMiddleInitial()
	{
		return middleInitial;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	@JsonProperty( value = "friendlyName", access = Access.READ_ONLY )
	public String getFriendlyName()
	{
		return friendlyName;
	}
	
	@JsonProperty( value = "formalName", access = Access.READ_ONLY )
	public String getFormalName()
	{
		return formalName;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public String getPhone()
	{
		return phone;
	}
	
	public String getAddressLine1()
	{
		return addressLine1;
	}
	
	public String getAddressLine2()
	{
		return addressLine2;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public String getState()
	{
		return state;
	}
	
	public String getZip()
	{
		return zip;
	}
	
	public String getCountry()
	{
		return country;
	}
	
	public Date getDob()
	{
		return dob;
	}
	
	public Branch getBranch()
	{
		return branch;
	}
	
	public UserType getUserType()
	{
		return userType;
	}
	
	public UserStatus getStatus()
	{
		return status;
	}
	
	public Role getRole()
	{
		return role;
	}
	
	public void setId( Integer id )
	{
		this.id = id;
	}
	
	public void setUserName( String userName )
	{
		this.userName = userName;
	}
	
	public void setFirstName( String firstName )
	{
		this.firstName = firstName;
	}
	
	public void setMiddleInitial( String middleInitial )
	{
		this.middleInitial = middleInitial;
	}
	
	public void setLastName( String lastName )
	{
		this.lastName = lastName;
	}
	
	public void setEmail( String email )
	{
		this.email = email;
	}
	
	public void setPhone( String phone )
	{
		this.phone = phone;
	}
	
	public void setAddressLine1( String addressLine1 )
	{
		this.addressLine1 = addressLine1;
	}
	
	public void setAddressLine2( String addressLine2 )
	{
		this.addressLine2 = addressLine2;
	}
	
	public void setCity( String city )
	{
		this.city = city;
	}
	
	public void setState( String state )
	{
		this.state = state;
	}
	
	public void setZip( String zip )
	{
		this.zip = zip;
	}
	
	public void setCountry( String country )
	{
		this.country = country;
	}
	
	public void setDob( Date dob )
	{
		this.dob = dob;
	}
	
	public void setBranch( Branch branch )
	{
		this.branch = branch;
	}
	
	public void setUserType( UserType userType )
	{
		this.userType = userType;
	}
	
	public void setStatus( UserStatus status )
	{
		this.status = status;
	}
	
	public void setRole( Role role )
	{
		this.role = role;
	}
}
