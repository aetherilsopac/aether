//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: Permission.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Models the PERMISSIONS table.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Apr 3, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "permissions")
public class Permission implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = 3106318083819667968L;

    private int id;
    private String permissionDesc;
    private String permissionName;

    @Id
    @Column(name = "id")
    public int getId()
    {
        return id;
    }

    @Column(name = "permission_desc", length = 50)
    public String getPermissionDesc()
    {
        return permissionDesc;
    }

    @Column(name = "permission_name", length = 50, nullable = false, unique = true)
    public String getPermissionName()
    {
        return permissionName;
    }

    public void setId( int id )
    {
        this.id = id;
    }

    public void setPermissionDesc( String permissionDesc )
    {
        this.permissionDesc = permissionDesc;
    }

    public void setPermissionName( String permissionName )
    {
        this.permissionName = permissionName;
    }
}
