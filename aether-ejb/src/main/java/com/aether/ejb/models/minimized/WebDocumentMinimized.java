//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models.minimized
 * File: WebDocumentMinimized.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Strips a Web Document of fields that do not need to be serialized in most cases.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 26, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models.minimized;

import java.io.Serializable;
import com.aether.ejb.enums.WebDocumentType;
import com.aether.ejb.models.WebDocument;

public class WebDocumentMinimized implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = 8454521198748804259L;

    private Integer id;
    private WebDocumentType documentType;
    private String friendlyUrl;
    private String title;
    private Boolean permanent;
    private String locale;

    public WebDocumentMinimized()
    {
    }

    public WebDocumentMinimized(WebDocument document)
    {
        id = document.getId();
        documentType = document.getDocumentType();
        friendlyUrl = document.getFriendlyUrl();
        title = document.getTitle();
        permanent = document.getPermanent();
        locale = document.getLocale();
    }

    public Integer getId()
    {
        return id;
    }

    public WebDocumentType getDocumentType()
    {
        return documentType;
    }

    public String getFriendlyUrl()
    {
        return friendlyUrl;
    }

    public String getTitle()
    {
        return title;
    }

    public Boolean getPermanent()
    {
        return permanent;
    }

    public String getLocale()
    {
        return locale;
    }

    public void setId( Integer id )
    {
        this.id = id;
    }

    public void setDocumentType( WebDocumentType documentType )
    {
        this.documentType = documentType;
    }

    public void setFriendlyUrl( String friendlyUrl )
    {
        this.friendlyUrl = friendlyUrl;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }

    public void setPermanent( Boolean permanent )
    {
        this.permanent = permanent;
    }

    public void setLocale( String locale )
    {
        this.locale = locale;
    }
}
