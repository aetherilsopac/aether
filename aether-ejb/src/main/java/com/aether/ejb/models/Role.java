
package com.aether.ejb.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Role implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = 3558109600538423516L;

    private int id;
    private String roleName;
    private String description;
    private List<Permission> permissions;

    @Id
    @Column(name = "id")
    public int getId()
    {
        return id;
    }

    @Column(name = "role_name", length = 20, nullable = false, unique = true)
    public String getRoleName()
    {
        return roleName;
    }

    @Column(name = "description", length = 200)
    public String getDescription()
    {
        return description;
    }

    //@formatter:off
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "roles_permissions",
        joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "permission_id", referencedColumnName = "id")}
    )
    public List<Permission> getPermissions() 
    {
        return permissions;
    }
    //@formatter:on

    public void setId( int id )
    {
        this.id = id;
    }

    public void setRoleName( String roleName )
    {
        this.roleName = roleName;
    }

    public void setDescription( String description )
    {
        this.description = description;
    }

    public void setPermissions( List<Permission> permissions )
    {
        this.permissions = permissions;
    }
}
