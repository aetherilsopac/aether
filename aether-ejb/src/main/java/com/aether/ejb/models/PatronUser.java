//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: PatronUser.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jul 29, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.aether.ejb.enums.UserSex;
import com.aether.ejb.enums.UserType;

@Entity
@Table(name = "patron_users")
public class PatronUser extends User implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = 1003726174239667144L;

    private String cardNumber;
    private PatronCategory patronCategory;
    private Date expiryDate;
    private Boolean debarred;
    private PatronUser guarantor;
    private String guarantorRelationship;
    private String notes;
    private UserSex sex;
    private String smsPhone;

    public PatronUser()
    {
        setUserType( UserType.PATRON );
        setDebarred( Boolean.FALSE );
        setSex( UserSex.M );
    }

    @Column(name = "cardnumber", length = 50)
    public String getCardNumber()
    {
        return cardNumber;
    }

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "patron_category_id", nullable = false)
    public PatronCategory getPatronCategory()
    {
        return patronCategory;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "date_expiry")
    public Date getExpiryDate()
    {
        return expiryDate;
    }

    @Column(name = "debarred", nullable = false)
    public Boolean getDebarred()
    {
        return debarred;
    }

    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "guarantor_id")
    public PatronUser getGuarantor()
    {
        return guarantor;
    }

    @Column(name = "guarantor_relationship", length = 100)
    public String getGuarantorRelationship()
    {
        return guarantorRelationship;
    }

    @Lob
    @Column(name = "notes", columnDefinition = "MEDIUMTEXT")
    public String getNotes()
    {
        return notes;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "sex", nullable = false, length = 1)
    public UserSex getSex()
    {
        return sex;
    }

    @Column(name = "sms_phone", length = 50)
    public String getSmsPhone()
    {
        return smsPhone;
    }

    public void setCardNumber( String cardNumber )
    {
        this.cardNumber = cardNumber;
    }

    public void setPatronCategory( PatronCategory patronCategory )
    {
        this.patronCategory = patronCategory;
    }

    public void setExpiryDate( Date expiryDate )
    {
        this.expiryDate = expiryDate;
    }

    public void setDebarred( Boolean debarred )
    {
        this.debarred = debarred;
    }

    public void setGuarantor( PatronUser guarantor )
    {
        this.guarantor = guarantor;
    }

    public void setGuarantorRelationship( String guarantorRelationship )
    {
        this.guarantorRelationship = guarantorRelationship;
    }

    public void setNotes( String notes )
    {
        this.notes = notes;
    }

    public void setSex( UserSex sex )
    {
        this.sex = sex;
    }

    public void setSmsPhone( String smsPhone )
    {
        this.smsPhone = smsPhone;
    }
}
