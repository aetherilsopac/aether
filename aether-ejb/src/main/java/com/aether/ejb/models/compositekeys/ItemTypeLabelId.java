//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models.compositekeys
 * File: ItemTypeLabelId.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Serves as a composite key for the ItemTypeLabel model.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 29, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models.compositekeys;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.aether.ejb.models.ItemTypeField;

@Embeddable
public class ItemTypeLabelId implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = 8592454742387911591L;

    private Integer itemTypeId;
    private String biblioColumnName;
    private String locale;

    public ItemTypeLabelId()
    {
    }

    public ItemTypeLabelId(ItemTypeField field, String locale)
    {
        this.setItemTypeId( field.getId().getItemTypeId() );
        this.setLocale( locale );
    }

    @Column(name = "item_type_id", nullable = false)
    public Integer getItemTypeId()
    {
        return itemTypeId;
    }

    @Column(name = "biblio_column_name", length = 100, nullable = false)
    public String getBiblioColumnName()
    {
        return biblioColumnName;
    }

    @Column(name = "locale", length = 10, nullable = false)
    public String getLocale()
    {
        return locale;
    }

    public void setItemTypeId( Integer itemTypeId )
    {
        this.itemTypeId = itemTypeId;
    }

    public void setBiblioColumnName( String biblioColumnName )
    {
        this.biblioColumnName = biblioColumnName;
    }

    public void setLocale( String locale )
    {
        this.locale = locale;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( biblioColumnName == null ) ? 0 : biblioColumnName.hashCode() );
        result = prime * result + ( ( itemTypeId == null ) ? 0 : itemTypeId.hashCode() );
        result = prime * result + ( ( locale == null ) ? 0 : locale.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj )
    {
        if( this == obj ) return true;
        if( obj == null ) return false;
        if( getClass() != obj.getClass() ) return false;
        ItemTypeLabelId other = (ItemTypeLabelId) obj;
        if( biblioColumnName == null )
        {
            if( other.biblioColumnName != null ) return false;
        }
        else if( !biblioColumnName.equals( other.biblioColumnName ) ) return false;
        if( itemTypeId == null )
        {
            if( other.itemTypeId != null ) return false;
        }
        else if( !itemTypeId.equals( other.itemTypeId ) ) return false;
        if( locale == null )
        {
            if( other.locale != null ) return false;
        }
        else if( !locale.equals( other.locale ) ) return false;
        return true;
    }
}
