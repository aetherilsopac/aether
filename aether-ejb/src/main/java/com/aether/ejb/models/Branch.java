//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: Branch.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Apr 3, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table( name = "branches" )
public class Branch extends AuditableEntity implements Serializable
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -1235592095490491928L;
	
	private String branchName;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String zip;
	private String phone;
	private String fax;
	private String email;
	private String replyTo;
	private String url;
	private Boolean holdsAllowed;
	
	@Column( name = "branch_name", length = 200, nullable = false )
	public String getBranchName()
	{
		return branchName;
	}
	
	@Column( name = "address_line1", length = 200 )
	public String getAddressLine1()
	{
		return addressLine1;
	}
	
	@Column( name = "address_line2", length = 200 )
	public String getAddressLine2()
	{
		return addressLine2;
	}
	
	@Column( name = "city", length = 50 )
	public String getCity()
	{
		return city;
	}
	
	@Column( name = "state", length = 50 )
	public String getState()
	{
		return state;
	}
	
	@Column( name = "zip", length = 10 )
	public String getZip()
	{
		return zip;
	}
	
	@Column( name = "phone", length = 20 )
	public String getPhone()
	{
		return phone;
	}
	
	@Column( name = "fax", length = 20 )
	public String getFax()
	{
		return fax;
	}
	
	@Column( name = "email", length = 50 )
	public String getEmail()
	{
		return email;
	}
	
	@Column( name = "replyto", length = 50 )
	public String getReplyTo()
	{
		return replyTo;
	}
	
	@Column( name = "url", length = 100 )
	public String getUrl()
	{
		return url;
	}
	
	@Column( name = "holds_allowed" )
	public Boolean getHoldsAllowed()
	{
		return holdsAllowed;
	}
	
	public void setBranchName( String branchName )
	{
		this.branchName = branchName;
	}
	
	public void setAddressLine1( String addressLine1 )
	{
		this.addressLine1 = addressLine1;
	}
	
	public void setAddressLine2( String addressLine2 )
	{
		this.addressLine2 = addressLine2;
	}
	
	public void setCity( String city )
	{
		this.city = city;
	}
	
	public void setState( String state )
	{
		this.state = state;
	}
	
	public void setZip( String zip )
	{
		this.zip = zip;
	}
	
	public void setPhone( String phone )
	{
		this.phone = phone;
	}
	
	public void setFax( String fax )
	{
		this.fax = fax;
	}
	
	public void setEmail( String email )
	{
		this.email = email;
	}
	
	public void setReplyTo( String replyTo )
	{
		this.replyTo = replyTo;
	}
	
	public void setUrl( String url )
	{
		this.url = url;
	}
	
	public void setHoldsAllowed( Boolean holdsAllowed )
	{
		this.holdsAllowed = holdsAllowed;
	}
}
