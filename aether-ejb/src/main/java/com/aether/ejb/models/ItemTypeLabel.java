//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.models
 * File: ItemTypeLabel.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * To allow Labels and tooltips of Bibliographic entry/item fields to be tailored to a specific Item Type and specific locale.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 29, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.aether.ejb.models.compositekeys.ItemTypeLabelId;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "item_type_labels")
public class ItemTypeLabel implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = -4035494023558969686L;

    private ItemTypeLabelId id;
    private ItemTypeField field;
    private String locale;
    private String name;
    private String description;

    public ItemTypeLabel()
    {
    }

    public ItemTypeLabel(ItemTypeField field, String locale, String name, String description)
    {
        this.setId( new ItemTypeLabelId( field, locale ) );
        this.setName( name );
        this.setDescription( description );
    }

    @EmbeddedId
    public ItemTypeLabelId getId()
    {
        return id;
    }

    //@formatter:off
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name = "item_type_id", referencedColumnName = "item_type_id", updatable = false, insertable = false),
        @JoinColumn(name = "biblio_column_name", referencedColumnName = "biblio_column_name", updatable = false, insertable = false)
    })
    //@formatter:on
    public ItemTypeField getField()
    {
        return field;
    }

    @Column(name = "locale", updatable = false, insertable = false)
    public String getLocale()
    {
        return locale;
    }

    @Column(name = "label_name", length = 100, nullable = false)
    public String getName()
    {
        return name;
    }

    @Column(name = "label_desc", length = 300)
    public String getDescription()
    {
        return description;
    }

    public void setId( ItemTypeLabelId id )
    {
        this.id = id;
    }

    public void setField( ItemTypeField field )
    {
        this.field = field;
    }

    public void setLocale( String locale )
    {
        this.locale = locale;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public void setDescription( String description )
    {
        this.description = description;
    }

}
