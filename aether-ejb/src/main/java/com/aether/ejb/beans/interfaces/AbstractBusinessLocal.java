package com.aether.ejb.beans.interfaces;

import java.io.Serializable;
import java.util.List;

import com.aether.ejb.models.User;

public interface AbstractBusinessLocal<T> extends Serializable
{
	public abstract T find( Integer primaryKey );
	
	public abstract void add( T entity, User creator );
	
	public abstract T update( T entity, User updater );
	
	public abstract boolean remove( T entity );
	
	public abstract boolean removeByPrimaryKey( Integer primaryKey );
	
	public abstract List<T> list();
}
