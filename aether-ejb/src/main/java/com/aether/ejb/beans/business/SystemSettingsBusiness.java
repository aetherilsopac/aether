//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.beans.business
 * File: GlobalSettingsBusiness.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Oct 27, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.beans.business;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.aether.ejb.beans.interfaces.SystemSettingsBusinessLocal;
import com.aether.ejb.models.SystemSetting;

@Stateless(mappedName = "ejb/SystemSettingBusiness")
public class SystemSettingsBusiness implements SystemSettingsBusinessLocal
{
    private static Logger log = LogManager.getLogger( SystemSettingsBusiness.class );

    @PersistenceContext(unitName = "AetherEJB")
    private EntityManager em;

    @PostConstruct
    private void init()
    {
        SystemSettingsBusiness.log.trace( "SystemSettingsBusiness EJB initialized..." );
    }

    @Override
    public List<SystemSetting> getAllSettings()
    {
        return em.createQuery( "select s from SystemSetting s", SystemSetting.class ).getResultList();
    }

    @Override
    public void saveAllSettings( List<SystemSetting> settings )
    {
        for ( int i = 0; i < settings.size(); i++ )
        {
            settings.set( i, em.merge( settings.get( i ) ) );
        }
        em.flush();
    }

    @Override
    public SystemSetting getSetting( String settingName )
    {
        //@formatter:off
        List<SystemSetting> results = em.createQuery( "select s from SystemSetting s where s.name = :settingName", SystemSetting.class )
                                        .setParameter( "settingName", settingName )
                                        .getResultList();
        //@formatter:on

        if( results.size() < 1 ) return null;
        return results.get( 0 );
    }

    @Override
    public SystemSetting saveSetting( SystemSetting setting )
    {
        setting = em.merge( setting );
        em.flush();
        return setting;
    }
}
