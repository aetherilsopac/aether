//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.beans.business
 * File: ItemTypeBusiness.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Oct 1, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.beans.business;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aether.ejb.beans.interfaces.ItemTypeBusinessLocal;
import com.aether.ejb.models.ItemType;
import com.aether.ejb.models.ItemTypeField;

@Stateless( mappedName = "ejb/ItemTypeBusiness" )
public class ItemTypeBusiness extends AbstractBusiness<ItemType> implements ItemTypeBusinessLocal
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = 7243226647993199769L;
	
	private static Logger log = LogManager.getLogger( ItemTypeBusiness.class );
	
	@PersistenceContext( unitName = "AetherEJB" )
	private EntityManager em;
	
	@PostConstruct
	private void init()
	{
		ItemTypeBusiness.log.trace( "ItemTypeBusiness EJB initialized..." );
	}
	
	@Override
	public List<ItemType> getAllActiveItemTypes()
	{
		//@formatter:off
        return em.createQuery( "select t from ItemType t where t.active = TRUE order by t.description", ItemType.class )
                 .getResultList();
        //@formatter:on
	}
	
	@Override
	public ItemType getDefaultItemType()
	{
		try
		{
			//@formatter:off
            return em.createQuery( "select t from ItemType t where t.default = TRUE", ItemType.class )
                     .getResultList()
                     .get( 0 );
            //@formatter:on
		}
		catch ( IndexOutOfBoundsException e )
		{
			return null;
		}
	}
	
	@Override
	public List<ItemTypeField> getItemTypeFieldsByLocale( ItemType itemType, String locale )
	{
		//@formatter:off
        TypedQuery<ItemTypeField> query = em.createQuery( "select f "
                                                        + "from ItemTypeField f "
                                                        + "inner join fetch f.labels l "
                                                        + "where "
                                                        + "     f.itemType.id = :itemTypeId and "
                                                        + "     l.locale = :locale", ItemTypeField.class );
        //@formatter:on
		
		query.setParameter( "itemTypeId", itemType.getId() );
		query.setParameter( "locale", locale );
		
		return query.getResultList();
	}
}
