package com.aether.ejb.beans.business;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aether.ejb.beans.interfaces.AbstractBusinessLocal;
import com.aether.ejb.models.AuditableEntity;
import com.aether.ejb.models.User;

public class AbstractBusiness<T extends AuditableEntity> implements AbstractBusinessLocal<T>
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -7121654711422082529L;
	
	private static Logger log = LogManager.getLogger( AbstractBusiness.class );
	
	@PersistenceContext( unitName = "AetherEJB" )
	protected EntityManager em;
	
	@Override
	public T find( Integer primaryKey )
	{
		try
		{
			return em.find( getEntityClass(), primaryKey );
		}
		catch ( Exception e )
		{
			//@formatter:off
			AbstractBusiness.log.error( 
					"An exception was encountered while trying to find an entity of " +
					"type [" + getEntityClass().getName() + "] for id [" + primaryKey + "].", e
			);
			//@formatter:on
			return null;
		}
	}
	
	@Override
	public void add( T entity, User creator )
	{
		try
		{
			entity.setCreatedBy( creator );
			entity.setDateCreated( new Date() );
			entity.setUpdatedBy( creator );
			entity.setDateUpdated( new Date() );
			
			em.persist( entity );
			em.flush();
		}
		catch ( Exception e )
		{
			//@formatter:off
			AbstractBusiness.log.error( 
					"An exception was encountered while trying to persist an entity of " +
					"type [" + getEntityClass().getName() + "].", e
			);
			//@formatter:on
		}
	}
	
	@Override
	public T update( T entity, User updater )
	{
		try
		{
			T originalEntity = find( entity.getId() );
			
			entity.setCreatedBy( originalEntity.getCreatedBy() );
			entity.setDateCreated( originalEntity.getDateCreated() );
			entity.setUpdatedBy( updater );
			entity.setDateUpdated( new Date() );
			
			entity = em.merge( entity );
			em.flush();
			
			return entity;
		}
		catch ( Exception e )
		{
			//@formatter:off
			AbstractBusiness.log.error(
					"An exception was encountered while trying to merge an entity of " +
					"type [" + getEntityClass().getName() + "].", e
			);
			//@formatter:on
			return null;
		}
	}
	
	@Override
	public boolean remove( T entity )
	{
		try
		{
			em.remove( entity );
			em.flush();
			
			return true;
		}
		catch ( Exception e )
		{
			//@formatter:off
			AbstractBusiness.log.error(
					"An exception was encountered while trying to merge an entity of " +
					"type [" + getEntityClass().getName() + "].", e
			);
			//@formatter:on
			return false;
		}
	}
	
	@Override
	public boolean removeByPrimaryKey( Integer primaryKey )
	{
		try
		{
			em.remove( em.find( getEntityClass(), primaryKey ) );
			em.flush();
			
			return true;
		}
		catch ( Exception e )
		{
			//@formatter:off
			AbstractBusiness.log.error(
					"An exception was encountered while trying to remove an entity of " +
					"type [" + getEntityClass().getName() + "] with primary key [" +
					primaryKey + "] of type [" + getPrimaryKeyClass().getName() + "].", e
			);
			//@formatter:on
			return false;
		}
	}
	
	@Override
	public List<T> list()
	{
		try
		{
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<T> cq = cb.createQuery( getEntityClass() );
			Root<T> rt = cq.from( getEntityClass() );
			
			TypedQuery<T> q = em.createQuery( cq.select( rt ) );
			return q.getResultList();
		}
		catch ( Exception e )
		{
			//@formatter:off
			AbstractBusiness.log.error(
					"An exception was encountered while trying to list all records " +
					"for entity of type [" + getEntityClass().getName() + "].", e
			);
			//@formatter:on
			return new ArrayList<T>( 0 );
		}
	}
	
	private Class<T> getEntityClass()
	{
		return getGenericArgumentClass( 0 );
	}
	
	private Class<T> getPrimaryKeyClass()
	{
		return getGenericArgumentClass( 1 );
	}
	
	@SuppressWarnings( "unchecked" )
	private Class<T> getGenericArgumentClass( int argumentIndex )
	{
		String className = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[argumentIndex].getTypeName();
		
		try
		{
			Class<?> entityClass = Class.forName( className );
			return (Class<T>) entityClass;
		}
		catch ( ClassNotFoundException e )
		{
			AbstractBusiness.log.error( "Error encountered while trying to get Class<?> for " + className, e );
			return null;
		}
	}
	
}
