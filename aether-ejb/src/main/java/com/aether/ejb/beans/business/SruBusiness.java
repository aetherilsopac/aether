//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.beans.business
 * File: SruBusiness.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 17, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.beans.business;

import java.io.StringReader;
import java.net.URI;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.marc4j.MarcReader;
import org.marc4j.MarcXmlReader;
import org.marc4j.marc.Record;
import org.xml.sax.InputSource;
import org.z3950.zing.cql.CQLNode;
import org.z3950.zing.cql.CQLRelation;
import org.z3950.zing.cql.CQLTermNode;

import com.aether.ejb.beans.interfaces.SruBusinessLocal;
import com.aether.ejb.models.Biblio;
import com.aether.ejb.models.SruSource;
import com.aether.ejb.srumarc4j.SruMarcXmlReader;
import com.aether.ejb.viewmodels.SruQuery;
import com.aether.ejb.viewmodels.SruResponseRecordSet;

@Stateless( mappedName = "ejb/SruBusiness" )
public class SruBusiness extends AbstractBusiness<SruSource> implements SruBusinessLocal
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -5312095511878187814L;
	
	private static Logger log = LogManager.getLogger( SruBusiness.class );
	
	@PersistenceContext( unitName = "AetherEJB" )
	private EntityManager em;
	
	@Override
	public List<SruSource> getAllSruSources()
	{
		return em.createQuery( "select s from SruSource s", SruSource.class ).getResultList();
	}
	
	@Override
	public List<SruSource> getAllActiveSruSources()
	{
		return em.createQuery( "select s from SruSource s where s.active = TRUE", SruSource.class ).getResultList();
	}
	
	@Override
	public SruResponseRecordSet getSruRecordSet( SruQuery query )
	{
		int firstRecord = ((query.getPage() - 1) * query.getPageSize()) + 1;
		
		String cqlQuery = compileSruQuery( query.getSruSource(), query.getBiblioId(), query.getBiblioIdType() );
		String sruResponseXml = executeSruQuery( query.getSruSource(), cqlQuery, firstRecord, query.getPageSize() );
		return parseSruResponseXml( sruResponseXml );
	}
	
	private String compileSruQuery( SruSource sruSource, String biblioId, String biblioIdType )
	{
		
		CQLNode rootNode = new CQLTermNode( sruSource.getSruProfile() + biblioIdType.toLowerCase(), new CQLRelation( "=" ), biblioId );
		return rootNode.toCQL();
	}
	
	private String executeSruQuery( SruSource sruSource, String cqlQuery, int startRecord, int maximumRecords )
	{
		try
		{
			CloseableHttpClient httpClient = HttpClients.createDefault();
			
			//@formatter:off
            URI httpUri = new URIBuilder(sruSource.getUrl())
                            .addParameter( "version", "1.1" )
                            .addParameter( "operation", "searchRetrieve" )
                            .addParameter( "query", cqlQuery )
                            .addParameter( "recordSchema", "marcxml" )
                            .addParameter( "recordPacking", "xml" )
                            .addParameter( "startRecord", String.valueOf( startRecord ) )
                            .addParameter( "maximumRecords", String.valueOf( maximumRecords ) )
                            .build();
            //@formatter:on
			
			HttpGet httpGetRequest = new HttpGet( httpUri.toString() );
			
			CloseableHttpResponse httpResponse = httpClient.execute( httpGetRequest );
			
			HttpEntity responseData = httpResponse.getEntity();
			
			return (responseData != null) ? EntityUtils.toString( responseData ) : null;
		}
		catch ( Exception e )
		{
			SruBusiness.log.error( e );
			return null;
		}
	}
	
	private SruResponseRecordSet parseSruResponseXml( String sruResponseXml )
	{
		SruMarcXmlReader reader = new SruMarcXmlReader( new StringReader( sruResponseXml ) );
		SruResponseRecordSet recordSet = reader.parse();
		
		if ( recordSet.getRawMarcXml() != null && recordSet.getRawMarcXml().length() > 0 )
		{
			MarcReader marcReader = new MarcXmlReader( new InputSource( new StringReader( recordSet.getRawMarcXml() ) ) );
			
			while ( marcReader.hasNext() )
			{
				Record marcRecord = marcReader.next();
				recordSet.getRecords().add( new Biblio( marcRecord, recordSet.getRawMarcXml() ) );
			}
		}
		
		return recordSet;
	}
}
