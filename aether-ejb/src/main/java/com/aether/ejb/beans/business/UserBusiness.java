//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.beans.business
 * File: UserBusiness.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Apr 2, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.beans.business;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.aether.ejb.beans.interfaces.UserBusinessLocal;
import com.aether.ejb.enums.UserStatus;
import com.aether.ejb.models.EmployeeUser;
import com.aether.ejb.models.PatronCategory;
import com.aether.ejb.models.PatronUser;
import com.aether.ejb.models.Role;
import com.aether.ejb.models.User;
import com.aether.ejb.models.UserPreferences;
import com.aether.ejb.models.minimized.UserMinimized;
import com.aether.ejb.viewmodels.PatronQuery;

@Stateless(mappedName = "ejb/UserBusiness")
public class UserBusiness extends AbstractBusiness<User> implements UserBusinessLocal
{
    /**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -800378890690670678L;

	private static Logger log = LogManager.getLogger( UserBusiness.class );

    @PersistenceContext(unitName = "AetherEJB")
    private EntityManager em;

    @PostConstruct
    private void init()
    {
        UserBusiness.log.trace( "UserBusiness EJB initialized..." );
    }

    @Override
    public List<UserMinimized> getAllUsersMinimized()
    {
        return list().stream().map( ( u ) -> {
            return new UserMinimized( u );
        } ).collect( Collectors.toList() );
    }

    @Override
    public List<EmployeeUser> getAllEmployeeUsers()
    {
        return em.createQuery( "select u from EmployeeUser u", EmployeeUser.class ).getResultList();
    }

    @Override
    public List<UserMinimized> getAllEmployeeUsersMinimized()
    {
        return getAllEmployeeUsers().stream().map( ( u ) -> {
            return new UserMinimized( u );
        } ).collect( Collectors.toList() );
    }

    @Override
    public List<PatronUser> getAllPatronUsers()
    {
        return em.createQuery( "select p from PatronUser p", PatronUser.class ).getResultList();
    }

    @Override
    public List<UserMinimized> getAllPatronUsersMinimized()
    {
        return getAllPatronUsers().stream().map( ( u ) -> {
            return new UserMinimized( u );
        } ).collect( Collectors.toList() );
    }

    @Override
    public List<Role> getAllRoles()
    {
        return em.createQuery( "select r from Role r", Role.class ).getResultList();
    }

    @Override
    public List<PatronCategory> getAllPatronCategories()
    {
        return em.createQuery( "select c from PatronCategory c", PatronCategory.class ).getResultList();
    }

    @Override
    public List<PatronCategory> getActivePatronCategories()
    {
        //@formatter:off
        return em.createQuery( "select c from PatronCategory c where c.active = TRUE", PatronCategory.class )
                 .getResultList();
        //@formatter:on
    }

    @Override
    public User getUserByUserName( String userName )
    {
        try
        {
            //@formatter:off
            return (User) em.createQuery( "select u from User u where u.userName = :userName" )
                            .setParameter( "userName", userName )
                            .getResultList()
                            .get( 0 );
            //@formatter:on
        }
        catch ( IndexOutOfBoundsException e )
        {
            return null;
        }
    }
    
    @Override
    public void add( User user, User creator )
    {
        user.setCreatedBy( creator );
        user.setDateCreated( new Date() );
        user.setUpdatedBy( creator );
        user.setDateUpdated( new Date() );
        em.persist( user );

        user.setPreferences( new UserPreferences()
        {
			private static final long serialVersionUID = 6759440483451910551L;
			{
                setId( user.getId() );
                setNavSidebarExpanded( Boolean.TRUE );
            }
        } );

        em.persist( user );
        em.flush();
    }

    @Override
    public long countPatrons( PatronQuery patronQuery )
    {
        //@formatter:off
        TypedQuery<Long> query = em.createQuery( "select count (p.id) from PatronUser p "
                                               + "where "
                                               +    "(:cardNumber = null or upper(p.cardNumber) like upper(:cardNumber)) and "
                                               +    "(:firstName = null or upper(p.firstName) like upper(:firstName)) and "
                                               +    "(:lastName = null or upper(p.lastName) like upper(:lastName)) and "
                                               +    "(:userName = null or upper(p.userName) like upper(:userName)) and "
                                               +    "status = :activeStatus", Long.class );
        //@formatter:on

        String cardNumber = ( patronQuery.getCardNumber() != null && patronQuery.getCardNumber().length() > 0 ) ? patronQuery.getCardNumber() + "%" : null;
        String firstName = ( patronQuery.getFirstName() != null && patronQuery.getFirstName().length() > 0 ) ? patronQuery.getFirstName() + "%" : null;
        String lastName = ( patronQuery.getLastName() != null && patronQuery.getLastName().length() > 0 ) ? patronQuery.getLastName() + "%" : null;
        String userName = ( patronQuery.getUserName() != null && patronQuery.getUserName().length() > 0 ) ? patronQuery.getUserName() + "%" : null;

        query.setParameter( "cardNumber", cardNumber );
        query.setParameter( "firstName", firstName );
        query.setParameter( "lastName", lastName );
        query.setParameter( "userName", userName );
        query.setParameter( "activeStatus", UserStatus.ACTIVE );

        Long totalResults = query.getSingleResult();

        return totalResults.longValue();
    }

    @Override
    public List<UserMinimized> queryPatrons( PatronQuery patronQuery )
    {
        //@formatter:off
        TypedQuery<PatronUser> query = em.createQuery( "select p from PatronUser p "
                                                     + "where "
                                                     +    "(:cardNumber = null or upper(p.cardNumber) like upper(:cardNumber)) and "
                                                     +    "(:firstName = null or upper(p.firstName) like upper(:firstName)) and "
                                                     +    "(:lastName = null or upper(p.lastName) like upper(:lastName)) and "
                                                     +    "(:userName = null or upper(p.userName) like upper(:userName)) and "
                                                     +    "status = :activeStatus", PatronUser.class );
        //@formatter:on

        String cardNumber = ( patronQuery.getCardNumber() != null && patronQuery.getCardNumber().length() > 0 ) ? patronQuery.getCardNumber() + "%" : null;
        String firstName = ( patronQuery.getFirstName() != null && patronQuery.getFirstName().length() > 0 ) ? patronQuery.getFirstName() + "%" : null;
        String lastName = ( patronQuery.getLastName() != null && patronQuery.getLastName().length() > 0 ) ? patronQuery.getLastName() + "%" : null;
        String userName = ( patronQuery.getUserName() != null && patronQuery.getUserName().length() > 0 ) ? patronQuery.getUserName() + "%" : null;

        query.setParameter( "cardNumber", cardNumber );
        query.setParameter( "firstName", firstName );
        query.setParameter( "lastName", lastName );
        query.setParameter( "userName", userName );
        query.setParameter( "activeStatus", UserStatus.ACTIVE );
        query.setFirstResult( ( patronQuery.getPage() - 1 ) * patronQuery.getPageSize() );
        query.setMaxResults( patronQuery.getPageSize() );

        return query.getResultList().stream().map( ( u ) -> {
            return new UserMinimized( u );
        } ).collect( Collectors.toList() );
    }

    @Override
    public List<UserMinimized> queryAllPatrons( PatronQuery patronQuery )
    {
        //@formatter:off
        TypedQuery<PatronUser> query = em.createQuery( "select p from PatronUser p "
                                                     + "where "
                                                     +    "(:cardNumber = null or upper(p.cardNumber) like upper(:cardNumber)) and "
                                                     +    "(:firstName = null or upper(p.firstName) like upper(:firstName)) and "
                                                     +    "(:lastName = null or upper(p.lastName) like upper(:lastName)) and "
                                                     +    "(:userName = null or upper(p.userName) like upper(:userName))", PatronUser.class );
        //@formatter:on

        String cardNumber = ( patronQuery.getCardNumber() != null && patronQuery.getCardNumber().length() > 0 ) ? patronQuery.getCardNumber() + "%" : null;
        String firstName = ( patronQuery.getFirstName() != null && patronQuery.getFirstName().length() > 0 ) ? patronQuery.getFirstName() + "%" : null;
        String lastName = ( patronQuery.getLastName() != null && patronQuery.getLastName().length() > 0 ) ? patronQuery.getLastName() + "%" : null;
        String userName = ( patronQuery.getUserName() != null && patronQuery.getUserName().length() > 0 ) ? patronQuery.getUserName() + "%" : null;

        query.setParameter( "cardNumber", cardNumber );
        query.setParameter( "firstName", firstName );
        query.setParameter( "lastName", lastName );
        query.setParameter( "userName", userName );
        query.setFirstResult( ( patronQuery.getPage() - 1 ) * patronQuery.getPageSize() );
        query.setMaxResults( patronQuery.getPageSize() );

        return query.getResultList().stream().map( ( u ) -> {
            return new UserMinimized( u );
        } ).collect( Collectors.toList() );
    }

}
