//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.ejb.beans.interfaces
 * File: ShelvingLocationBusiness.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Feb 09, 2017, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.beans.business;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aether.ejb.beans.interfaces.ShelvingLocationBusinessLocal;
import com.aether.ejb.models.ShelvingLocation;
import com.aether.ejb.models.minimized.ShelvingLocationMinimized;

@Stateless( mappedName = "ejb/ShelvingLocationBusiness" )
public class ShelvingLocationBusiness extends AbstractBusiness<ShelvingLocation> implements ShelvingLocationBusinessLocal
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -3810625007589611904L;
	
	private static Logger log = LogManager.getLogger( ShelvingLocationBusiness.class );
	
	@PersistenceContext( unitName = "AetherEJB" )
	private EntityManager em;
	
	@PostConstruct
	private void init()
	{
		ShelvingLocationBusiness.log.trace( "ShelvingLocationBusiness EJB initialized..." );
	}
	
	@Override
	public ShelvingLocation getShelvingLocationByName( String name )
	{
		try
		{
			//@formatter:off
			return em.createQuery( "select l from ShelvingLocation l where upper(l.name) = upper(:name)", ShelvingLocation.class )
					 .setParameter( "name", name )
					 .getResultList()
					 .get( 0 );
			//@formatter:on
		}
		catch ( IndexOutOfBoundsException e )
		{
			return null;
		}
	}
	
	@Override
	public List<ShelvingLocationMinimized> getAllShelvingLocationsMinimized()
	{
		//@formatter:off
		return list()
				.stream()
				.map( (l) -> {
					return new ShelvingLocationMinimized( l );
				})
				.collect( Collectors.toList() );
		//@formatter:on
	}
	
	@Override
	public List<ShelvingLocation> getActiveShelvingLocations()
	{
		//@formatter:off
		return em.createQuery( "select l from ShelvingLocation l where l.active = TRUE order by name", ShelvingLocation.class )
				 .getResultList();
		//@formatter:on
	}
	
	@Override
	public List<ShelvingLocationMinimized> getActiveShelvingLocationsMinimized()
	{
		//@formatter:off
		return getActiveShelvingLocations()
				.stream()
				.map( (l) -> {
					return new ShelvingLocationMinimized( l );
				})
				.collect( Collectors.toList() );
		//@formatter:on
	}
	
	@Override
	public Long countMatchingShelvingLocations( ShelvingLocation location )
	{
		Long count = null;
		
		if ( location.getId() == null ) location.setId( -1 );
		
		//@formatter:off
		count = em.createQuery( "select count(l) from ShelvingLocation l where l.name = :name and l.id != :id", Long.class )
				  .setParameter( "name", location.getName() )
				  .setParameter( "id", location.getId() )
				  .getSingleResult();
		
		if( location.getId() == -1 ) location.setId( null );
		
		return count;
	}
	
}
