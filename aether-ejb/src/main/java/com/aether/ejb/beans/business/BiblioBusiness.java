//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.beans.business
 * File: SruBusiness.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 17, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.beans.business;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aether.ejb.beans.interfaces.BiblioBusinessLocal;
import com.aether.ejb.indexing.IndexManagerLocal;
import com.aether.ejb.models.Biblio;
import com.aether.ejb.models.User;

@Stateless( mappedName = "ejb/BiblioBusiness" )
public class BiblioBusiness extends AbstractBusiness<Biblio> implements BiblioBusinessLocal
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = 1141308721889216826L;
	
	private static Logger log = LogManager.getLogger( BiblioBusiness.class );
	
	@PersistenceContext( unitName = "AetherEJB" )
	private EntityManager em;
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/IndexManager" )
	private IndexManagerLocal indexManagerEjb;
	
	@PostConstruct
	private void init()
	{
		BiblioBusiness.log.trace( "BiblioBusiness EJB initialized..." );
	}
	
	@Override
	public List<Biblio> getBibliosByIds( List<Integer> ids )
	{
		//@formatter:off
        return em.createQuery( "select b from Biblio b where b.id in :ids", Biblio.class )
                 .setParameter( "ids", ids )
                 .getResultList();
        //@formatter:on
	}
	
	@Override
	public void add( Biblio biblio, User creator )
	{
		biblio.setCreatedBy( creator );
		biblio.setDateCreated( new Date() );
		biblio.setUpdatedBy( creator );
		biblio.setDateUpdated( new Date() );
		
		em.persist( biblio );
		em.flush();
		
		indexManagerEjb.addDocument( biblio );
	}
	
	@Override
	public Biblio update( Biblio biblio, User modifer )
	{
		biblio.setUpdatedBy( modifer );
		biblio.setDateUpdated( new Date() );
		
		biblio = em.merge( biblio );
		em.flush();
		
		indexManagerEjb.updateDocument( biblio );
		
		return biblio;
	}
	
	@Override
	public List<Biblio> getBatch( int skip, int take )
	{
		//@formatter:off
        return em.createQuery( "select b from Biblio b", Biblio.class )
                 .setFirstResult( skip )
                 .setMaxResults( take )
                 .getResultList();
        //@formatter:on
	}
	
	@Override
	public Long getTotal()
	{
		return em.createQuery( "select count(b) from Biblio b", Long.class ).getSingleResult();
	}
}
