//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.beans.interfaces
 * File: WebDocumentBusinessLocal.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Local interface for WebDocumentBusiness EJB.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 23, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.beans.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.aether.ejb.enums.WebDocumentType;
import com.aether.ejb.models.WebDocument;
import com.aether.ejb.models.minimized.WebDocumentMinimized;

@Local
public interface WebDocumentBusinessLocal extends AbstractBusinessLocal<WebDocument>
{
	public abstract List<WebDocumentMinimized> getAllWebDocumentsMinimized();
	
	public abstract Long countMatchingWebDocuments( WebDocument document );
	
	public abstract WebDocument getDocumentByIdAndType( Integer id, WebDocumentType documentType );
	
	public abstract WebDocument getDocumentByUrlAndLocale( String friendlyUrl, String locale, WebDocumentType documentType );
	
	public abstract WebDocument getDocumentByUrlAndLocaleWithFallbackLocale( String friendlyUrl, String locale, String fallbackLocale, WebDocumentType documentType );
}