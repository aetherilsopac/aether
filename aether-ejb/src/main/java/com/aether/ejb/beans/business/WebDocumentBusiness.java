//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.beans.business
 * File: WebDocumentBusiness.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Used for querying web documents (Help, Wiki, Fragments, etc.)
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 23, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.ejb.beans.business;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aether.ejb.beans.interfaces.WebDocumentBusinessLocal;
import com.aether.ejb.enums.WebDocumentType;
import com.aether.ejb.models.User;
import com.aether.ejb.models.WebDocument;
import com.aether.ejb.models.minimized.WebDocumentMinimized;

@Stateless( mappedName = "ejb/WebDocumentBusiness" )
public class WebDocumentBusiness extends AbstractBusiness<WebDocument> implements WebDocumentBusinessLocal
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -6121903960745764232L;
	
	private static Logger log = LogManager.getLogger( WebDocumentBusiness.class );
	
	@PersistenceContext( unitName = "AetherEJB" )
	private EntityManager em;
	
	@PostConstruct
	private void init()
	{
		WebDocumentBusiness.log.trace( "WebDocumentBusiness EJB initialized..." );
	}
	
	@Override
	public List<WebDocumentMinimized> getAllWebDocumentsMinimized()
	{
		return list().stream().map( ( d ) -> {
			return new WebDocumentMinimized( d );
		} ).collect( Collectors.toList() );
	}
	
	@Override
	public void add( WebDocument document, User creator )
	{
		document.setPermanent( Boolean.FALSE );
		super.add( document, creator );
	}
	
	@Override
	public WebDocument update( WebDocument document, User updater )
	{
		WebDocument originalDocument = find( document.getId() );
		document.setPermanent( originalDocument.getPermanent() );
		
		if ( originalDocument.getPermanent() )
		{
			document.setDocumentType( originalDocument.getDocumentType() );
		}
		
		return super.update( document, updater );
	}
	
	@Override
	public Long countMatchingWebDocuments( WebDocument document )
	{
		if ( document.getId() == null ) document.setId( -1 );
		
		//@formatter:off
        return em.createQuery( "select count(d) from WebDocument d " +
                               "where d.friendlyUrl = :friendlyUrl " +
                                 "and d.locale = :locale " +
                                 "and d.documentType = :documentType " +
                                 "and d.id != :id", Long.class )
                 .setParameter( "friendlyUrl", document.getFriendlyUrl() )
                 .setParameter( "locale", document.getLocale() )
                 .setParameter( "documentType", document.getDocumentType() )
                 .setParameter( "id", document.getId() )
                 .getSingleResult();
        //@formatter:on
	}
	
	@Override
	public WebDocument getDocumentByIdAndType( Integer id, WebDocumentType documentType )
	{
		try
		{
			//@formatter:off
            return em.createQuery( "select d from WebDocument d where d.id = :id and d.documentType = :documentType", WebDocument.class )
                     .setParameter( "id", id )
                     .setParameter( "documentType", documentType )
                     .getResultList()
                     .get( 0 );
            //@formatter:on
		}
		catch ( IndexOutOfBoundsException e )
		{
			return null;
		}
	}
	
	@Override
	public WebDocument getDocumentByUrlAndLocale( String friendlyUrl, String locale, WebDocumentType documentType )
	{
		try
		{
			//@formatter:off
            return em.createQuery( "select d from WebDocument d where d.friendlyUrl = :friendlyUrl and d.locale = :locale and d.documentType = :documentType", WebDocument.class )
                     .setParameter( "friendlyUrl", friendlyUrl )
                     .setParameter( "locale", locale )
                     .setParameter( "documentType", documentType )
                     .getResultList()
                     .get( 0 );
            //@formatter:on
		}
		catch ( IndexOutOfBoundsException e )
		{
			return null;
		}
	}
	
	@Override
	public WebDocument getDocumentByUrlAndLocaleWithFallbackLocale( String friendlyUrl, String locale, String fallbackLocale, WebDocumentType documentType )
	{
		WebDocument resultDocument = getDocumentByUrlAndLocale( friendlyUrl, locale, documentType );
		if ( resultDocument == null )
		{
			return getDocumentByUrlAndLocale( friendlyUrl, fallbackLocale, documentType );
		}
		return resultDocument;
	}
	
}
