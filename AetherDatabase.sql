CREATE DATABASE  IF NOT EXISTS `aether` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `aether`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: aether
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `biblios`
--

DROP TABLE IF EXISTS `biblios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biblios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` text COMMENT 'MARC 100$a.',
  `title` text COMMENT 'MARC 245$a.',
  `notes` text,
  `serial` tinyint(1) DEFAULT NULL COMMENT 'Boolean indicating if this is a serial publication (MARC Leader, Position 7, Value ''s'').',
  `copyright_date` date DEFAULT NULL COMMENT 'Publication/copyright date 260$c.\n',
  `volume_info` text COMMENT 'Volume information (362$a).',
  `item_type_id` int(11) DEFAULT NULL,
  `isbn` text COMMENT 'ISBN (020$a).',
  `issn` text COMMENT 'ISSN (022$a).',
  `ean` text COMMENT 'EAN (024 3#$a).',
  `publication_year` text COMMENT 'Date of publication (260$c).',
  `publisher_name` text COMMENT 'Publishing company (260$b).',
  `publication_location` text COMMENT 'Place of publishing (260$a).',
  `series_title` text COMMENT 'Title of series (490$a).',
  `series_issn` text COMMENT 'ISSN of series (490$x).',
  `series_volume` text COMMENT 'Volume/sequential designation (490$v).',
  `edition_statement` text COMMENT 'Edition statement (250$a).',
  `edition_responsibility` text COMMENT 'Edition responsibility (250$b).',
  `abstract` text COMMENT 'Summary from MARC record 520$a.',
  `created_by` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` datetime NOT NULL,
  `physical_other` text COMMENT 'Illustrations (300$b).',
  `physical_extent` text COMMENT 'Pages, volumes, discs, etc. (300$a).',
  `physical_dimensions` text COMMENT 'Size of item in inches, centimeters, or millimeters (300$c).',
  `lccn` varchar(25) DEFAULT NULL COMMENT 'Library of Congress Control Number (MARC 010$a).',
  `control_number` varchar(25) DEFAULT NULL COMMENT 'Control number (MARC Control Field 001).',
  `marc` text COMMENT 'Original MARC record.',
  `url` text COMMENT 'URL from MARC (856$u).',
  `target_audience` text COMMENT 'Recommended reading level (521$a).',
  `total_issues` int(11) DEFAULT NULL COMMENT 'Total number of times this item has been loaned out.',
  `record_type` varchar(1) DEFAULT NULL COMMENT 'Type of record (MARC Leader, Position 06).',
  `dewey_decimal` varchar(7) DEFAULT NULL COMMENT 'Dewey Decimal Classification (082$a).',
  `subjects` text COMMENT 'All subject entries separated by new line characters (600$a, 610$a, 611$a, 630$a, 648$a, 650$a, 651$a).',
  `call_number` varchar(100) DEFAULT NULL COMMENT 'Library of Congress Call Number or local call number, if replaced (050$a + '' '' + 050$b).',
  `responsibility_statement` text COMMENT 'Statement of other responsibility (e.g. illustrations, etc.) (MARC 245$c).',
  PRIMARY KEY (`id`),
  KEY `biblio_created_by_fk_idx` (`created_by`),
  KEY `biblio_updated_by_fk_idx` (`updated_by`),
  KEY `biblio_item_type_fk_idx` (`item_type_id`),
  CONSTRAINT `biblio_created_by_fk` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `biblio_item_type_fk` FOREIGN KEY (`item_type_id`) REFERENCES `item_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `biblio_updated_by_fk` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biblios`
--

LOCK TABLES `biblios` WRITE;
/*!40000 ALTER TABLE `biblios` DISABLE KEYS */;
INSERT INTO `biblios` (`id`, `author`, `title`, `notes`, `serial`, `copyright_date`, `volume_info`, `item_type_id`, `isbn`, `issn`, `ean`, `publication_year`, `publisher_name`, `publication_location`, `series_title`, `series_issn`, `series_volume`, `edition_statement`, `edition_responsibility`, `abstract`, `created_by`, `date_created`, `updated_by`, `date_updated`, `physical_other`, `physical_extent`, `physical_dimensions`, `lccn`, `control_number`, `marc`, `url`, `target_audience`, `total_issues`, `record_type`, `dewey_decimal`, `subjects`, `call_number`, `responsibility_statement`) VALUES (7,'Rowling, J. K.','Harry Potter and the goblet of fire /',NULL,0,'2013-01-01','',2,'9780545582957 (pbk)\n0545582954 (pbk)',NULL,'','[2013], c2000.','Scholastic,','New York :',NULL,NULL,NULL,'','','Fourteen-year-old Harry Potter joins the Weasleys at the Quidditch World Cup, then enters his fourth year at Hogwarts Academy where he is mysteriously entered in an unusual contest that challenges his wizarding skills, friendships and character, amid signs that an old enemy is growing stronger.',1,'2016-10-23 00:25:34',1,'2016-10-23 00:25:34','ill. ;','ix, 734, [16] p. :','21 cm.','  2000131084','18213486',NULL,'','',0,'a','[Fic]','Potter, Harry.\nHogwarts School of Witchcraft and Wizardry (Imaginary organization).\nGranger, Hermione (Fictitious character).\nWeasley, Ron (Fictitious character).\nWizards.\nMagic.\nSchools.\nEngland.\nPotter, Harry.\nHogwarts School of Witchcraft and Wizardry (Imaginary place).\nWizards.\nMagic.\nSchools.\nEngland.','PZ7.R79835 Hal 2013','by J.K. Rowling ; illustrations by Mary GrandPré.'),(8,'McConnell, Steve.','Code complete : a practical handbook of software construction /',NULL,0,'1993-01-01','',2,'1556154844 :',NULL,'','c1993.','Microsoft Press,','Redmond, Wash. :',NULL,NULL,NULL,'','','',1,'2016-10-23 00:44:18',1,'2016-10-23 00:44:18','ill. ;','xviii, 857 p. :','24 cm.','   92041059 ','2033615',NULL,'','',0,'a','005.1','Computer software.','QA76.76.D47 M39 1993','Steve McConnell.'),(9,'Rowling, J. K.','Harry Potter and the prisoner of Azkaban /',NULL,0,'1999-01-01','',2,'0439136350 (hc)\n0439136369 (pb)',NULL,'','1999.','Arthur A. Levine Books,','New York :',NULL,NULL,NULL,'','','During his third year at Hogwarts School for Witchcraft and Wizardry, Harry Potter must confront the devious and dangerous wizard responsible for his parents\' deaths.',1,'2016-10-23 00:45:19',1,'2016-10-23 00:45:19','ill. ;','ix, 435 p. :','24 cm.','   99023982 ','4995027',NULL,'','',0,'a','[Fic]','Potter, Harry.\nGranger, Hermione (Fictitious character).\nWeasley, Ron (Fictitious character).\nHogwarts School of Witchcraft and Wizardry (Imaginary organization).\nWizards.\nMagic.\nSchools.\nEngland.\nWizards.\nMagic.\nSchools.\nEngland.','PZ7.R79835 Ham 1999','by J.K. Rowling.'),(10,'Robert C. Martin','Clean code :a handbook of agile software craftsmanship /',NULL,0,'2009-01-01','',2,'0132350882 (pbk. : alk. paper)',NULL,'','c2009.','Prentice Hall,','Upper Saddle River, NJ :',NULL,NULL,NULL,'','','',1,'2016-10-23 19:51:46',1,'2016-10-25 22:56:19','ill. ;','xxix, 431 p. :','24 cm.','  2008024750','15315370',NULL,'http://www.loc.gov/catdir/toc/ecip0820/2008024750.html','',0,'a','005.1','Agile software development..\nComputer software.','QA76.76.D47 C583 2009','Robert C. Martin ... [et al.]'),(11,'Gamma, Erich.','Design patterns :elements of reusable object-oriented software /',NULL,0,'1995-01-01','',2,'0201633612 (acid-free paper)',NULL,'','c1995.','Addison-Wesley,','Reading, Mass. :',NULL,NULL,NULL,'','','',1,'2016-10-23 20:15:14',1,'2016-10-23 20:15:14','ill. ;','xv, 395 p. :','25 cm.','94034264','1598167',NULL,'','',0,'a','005.1/2','Object-oriented programming (Computer science)\nComputer software\nSoftware patterns.','QA76.64 .D47 1995','Erich Gamma ... [et al.].'),(12,'Rowling, J. K.','Harry Potter and the prisoner of Azkaban /',NULL,0,NULL,'',2,'0439136350 (hc)\n0439136369 (pb)',NULL,'','1999.','Arthur A. Levine Books,','New York :',NULL,NULL,NULL,'','','During his third year at Hogwarts School for Witchcraft and Wizardry, Harry Potter must confront the devious and dangerous wizard responsible for his parents\' deaths.',1,'2016-10-23 22:23:53',1,'2016-10-23 22:23:53','ill. ;','ix, 435 p. :','24 cm.','99023982','4995027',NULL,'','',0,'a','[Fic]','Potter, Harry\nGranger, Hermione (Fictitious character)\nWeasley, Ron (Fictitious character)\nHogwarts School of Witchcraft and Wizardry (Imaginary organization)\nWizards\nMagic\nSchools\nEngland\nWizards\nMagic\nSchools\nEngland','PZ7.R79835 Ham 1999','by J.K. Rowling.'),(13,' Copeland, Lee.','A practitioner\'s guide to software test design /',NULL,0,NULL,'',2,'158053791X',NULL,'','2004.','Artech House,','Boston, Mass. ;',NULL,NULL,NULL,'','','',1,'2016-10-24 22:21:51',1,'2016-10-24 22:21:51','ill. ;','xvii, 294 p. :','24 cm.','2004298352','13545369',NULL,'','',0,'a','005.14','Computer software','QA76.76.T48 C66 2004','Lee Copeland.'),(14,'N/A','Harry Potter and the sorcerer\'s stone.',NULL,0,NULL,'',2,'N/A',NULL,'','2001.','','',NULL,NULL,NULL,'','','',1,'2016-10-24 22:30:51',1,'2016-10-24 22:30:51','sd., col. ;','18 film reels of 18 on 9 :','35 mm.','2002637960','12700175',NULL,'','',0,'g','','','CGD 2281-2289 (viewing print)','');
/*!40000 ALTER TABLE `biblios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branch_borrower_circulation_rules`
--

DROP TABLE IF EXISTS `branch_borrower_circulation_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branch_borrower_circulation_rules` (
  `id` int(11) NOT NULL,
  `branch` int(11) DEFAULT NULL,
  `patron_category` int(11) DEFAULT NULL,
  `max_issues` smallint(6) DEFAULT NULL,
  `max_onsite_issues` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `branch_patron_cat_uk` (`branch`,`patron_category`),
  KEY `patron_cat_branch_borr_circ_rules_idx` (`patron_category`),
  KEY `branches_branch_borr_circ_rule_idx` (`branch`),
  CONSTRAINT `branches_branch_borr_circ_rules` FOREIGN KEY (`branch`) REFERENCES `branches` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `patron_cat_branch_borr_circ_rules` FOREIGN KEY (`patron_category`) REFERENCES `patron_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branch_borrower_circulation_rules`
--

LOCK TABLES `branch_borrower_circulation_rules` WRITE;
/*!40000 ALTER TABLE `branch_borrower_circulation_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch_borrower_circulation_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(200) NOT NULL,
  `address_line1` varchar(200) DEFAULT NULL,
  `address_line2` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `replyto` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `holds_allowed` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` (`id`, `branch_name`, `address_line1`, `address_line2`, `city`, `state`, `zip`, `phone`, `fax`, `email`, `replyto`, `url`, `holds_allowed`) VALUES (1,'Default Branch','174 Sportsman\'s Club Road',NULL,'Leesburg','GA','31763','2293082820',NULL,'branch@default.com',NULL,NULL,1),(2,'Leesburg Library','245 Walnut Ave S',NULL,'Leesburg','GA','31763','2297592369',NULL,'questions@leecountylibrary.org',NULL,NULL,1);
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_users`
--

DROP TABLE IF EXISTS `employee_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_users` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `employee_users_user_fk` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_users`
--

LOCK TABLES `employee_users` WRITE;
/*!40000 ALTER TABLE `employee_users` DISABLE KEYS */;
INSERT INTO `employee_users` (`id`) VALUES (1);
/*!40000 ALTER TABLE `employee_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issues`
--

DROP TABLE IF EXISTS `issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issues` (
  `id` int(11) NOT NULL,
  `patron` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `branch` int(11) NOT NULL COMMENT 'location the item was checked out',
  `date_issued` datetime NOT NULL,
  `date_due` datetime NOT NULL,
  `date_returned` datetime DEFAULT NULL,
  `date_last_renewed` datetime DEFAULT NULL COMMENT 'date the item was last renewed',
  `renewal_count` tinyint(4) DEFAULT NULL COMMENT 'number of times the item has been renewed',
  `auto_renew` tinyint(1) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `onsite_checkout` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `issues_patrons_fk_idx` (`patron`),
  KEY `issues_branch_fk_idx` (`branch`),
  CONSTRAINT `issues_branch_fk` FOREIGN KEY (`branch`) REFERENCES `branches` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `issues_patrons_fk` FOREIGN KEY (`patron`) REFERENCES `patron_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issues`
--

LOCK TABLES `issues` WRITE;
/*!40000 ALTER TABLE `issues` DISABLE KEYS */;
/*!40000 ALTER TABLE `issues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_type_fields`
--

DROP TABLE IF EXISTS `item_type_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_type_fields` (
  `item_type_id` int(11) NOT NULL,
  `biblio_column_name` varchar(100) NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Determines if the field is shown.',
  PRIMARY KEY (`item_type_id`,`biblio_column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_type_fields`
--

LOCK TABLES `item_type_fields` WRITE;
/*!40000 ALTER TABLE `item_type_fields` DISABLE KEYS */;
INSERT INTO `item_type_fields` (`item_type_id`, `biblio_column_name`, `is_visible`) VALUES (1,'abstract',1),(1,'author',1),(1,'call_number',1),(1,'copyright_date',1),(1,'dewey_decimal',1),(1,'ean',0),(1,'edition_responsibility',1),(1,'edition_statement',1),(1,'isbn',1),(1,'issn',0),(1,'lccn',1),(1,'notes',1),(1,'physical_dimensions',1),(1,'physical_extent',1),(1,'physical_other',1),(1,'publication_location',1),(1,'publication_year',1),(1,'publisher_name',1),(1,'record_type',1),(1,'responsibility_statement',1),(1,'serial',0),(1,'series_issn',0),(1,'series_title',0),(1,'series_volume',0),(1,'subjects',1),(1,'target_audience',1),(1,'title',1),(1,'total_issues',1),(1,'url',1),(1,'volume_info',1),(2,'abstract',1),(2,'author',1),(2,'call_number',1),(2,'copyright_date',1),(2,'dewey_decimal',1),(2,'ean',0),(2,'edition_responsibility',1),(2,'edition_statement',1),(2,'isbn',1),(2,'issn',0),(2,'lccn',1),(2,'notes',1),(2,'physical_dimensions',1),(2,'physical_extent',1),(2,'physical_other',1),(2,'publication_location',1),(2,'publication_year',1),(2,'publisher_name',1),(2,'record_type',1),(2,'responsibility_statement',1),(2,'serial',0),(2,'series_issn',0),(2,'series_title',0),(2,'series_volume',0),(2,'subjects',1),(2,'target_audience',1),(2,'title',1),(2,'total_issues',1),(2,'url',1),(2,'volume_info',1);
/*!40000 ALTER TABLE `item_type_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_type_labels`
--

DROP TABLE IF EXISTS `item_type_labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_type_labels` (
  `item_type_id` int(11) NOT NULL,
  `locale` varchar(10) NOT NULL,
  `biblio_column_name` varchar(100) NOT NULL,
  `label_name` varchar(100) NOT NULL,
  `label_desc` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`item_type_id`,`locale`,`biblio_column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='							';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_type_labels`
--

LOCK TABLES `item_type_labels` WRITE;
/*!40000 ALTER TABLE `item_type_labels` DISABLE KEYS */;
INSERT INTO `item_type_labels` (`item_type_id`, `locale`, `biblio_column_name`, `label_name`, `label_desc`) VALUES (1,'en_US','abstract','Abstract','Short description of this item.'),(1,'en_US','author','Authors','Names of those responsible for creating this item.'),(1,'en_US','call_number','Call Number','Library of Congress Call Number.'),(1,'en_US','copyright_date','Copyright Date','Date of the current copyright on this item.'),(1,'en_US','dewey_decimal','Dewey Decimal Class','The Dewey Decimal call number assigned by the Library of Congress (LC), Library and Archives Canada (LAC), or other national agencies.'),(1,'en_US','ean','EAN','International (or European) Article Number.'),(1,'en_US','edition_responsibility','Edition Responsibility','Parellel statement further describing this items edition or statement of responsibility relating to one or more, but not all, editions.'),(1,'en_US','edition_statement','Edition Statement','The words and numbers describing the edition of this item including words such as \"revised\" or \"enlarged.\"'),(1,'en_US','isbn','ISBN','International Standard Book Numbers that apply to this item. May be 10 or 13 digits.'),(1,'en_US','issn','ISSN','International Standard Serial Numbers that apply to this item. Should be 8 digits in length.'),(1,'en_US','lccn','LCCN','Library of Congress Control Number that applies to this item.'),(1,'en_US','notes','Notes','Any librarian/facility notes pertaining to this item.'),(1,'en_US','physical_dimensions','Dimensions','Physical size of the item.'),(1,'en_US','physical_extent','Extent','Description of the length of the media of this item (pages for books, running time for cassettes, etc.)'),(1,'en_US','physical_other','Illustrations','Describes any illustrative matter including color maps, portraits, illustrations, color (or lack of), etc.'),(1,'en_US','publication_location','Place of Publication','Place the item was published or was distributed from.'),(1,'en_US','publication_year','Publication Date','Date the item was published.'),(1,'en_US','publisher_name','Publisher','Name of the publisher or distributor.'),(1,'en_US','record_type','Record Type','Type category this item belongs to (as defined by Position 6 of the MARC record leader).'),(1,'en_US','responsibility_statement','Other Responsibility','Statement of other responsibility (e.g. illustrations).'),(1,'en_US','serial','Is Serial','Indicator defining if this item is a serial publication.'),(1,'en_US','series_issn','Series ISSN','ISSN of a series to which this item belongs.'),(1,'en_US','series_title','Series Title','Title of a series to which this item belongs.'),(1,'en_US','series_volume','Series Volume','Volume number or other sequential designation used in conjunction with a series to which this item belongs.'),(1,'en_US','subjects','Subjects','All subject entries for this item.'),(1,'en_US','target_audience','Target Audience','A note about the target audience for the item. May describe reading grade level, age level, interest grade level, special audience, or motivation/interest level.'),(1,'en_US','title','Title','Title of the item.'),(1,'en_US','total_issues','Total Issues','Total number of times this item has been loaned out.'),(1,'en_US','url','URL','Location of an electronic version of this item or a related electronic resource.'),(1,'en_US','volume_info','Volume Information','Sequential designation and/or date of publication (e.g. edition number, issue number, volume number, etc.).'),(2,'en_US','abstract','Abstract','Short description of this item.'),(2,'en_US','author','Authors','Names of those responsible for creating this item.'),(2,'en_US','call_number','Call Number','Library of Congress Call Number.'),(2,'en_US','copyright_date','Copyright Date','Date of the current copyright on this item.'),(2,'en_US','dewey_decimal','Dewey Decimal Class','The Dewey Decimal call number assigned by the Library of Congress (LC), Library and Archives Canada (LAC), or other national agencies.'),(2,'en_US','ean','EAN','International (or European) Article Number.'),(2,'en_US','edition_responsibility','Edition Responsibility','Parellel statement further describing this items edition or statement of responsibility relating to one or more, but not all, editions.'),(2,'en_US','edition_statement','Edition Statement','The words and numbers describing the edition of this item including words such as \"revised\" or \"enlarged.\"'),(2,'en_US','isbn','ISBN','International Standard Book Numbers that apply to this item. May be 10 or 13 digits.'),(2,'en_US','issn','ISSN','International Standard Serial Numbers that apply to this item. Should be 8 digits in length.'),(2,'en_US','lccn','LCCN','Library of Congress Control Number that applies to this item.'),(2,'en_US','notes','Notes','Any librarian/facility notes pertaining to this item.'),(2,'en_US','physical_dimensions','Dimensions','Physical size of the item.'),(2,'en_US','physical_extent','Extent','Description of the length of the media of this item (pages for books, running time for cassettes, etc.)'),(2,'en_US','physical_other','Illustrations','Describes any illustrative matter including color maps, portraits, illustrations, color (or lack of), etc.'),(2,'en_US','publication_location','Place of Publication','Place the item was published or was distributed from.'),(2,'en_US','publication_year','Publication Date','Date the item was published.'),(2,'en_US','publisher_name','Publisher','Name of the publisher or distributor.'),(2,'en_US','record_type','Record Type','Type category this item belongs to (as defined by Position 6 of the MARC record leader).'),(2,'en_US','responsibility_statement','Other Responsibility','Statement of other responsibility (e.g. illustrations).'),(2,'en_US','serial','Is Serial','Indicator defining if this item is a serial publication.'),(2,'en_US','series_issn','Series ISSN','ISSN of a series to which this item belongs.'),(2,'en_US','series_title','Series Title','Title of a series to which this item belongs.'),(2,'en_US','series_volume','Series Volume','Volume number or other sequential designation used in conjunction with a series to which this item belongs.'),(2,'en_US','subjects','Subjects','All subject entries for this item.'),(2,'en_US','target_audience','Target Audience','A note about the target audience for the item. May describe reading grade level, age level, interest grade level, special audience, or motivation/interest level.'),(2,'en_US','title','Title','Title of the item.'),(2,'en_US','total_issues','Total Issues','Total number of times this item has been loaned out.'),(2,'en_US','url','URL','Location of an electronic version of this item or a related electronic resource.'),(2,'en_US','volume_info','Volume Information','Sequential designation and/or date of publication (e.g. edition number, issue number, volume number, etc.).');
/*!40000 ALTER TABLE `item_type_labels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_types`
--

DROP TABLE IF EXISTS `item_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) DEFAULT NULL,
  `loanable` tinyint(1) NOT NULL DEFAULT '1',
  `icon` blob COMMENT 'Icon used for each item type. Size of icon is limited to 64KB.',
  `checkin_message` varchar(250) DEFAULT NULL,
  `checkin_message_style` varchar(50) DEFAULT NULL,
  `opac_searchable` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `default` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_types`
--

LOCK TABLES `item_types` WRITE;
/*!40000 ALTER TABLE `item_types` DISABLE KEYS */;
INSERT INTO `item_types` (`id`, `description`, `loanable`, `icon`, `checkin_message`, `checkin_message_style`, `opac_searchable`, `active`, `default`) VALUES (1,'DEFAULT',0,NULL,NULL,NULL,1,0,1),(2,'Book',1,NULL,NULL,NULL,1,1,0);
/*!40000 ALTER TABLE `item_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patron_categories`
--

DROP TABLE IF EXISTS `patron_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patron_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) NOT NULL,
  `description` mediumtext COMMENT 'Description of this patron category.',
  `enrollmentperiod` smallint(6) DEFAULT NULL COMMENT 'Number of months before the patron''s enrolled must be renewed; null if enrollmentperioddate is set.',
  `enrollmentperioddate` datetime DEFAULT NULL COMMENT 'Date the enrollment expires without renewal; null if enrollmentperiod is set.',
  `minimumage` smallint(6) DEFAULT NULL COMMENT 'Minimum age a patron must be to be assigned to this category. NULL if no minimum age.',
  `maximumage` smallint(6) DEFAULT NULL COMMENT 'Maximum age a patron can be while being assigned to this category. NULL if no maximum age.',
  `max_issues` smallint(6) DEFAULT NULL COMMENT 'Maximum number of issues a patron is this category is allowed to receive.',
  `max_onsite_issues` smallint(6) DEFAULT NULL COMMENT 'Maximum number of on-site issues (issues made that cannot be taken out of the facility) a patron in this category is allowed to receive.',
  `patron_type` varchar(1) DEFAULT 'A' COMMENT 'Overarching category (e.g. A = Adult; C = Child; P = Professional; O = Organization, S = Staff).',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_name_UNIQUE` (`category_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patron_categories`
--

LOCK TABLES `patron_categories` WRITE;
/*!40000 ALTER TABLE `patron_categories` DISABLE KEYS */;
INSERT INTO `patron_categories` (`id`, `category_name`, `description`, `enrollmentperiod`, `enrollmentperioddate`, `minimumage`, `maximumage`, `max_issues`, `max_onsite_issues`, `patron_type`, `active`) VALUES (1,'Adult','Adult patrons.',NULL,NULL,18,NULL,5,5,'A',1),(2,'Child','Child patrons.',NULL,NULL,8,17,2,2,'C',1);
/*!40000 ALTER TABLE `patron_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patron_users`
--

DROP TABLE IF EXISTS `patron_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patron_users` (
  `id` int(11) NOT NULL,
  `cardnumber` varchar(50) DEFAULT NULL,
  `patron_category_id` int(11) DEFAULT NULL,
  `date_expiry` date DEFAULT NULL,
  `debarred` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Has this person been prohibited from using the library? (1 = PROHIBITED; 0 = ALLOWED)',
  `guarantor_id` int(11) DEFAULT NULL COMMENT 'If patron is not an adult or requires a guardian, this is a foreign key to his/her guardian.',
  `guarantor_relationship` varchar(100) DEFAULT NULL,
  `notes` mediumtext,
  `sex` varchar(1) NOT NULL DEFAULT 'M',
  `sms_phone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cardnumber_UNIQUE` (`cardnumber`),
  KEY `member_users_cat_fk_idx` (`patron_category_id`),
  KEY `member_users_guar_fk_idx` (`guarantor_id`),
  CONSTRAINT `member_users_cat_fk` FOREIGN KEY (`patron_category_id`) REFERENCES `patron_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `member_users_guar_fk` FOREIGN KEY (`guarantor_id`) REFERENCES `patron_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `member_users_users_fk` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patron_users`
--

LOCK TABLES `patron_users` WRITE;
/*!40000 ALTER TABLE `patron_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `patron_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `permission_name` varchar(50) NOT NULL,
  `permission_desc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_name_UNIQUE` (`permission_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `permission_name`, `permission_desc`) VALUES (0,'ROLE_USER','User'),(1,'ROLE_CIRCULATION','Circulation'),(2,'ROLE_PATRON_MAINTENANCE','Patron Maintenance'),(3,'ROLE_ACQUISITION','Acquisition'),(4,'ROLE_LIBRARY_MANAGEMENT','Library Management'),(5,'ROLE_USER_MAINTENANCE','User Maintenance'),(6,'ROLE_ADMIN','Administrator'),(7,'ROLE_CATALOGUE_MAINTENANCE','Catalogue Maintenance');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(20) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_roles_name_uk` (`role_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `role_name`, `description`) VALUES (0,'Librarian','Librarian role.'),(1,'Admin','Administrator.'),(2,'Patron','Patron.');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_permissions`
--

DROP TABLE IF EXISTS `roles_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `erp_perm_fk_idx` (`permission_id`),
  CONSTRAINT `erp_perm_fk` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `erp_role_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_permissions`
--

LOCK TABLES `roles_permissions` WRITE;
/*!40000 ALTER TABLE `roles_permissions` DISABLE KEYS */;
INSERT INTO `roles_permissions` (`role_id`, `permission_id`) VALUES (0,0),(1,0),(0,2),(1,2),(0,4),(1,5),(1,6),(0,7),(1,7);
/*!40000 ALTER TABLE `roles_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sru_sources`
--

DROP TABLE IF EXISTS `sru_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sru_sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization` varchar(50) NOT NULL COMMENT 'Name of organization that provides the SRU interface and hosts the database behind this interface.',
  `database_name` varchar(50) DEFAULT NULL COMMENT 'Name of the database accessed by this SRU interface.',
  `url` varchar(250) NOT NULL COMMENT 'URL to the SRU interface (e.g. http://lx2.loc.gov:210/LCDB).',
  `sru_profile` varchar(50) NOT NULL,
  `sru_indices` text NOT NULL,
  `description` text COMMENT 'Description of the information provided by this database.',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Should users be allowed to select this SRU server when executing queries (1 = YES; 0 = NO).',
  `created_by` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sru_sources_updated_by_fk_idx` (`updated_by`),
  KEY `sru_sources_created_by_fk_idx` (`created_by`),
  CONSTRAINT `sru_sources_created_by_fk` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sru_sources_updated_by_fk` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sru_sources`
--

LOCK TABLES `sru_sources` WRITE;
/*!40000 ALTER TABLE `sru_sources` DISABLE KEYS */;
INSERT INTO `sru_sources` (`id`, `organization`, `database_name`, `url`, `sru_profile`, `sru_indices`, `description`, `active`, `created_by`, `date_created`, `updated_by`, `date_updated`) VALUES (1,'LOC','Online Catalog','http://lx2.loc.gov:210/LCDB','bath.','ISBN;ISSN;LCCN','Library of Congress Online Catalog',1,1,'2016-09-20 20:51:32',1,'2016-09-20 20:51:32'),(2,'GA Pub. Library','Evergreen','http://gapines.org/opac/extras/sru?','eg.','ISBN;ISSN;UPC','Georgia Public Library',1,1,'2016-09-20 22:00:45',1,'2016-09-20 22:00:45');
/*!40000 ALTER TABLE `sru_sources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_settings`
--

DROP TABLE IF EXISTS `system_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_settings` (
  `setting_name` varchar(100) NOT NULL COMMENT 'Key by which the setting can be retrieved.',
  `setting_value` text COMMENT 'Value of the setting. It is recommended that any non-string values be serialized to some form of string representation (XML, JSON, etc.).',
  PRIMARY KEY (`setting_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store global settings for controlling Aether and its interfaces.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_settings`
--

LOCK TABLES `system_settings` WRITE;
/*!40000 ALTER TABLE `system_settings` DISABLE KEYS */;
INSERT INTO `system_settings` (`setting_name`, `setting_value`) VALUES ('CUSTOM_LOGO','true'),('GOOGLE_API_KEY','AIzaSyB0rl88gffC_fQd9yIvRAJ4ndXiv4x1auk'),('STORAGE_DIRECTORY','C:\\Users\\Clint Bush\\Documents\\Aether Project'),('SYSTEM_AVAILABLE','true'),('SYSTEM_TITLE','Aether');
/*!40000 ALTER TABLE `system_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_permissions`
--

DROP TABLE IF EXISTS `user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`permission_id`),
  KEY `permission_id_idx` (`permission_id`),
  CONSTRAINT `permission_id_fk` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_permissions`
--

LOCK TABLES `user_permissions` WRITE;
/*!40000 ALTER TABLE `user_permissions` DISABLE KEYS */;
INSERT INTO `user_permissions` (`user_id`, `permission_id`) VALUES (1,0),(1,2),(1,5),(1,6),(1,7);
/*!40000 ALTER TABLE `user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_preferences`
--

DROP TABLE IF EXISTS `user_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_preferences` (
  `user_id` int(11) NOT NULL,
  `is_nav_sidebar_expanded` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `user_preference_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_preferences`
--

LOCK TABLES `user_preferences` WRITE;
/*!40000 ALTER TABLE `user_preferences` DISABLE KEYS */;
INSERT INTO `user_preferences` (`user_id`, `is_nav_sidebar_expanded`) VALUES (1,'N');
/*!40000 ALTER TABLE `user_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `mi` varchar(1) DEFAULT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `email2` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `phone2` varchar(50) DEFAULT NULL,
  `utype` varchar(10) NOT NULL DEFAULT 'PATRON',
  `password_hash` varchar(60) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'PENDING',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `address_line1` varchar(100) DEFAULT NULL,
  `address_line2` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `dob` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_uname_uk` (`uname`),
  KEY `users_created_by_fk` (`created_by`),
  KEY `users_updated_by_fk` (`updated_by`),
  KEY `users_branch_fk_idx` (`branch_id`),
  KEY `users_role_fk_idx` (`role_id`),
  CONSTRAINT `users_branch_fk` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `users_created_by_fk` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `users_role_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `users_updated_by_fk` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `uname`, `fname`, `mi`, `lname`, `email`, `email2`, `phone`, `phone2`, `utype`, `password_hash`, `status`, `date_created`, `created_by`, `date_updated`, `updated_by`, `address_line1`, `address_line2`, `city`, `state`, `zip`, `country`, `dob`, `branch_id`, `role_id`) VALUES (1,'admin','Admin',NULL,'User','admin@user.com',NULL,'2293082820',NULL,'EMPLOYEE','$2a$10$1eKrq965QhUQXV2/i8eFBOkhquovYiqTAwXQaVmEUq07VemLSKuT2','ACTIVE','2016-08-13 21:33:59',1,'2016-11-11 20:53:04',1,'174 Sportsman\'s Club Road',NULL,'Leesburg','GA','31763','USA','1987-09-11',1,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-15 18:53:07
