# README #

Aether is an Integrated Library System (ILS) and an OPAC (Online Public Access Catalog) for small to medium-sized libraries.

### Features ###

* Simple and easy-to-use!
* Natural language searchable
* Connects to large libraries like the Library of Congress and State libraries for easily importing information about cataloged items by LCCN, ISBN, ISSN, UPC, etc.
* Connects to Amazon to download pictures of cataloged items
* Allows custom labels/descriptions of data fields for custom-defined item types
* Patrons can request items be reserved
* Patron linking for dependent-guardian relationships
* Patron classification to place checkout, category, and other restrictions based on age/organization/affiliation
* Generate reports in Microsoft Word, Microsoft Excel, or Comma-Separated Value (CSV) format
* Completely free and open-source

### System Design ###
* Can be localized for different languages and dialects
* Fast! Aether is built on state-of-the-art Server-side and Client-side MVC frameworks combining Spring MVC and AngularJS to deliver a blazing-fast application.
* Responsive! Aether is built using Bootstrap and provides a natural look and feel across devices including laptops, desktops, tablets, and phones

### How do I get set up? ###

* Install MySQL and run the Database Initialization Script
* Download and extract Red Hat(R) Wildfly + Aether ZIP file
* Ensure your firewall allows incoming/outgoing connections on ports 80 (or 443 if you configure Wildfly for SSL) and 3306
* Start Red Hat(R) Wildlfy
* In the future, I plan to add a Setup wizard to guide users through the initial configuration of the system. Presently, to get started, just log in as admin/PA$$w0rd12!@ and configure the system.

### Contribution guidelines ###

* If you'd like to contribute, please contact me at cbush06@gmail.com.
* I encourage participation and will gladly allow you to complete a section of Aether. My only guidelines are as follows:
    * Follow the architecture I have created for interacting with the database via entity and stateless session beans.
    * Follow the architecture I have created for combining Spring MVC and AngularJS.
    * Follow my examples for integrating localization into the views.
    * Comment only when necessary to describe a overly complex or abstract algorithm.
    * Use meaningful names (even if they are long) for classes, methods, variables, etc.
    * Create a branch off of the Git trunk for your contributions.

### Who do I talk to? ###

* My name is Clinton Bush. I have been developing software for over 15 years and professionally for the last 5 years. I have worked for a university, the Air Force, and the Marine Corps as a software developer. I have been serving in the United States Army Reserve for the last 10 years.
* If you need to contact me, my e-mail address is **cbush06@gmail.com**

### Why build Aether? ###
* I developed a small web interface/OPAC for the ILS called Biblioteq (http://biblioteq.sourceforge.net/) several years ago and have wanted to build a full-featured online ILS/OPAC ever since.
* ILSs and OPACs are challenging systems to build, as the database must be well-architected and the uesr interface must be powerful. Personally, I love a challenge and and particularly love simplifying complex things to provide a rewarding and empowering experience for others.
* All the open-source, free online ILS/OPAC systems are (in my opinion) very complex. I have reviewed several including:
    * **Evergreen** is a superb ILS and OPAC system with more features than I care to recount. For most, however, I believe Evergreen would present both a large investment in setup and configuration time and a tremendous learning curve.
    * **Koha** is another great ILS and OPAC, but again, I believe it is very complex and presents a huge learning curve to users. From a technological perspective, I also don't like the fact that it's developed in PERL. I have done PERL development in the past, but believe its day has come and gone.
    * **OpenBiblio** is a much simpler system, but lacks many of the features users have come to expect in modern ILS and OPAC systems.
    * **NewGenLib** provides a feature-set similar to that of Koha, but is built using JavaSE (in a desktop format). Also like Koha, it presents a challenging learning curve.
* In summary, I aim to provide a system that is simple, yet full-featured. That provides the essentials for managing a library, but is easy enough to use that you don't have to be a librarian to understand it. I want Aether to be a pleasure to use for both the staff and the patrons of libraries.