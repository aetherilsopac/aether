//@formatter:off
/*******************************************************************************
 * ####################### # FILE DESCRIPTOR # #######################
 * Application: AetherWEB Directory: content/js/interfaces File:
 * AbstractDataGateway.js
 * 
 * ####################### # MIT LICENSE # ####################### Copyright (c)
 * 2016, Clinton Bush.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * ####################### # Purpose # ####################### Provides a common
 * interface for building Data Table Gateways in JavaScript. This abstracts away
 * nearly all AJAX related logic and CSRF concerns imposed by the Spring
 * Security Framework.
 * 
 * ####################### # Revision # ####################### Jun 8, 2016,
 * Clinton Bush, 1.0.0, New file.
 * 
 * **************************************************************************************************************************************************************
 */
// @formatter:on
define(
        [ "jquery", "underscore" ],
        function() {
            var AbstractDataGateway = function( path ) {
                var self = this;
                self.path = AppRoot + path;
            };
            
            AbstractDataGateway.prototype.httpGet = function( action ) {
                var self = this;
                var deferred = $.Deferred();
                
                $.ajax( {
                    method: "GET",
                    url: self.path + "/" + action
                } ).done( function( data ) {
                    var jsonResponseModel = data;
                    deferred.resolve( jsonResponseModel );
                } ).fail( function() {
                    deferred.reject( Utility.defaultJsonError );
                } );
                
                return deferred.promise();
            };
            
            AbstractDataGateway.prototype.httpPost = function( action, data, headers ) {
                var self = this;
                var deferred = $.Deferred();
                var csrfHeaderName = $( "meta[name='_csrf_header']" ).attr(
                        "content" );
                var csrfToken = $( "meta[name='_csrf']" ).attr( "content" );
                
                if ( _.isNull( headers ) || _.isUndefined( headers ) )
                    headers = {};
                
                if ( !( _.isNull( csrfHeaderName ) || _.isUndefined( csrfHeaderName ) ) ) {
                    headers[csrfHeaderName] = csrfToken;
                }
                
                if ( !( _.has( headers, "Accept" ) || _.has( headers,
                        "Content-Type" ) ) ) {
                    headers["Accept"] = "application/json";
                    headers["Content-Type"] = "application/json";
                }
                
                $.ajax( {
                    method: "POST",
                    url: self.path + "/" + action,
                    dataType: "json",
                    data: !( _.isNull(data) || _.isUndefined(data) ) ? JSON.stringify( data ) : null,
                    headers: headers
                } ).done( function( data ) {
                    var jsonResponseModel = data;
                    deferred.resolve( jsonResponseModel );
                } ).fail( function() {
                    deferred.reject( Utility.defaultJsonError );
                } );
                
                return deferred.promise();
            };
            
            /**
             * Returns a promise, posts the JSON data and files to the specified action, and calls the progressCallback
             * passing it a percentage complete (0 - 100).
             * 
             * The HTTP Post parts are named as followed:
             * - data: The JSON data uploaded.
             * - file0...fileN: Files uploaded in the POST.
             * 
             * @param action Partial path to the web service that will receive this upload. "action" will be appended to the
             *               path specified when this instance of AbstractDataGateway was created.
             * @param data JSON data to be POSTed to the server.
             * @param files Array of Web API File objects to be POSTed to the server.
             * @param headers Associative array of header values to set for the HTTP POST.
             * @param progressCallback Method that accepts a integer indicating the progress of the upload as a percentage (0 - 100).
             * @returns Promise object with .done() and .fail() methods that accept callback methods as their arguments.
             */
            AbstractDataGateway.prototype.httpPostWithFiles = function ( action, data, files, headers, progressCallback ) {
                var self = this;
                var deferred = $.Deferred();
                var csrfHeaderName = $( "meta[name='_csrf_header']" ).attr( "content" );
                var csrfToken = $( "meta[name='_csrf']" ).attr( "content" );
                var payload = new FormData();
                
                if ( _.isUndefined( headers ) || _.isNull( headers ) )
                    headers = {};
                
                if ( !( _.isNull( csrfHeaderName ) || _.isUndefined( csrfHeaderName ) ) ) {
                    headers[csrfHeaderName] = csrfToken;
                }
                
                // Build FormData payload
                payload.append( "data", JSON.stringify( data ) );
                if( _.isArray( files ) ) {
                    for(var i in files) {
                        if( files[i] instanceof File ) payload.append( "file" + i, files[i] );
                    }
                }
                else if( !( _.isUndefined( files ) || _.isNull( files ) ) && files instanceof File ) {
                    payload.append( "file0", files );
                }
                
                $.ajax( {
                    method: "POST",
                    url: self.path + "/" + action,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    data: payload,
                    headers: headers,
                    xhr: function()
                    {
                      var xhr = new window.XMLHttpRequest();
                      
                      //Upload progress
                      if( !(_.isUndefined( progressCallback ) ) ) {
                          // Set the upload progress event listener for the XMLHttpRequest object
                          xhr.upload.addEventListener("progress", function( evt ) {
                              // If we know the total size of the transfer, invoke
                              // the callback and pass it the percentage complete
                              if( evt.lengthComputable ) {
                                  progressCallback( ( evt.loaded / evt.total ) * 100 );
                              }
                          }, false);
                      }
                      
                      return xhr;
                    }
                } ).done( function( data ) {
                    var jsonResponseModel = data;
                    deferred.resolve( jsonResponseModel );
                } ).fail( function() {
                    deferred.reject( Utility.defaultJsonError );
                } );
                
                return deferred.promise();
            };
            
            AbstractDataGateway.prototype.httpPut = function( action, data, headers ) {
                var self = this;
                var deferred = $.Deferred();
                var csrfHeaderName = $( "meta[name='_csrf_header']" ).attr(
                        "content" );
                var csrfToken = $( "meta[name='_csrf']" ).attr( "content" );
                
                if ( _.isNull( headers ) || _.isUndefined( headers ) )
                    headers = {};
                
                if ( !( _.isNull( csrfHeaderName ) || _.isUndefined( csrfHeaderName ) ) ) {
                    headers[csrfHeaderName] = csrfToken;
                }
                
                if ( !( _.has( headers, "Accept" ) || _.has( headers,
                        "Content-Type" ) ) ) {
                    headers["Accept"] = "application/json";
                    headers["Content-Type"] = "application/json";
                }
                
                $.ajax( {
                    method: "PUT",
                    url: self.path + "/" + action,
                    dataType: "json",
                    data: !( _.isNull(data) || _.isUndefined(data) ) ? JSON.stringify( data ) : null,
                    headers: headers
                } ).done( function( data ) {
                    var jsonResponseModel = data;
                    deferred.resolve( jsonResponseModel );
                } ).fail( function() {
                    deferred.reject( Utility.defaultJsonError );
                } );
                
                return deferred.promise();
            };
            
            AbstractDataGateway.prototype.httpDelete = function( action, headers ) {
                var self = this;
                var deferred = $.Deferred();
                var csrfHeaderName = $( "meta[name='_csrf_header']" ).attr( "content" );
                var csrfToken = $( "meta[name='_csrf']" ).attr( "content" );
                
                if ( _.isNull( headers ) || _.isUndefined( headers ) )
                    headers = {};
                
                if ( !( _.isNull( csrfHeaderName ) || _.isUndefined( csrfHeaderName ) ) ) {
                    headers[csrfHeaderName] = csrfToken;
                }
                
                if ( !( _.has( headers, "Accept" ) || _.has( headers,
                        "Content-Type" ) ) ) {
                    headers["Accept"] = "application/json";
                }
                
                $.ajax( {
                    method: "DELETE",
                    url: self.path + "/" + action,
                    headers: headers
                } ).done( function( data ) {
                    var jsonResponseModel = data;
                    deferred.resolve( jsonResponseModel );
                } ).fail( function() {
                    deferred.reject( Utility.defaultJsonError );
                } );
                
                return deferred.promise();
            };
            
            return AbstractDataGateway;
        } );