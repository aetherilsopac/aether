//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/controllers/Admin/ShelvingMaintenance
 * File: ShelvingMaintenanceEditController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and validation for Shelving Maintenance Edit screen.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * May 29, 2017, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/ShelvingGateway", "underscore" ], function( AetherApp, ShelvingGateway ) {
    
    AetherApp.controller( "ShelvingMaintenanceEdit", [
         '$scope', '$window',
         function( $scope, $window ) {
             $scope.AppRoot = AppRoot;
             var model = $scope.model = {};
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     shelvingLocation: Utility.getPageSessionItem( "ShelvingMaintenanceEdit", "shelvingLocation" )
                 };
             };
             
             var save = $scope.save = function() {
                 if(validateForm()) {
                     // Save the Location
                     ShelvingGateway.update( model.shelvingLocation )
                         .done( function ( payload ) {
                             if( payload.status == Utility.jsonResponse.SUCCESS ) {
                                 model.shelvingLocation = payload.data;
                                 $window.location.assign( AppRoot + "Admin/ShelvingMaintenance" );
                             }
                             else {
                                 Utility.showAlert( "messages-container", "warning", true, 0, payload.message );
                             }
                         })
                         .fail( function ( message ) {
                             alert( "Save failed!" );
                             $window.location.reload( true );
                         });
                 }
             };
             
             var validateForm = function() {
                 return Utility.validateForm( 'editShelfForm', 'messages-container' );
             };
             
             var cancel = $scope.cancel = function() {
                 $window.location.assign( AppRoot + "Admin/ShelvingMaintenance" );
             };
             
             init();
         } 
     ] );
} );