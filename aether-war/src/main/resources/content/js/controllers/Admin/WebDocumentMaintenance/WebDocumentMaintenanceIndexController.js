//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/controllers/Admin/WebDocumentMaintenance
 * File: WebDocumentMaintenanceIndexController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides functionality to Web Document Maintenance Index page.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 26, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/WebDocumentGateway", "underscore" ], function( AetherApp, WebDocumentGateway ) {
    AetherApp.controller( "WebDocumentMaintenanceIndex", [
         '$scope', 'NgTableParams', 'ngTableEventsChannel', '$window',
         function( $scope, NgTableParams, ngTableEventsChannel, $window ) {
             $scope.AppRoot = AppRoot;
             var model = $scope.model = {};
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     firstRecord: 0,
                     lastRecord: 0,
                     totalRecords: 0,
                     documents: Utility.getPageSessionItem("WebDocumentMaintenanceIndex", "documents"),
                     documentTypes: Utility.getPageSessionItem("WebDocumentMaintenanceIndex", "documentTypes"),
                     tableParams: new NgTableParams({
                         count: 10,
                         sorting: {
                             title: 'asc'
                         }
                     },
                     {
                    	 counts: [5, 10, 15, 20],
                         total: documents.length,
                         getData: function( $defer, params ) {
                             $defer.resolve(model.documents.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                             params.total(model.documents.length);
                         }
                     })
                 };
                 
                 model.totalRecords = model.documents.length;
                 
                 ngTableEventsChannel.onPagesChanged(
                     function(tableParams) {
                         model.firstRecord = ((tableParams.page() - 1) * tableParams.count()) + 1;
                         model.lastRecord = tableParams.page() * tableParams.count();
                         model.lastRecord = ( model.totalRecords < model.lastRecord ) ? model.totalRecords : model.lastRecord;
                     }
                 );
                 
                 model.tableParams.reload();
             };
             
             var editDocument = $scope.editDocument = function( document ) {
                 $window.location = AppRoot + "Admin/WebDocumentMaintenance/Edit/" + document.id;
             };
             
             var convertDocumentType = $scope.convertDocumentType = function( documentType ) {
                 var value = model.documentTypes[documentType];
                 if(!_.isUndefined(value)) return value;
                 return "";
             };
             
             init();
         } 
     ] );
} );