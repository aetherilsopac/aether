//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/controllers/Admin/EmployeeMaintenance
 * File: UserMaintenanceAddController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and validation for User Maintenance Add screen.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 17, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/EmployeeGateway", "underscore", "js/directives/angular-checkboxlist", "js/directives/angular-password" ], function( AetherApp, EmployeeGateway ) {
    
    // Add the Checkbox List directive
    AetherApp.requires.push( 'ngCheckboxList', 'ngPassword' );
    
    AetherApp.controller( "EmployeeMaintenanceAdd", [
         '$scope', 'NgTableParams', '$filter', '$window',
         function( $scope, NgTableParams, $filter, $window ) {
             $scope.AppRoot = AppRoot;
             var model = $scope.model = {};
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     user: Utility.getPageSessionItem( "UserMaintenanceAdd", "user" ),
                     branches: Utility.getPageSessionItem( "UserMaintenanceAdd", "branches" ),
                     roles: Utility.getPageSessionItem( "UserMaintenanceAdd", "roles" ),
                     statuses: Utility.getPageSessionItem( "UserMaintenanceAdd", "statuses" ),
                     dobIsOpen: false,
                     newPassword: ''
                 };
                 
                 if(!(_.isNull(model.user)) && !(_.isNull(model.user.dob))) {
                     model.user.dob = new Date(model.user.dob);
                 }
                 
                 // Ensure user's permissions is defined
                 model.user.permissions = (model.user.permissions != null) ? model.user.permissions : [];
                 
                 // Mark permissions assigned to the user as selected 
                 // within the list of permissions of the assigned role
                 _.each(model.user.role.permissions, function(rolePermission, index, list) {
                     var indexInAssignedPermissions = _.findIndex(model.user.permissions, function(permission) {
                         return permission.id == rolePermission.id;
                     });
                     
                     rolePermission.selected = (indexInAssignedPermissions > -1) ? true : false;
                 });
             };
             
             var save = $scope.save = function() {
                 if(validateForm()) {
                     // Update the user's list of permissions so they match the selected permissions
                     model.user.permissions.splice( 0, model.user.permissions.length );
                     _.each( model.user.role.permissions, function( rolePermission, index, list ) {
                         if( rolePermission.selected ) model.user.permissions.push( rolePermission );
                     });
                     
                     // Save the User
                     EmployeeGateway.addEmployee( model.user )
                         .done( function ( payload ) {
                             model.user = payload.data;
                             $window.location.assign( AppRoot + "Admin/EmployeeMaintenance" );
                         })
                         .fail( function ( message ) {
                             alert( "Save failed!" );
                             $window.location.reload( true );
                         });
                 }
             };
             
             var validateForm = function() {
                 return Utility.validateForm( 'addUserForm', 'messages-container' );
             };
             
             var cancel = $scope.cancel = function() {
                 $window.location.href = AppRoot + "Admin/EmployeeMaintenance";
             };
             
             init();
         } 
     ] );
} );