//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/controllers/Admin/EmployeeMaintenance
 * File: UserMaintenanceIndexController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 17, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/EmployeeGateway" ], function( AetherApp, EmployeeGateway ) {
    AetherApp.controller( "EmployeeMaintenanceIndex", [
         '$scope', 'NgTableParams', 'ngTableEventsChannel', '$window',
         function( $scope, NgTableParams, ngTableEventsChannel, $window ) {
             $scope.AppRoot = AppRoot;
             var model = $scope.model = {};
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     users: Utility.getPageSessionItem("UserMaintenanceIndex", "users"),
                     firstRecord: 0,
                     lastRecord: 0,
                     totalRecords: 0,
                     tableParams: new NgTableParams({
                         count: 10,
                         sorting: {
                             lastName: 'asc'
                         }
                     },
                     {
                    	 counts: [5, 10, 15, 20],
                         total: users.length,
                         getData: function( $defer, params ) {
                             $defer.resolve(model.users.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                             params.total(model.users.length);
                         }
                     })
                 };
                 
                 model.totalRecords = model.users.length;
                 
                 ngTableEventsChannel.onPagesChanged(
                     function(tableParams) {
                         model.firstRecord = ((tableParams.page() - 1) * tableParams.count()) + 1;
                         model.lastRecord = tableParams.page() * tableParams.count();
                         model.lastRecord = ( model.totalRecords < model.lastRecord ) ? model.totalRecords : model.lastRecord;
                     }
                 );
                 
                 model.tableParams.reload();
             };
             
             var editEmployee = $scope.editEmployee = function(employee) {
                 $window.location = AppRoot + "Admin/EmployeeMaintenance/Edit/" + employee.id;
             };
             
             init();
         } 
     ] );
} );