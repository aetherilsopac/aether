//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/controllers/Admin/ResourceMaintenance
 * File: ResourceMaintenanceIndexController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides functionality to Resource Maintenance Index page.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jan 10, 2017, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/ResourceGateway", "underscore", "js/directives/angular-checkboxlist", "js/fragment-controllers/ConfirmationDialogController" ],
function( AetherApp, ResourceGateway ) {
    
    // Add the Checkbox List directive
    AetherApp.requires.push( 'ngCheckboxList' ); 
    
    AetherApp.controller( "ResourceMaintenanceIndex", [
         '$scope', '$uibModal', 'NgTableParams', 'ngTableEventsChannel', '$window', '$sce', 'ConfirmationDialogConfig',
         function( $scope, $uibModal, NgTableParams, ngTableEventsChannel, $window, $sce, ConfirmationDialogConfig ) {
             $scope.AppRoot = AppRoot;
             var model = $scope.model = {};
             
             var init = function() {
                 model = $scope.model = {
                     resources: Utility.getPageSessionItem("ResourceMaintenanceIndex", "resources"),
                     firstRecord: 0,
                     lastRecord: 0,
                     totalRecords: 0,
                     selectAll: false,
                     tableParams: new NgTableParams({
                         count: 10,
                         sorting: {
                             title: 'asc'
                         }
                     },
                     {
                         counts: [5, 10, 15, 20],
                         total: resources.length,
                         getData: function( $defer, params ) {
                             $defer.resolve(model.resources.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                             params.total(model.resources.length);
                         }
                     })
                 };
                 
                 model.totalRecords = model.resources.length;
                 
                 ngTableEventsChannel.onPagesChanged(
                     function(tableParams) {
                         model.firstRecord = ((tableParams.page() - 1) * tableParams.count()) + 1;
                         model.lastRecord = tableParams.page() * tableParams.count();
                         model.lastRecord = ( model.totalRecords < model.lastRecord ) ? model.totalRecords : model.lastRecord;
                     }
                 );
                 
                 model.tableParams.reload();
             };
             
             var reloadResources = $scope.reloadResources = function() {
            	 ResourceGateway.listResources()
            	 	.done( function( payload ) {
            	 		if( payload.status == Utility.jsonResponse.SUCCESS ) {
            	 			model.resources = payload.data;
            	 			model.tableParams.reload();
            	 		}
            	 		else {
            	 			Utility.showAlert( "messages-container", "warning", true, 0, payload.message );
            	 		}
            	 	})
		            .fail( function( message ) {
		                alert( message );
		                $window.location.reload( true );
		            });
             };
             
             var onSelectAll = $scope.onSelectAll = function() {
                 for(var nextResourceIdx in model.resources) {
                     model.resources[nextResourceIdx].selected = true;
                 }
             };
             
             var onDeselectAll = $scope.onDeselectAll = function() {
                 for(var nextResourceIdx in model.resources) {
                     model.resources[nextResourceIdx].selected = false;
                 }
             };
             
             var confirmDelete = $scope.confirmDelete = function() {
            	 // If not at least 1 item is selected, quit...
            	 var selectedCount = model.resources.reduce( function(total, item){
            		 if( typeof(item.selected) !== 'undefined' && item.selected ){
            			 return total + 1;
            		 }
            		 return total;
            	 }, 0);
            	 if( selectedCount < 1) return;
            	 
                 ConfirmationDialogConfig.yesButtonLabel = Utility.getPageSessionItem( "ResourceMaintenanceIndex", "deleteButtonLabel" );
                 ConfirmationDialogConfig.noButtonLabel = Utility.getPageSessionItem( "ResourceMaintenanceIndex", "cancelButtonLabel" );
                 ConfirmationDialogConfig.modalTitle = Utility.getPageSessionItem( "ResourceMaintenanceIndex", "confirmModalTitle" );
                 
                 // Compile the list of selected items to show the user...
                 var modalMessage = Utility.getPageSessionItem( "ResourceMaintenanceIndex", "confirmModalMessage" );
                 modalMessage += "<ul>";
                 model.resources.forEach( function(item){
                	 if( typeof(item.selected) !== 'undefined' && item.selected ) {
                		 modalMessage += "<li>" + item.name + "</li>";
                	 }
                 })
                 modalMessage += "</ul>";
                 
                 ConfirmationDialogConfig.modalMessage = $sce.trustAsHtml( modalMessage );
                 
                 var confirmDeleteModal = $uibModal.open({
                     templateUrl: AppRoot + "js-fragments/confirmation-dialog",
                     controller: "ConfirmationDialogController"
                 });
                 
                 confirmDeleteModal.result.then(function() {
                	 var resourceNames = model.resources.filter( function(item){
	                		 return (typeof(item.selected) !== 'undefined') ? item.selected : false;
	                	 }).map( function(item){
	                		 return item.name;
	                	 });
                	 
                     ResourceGateway.deleteResources( resourceNames )
                         .done( function( payload ) {
                             if( payload.status == Utility.jsonResponse.SUCCESS ) {
                            	 Utility.showAlert( "messages-container", "success", true, 0, payload.message );
                                 reloadResources();
                             }
                             else {
                                 Utility.showAlert( "messages-container", "warning", true, 0, payload.message );
                             }
                         })
                         .fail( function( message ) {
                             alert( message );
                             $window.location.reload( true );
                         });
                 },
                 function() {
                     // User clicked 'Cancel', so do nothing...
                 });
             };
             
             init();
         } 
     ] );
} );