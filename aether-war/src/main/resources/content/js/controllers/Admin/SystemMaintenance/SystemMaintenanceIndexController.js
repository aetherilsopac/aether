//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/controllers/Admin
 * File: SystemMainteannceIndexController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic, validation, etc. for the System Maintenance page.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Oct 27, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/SystemSettingsGateway", "js/factories/angular-stomp", "js/fragment-controllers/ConfirmationDialogController" ], 
function( AetherApp, SystemSettingsGateway ) {
    
    // Add the STOMP factory module
    AetherApp.requires.push( 'ngStomp' );
    
    AetherApp.controller( "SystemMaintenanceIndex", [
         '$scope', 'NgTableParams', '$filter', '$window', '$uibModal', 'ngStompFactory', 'ConfirmationDialogConfig',
         function( $scope, NgTableParams, $filter, $window, $uibModal, AngularStomp, ConfirmationDialogConfig ) {
             $scope.AppRoot = AppRoot;
             var stompConfig = null;
             var stomp = null;
             var model = $scope.model = {};
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     systemSettings: Utility.getPageSessionItem("SystemMaintenanceIndex", "systemSettings"),
                     isReindexing: Utility.getPageSessionItem("SystemMaintenanceIndex", "isReindexing"),
                     reindexProgress: Utility.getPageSessionItem("SystemMaintenanceIndex", "reindexProgress"),
                     customLogo: null
                 };
                 
                 // Connect to STOMP endpoint and listen for reindexing status
                 stompConfig = {
                     connectedCallback: function() {
                         stomp.subscribe( '/admin/reindexing/progress', function( messagePacket ) {
                             $scope.$apply( function() {
                                 model.reindexProgress = parseInt( messagePacket.body, 10 );
                             });
                         });
                     },
                     errorCallback: function( errorPacket ) {
                         alert( errorPacket.headers.message );
                     },
                     disconnectCallback: function() {
                         // Do nothing when STOMP is voluntarily disconnected
                     }
                 };
                 
                 stomp = new AngularStomp( StompEndpoint, stompConfig );
                 
                 // Display a preview of the image
                 $( function() {
                     $("#flCustomLogo").change( function() {
                         var self = this;
                         var reader = new FileReader();
                         
                         if(self.files.length > 0) {
                             reader.onload = function(e) {
                                 $("#customLogo").css("background", "none");
                                 $("#customLogo > img").attr("src", e.target.result);
                                 $("#customLogo > img").css("display", "inline");
                             };
                             
                             reader.readAsDataURL( self.files[0] );
                             model.customLogo = self.files[0];
                         }
                     });
                 });
                 
                 //model.tableParams.reload();
             };
             
             var destroy = function() {
                 if( stomp.isConnected() ) {
                     stomp.unsubscribe( '/admin/reindexing/progress' );
                     stomp.disconnect();
                 }
             };
             
             var save = $scope.save = function() {
                 // Do a multipart upload if a logo is provided OTHERWISE just send JSON
                 if( model.systemSettings.useCustomLogo && model.customLogo != null ) {
                     SystemSettingsGateway.save( model.systemSettings, model.customLogo )
                         .done( function( payload ) {
                             model.systemSettings = payload.data;
                             $window.location.reload( true );
                         })
                         .fail( function( message ) {
                             alert( message );
                             $window.location.reload( true );
                         });
                 }
                 else {
                     SystemSettingsGateway.save( model.systemSettings )
                         .done( function( payload ) {
                             model.systemSettings = payload.data;
                             $window.location.reload( true );
                         })
                         .fail( function( message ) {
                             alert( message );
                             $window.location.reload( true );
                         });
                 }
             };
             
             var confirmReindex = $scope.confirmReindex = function() {
                 ConfirmationDialogConfig.yesButtonLabel = Utility.getPageSessionItem( "SystemMaintenanceIndex", "yesButtonLabel" );
                 ConfirmationDialogConfig.noButtonLabel = Utility.getPageSessionItem( "SystemMaintenanceIndex", "noButtonLabel" );
                 ConfirmationDialogConfig.modalTitle = Utility.getPageSessionItem( "SystemMaintenanceIndex", "confirmModalTitle" );
                 ConfirmationDialogConfig.modalMessage = Utility.getPageSessionItem( "SystemMaintenanceIndex", "confirmModalMessage" );
                 
                 var confirmReindexModal = $uibModal.open({
                     templateUrl: AppRoot + "js-fragments/confirmation-dialog",
                     controller: "ConfirmationDialogController"
                 });
                 
                 confirmReindexModal.result.then(function() {
                     reindex();
                 },
                 function() {
                     // User clicked 'Cancel', so do nothing...
                 });
             };
             
             var reindex = function() {
                 SystemSettingsGateway.reindex()
                     .done( function(payload) {
                         $window.location.reload( true );
                     })
                     .fail( function(message) {
                         alert(message);
                         $window.location.reload( true );
                     });
             };
             
             init();
         }
     ] );
} );