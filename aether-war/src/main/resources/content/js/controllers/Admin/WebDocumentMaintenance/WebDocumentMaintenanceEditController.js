//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/controllers/Admin/WebDocumentMaintenance
 * File: WebDocumentMaintenanceEditController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and validation for Web Document Maintenance Edit screen.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 28, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/ResourceGateway", "js/datagateways/WebDocumentGateway", "js/fragment-controllers/ImageSelectorController", "js/fragment-controllers/WebDocumentSelectorController", "underscore", "angular-tinymce", "js/fragment-controllers/ConfirmationDialogController" ], 
function( AetherApp, ResourceGateway, WebDocumentGateway ) {
    
    // Add the TinyMCE editor directive
    AetherApp.requires.push( 'ui.tinymce' );
    
    AetherApp.controller( "WebDocumentMaintenanceEdit", [
         '$scope', '$window', '$uibModal', '$rootScope', '$controller', '$location', 'ConfirmationDialogConfig',
         function( $scope, $window, $uibModal, $rootScope, $controller, $location, ConfirmationDialogConfig ) {
             $scope.AppRoot = AppRoot;
             var model = $scope.model = {};
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     webDocument: Utility.getPageSessionItem( "WebDocumentMaintenanceEdit", "webDocument" ),
                     webDocumentTypes: Utility.getPageSessionItem( "WebDocumentMaintenanceEdit", "webDocumentTypes" ),
                     tinymceOptions: {
                         plugins: 'link code image fullscreen',
                         toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | image link code | fullscreen',
                         relative_urls : false,
                         remove_script_host : true,
                         convert_urls : true,
                         images_upload_url: AppRoot + "Data/Resources",
                         file_picker_types: 'image file',
                         file_picker_callback: function( callback, currentValue, meta ) {
                             if( meta.filetype == 'image' ) {
                                 // Get refernce to BLOB cache in case the user uploads a new image
                                 var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                                 
                                 $("#mce-modal-block").css("z-index", "10002");
                                 $(".mce-window.mce-floatpanel").css("z-index", "10003");
                                 
                                 var imageSelectorModal = $uibModal.open({
                                     templateUrl: AppRoot + "js-fragments/image-selector",
                                     scope: $rootScope,
                                     controller: "ImageSelectorController",
                                     size: "lg",
                                     windowClass: "image-selector-modal",
                                     resolve: {
                                         currentValue: function() { return currentValue; },
                                         blobCache: function() { return blobCache; }
                                     } 
                                 });
                                 
                                 imageSelectorModal.result.then(
                                     function(selectedImage) {
                                         callback( selectedImage, { } );
                                     },
                                     function() {
                                     }
                                 );
                             }
                             
                             if( meta.filetype == 'file' ) {
                                 $("#mce-modal-block").css("z-index", "500");
                                 $(".mce-window.mce-floatpanel").css("z-index", "501");
                                 
                                 var webDocumentSelectorModal = $uibModal.open({
                                     templateUrl: AppRoot + "js-fragments/web-document-selector",
                                     scope: $rootScope,
                                     controller: "WebDocumentSelectorController",
                                     size: "lg",
                                     resolve: {
                                         currentValue: function() { return currentValue; },
                                         documentTypes: function() { return self.webDocumentTypes; }
                                     } 
                                 });
                                 
                                 webDocumentSelectorModal.result.then(
                                     function( selection ) {
                                         callback( selection.url, { 'text': selection.document.title } );
                                     },
                                     function() {
                                     }
                                 );
                             }
                         },
                         images_upload_handler: function (blobInfo, success, failure) {
                             ResourceGateway.addResource( new File( [ blobInfo.blob() ], blobInfo.filename() ) )
                                 .done( function( payload ) {
                                     success( payload.data );
                                 })
                                 .fail( function( message ) {
                                     failure( message );
                                 })
                         },
                         resize: false,
                         statusbar: false,
                         height: '100%'
                     }
                 };
             };
             
             var save = $scope.save = function() {
                 if(validateForm()) {
                     // Save the User
                     WebDocumentGateway.saveDocument( model.webDocument )
                         .done( function ( payload ) {
                             if( payload.status == Utility.jsonResponse.SUCCESS ) {
                                 model.webDocument = payload.data;
                                 
                                 // If a return address was provided, go there. This is normally
                                 // used when the user first visits a page and then clicks the "Edit"
                                 // button on that page. This feature allows them to return to the page
                                 // when done with editing.
                                 var queryString = Utility.getUrlQueryParams();
                                 if(_.has(queryString, 'return')) {
                                     $window.location.assign( queryString['return'] );
                                 }
                                 else {
                                     $window.location.assign( AppRoot + "Admin/WebDocumentMaintenance" );
                                 }
                             }
                             else {
                                 Utility.showAlert( "messages-container", "warning", true, 0, payload.message );
                             }
                         })
                         .fail( function ( message ) {
                             alert( message );
                             $window.location.reload( true );
                         });
                 }
             };
             
             var validateForm = function() {
                 return Utility.validateForm( 'editWebDocumentForm', 'messages-container' );
             };
             
             var cancel = $scope.cancel = function() {
                 // If a return address was provided, go there. This is normally
                 // used when the user first visits a page and then clicks the "Edit"
                 // button on that page. This feature allows them to return to the page
                 // when done with editing.
                 var queryString = Utility.getUrlQueryParams();
                 if(_.has(queryString, 'return')) {
                     $window.location.assign( queryString['return'] );
                 }
                 else {
                     $window.location.assign( AppRoot + "Admin/WebDocumentMaintenance" );
                 }
             };
             
             var confirmDelete = $scope.confirmDelete = function() {
                 ConfirmationDialogConfig.yesButtonLabel = Utility.getPageSessionItem( "WebDocumentMaintenanceEdit", "deleteButtonLabel" );
                 ConfirmationDialogConfig.noButtonLabel = Utility.getPageSessionItem( "WebDocumentMaintenanceEdit", "cancelButtonLabel" );
                 ConfirmationDialogConfig.modalTitle = Utility.getPageSessionItem( "WebDocumentMaintenanceEdit", "confirmModalTitle" );
                 ConfirmationDialogConfig.modalMessage = Utility.getPageSessionItem( "WebDocumentMaintenanceEdit", "confirmModalMessage" );
                 
                 var confirmDeleteModal = $uibModal.open({
                     templateUrl: AppRoot + "js-fragments/confirmation-dialog",
                     controller: "ConfirmationDialogController"
                 });
                 
                 confirmDeleteModal.result.then(function() {
                     WebDocumentGateway.deleteDocument( model.webDocument )
                         .done( function( payload ) {
                             if( payload.status == Utility.jsonResponse.SUCCESS ) {
                                 $window.location.assign( AppRoot + "Admin/WebDocumentMaintenance" );
                             }
                             else {
                                 Utility.showAlert( "messages-container", "warning", true, 0, payload.message );
                             }
                         })
                         .fail( function( message ) {
                             alert( message );
                             $window.location.reload( true );
                         });
                 },
                 function() {
                     // User clicked 'Cancel', so do nothing...
                 });
             };
             
             init();
         }
     ] );
} );