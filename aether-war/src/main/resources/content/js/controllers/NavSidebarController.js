//@formatter:off
/*******************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB 
 * Directory: content/js/controllers 
 * File: NavSidebarController.js
 * 
 * ####################### 
 * #     MIT LICENSE     # 
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * ####################### 
 * #       Purpose       # 
 * ####################### 
 * Handles updating the user's preferences for the Navigation Sidebar.
 * 
 * ####################### 
 * #       Revision      # 
 * ####################### 
 * Jun 11, 2016, Clinton Bush, 1.0.0, New file.
 * 
 * **************************************************************************************************************************************************************
 */
// @formatter:on
define( [ "js/AetherApp", "js/datagateways/UserGateway" ], function( AetherApp, UserGateway ) {
    AetherApp.controller( "NavSidebar", [
        '$scope',
        function( $scope ) {
            var mainColumn = $( ".container-flex.main" ).first();
            var navbarSwap = $( "ul.nav.nav-sidebar > li.swap" ).first();
            
            var model = $scope.model = {};
            
            var init =
                    $scope.init =
                            function() {
                                model =
                                    $scope.model =
                                    {
                                        isExpanded: ( mainColumn.hasClass( "expanded" ) || mainColumn.hasClass( "pre-expanded" ) )
                                                || !( mainColumn.hasClass( "collapsed" ) || mainColumn.hasClass( "pre-collapsed" ) )
                                    };
                                
                                navbarSwap.click( swapColumnState );
                            }

            var swapColumnState = $scope.swapColumnState = function() {
                if ( model.isExpanded ) {
                    mainColumn.removeClass( "pre-expanded expanded" ).addClass( "collapsed" );
                    model.isExpanded = false;
                    
                    CurrentUser.preferences.navSidebarExpanded = false;
                } else {
                    mainColumn.removeClass( "pre-collapsed collapsed" ).addClass( "expanded" );
                    model.isExpanded = true;
                    
                    CurrentUser.preferences.navSidebarExpanded = true;
                }
                
                // Save the updated preferences
                UserGateway.updatePreferences( CurrentUser.preferences )
                    .done( function( payload ) {
                        CurrentUser.preferences = payload.data;
                    })
                    .fail( function( message ) {
                        alert( "FAIL: " + message );
                    });
            };
            
            init();
        } ] );
} );