//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/controllers/All/UserProfile
 * File: UserProfileController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and validation for User Profile screen.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 21, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/UserGateway", "underscore", "js/directives/angular-password" ], function( AetherApp, UserGateway ) {
    
    // Add the Checkbox List directive
    AetherApp.requires.push( 'ngPassword' );
    
    AetherApp.controller( "UserProfile", [
         '$scope', '$window',
         function( $scope, $window ) {
             $scope.AppRoot = AppRoot;
             var model = $scope.model = {};
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     user: Utility.getPageSessionItem( "UserProfile", "user" ),
                     branches: Utility.getPageSessionItem( "UserProfile", "branches" ),
                     dobIsOpen: false,
                     isChangePassword: false,
                     newPassword: ''
                 };
                 
                 if(!(_.isNull(model.user)) && !(_.isNull(model.user.dob))) {
                     model.user.dob = new Date(model.user.dob);
                 }
             };
             
             var save = $scope.save = function() {
                 if(validateForm()) {
                     // Save the User
                     UserGateway.saveUser( model.user )
                         .done( function ( payload ) {
                             model.user = payload.data;
                             $window.location.assign( AppRoot + "All/UserProfile" );
                         })
                         .fail( function ( message ) {
                             alert( "Save failed!" );
                             $window.location.reload( true );
                         });
                 }
             };
             
             var validateForm = function() {
                 return Utility.validateForm( 'userProfileForm', 'messages-container' );
             };
             
             var cancel = $scope.cancel = function() {
                 $window.location.href = AppRoot;
             };
             
             init();
         } 
     ] );
} );