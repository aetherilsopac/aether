//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/controllers
 * File: CatalogueMaintenanceIndexController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and function of Catalogue Maintenance home screen.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 11, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/BiblioGateway" ], function( AetherApp, BiblioGateway ) {
    AetherApp.controller( "CatalogueMaintenanceIndex", [
         '$scope', 'NgTableParams', 'ngTableEventsChannel', '$filter', '$window',
         function( $scope, NgTableParams, ngTableEventsChannel, $filter, $window ) {
             $scope.AppRoot = AppRoot;
             var model = $scope.model = {};
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     recordTypeIconsMap: Utility.getPageSessionItem( "CatalogueMaintenanceIndex", "recordTypeIconsMap" ),
                     recordTypeLabelsMap: Utility.getPageSessionItem( "CatalogueMaintenanceIndex", "recordTypeLabelsMap" ),
                     query: {
                         query: '',
                         field: 'FIELD_BIBLIO_ALL',
                         page: 1,
                         pageSize: 10
                     },
                     firstRecord: 0,
                     lastRecord: 0,
                     totalResults: 0,
                     biblios: [],
                     tableParams: new NgTableParams({
                         count: 10
                     },
                     {
                         counts: [5, 10, 15, 20],
                         getData: function( params ) {
                             return model.biblios;
                         }
                     })
                 };
                 
                 ngTableEventsChannel.onPagesChanged(
                     function(tableParams) {
                         model.query.page = tableParams.page();
                         model.query.pageSize = tableParams.count();
                         
                         if(model.biblios.length > 0) executeQuery();
                     },
                     $scope
                 );
             };
             
             var searchOnEnter = $scope.searchOnEnter = function( evt ) {
                 // If user presses Enter, execute the search
                 if(evt.which == 13) {
                     executeQuery();
                     evt.stopPropagation();
                 }
             };
             
             var executeQuery = $scope.executeQuery = function() {
                 $("#loadingIcon").css("display", "block");
                 
                 BiblioGateway.searchBiblios( model.query )
                     .done( function( payload ) {
                         model.totalResults = payload.data.totalResults;
                         model.firstRecord = ( model.totalResults > 0 ) ? ( model.query.pageSize * ( model.query.page - 1 ) ) + 1 : 0;
                         model.lastRecord = ( model.totalResults >= model.firstRecord + ( model.query.pageSize - 1 ) ) ? model.firstRecord + ( model.query.pageSize - 1 ) : model.totalResults;
                         model.tableParams.total( payload.data.totalResults );
                         model.biblios = payload.data.biblios;
                         model.tableParams.reload();
                         $("#loadingIcon").css("display", "none");
                     })
                     .fail( function( message ) {
                         model.totalResults = 0;
                         model.firstRecord = 0;
                         model.lastRecord = 0;
                         model.tableParams.total( 0 );
                         model.biblios = [];
                         model.tableParams.reload();
                         $("#loadingIcon").css("display", "none");
                     });
             };
             
             var editBiblio = $scope.editBiblio = function( biblio ) {
                 $window.location.assign( AppRoot + 'Librarian/CatalogueMaintenance/Edit/' + biblio.id );
             };
             
             init();
         } 
     ] );
} );