//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/controllers
 * File: PatronMaintenanceEditController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and validation for Patron Maintenance Edit screen.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Aug 13, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/PatronGateway", "js/fragment-controllers/PatronSearchController", "underscore", "js/directives/angular-password" ], function( AetherApp, PatronGateway ) {
    
    // Add the Checkbox List directive
    AetherApp.requires.push( 'ngPassword' );
    
    AetherApp.controller( "PatronMaintenanceEdit", [
         '$scope', 'NgTableParams', '$filter', '$window', '$uibModal',
         function( $scope, NgTableParams, $filter, $window, $uibModal ) {
             $scope.AppRoot = AppRoot;
             var model = $scope.model = {};
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     patron: Utility.getPageSessionItem( "PatronMaintenanceEdit", "patron" ),
                     branches: Utility.getPageSessionItem( "PatronMaintenanceEdit", "branches" ),
                     statuses: Utility.getPageSessionItem( "PatronMaintenanceEdit", "statuses" ),
                     patronCategories: Utility.getPageSessionItem( "PatronMaintenanceEdit", "patronCategories" ),
                     sexes: Utility.getPageSessionItem( "PatronMaintenanceEdit", "sexes" ),
                     dobIsOpen: false,
                     isChangePassword: false,
                     newPassword: ''
                 };
                 
                 if(!(_.isNull(model.patron)) && !(_.isNull(model.patron.dob))) {
                     model.patron.dob = new Date(model.patron.dob);
                 }
                 
                 if(!(_.isNull(model.patron)) && !(_.isNull(model.patron.expiryDate))) {
                     model.patron.expiryDate = new Date(model.patron.expiryDate);
                 }
                 
                 // Ensure user's permissions is defined
                 model.patron.permissions = (model.patron.permissions != null) ? model.patron.permissions : [];
             };
             
             var findGuarantor = $scope.findGuarantor = function() {
                 var patronSearchModal = $uibModal.open({
                     templateUrl: AppRoot + "js-fragments/patron-search",
                     controller: "PatronSearchController",
                     size: "lg",
                     resolve: {
                         
                     }
                 });
                 
                 patronSearchModal.result.then(
                     function(selectedPatron) {
                         model.patron.guarantor = selectedPatron;
                     },
                     function() {
                     }
                 );
             };
             
             var save = $scope.save = function() {
                 if(validateForm()) {
                     // Save the User
                     PatronGateway.savePatron( model.patron )
                         .done( function ( payload ) {
                             model.patron = payload.data;
                             $window.location.assign( AppRoot + "Librarian/PatronMaintenance" );
                         })
                         .fail( function ( message ) {
                             $window.location.reload( true );
                         });
                 }
             };
             
             var validateForm = function() {
                 return Utility.validateForm( 'editPatronForm', 'messages-container' );
             };
             
             var cancel = $scope.cancel = function() {
                 $window.location.href = AppRoot + "Librarian/PatronMaintenance";
             };
             
             init();
         } 
     ] );
} );