//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/controllers
 * File: CatalogueMaintenanceAddController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and validation for the Add Bibliographic Catalogue Item screen.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 16, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/BiblioGateway", "js/fragment-controllers/BiblioImportController" ], function( AetherApp, BiblioGateway ) {
    AetherApp.controller( "CatalogueMaintenanceAdd", [
         '$scope', 'NgTableParams', '$filter', '$window', '$uibModal',
         function( $scope, NgTableParams, $filter, $window, $uibModal ) {
             $scope.AppRoot = AppRoot;
             var model = $scope.model = {};
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     biblio: {},
                     patronCategories: Utility.getPageSessionItem( "CatalogueMaintenanceAdd", "patronCategories" )
                 };
             };
             
             var importRecord = $scope.importRecord = function() {
                 var biblioImportModal = $uibModal.open({
                     templateUrl: AppRoot + "js-fragments/biblio-import",
                     controller: "BiblioImportController",
                     size: "lg",
                     resolve: {
                         
                     }
                 });
                 
                 biblioImportModal.result.then(
                     function(importedRecord) {
                         model.biblio = importedRecord;
                         model.biblio.itemType = Utility.getPageSessionItem( "CatalogueMaintenanceAdd", "itemType" );
                         model.biblio.copyrightDate = new Date(model.biblio.copyrightDate)
                     },
                     function() {
                     }
                 );
             };
             
             var save = $scope.save = function() {
                 if(validateForm()) {
                     // Save the Biblio
                     BiblioGateway.addBiblio( model.biblio )
                         .done( function ( payload ) {
                             model.biblio = payload.data;
                             $window.location.assign( AppRoot + "Librarian/CatalogueMaintenance" );
                         })
                         .fail( function ( message ) {
                             alert( "Save failed!" );
                             $window.location.reload( true );
                         });
                 }
             };
             
             var validateForm = function() {
                 return Utility.validateForm( 'catalogueAddForm', 'messages-container' );
             };
             
             var cancel = $scope.cancel = function() {
                 $window.location.href = AppRoot + "Librarian/CatalogueMaintenance";
             };
             
             init();
         } 
     ] );
} );