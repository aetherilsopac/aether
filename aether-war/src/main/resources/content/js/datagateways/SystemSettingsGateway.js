//@formatter:off
/***************************************************************************************************************************************************************
 * ####################### 
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB 
 * Directory: content/js/datagateways 
 * File: SystemSettingsGateway.js
 * 
 * ####################### 
 * #     MIT LICENSE     # 
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * ####################### 
 * #       Purpose       #
 * ####################### 
 * Handles updating and retrieving AetherSystemsSettings and SystemSetting data.
 * 
 * #######################
 * #       Revision      #
 * ####################### 
 * Nov 9, 2016, Clinton Bush, 1.0.0, 
 *      New file.
 * 
 ***************************************************************************************************************************************************************
 */
// @formatter:on
define( [ "js/interfaces/AbstractDataGateway" ],
    function( AbstractDataGateway ) {
        var SystemSettingsGateway = new AbstractDataGateway( "Data/SystemSettings" );
        
        SystemSettingsGateway.get = function() {
            var self = this;
            return self.httpGet( );
        };
        
        SystemSettingsGateway.save = function( settings, files ) {
            var self = this;
            return self.httpPostWithFiles( "", settings, files );
        };
        
        SystemSettingsGateway.reindex = function() {
            var self = this;
            return self.httpPost( "Reindex", null, {} );
        };
        
        return SystemSettingsGateway;
    } 
);