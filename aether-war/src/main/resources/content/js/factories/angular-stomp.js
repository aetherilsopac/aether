//@formatter:off
/*******************************************************************************
 * ####################### 
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB 
 * Directory: content/js 
 * File: angular-stomp.js
 * 
 * ####################### 
 * #     MIT LICENSE     # 
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * ####################### 
 * #       Purpose       # 
 * ####################### 
 * AngularJS wrapper for STOMP.js.
 * 
 * ####################### 
 * #       Revision      #
 * ####################### 
 * Dec 16, 2016, C. Bush,
 *     Initial draft.
 * 
 * **************************************************************************************************************************************************************
 */
// @formatter:on
define( ['underscore', 'stompjs'], function() {
    var stompModule = angular.module('ngStomp', []);
    stompModule.factory('ngStompFactory', [ '$window',
        function ( $window ) {
            /**
             * Default configuration for the STOMP.js library
             */
            var defaultConfiguration = {
                clientHeartbeat: 5000,          // Client sends heartbeat to server every 5s
                serverHeartbeat: 5000,         // Client receives heartbeat from server every 10s
                receipt: false,                 // Should receipts be requested?
                //transaction: false,             // Should transactions be used?
                connectedCallback: undefined,   // Method called when a connection is successfully established with the STOMP server.
                errorCallback: undefined,       // Method called the attempt to connect with the server fails. This method will be passed a JSON object containing the STOMP ERROR frame.
                debugHandler: undefined,        // Method called to handle debug messages.
                disconnectCallback: undefined,  // Method called when the connection is successfully disconnected via the disconnect() method.
                login: undefined,               // Login ID to use for authentication
                passcode: undefined,            // Passcode to use for authentication
                host: undefined,                // Virtual hostname (if required)
                additionalHeaders: undefined    // Object containing any additional headers to be sent
            };
            
            /**
             * Returns a constructor that will connect to a STOMP server and return an object
             * with subscribe, unsubscribe, send, and disconnect methods.
             * @param url End-point URL where the client should connect to the server at.
             * @param configuration Optional configuration object that overrides the default configuration. 
             */
            return function ( url, configuration ) {
                var self = this;
                
                // For any parameters left undefined in configuration, copy over the
                // default parameters.
                if (typeof (configuration) === 'undefined') {
                    configuration = {};
                }
    
                for (var property in defaultConfiguration) {
                    if (typeof (configuration[property]) === 'undefined') {
                        configuration[property] = defaultConfiguration[property];
                    }
                }
                
                // Setup the STOMP.js client
                self.client = Stomp.client(url);
                
                // Set heartbeat frequencies
                self.client.heartbeat.outgoing = configuration.clientHeartbeat;
                self.client.heartbeat.incoming = configuration.serverHeartbeat;
                
                // If defined, set debug handler
                if( !_.isUndefined(configuration.debugHandler) && _.isFunction(configuration.debugHandler) ) {
                    self.client.debug = configuration.debugHandler;
                }
                
                // Wrapper for STOMP.js's connect() method
                var connectHeaders = {};
                if( !_.isUndefined(configuration.login) ) connectHeaders.login = configuration.login;
                if( !_.isUndefined(configuration.passcode) ) connectHeaders.passcode = configuration.passcode;
                if( !_.isUndefined(configuration.host) ) connectHeaders.host = configuration.host;
                for(var property in configuration.additionalHeaders) {
                    if(typeof (configuration.additionalHeaders[property]) !== 'undefined') {
                        connectHeaders[property] = configuration.additionalHeaders[property];
                    }
                }
                
                // Create the disconnect() method
                self.disconnect = function() {
                    if( self.client.connected ) {
                        if( !_.isUndefined(configuration.disconnectCallback) ) {
                            self.client.disconnect(configuration.disconnectCallback);
                        }
                        else {
                            self.client.disconnect();
                        }
                    }
                };
                
                // Create the subscribe() method
                self.subscriptions = {};
                self.subscribe = function( endpoint, listener ) {
                    if( !_.has( self.subscriptions, endpoint ) ){
                        self.subscriptions[endpoint] = self.client.subscribe( endpoint, listener );
                    }
                };
                
                // Create the unsubscribe() method
                self.unsubscribe = function( endpoint ) {
                    if( _.has( self.subscriptions, endpoint ) ) {
                        self.subscriptions[endpoint].unsubscribe();
                        delete self.subscriptions[endpoint];
                    }
                }
                
                // Create the send() method
                self.send = function( endpoint, message, headers ) {
                    if( _.isUndefined(headers) ) headers = {};
                    self.client.send( endpoint, headers, message );
                };
                
                // Provide predicate for checking connection status
                self.isConnected = function() {
                    return self.client.connected;
                };
                
                // Connect!
                if( !_.isUndefined(configuration.connectedCallback) ) {
                    if( !_.isUndefined(configuration.errorCallback) ) {
                        self.client.connect( connectHeaders, configuration.connectedCallback, configuration.errorCallback );
                    }
                    else {
                        self.client.connect( connectHeaders, configuration.connectedCallback );
                    }
                }
                else {
                    self.client.connect( connectHeaders );
                }
            };
        }
    ]);
});