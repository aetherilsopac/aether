﻿//@formatter:off
/*******************************************************************************
 * ####################### 
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB 
 * Directory: content/js 
 * File: angular-loading.js
 * 
 * ####################### 
 * #     MIT LICENSE     # 
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * ####################### 
 * #       Purpose       # 
 * ####################### 
 * Creates a sliding dot, loading indicator on the specified element.
 * 
 * ####################### 
 * #       Revision      #
 * ####################### 
 * Jun 11, 2016, Clinton Bush, 1.0.0, 
 *      New file.
 * 
 * **************************************************************************************************************************************************************
 */
// @formatter:on
define( ['angular'], function() {
    var loadingModule = angular.module('ngLoading', []);
    loadingModule.factory('ngLoadingFactory', [
        function () {
            /**
             * Default configuration for the Angular Loading Control.
             */
            var defaultConfiguration = {
                overlayBackgroundColorCss: '#000000',
                overlayOpacityCss: '0.6',
                iconBorderStyle: 'solid',
                iconBorderColorCss: '#000000',
                iconBorderWidthCss: '0px',
                iconBorderRadiusCss: '50%',
                iconBackgroundColorCss: '#000000',
                iconDiameterPx: 12,
                iconCount: 5,
                iconSpacingPx: 25,
                iconTravelTimeMs: 700,
                iconPauseTimeMs: 500
            };
    
            /**
             * Calculates a fraction based on the time elapsed versus the
             * specified travel time. Then, scales that fraction logarithmically
             * so that it either slows down or speeds up over time.
             */
            function calculateMultiplier(elapsedTime, configuration) {
                var logInput = elapsedTime / ( configuration.iconTravelTimeMs / 10 );
    
                if (elapsedTime <= configuration.iconTravelTimeMs) {
                    logInput = log10(logInput);
                }
                else {
                    logInput = log10(logInput - 9);
                }
    
                return logInput;
            }
    
            /**
             * Calculates the log base 10 of a number.
             */
            function log10(input) {
                return Math.log(input) / Math.LN10;
            }
    
            /**
             * Returns a function that will apply the Angular Loading Control
             * to the element specified by elementId. The return value of this function
             * is a method that, when called, will remove and destroy the loading control.
             */
            return function (elementId, configuration) {
                // For any parameters left undefined in configuration, copy over the
                // default parameters.
                if (typeof (configuration) === 'undefined') {
                    configuration = {};
                }
    
                for (var property in defaultConfiguration) {
                    if (typeof (configuration[property]) === 'undefined') {
                        configuration[property] = defaultConfiguration[property];
                    }
                }
    
                var parentElementObj = $("#" + elementId);
    
                var newLoadingIndicator = {
                    parentElement: parentElementObj,
                    origPositioning: parentElementObj.css("position"),
                    elapsedTime: 0,
                    shadowOverlayElement: $("<div/>")
                        .width('100%')//parentElementObj.innerWidth())
                        .height('100%')//parentElementObj.innerHeight())
                        .css({
                            "position": "absolute",
                            "top": "0px",
                            "left": "0px",
                            "z-index": "10000",
                            "opacity": configuration.overlayOpacityCss,
                            "background-color": configuration.overlayBackgroundColorCss,
                            "overflow": "hidden"
                        }),
                    overlayElement: $("<div/>")
                        .width('100%')//parentElementObj.innerWidth())
                        .height('100%')//parentElementObj.innerHeight())
                        .css({
                            "position": "absolute",
                            "top": "0px",
                            "left": "0px",
                            "z-index": "11000",
                            "overflow": "hidden"
                        }),
                    parentElementTotalWidth: parentElementObj.innerWidth(),
                    loadingIconsTotalWidth: (configuration.iconDiameterPx + configuration.iconSpacingPx) * configuration.iconCount,
                    loadingIconsXOffset: function () { return (parentElementObj.innerWidth() - this.loadingIconsTotalWidth) / 2; },
                    loadingIconsYOffset: function () { return (parentElementObj.innerHeight() - configuration.iconDiameterPx) / 2; },
                    loadingIconObjs: [],
                    timer: null
                };
    
    
                // Change element to relative positioning
                newLoadingIndicator.parentElement.css("position", "relative");
    
    
                // Add the overlay to the parent
                newLoadingIndicator.parentElement.append(newLoadingIndicator.shadowOverlayElement);
                newLoadingIndicator.parentElement.append(newLoadingIndicator.overlayElement);
    
                // Construct the loading icons
                for (var i = 0; i < configuration.iconCount; i++) {
                    newLoadingIndicator.loadingIconObjs.push({
                        y: newLoadingIndicator.loadingIconsYOffset(),
                        xOffscreen: (((configuration.iconDiameterPx + configuration.iconSpacingPx) * i) - newLoadingIndicator.loadingIconsTotalWidth),
                        xHome: newLoadingIndicator.loadingIconsXOffset() + (configuration.iconDiameterPx + configuration.iconSpacingPx) * i,
                        jObject: $("<div/>")
                                    .css({
                                        "position": "absolute",
                                        "top": '50%',//newLoadingIndicator.loadingIconsYOffset() + "px",
                                        "left": (((configuration.iconDiameterPx + configuration.iconSpacingPx) * i) - newLoadingIndicator.loadingIconsTotalWidth) + "px",
                                        "border-style": configuration.iconBorderStyle,
                                        "border-color": configuration.iconBorderColorCss,
                                        "border-width": configuration.iconBorderWidthCss,
                                        "border-radius": configuration.iconBorderRadiusCss,
                                        "width": configuration.iconDiameterPx + "px",
                                        "height": configuration.iconDiameterPx + "px",
                                        "background-color": configuration.iconBackgroundColorCss,
                                        "z-index": "11000"
                                    }),
                        updatePosition: function (currentTimeElapsed) {
                            var newXPos = parseInt(this.jObject.offset().left);
    
                            if (currentTimeElapsed <= configuration.iconTravelTimeMs) {
                                newXPos = this.xHome - ((1.0 - calculateMultiplier(currentTimeElapsed, configuration)) * (this.xHome + configuration.iconDiameterPx));
                            }
                            else {
                                newXPos = this.xHome + calculateMultiplier(currentTimeElapsed, configuration) * (newLoadingIndicator.parentElementTotalWidth - this.xHome);
                            }
    
                            this.jObject.css("left", newXPos + "px");
                        }
                    });
    
                    newLoadingIndicator.overlayElement.append(newLoadingIndicator.loadingIconObjs[i].jObject);
                }
    
                newLoadingIndicator.timer = setInterval(
                   function () {
                       newLoadingIndicator.elapsedTime += 10;
    
                       // Delay increments - the time each succeeding dot waits to enter the screen (this number is multiplied
                       // by the dot's index to get the actual millisecond delay used)
                       var delayIncrement = 200;
    
                       // Shift the value to the right by 10% of the travelTime to ensure 
                       // we never try to take log10() of a number less than 1.
                       var shiftedTime = newLoadingIndicator.elapsedTime + (configuration.iconTravelTimeMs / 10);
    
                       // Delay incurred by staggering the dots while entering the stage
                       var totalDelayTime = (newLoadingIndicator.loadingIconObjs.length - 1) * delayIncrement;
    
                       if (shiftedTime <= configuration.iconTravelTimeMs + totalDelayTime) {
                           for (var i = 0; i < newLoadingIndicator.loadingIconObjs.length; i++) {
                               var delay = (newLoadingIndicator.loadingIconObjs.length - (i + 1)) * delayIncrement;
                               var actualTimeMoving = shiftedTime - delay;
    
                               if (actualTimeMoving >= (configuration.iconTravelTimeMs / 10) && actualTimeMoving <= configuration.iconTravelTimeMs) {
                                   newLoadingIndicator.loadingIconObjs[i].updatePosition(actualTimeMoving);
                               }
                           }
                       }
                       else if (shiftedTime > (configuration.iconTravelTimeMs + totalDelayTime) && shiftedTime <= (configuration.iconTravelTimeMs + totalDelayTime + configuration.iconPauseTimeMs)) {
                           // do nothing while the icons wait
                       }
                       else if (shiftedTime > (configuration.iconTravelTimeMs + totalDelayTime + configuration.iconPauseTimeMs) && shiftedTime < (2 * (configuration.iconTravelTimeMs + totalDelayTime) + configuration.iconPauseTimeMs)) {
                           for (var i = 0; i < newLoadingIndicator.loadingIconObjs.length; i++) {
                               var delay = (newLoadingIndicator.loadingIconObjs.length - (i + 1)) * delayIncrement;
                               var actualTimeMoving = shiftedTime - totalDelayTime - configuration.iconPauseTimeMs - delay;
    
                               if (actualTimeMoving <= (configuration.iconTravelTimeMs * 2) && actualTimeMoving > configuration.iconTravelTimeMs) {
                                   newLoadingIndicator.loadingIconObjs[i].updatePosition(actualTimeMoving);
                               }
                           }
                       }
                       else {
                           // hide the icons offscreen
                           for (var i = 0; i < newLoadingIndicator.loadingIconObjs.length; i++) {
                               newLoadingIndicator.loadingIconObjs[i].jObject.css("left", newLoadingIndicator.loadingIconObjs.xOffscreen + "px");
                           }
    
                           // reset the elapsed time
                           newLoadingIndicator.elapsedTime = 0;
                       }
                   },
                   10
                );
    
                // Return a callback function to undo everything
                return function () {
                    newLoadingIndicator.overlayElement.remove();
                    newLoadingIndicator.shadowOverlayElement.remove();
                    newLoadingIndicator.parentElement.css("position", newLoadingIndicator.origPositioning);
                    clearInterval(newLoadingIndicator.timer);
                };
            };
        }
    ]);
});