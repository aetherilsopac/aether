//@formatter:off
/*******************************************************************************
 * ####################### 
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB 
 * Directory: content/js 
 * File: main.js
 * 
 * ####################### 
 * #     MIT LICENSE     # 
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * ####################### 
 * #       Purpose       # 
 * ####################### 
 * Initializes Require JS.
 * 
 * ####################### 
 * #       Revision      #
 * ####################### 
 * Jun 11, 2016, Clinton Bush, 1.0.0, 
 *      New file.
 * 
 * **************************************************************************************************************************************************************
 */
// @formatter:on
requirejs
    .config( {
        baseUrl : AppRoot,
        paths : {
            // Path aliases
            "angular" : "webjars/angularjs/1.5.2/angular.min",
            "angular-sanitize" : "webjars/angularjs/1.5.2/angular-sanitize.min",
            "angular-route" : "webjars/angularjs/1.5.2/angular-route.min",
            "angular-animate" : "webjars/angularjs/1.5.2/angular-animate.min",
            "angular-bootstrap" : "webjars/angular-ui-bootstrap/1.2.5/ui-bootstrap-tpls.min",
            "jquery" : "webjars/jquery/2.2.2/dist/jquery.min",
            "bootstrap" : "packages/bootstrap-3.3.6-dist/js/bootstrap.min",
            "underscore" : "webjars/underscore/1.8.3/underscore-min",
            "ngtable" : "webjars/ng-table/1.0.0-beta.9/ng-table.min",
            "ui.mask" : "js/libs/mask.min",
            "ui.select" : "webjars/angular-ui-select/0.17.1/select.min",
            "ngfileupload" : "webjars/github-com-nervgh-angular-file-upload/v2.3.4/dist/angular-file-upload.min",
            "stompjs": "webjars/stompjs/2.3.3/lib/stomp.min",
            "tinymce": "webjars/tinymce/4.4.3/tinymce.min",
            "angular-tinymce": "webjars/angular-ui-tinymce/0.0.17/dist/tinymce.min"
        },
        shim : {
            "bootstrap" : {
                deps : [ "jquery" ]
            },
            "angular" : {
                deps : [ "jquery" ],
                exports: 'angular'
            },
            "angular-sanitize" : {
                deps : [ "angular" ]
            },
            "angular-route" : {
                deps : [ "angular" ]
            },
            "angular-animate" : {
                deps : [ "angular" ]
            },
            "angular-bootstrap" : {
                deps : [ "angular" ]
            },
            "angular-tinymce" : {
                deps: [ "tinymce" ]
            },
            "ngtable" : {
                deps : [ "angular" ]
            },
            "ui.mask" : {
                deps : [ "angular" ]
            },
            "ui.select" : {
                deps : [ "angular" ]
            },
            "tinymce" : {
                deps : [ "jquery", "webjars/tinymce/4.4.3/jquery.tinymce.min" ]
            },
            "underscore" : {
                exports: '_'
            }
        }
    } 
);

console.log( "RequireJS configured..." );