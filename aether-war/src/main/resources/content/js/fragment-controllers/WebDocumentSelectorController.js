//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/fragment-controllers
 * File: WebDocumentSelectorController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and validation for Web Document Selector dialog.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jan 02, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/WebDocumentGateway", "underscore", "jquery" ], function( AetherApp, WebDocumentGateway ) {
    
    AetherApp.controller( "WebDocumentSelectorController", [
         '$scope', '$uibModalInstance', 'NgTableParams', 'currentValue', 'documentTypes',
         function( $scope, $uibModalInstance, NgTableParams, currentValue, documentTypes ) {
             var model = $scope.model = {};
             $scope.AppRoot = AppRoot;
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     webDocuments: [],
                     documentTypes: documentTypes,
                     forceLocaleSelection: false,
                     tableParams: new NgTableParams({
                         count: 5,
                         sorting: {
                             title: 'asc'
                         }
                     },
                     {
                         counts: [ 5, 10, 15, 20 ],
                         getData: function( params ) {
                             return model.webDocuments;
                         }
                     })
                 };
                 
                 WebDocumentGateway.listWebDocuments()
                     .done( function( payload ) {
                         // Only documents that are Help or Wiki documents can be linked to
                         model.webDocuments = _.filter( payload.data, function(document) {
                             return document.documentType == 'W' || document.documentType == 'H';
                         });
                         
                         model.tableParams.reload();
                     })
                     .fail( function( message ) {
                         alert( message );
                     });
             };
             
             var convertDocumentType = $scope.convertDocumentType = function( documentType ) {
                 var value = model.documentTypes[documentType];
                 if(!_.isUndefined(value)) return value;
                 return "";
             };
             
             var selectWebDocument = $scope.selectWebDocument = function( webDocument ) {
                 var url = AppRoot;
                 
                 switch( webDocument.documentType ) {
                     case 'W':
                         url += 'Wiki/' + webDocument.friendlyUrl;
                         break;
                     
                     case 'H':
                         url += 'Help/' + webDocument.friendlyUrl;
                         break;
                 }
                 
                 if( model.forceLocaleSelection ) {
                     url += '/' + webDocument.locale;
                 }
                 
                 // Combine the document and url into the result
                 var result = {
                     'document': webDocument,
                     'url': url
                 };
                 
                 $uibModalInstance.close( result );
             }
             
             var cancel = $scope.cancel = function() {
                 $uibModalInstance.dismiss();
             };
             
             init();
         } 
     ] );
    
} );