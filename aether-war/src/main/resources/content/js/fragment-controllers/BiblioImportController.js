//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/fragment-controllers
 * File: BiblioImportController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and validation for Bibliographic Record Import dialog.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 17, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/SruGateway", "underscore" ], function( AetherApp, SruGateway ) {
    
    AetherApp.controller( "BiblioImportController", [
         '$scope', '$uibModalInstance', 'NgTableParams', 'ngTableEventsChannel', '$interval',
         function( $scope, $uibModalInstance, NgTableParams, ngTableEventsChannel, $interval ) {
             var model = $scope.model = {};
             var queryDelay = 500;
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     query: {
                         biblioId: '',
                         sruSource: null,
                         biblioIdType: '',
                         pageSize: 3,
                         page: 1
                     },
                     changeTimeStamp: 0,
                     records: [],
                     totalRecords: 0,
                     biblioIdTypes: [],
                     sruSources: [],
                     rawMarcXml: '',
                     tableParams: new NgTableParams({
                         count: 5,
                         sorting: {
                             title: 'asc'
                         }
                     },
                     {
                         counts: [5, 10, 15, 20],
                         getData: function( params ) {
                             return model.records;
                         }
                     })
                 };
                 
                 SruGateway.getSruSources()
                     .done( function( payload ) {
                         model.sruSources = payload.data;
                         model.query.sruSource = model.sruSources[0];
                         model.biblioIdTypes = model.query.sruSource.sruIndices.split(';');
                         model.query.biblioIdType = model.biblioIdTypes[0];
                     })
                     .fail( function( message ) {
                         alert( "Faild to retrieve list of SRU sources." );
                     });
                 
                 $scope.$watch('model.query.sruSource', function(newValue, oldValue) {
                     if(newValue !== oldValue) {
                         model.biblioIdTypes = newValue.sruIndices.split(';');
                     }
                 });
                 
                 ngTableEventsChannel.onPagesChanged(
                     function(tableParams) {
                         model.query.page = tableParams.page();
                         model.query.pageSize = tableParams.count();
                         executeQuery();
                     },
                     $scope
                 );
             };
             
             var executeQuery = $scope.executeQuery = function() {
                 if( isValidQuery() ) {
                     $( "#loadingIcon" ).css( "display", "block" );
                     
                     var result = SruGateway.executeSruQuery( model.query );
                     result.done(function( payload ) {
                         model.tableParams.total( payload.data.totalResults );
                         model.records = payload.data.records;
                         model.rawMarcXml = payload.data.rawMarcXml;
                         model.tableParams.reload();
                         $( "#loadingIcon" ).css( "display", "none" );
                     });
                     result.fail(function( message ) {
                         model.tableParams.total( 0 );
                         model.records = [];
                         model.tableParams.reload();
                         $( "#loadingIcon" ).css( "display", "none" );
                     });
                 }
                 else {
                     model.records = [];
                     model.tableParams.reload();
                 }
             };
             
             var isValidQuery = $scope.isValidQuery = function() {
                 return ( model.query.biblioId != null && model.query.biblioId.length > 0 ) &&
                        ( model.query.biblioIdType != null ) &&
                        ( model.query.sruSource != null );
             }
             
             var selectRecord = $scope.selectRecord = function( selectedRecord ) {
                 $uibModalInstance.close( selectedRecord );
             }
             
             var cancel = $scope.cancel = function() {
                 $uibModalInstance.dismiss();
             };
             
             init();
         } 
     ] );
    
} );