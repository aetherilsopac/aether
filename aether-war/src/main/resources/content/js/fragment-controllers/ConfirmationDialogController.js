//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/fragment-controllers
 * File: PatronSearchController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and validation for Patron Search dialog.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 04, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "underscore" ], function( AetherApp ) {
    
    AetherApp.constant( 'ConfirmationDialogConfig', {
        yesButtonLabel: 'Yes',
        noButtonLabel: 'No',
        showNoButton: true,
        modalTitle: 'Are you sure?',
        modalMessage: 'Are you sure you wish to continue?'
    });
    
    AetherApp.controller( "ConfirmationDialogController", [
         '$scope', '$uibModalInstance', 'ConfirmationDialogConfig',
         function( $scope, $uibModalInstance, ConfirmationDialogConfig, config ) {
             
             var init = function() {
                 // Copy any unset configuration parameters in from the constant ngPasswordConfig
                 if( _.isUndefined( $scope.config ) || _.isNull( $scope.config ) ) {
                     $scope.config = {};
                 }
                 _.defaults( $scope.config, ConfirmationDialogConfig );
             };
             
             var yes = $scope.yes = function() {
                 $uibModalInstance.close();
             };
             
             var no = $scope.no = function() {
                 $uibModalInstance.dismiss();
             };
             
             init();
         } 
     ] );
    
} );