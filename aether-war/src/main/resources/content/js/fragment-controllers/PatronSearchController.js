//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/fragment-controllers
 * File: PatronSearchController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and validation for Patron Search dialog.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 04, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/PatronGateway", "underscore" ], function( AetherApp, PatronGateway ) {
    
    AetherApp.controller( "PatronSearchController", [
         '$scope', '$uibModalInstance', 'NgTableParams', 'ngTableEventsChannel', '$interval',
         function( $scope, $uibModalInstance, NgTableParams, ngTableEventsChannel, $interval ) {
             var model = $scope.model = {};
             var queryDelay = 500;
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     query: {
                         cardNumber: '',
                         lastName: '',
                         firstName: '',
                         userName: '',
                         pageSize: 3,
                         page: 1
                     },
                     tableQuery: null,
                     queryInterval: null,
                     changeTimeStamp: 0,
                     patrons: [],
                     totalPatrons: 0,
                     tableParams: new NgTableParams({
                         count: 5,
                         sorting: {
                             lastName: 'asc'
                         }
                     },
                     {
                         counts: [5, 10, 15, 20],
                         getData: function(params) {
                             return model.patrons;
                         }
                     })
                 };
                 
                 $scope.$watch('model.query.cardNumber', function( newValue, oldvalue ) {
                     model.tableParams.page(1);
                     model.changeTimeStamp = (new Date()).getTime();
                     model.query.page = 1;
                     executeQuery();
                 }, true);
                 
                 $scope.$watch('model.query.firstName', function( newValue, oldvalue ) {
                     model.tableParams.page(1);
                     model.changeTimeStamp = (new Date()).getTime();
                     model.query.page = 1;
                     executeQuery();
                 }, true);
                 
                 $scope.$watch('model.query.lastName', function( newValue, oldvalue ) {
                     model.tableParams.page(1);
                     model.changeTimeStamp = (new Date()).getTime();
                     model.query.page = 1;
                     executeQuery();
                 }, true);
                 
                 $scope.$watch('model.query.userName', function( newValue, oldvalue ) {
                     model.tableParams.page(1);
                     model.changeTimeStamp = (new Date()).getTime();
                     model.query.page = 1;
                     executeQuery();
                 }, true);
                 
                 model.queryInterval = $interval(function() {
                     var now = (new Date()).getTime();
                     if(model.changeTimeStamp != null && (now - model.changeTimeStamp) > queryDelay) {
                         executeQuery();
                         model.changeTimeStamp = null;
                     }
                 }, 500);
                 
                 ngTableEventsChannel.onPagesChanged(
                     function(tableParams) {
                         model.query.page = tableParams.page();
                         model.query.pageSize = tableParams.count();
                         executeQuery();
                     },
                     $scope
                 );
                 
                 $scope.$on('$destroy', function() {
                     if(model.queryInterval) $interval.cancel(model.queryInterval);
                 })
             };
             
             var executeQuery = $scope.executeQuery = function() {
                 if( isValidQuery() ) {
                     $( "#loadingIcon" ).css( "display", "inline-block" );
                     
                     var result = PatronGateway.query( model.query );
                     result.done(function( payload ) {
                         model.tableParams.total(payload.data.totalResults)
                         model.patrons = payload.data.patrons;
                         model.tableParams.reload();
                         $( "#loadingIcon" ).css( "display", "none" );
                     });
                     result.fail(function( message ) {
                         model.tableParams.total(0);
                         model.patrons = [];
                         model.tableParams.reload();
                         $( "#loadingIcon" ).css( "display", "none" );
                     });
                 }
                 else {
                     model.patrons = [];
                     model.tableParams.reload();
                 }
             };
             
             var isValidQuery = $scope.isValidQuery = function() {
                 return (model.query.cardNumber != null && model.query.cardNumber.length > 0) ||
                        (model.query.lastName != null && model.query.lastName.length > 0) ||
                        (model.query.firstName != null && model.query.firstName.length > 0) ||
                        (model.query.userName != null && model.query.userName.length > 0);
             }
             
             var selectPatron = $scope.selectPatron = function(selectedPatron) {
                 $uibModalInstance.close(selectedPatron);
             }
             
             var cancel = $scope.cancel = function() {
                 $uibModalInstance.dismiss();
             };
             
             init();
         } 
     ] );
    
} );