//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/fragment-controllers
 * File: ImageSelectorController.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides logic and validation for Image Selector dialog.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 30, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( [ "js/AetherApp", "js/datagateways/ResourceGateway", "underscore", "jquery" ], function( AetherApp, ResourceGateway ) {
    
    AetherApp.controller( "ImageSelectorController", [
         '$scope', '$uibModalInstance', 'currentValue', 'blobCache',
         function( $scope, $uibModalInstance, currentValue, blobCache ) {
             var model = $scope.model = {};
             $scope.AppRoot = AppRoot;
             
             var init = function() {
                 model = $scope.model = {
                     self: this,
                     images: []
                 };
                 
                 $uibModalInstance.rendered.then( function() {
                     // Handle the user selecting an image for upload
                     $( '#imageSelectorFile' ).change( function( evt ) {
                         $scope.$apply( function() {
                             uploadImage( evt.target.files[0] );
                         });
                     });
                     $( '')
                 });
                 
                 ResourceGateway.listImageResourceNames()
                     .done( function( payload ) {
                         $scope.$apply( function() {
                             model.images = payload.data;
                         });
                     })
                     .fail( function( message ) {
                         alert( message );
                     });
             };
             
             var uploadImage = $scope.uploadImage = function( file ) {
                 // Add selected image to TinyMCE blob cache for auto-upload
                 var id = 'blobid' + (new Date()).getTime();
                 var blobInfo = blobCache.create(id, file);
                 blobCache.add(blobInfo);
                 
                 selectImage( blobInfo.blobUri() );
             }
             
             var selectImage = $scope.selectImage = function( fileName ) {
                 $uibModalInstance.close( fileName );
             }
             
             var cancel = $scope.cancel = function() {
                 $uibModalInstance.dismiss();
             };
             
             init();
         } 
     ] );
    
} );