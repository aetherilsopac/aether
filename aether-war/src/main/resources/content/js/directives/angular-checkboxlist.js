//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/directives
 * File: angular-checkboxlist.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * These directives allow a list of checkboxes to work in concert with a "select all" checkbox.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 29, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( ['angular', 'underscore'], function() {
    var checkboxListModule = angular.module('ngCheckboxList', []);
    checkboxListModule.directive( 'checkboxList', function() {
        return {
            restrict: 'A',
            scope: {
                'onSelectAll': '&onSelectAll',
                'onDeselectAll': '&onDeselectAll'
            },
            controller: [ '$scope', function( $scope ) {
                var checkboxList = {
                    checkboxes: [],
                    selectAllCheckbox: null,
                    isAllChecked: function() {
                        var allChecked = true;
                        
                        for( var i in checkboxList.checkboxes ) {
                            if( !( checkboxList.checkboxes[i].isChecked() ) ) {
                                allChecked = false;
                                break;
                            }
                        }
                        
                        return allChecked;
                    },
                    checkboxChanged: function( isChecked ) {
                        if( checkboxList.selectAllCheckbox != null ) {
                            if( checkboxList.isAllChecked() ) {
                                checkboxList.selectAllCheckbox.setChecked( true );
                            }
                            else {
                                checkboxList.selectAllCheckbox.setChecked( false );
                            }
                        }
                    },
                    selectAllCheckboxChanged: function( isChecked ) {
                        if(!isChecked && !(checkboxList.isAllChecked())) return;
                        
                        for( var i in checkboxList.checkboxes ) {
                            checkboxList.checkboxes[i].setChecked( isChecked );
                        }
                        
                        if(isChecked) {
                            $scope.onSelectAll();
                        }
                        else {
                            $scope.onDeselectAll();
                        }
                    },
                    addCheckbox: function( checkbox ) {
                        checkboxList.checkboxes.push( checkbox );
                        $scope.$watch(
                            checkbox.isChecked,
                            function( newValue, oldValue ) {
                                checkboxList.checkboxChanged( newValue );
                            }
                        );
                    },
                    setSelectAllCheckbox: function ( checkbox ) {
                        checkboxList.selectAllCheckbox = checkbox;
                        checkbox.element.change( function() { $scope.$apply( checkboxList.selectAllCheckboxChanged( checkbox.isChecked() ) ); } );
                    }
                };
                
                $scope.$evalAsync(function() {
                    checkboxList.checkboxChanged();
                });
                
                $scope.checkboxList = this.checkboxList = checkboxList;
            }]
        }
    });
    checkboxListModule.directive( 'checkboxListItem', function() {
        return {
            restrict: 'A',
            require: '^^checkboxList',
            scope: {
                ngModel: '=',
                ngChecked: '=?'
            },
            controller: [ '$scope', function( $scope ) {
                var checkbox = {
                    setChecked: function( isChecked ) {
                        $scope.ngModel = isChecked;
                        if(!_.isUndefined( $scope.ngChecked ) && !_.isNull( $scope.ngChecked ) ) {
                            $scope.ngChecked = isChecked;
                        }
                    },
                    isChecked: function() {
                        return $scope.ngModel;
                    }
                };
                
                $scope.checkbox = this.checkbox = checkbox;
            }],
            link: function( scope, element, attrs, checkboxListCtrl ) {
                scope.checkbox.element = element;
                checkboxListCtrl.checkboxList.addCheckbox( scope.checkbox );
            }
        }
    });
    checkboxListModule.directive( 'checkboxListSelectall', function() {
        return {
            restrict: 'A',
            require: '^^checkboxList',
            scope: {
                ngModel: '=?',
                ngChecked: '=?'
            },
            link: function( scope, element, attrs, checkboxListCtrl ) {
                var selectAllCheckbox = {
                    isChecked: function() {
                        return element.prop( 'checked' );
                    },
                    setChecked: function( isChecked ) {
                        element.prop( 'checked', isChecked );
                        if(!_.isUndefined( scope.ngModel ) && !_.isNull( scope.ngModel ) ) {
                            scope.ngModel = isChecked;
                        }
                        if(!_.isUndefined( scope.ngChecked ) && !_.isNull( scope.ngChecked ) ) {
                            scope.ngChecked = isChecked;
                        }
                    },
                    element: element
                };
                
                element.change(function() { 
                    scope.$apply(function() {
                        selectAllCheckbox.setChecked( element.prop( 'checked' ) );
                    });
                });
                
                scope.selectAllCheckbox = selectAllCheckbox;
                
                checkboxListCtrl.checkboxList.setSelectAllCheckbox( scope.selectAllCheckbox );
            }
        }
    });
});