//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Directory: content/js/directives
 * File: angular-password.js
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * New password control with a "Password" and "Confirm Password" field.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jul 6, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on
define( ['angular', 'underscore', 'bootstrap'], function() {
    var passwordModule = angular.module('ngPassword', []);
    passwordModule.constant( 'ngPasswordConfig', {
        passwordLabel1: 'Password',
        passwordLabel2: 'Confirm Password',
        maxLength: 20,
        minLength: 8,
        specialChars: '!@#$%\\^&*\\-_+=',
        specialCharsRequired: 2,
        upperCharsRequired: 2,
        lowerCharsRequired: 2,
        numberCharsRequired: 2,
        popoverTitle: 'Password Requirements',
        popoverLocation: 'right',
        popoverInvalidClass: 'ng-password-invalid',
        popoverValidClass: 'ng-password-valid',
        upperCharsText: '{0} uppercase characters required',
        lowerCharsText: '{0} lowercase characters required',
        specialCharsText: '{0} special characters required',
        numberCharsText: '{0} number characters required',
        passwordsMatchText: 'passwords match',
        minLengthText: '{0} characters minimum',
        maxLengthText: '{0} characters maximum'
    });
    passwordModule.directive( 'ngPassword', ['$compile', '$interval', function($compile, $interval) {
        return {
            restrict: 'E',
            scope: {
                id: '@id',
                passwordId: '@',
                confirmPasswordId: '@',
                enabled: '=?',
                config: '=?',
                ngModel: '='
            },
            controller: [ '$scope', 'ngPasswordConfig', function( $scope, ngPasswordConfig ) {
                var model = $scope.model = {
                    password1: '',
                    password2: '',
                    complexityValidations: {},
                    passwordsMatch: false,
                    complexityMet: false
                };
                
                // Copy any unset configuration parameters in from the constant ngPasswordConfig
                if( _.isUndefined( $scope.config ) || _.isNull( $scope.config ) ) {
                    $scope.config = {};
                }
                _.defaults( $scope.config, ngPasswordConfig );
                
                // Set any undefined or null scope parameters
                if( _.isUndefined( $scope.id ) || _.isNull( $scope.id ) ) {
                    $scope.id = "ngPassword" + Math.round( Math.random() * 100000 ); // id is "ngPassword" followed by random 5-digit number
                }
                
                if( _.isUndefined( $scope.passwordId ) || _.isNull ( $scope.passwordId ) ) {
                    $scope.passwordId = $scope.id + "Password1";
                }
                
                if( _.isUndefined( $scope.confirmPasswordId ) || _.isNull( $scope.confirmPasswordId ) ) {
                    $scope.confirmPasswordId = $scope.id + "Password2";
                }
                
                // Build validations list
                if( $scope.config.specialCharsRequired > 0 ) {
                    model.complexityValidations['specialCharsRequired'] = {
                        pattern: new RegExp("([" + $scope.config.specialChars + "]{1})", "g"),
                        isValid: false,
                        message: $scope.config.specialCharsText.replace( '{0}', $scope.config.specialCharsRequired ),
                        validate: function( password ) {
                            var self = this;
                            var matchCount = (password.match(self.pattern) || []).length;
                            self.isValid = matchCount >= $scope.config.specialCharsRequired;
                            return self.isValid;
                        }
                    };
                }
                
                if( $scope.config.upperCharsRequired > 0 ) {
                    model.complexityValidations['upperCharsRequired'] = {
                        pattern: new RegExp("([A-Z]{1})", "g"),
                        isValid: false,
                        message: $scope.config.upperCharsText.replace( '{0}', $scope.config.upperCharsRequired ),
                        validate: function( password ) {
                            var self = this;
                            var matchCount = (password.match(self.pattern) || []).length;
                            self.isValid = matchCount >= $scope.config.upperCharsRequired;
                            return self.isValid;
                        }
                    };
                }
                
                if( $scope.config.lowerCharsRequired > 0 ) {
                    model.complexityValidations['lowerCharsRequired'] = {
                        pattern: new RegExp("([a-z]{1})", "g"),
                        isValid: false,
                        message: $scope.config.lowerCharsText.replace( '{0}', $scope.config.lowerCharsRequired ),
                        validate: function( password ) {
                            var self = this;
                            var matchCount = (password.match(self.pattern) || []).length;
                            self.isValid = matchCount >= $scope.config.lowerCharsRequired;
                            return self.isValid;
                        }
                    };
                }
                
                if( $scope.config.numberCharsRequired > 0 ) {
                    model.complexityValidations['numberCharsRequired'] = {
                        pattern: new RegExp("([\\d]{1})", "g"),
                        isValid: false,
                        message: $scope.config.numberCharsText.replace( '{0}', $scope.config.numberCharsRequired ),
                        validate: function( password ) {
                            var self = this;
                            var matchCount = (password.match(self.pattern) || []).length;
                            self.isValid = matchCount >= $scope.config.numberCharsRequired;
                            return self.isValid;
                        }
                    };
                }
                
                model.complexityValidations['minLength'] = {
                    isValid: false,
                    message: $scope.config.minLengthText.replace( '{0}', $scope.config.minLength ),
                    validate: function( password ) {
                        var self = this;
                        self.isValid = password.length >= $scope.config.minLength;
                        return self.isValid;
                    }
                };
                
                // Add watchers to the password fields for real-time validation
                $scope.$watch( 'model.password1', function( newValue, oldValue ) {
                    if(newValue !== oldValue) {
                        if( validatePasswordComplexity( newValue ) & validatePasswordsMatch( newValue, model.password2 ) ) {
                            setModel( newValue );
                            return;
                        }
                        setModel( null );
                    }
                });
                
                $scope.$watch( 'model.password2', function( newValue, oldValue ) {
                    if(newValue !== oldValue) {
                        if( validatePasswordsMatch( model.password1, newValue ) ) {
                            setModel( newValue );
                            return;
                        }
                        setModel( null );
                    }
                });
                
                $scope.$watch( 'enabled', function( newValue, oldValue )  {
                    if(newValue == false) {
                        var password1Element = $( '#' + $scope.passwordId ).first();
                        var password2Element = $( '#' + $scope.confirmPasswordId ).first();
                        
                        if(password1Element.length > 0 && password2Element.length > 0) {
                            var password1Controller = password1Element.controller( 'ngModel' );
                            var password2Controller = password2Element.controller( 'ngModel' );
                            
                            model.password1 = "";
                            model.password2 = "";
                            
                            password1Controller.$setValidity( 'password-complexity', true );
                            password1Controller.$setValidity( 'passwords-match', true );
                            
                            password2Controller.$setValidity( 'password-complexity', true );
                            password2Controller.$setValidity( 'passwords-match', true );
                        }
                    }
                    else {
                        validatePasswordComplexity( model.password1 );
                        validatePasswordsMatch( model.password1, model.password2 );
                    }
                });
                
                var validatePasswordComplexity = function( password ) {
                    var password1Element = $( '#' + $scope.passwordId ).first();
                    var password2Element = $( '#' + $scope.confirmPasswordId ).first();
                    
                    if(password1Element.length > 0 && password2Element.length > 0) {
                        if( $scope.enabled ) {
                            if( !_.isUndefined( password1Element ) && !_.isUndefined( password2Element ) ) {
                                model.complexityMet = false;
                                
                                if( typeof( password ) !== 'undefined' ) {
                                    var isPasswordComplexEnough = true;
                                    for( var validationName in model.complexityValidations ) {
                                        if( !model.complexityValidations[validationName].validate( password ) ) {
                                            isPasswordComplexEnough = false;
                                        }
                                    }
                                    password1Element.controller( 'ngModel' ).$setValidity( 'password-complexity', isPasswordComplexEnough );
                                    password2Element.controller( 'ngModel' ).$setValidity( 'password-complexity', isPasswordComplexEnough );
                                    model.complexityMet = isPasswordComplexEnough;
                                    return isPasswordComplexEnough;
                                }
                            }
                            
                            for( var validationName in model.complexityValidations ) {
                                model.complexityValidations[validationName].validate( '' );
                            }
                        }
                    }
                    
                    return false;
                };
                
                var validatePasswordsMatch = function( password1, password2 ) {
                    var password1Element = $( '#' + $scope.passwordId ).first();
                    var password2Element = $( '#' + $scope.confirmPasswordId ).first();
                    
                    if(password1Element.length > 0 && password2Element.length > 0) {
                        if( $scope.enabled ) {
                            if( !_.isUndefined( password1Element ) && !_.isUndefined( password2Element ) ) {
                                model.passwordsMatch = false;
                                
                                if( typeof( password1 ) !== 'undefined' && typeof( password2 ) !== 'undefined' ) {
                                    if( password1 == password2 && !(typeof(password1) === 'undefined') && password1 != null && password1.length > 0) {
                                        model.passwordsMatch = true;
                                    }
                                }
                                password1Element.controller( 'ngModel' ).$setValidity( 'passwords-match', model.passwordsMatch );
                                password2Element.controller( 'ngModel' ).$setValidity( 'passwords-match', model.passwordsMatch );
                                return model.passwordsMatch;
                            }
                            
                            password1Element.controller( 'ngModel' ).$setValidity( 'passwords-match', false );
                            password2Element.controller( 'ngModel' ).$setValidity( 'passwords-match', false );
                        }
                    }
                    
                    return false;
                };
                
                var setModel = $scope.setModel = function( value ) {
                    $scope.ngModel = value;
                };
            }],
            link: function(scope, element, attrs) {
                scope.$evalAsync(function(scope) {
                    // Register the Bootstrap popover
                    $('#' + scope.id).popover({
                        animation: false,
                        container: 'body',
                        trigger: 'manual',
                        placement: scope.config.popoverLocation,
                        title: scope.config.popoverTitle,
                        html: true,
                        content: '<ul style="list-style-type: none; padding: 0px; margin: 0px;">\
                                      <li>\
                                          <span ng-class="[\'glyphicon\', {\'glyphicon-ok text-success\': model.passwordsMatch}, {\'glyphicon-remove text-danger\': !model.passwordsMatch}]"></span>\
                                          <span ng-class="[{\'text-success\': model.passwordsMatch}, {\'text-danger\': !model.passwordsMatch}]" ng-bind="config.passwordsMatchText"></span>\
                                      </li>\
                                      <li ng-repeat="validation in model.complexityValidations">\
                                          <span ng-class="[\'glyphicon\', {\'glyphicon-ok text-success\': validation.isValid}, {\'glyphicon-remove text-danger\': !validation.isValid}]"></span>\
                                          <span ng-class="[{\'text-success\': validation.isValid}, {\'text-danger\': !validation.isValid}]" ng-bind="validation.message"></span>\
                                      </li>\
                                  </ul>',
                        template: '<div class="popover" role="tooltip">\
                                       <div class="arrow"></div>\
                                       <h3 class="popover-title"></h3>\
                                       <div id="' + scope.id + 'Popover" class="popover-content"></div>\
                                   </div>'
                    });
                    
                    $('#' + scope.id).on('inserted.bs.popover', function() {
                        var popoverElementContents = angular.element('#' + scope.id + 'Popover').first().contents();
                        $compile(popoverElementContents)(scope);
                        var popover = $('#' + scope.id + 'Popover').first().parent();
                        popover.css('visibility', 'hidden');
                    });
                    
                    $('#' + scope.id).on('shown.bs.popover', function() {
                        $interval( function() {
                            var target = $('#' + scope.id).first();
                            var targetPosition = target.offset();
                            var popover = $('#' + scope.id + 'Popover').first().parent();
                            
                            var newX = targetPosition.left + target.width() + 10;
                            var newY = targetPosition.top + (target.height() / 2);
                            newY = newY - (popover.height() / 2);
                            
                            popover.offset({ top: newY, left: newX });
                            
                            popover.css('visibility', 'visible');
                        }, 20);
                    });
                    
                    // Show the popover when Password1 is focused and hide when its blurred
                    $('#' + scope.passwordId).focus(function(e){
                        if(scope.enabled) {
                            $('#' + scope.id).popover( 'show' );
                        }
                    });
                    $('#' + scope.passwordId).blur(function(e){
                        if(scope.enabled) {
                            $('#' + scope.id).popover( 'hide' );
                        }
                    });
                    
                    // Show the popover when Password2 is focused and hide when its blurred
                    $('#' + scope.confirmPasswordId).focus(function(e){
                        if(scope.enabled) {
                            $('#' + scope.id).popover( 'show' );
                        }
                    });
                    $('#' + scope.confirmPasswordId).blur(function(e){
                        if(scope.enabled) {
                            $('#' + scope.id).popover( 'hide' );
                        }
                    });
                });
            },
            template: "\
                <div id=\"{{id}}\">\
                    <div class=\"form-group\"> \
                        <label for=\"{{passwordId}}\" ng-class=\"[ 'col-sm-3 control-label', { 'required': enabled }]\" ng-bind=\"config.passwordLabel1\"></label> \
                        <div class=\"col-sm-9\"> \
                            <input type=\"password\" id=\"{{passwordId}}\" name=\"{{passwordId}}\" ng-model=\"model.password1\" class=\"form-control\" ng-readonly=\"!enabled\" ng-required=\"enabled\" maxlength=\"{{ maxLength }}\" ng-maxlength=\"maxLength\" ng-minlength=\"minLength\" /> \
                        </div> \
                    </div> \
                    \
                    <div class=\"form-group\"> \
                        <label for=\"{{confirmPasswordId}}\" ng-class=\"[ 'col-sm-3 control-label', { 'required': enabled }]\" ng-bind=\"config.passwordLabel2\"></label> \
                        <div class=\"col-sm-9\"> \
                            <input type=\"password\" id=\"{{confirmPasswordId}}\" name=\"{{confirmPasswordId}}\" ng-model=\"model.password2\" class=\"form-control\" ng-readonly=\"!enabled\" ng-required=\"enabled\" maxlength=\"{{ maxLength }}\" ng-maxlength=\"maxLength\" ng-minlength=\"minLength\" /> \
                        </div> \
                    </div>\
                </div>\
            "
        };
    }]);
});