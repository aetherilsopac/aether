//@formatter:off
/*******************************************************************************
 * ####################### 
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB 
 * Directory: content/js 
 * File: util.js
 * 
 * ####################### 
 * #     MIT LICENSE     # 
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * ####################### 
 * #       Purpose       # 
 * ####################### 
 * Provides utility functions for a variety of tasks in an Angular Application.
 * 
 * ####################### 
 * #       Revision      #
 * ####################### 
 * Jun 11, 2016, Clinton Bush, 1.0.0, 
 *      New file.
 * 
 * **************************************************************************************************************************************************************
 */
// @formatter:on
(function(window) {
    
    /**
     * Utility class providing utility methods and enumerations.
     */
    if(window.Utility) {
        console.log("Utility class already loaded.");
    }
    else {
        var Utility = {
            /**
             * Default JSON error message.
             */
        	defaultJsonError: "We are sorry, the system has encountered an error.",
        	
            /**
             * Alert types that can be used with the showAlert() method.
             */
            alertTypes: {
                            "danger": "alert-danger",
                            "warning": "alert-warning",
                            "success": "alert-success",
                            "info": "alert-info",
                            "primary": "alert-info"
                        },
            
            /**
             * This serves as an the JavaScript counterpart of the
             * JsonResponseModelStatus enum in the .NET app.
             */
            jsonResponse: {
                              SUCCESS: "SUCCESS",
                              INVALID: "INVALID",
                              EXCEPTION: "EXCEPTION"
                          },
            
            /**
             * Array of key-value pair objects where the key is the ID to be
             * used for the hover alert and the value is an object with the
             * following properties: parentId: ID of the parent container for
             * the hover alert. alertType: Use the Utility.alertTypes
             * enumeration by providing one of its keys. This will determine the
             * style used for the hover alert. left: CSS position used to
             * position the hover alert. right: See left. top: See left. bottom:
             * See left. visible: Boolean that determines if this should be
             * visible. tooltip: Text shown when the hover alert is hovered over
             * by the user. forId: Indicates an element the hover alert
             * represents. callback: Function executed when the user clicks the
             * hover alert. Usually, this will be an action that takes the user
             * to the element the hover alert represents.
             */
            hoverAlerts: [],

            /**
             * Show a Twitter Bootstrap alert within the specified parent
             * container.
             * 
             * @param parentId
             *            ID of the parent container for the new alert.
             * @param alertType
             *            Type of alert to display (as specified by
             *            Utility.alertTypes).
             * @param isCloseable
             *            Indicates if the alert should display a close icon for
             *            the user.
             * @param autoHideDuration
             *            Set to 0 to disable auto-hide. Any value greater than
             *            0 will be treated as the duration in seconds for which
             *            the alert should be shown.
             * @param alertBody
             *            Content of the alert.
             */
            showAlert: function(parentId, alertType, isCloseable, autoHideDuration, alertBody) {
                var parentContainer = $("#" + parentId);
                var bsAlert = null;
                var alertClass = null;
                var bsAlertId = "bootstrapAlert" + Math.ceil(Math.random() * 11);
                
                // Verify parent container exists
                if(parentContainer == null) {
                    throw "showAlert called with non-existent parentId [" + parentId + "].";
                }
                
                // Verify alertType is valid
                alertClass = Utility.alertTypes[alertType];
                if(alertClass == null) {
                    throw "showAlert called with invalid alertType [" + alertType + "].";
                }
                
                // Build the alert
                bsAlert = $(  "<div/>", 
                             {
                                 "id": bsAlertId,
                                 "class": "alert " + alertClass + " fade in",
                                 "role": "alert"
                             }
                          );
                
                // If it's closeable, add a close button
                if(isCloseable == true) {
                    var closeButton =   $(  "<button/>",
                                                    {
                                                "class": "close",
                                                "aria-label": "Close",
                                                "type": "button"
                                            }
                                        ).click(
                                            function () {
                                                bsAlert.alert('close');
                                            }
                                        );
                    
                    $(  "<span/>",
                        {
                            "aria-hidden": "true",
                            "text": "\u00D7"
                        }
                    ).appendTo(closeButton);
                    
                    closeButton.appendTo(bsAlert);
                }
                
                // Add the content to the alert
                bsAlert.append(alertBody);
                
                // Add the alert to the parent container
                bsAlert.appendTo(parentContainer);
                
                // If it's auto-hide
                if(autoHideDuration > 0) {
                    setTimeout( 
                            function () {
                                bsAlert.alert('close');
                            },
                            autoHideDuration * 1000    
                    );
                }
            },

            /**
             * Show a floating icon near the top of the screen to draw the
             * user's attention to some element on the screen.
             * 
             * @param alertId
             *            ID to be used for the hover alert.
             * @param parentId
             *            ID of the parent container for the new alert.
             * @param forId
             *            Indicates an element the hover alert represents.
             * @param alertType
             *            Type of alert to display (as specified by
             *            Utility.alertTypes).
             * @param tooltip
             *            Text to display as a tooltip when the user hovers over
             *            the hover alert.
             * @param callback
             *            Function to execute when the user clicks the hover
             *            alert.
             * @param data
             *            Optional data for miscellaneous use.
             */
            addHoverAlert: function (alertId, parentId, forId, alertType, tooltip, callback) {
                var screenOffset = 60;
                var alertSpacing = 40;

                var hoverAlert = {
                    "alertId": alertId,
                    "parentId": parentId,
                    "alertType": alertType,
                    "tooltip": tooltip,
                    "callback": callback
                };

                var hoverButton = $("<button/>");
                hoverButton.addClass("hover-alert btn");
                hoverButton.prop("id", alertId);
                hoverButton.attr("for", forId);
                hoverButton.tooltip({
                    "container": "#" + alertId + "-div",
                    "html": true,
                    "title": tooltip,
                    "placement": "left"
                });
                hoverButton.hide();

                var hoverButtonDiv = $("<div/>");
                hoverButtonDiv.prop("id", alertId + "-div");
                hoverButtonDiv.append(hoverButton);

                switch (alertType) {
                    case "danger": {
                        hoverButton.addClass("btn-danger");
                        hoverButton.append(
                            $("<span/>").addClass("glyphicon glyphicon-exclamation-sign")
                        );
                        break;
                    }
                    case "warning": {
                        hoverButton.addClass("btn-warning");
                        hoverButton.append(
                            $("<span/>").addClass("glyphicon glyphicon-warning-sign")
                        );
                        break;
                    }
                    case "success": {
                        hoverButton.addClass("btn-success");
                        hoverButton.append(
                            $("<span/>").addClass("glyphicon glyphicon-ok-sign")
                        );
                        break;
                    }
                    case "info": {
                        hoverButton.addClass("btn-info");
                        hoverButton.append(
                            $("<span/>").addClass("glyphicon glyphicon-info-sign")
                        );
                        break;
                    }
                    case "primary": {
                        hoverButton.addClass("btn-primary");
                        hoverButton.append(
                            $("<span/>").addClass("glyphicon glyphicon-info-sign")
                        );
                        break;
                    }
                    default: {
                        hoverButton.addClass("btn-default");
                        hoverButton.append(
                            $("<span/>").addClass("glyphicon glyphicon-comment")
                        );
                        break;
                    }
                }

                hoverButton.css("right", alertSpacing + "px");
                hoverAlert["right"] = alertSpacing;

                hoverButton.css("top", (((this.hoverAlerts.length + 1) * alertSpacing) + (this.hoverAlerts.length * hoverButton.height()) + screenOffset) + "px");
                hoverAlert["top"] = ((this.hoverAlerts.length + 1) * alertSpacing) + (this.hoverAlerts.length * hoverButton.height()) + screenOffset;

                hoverButton.click(function (e) {
                    e.preventDefault();
                    hoverAlert["callback"]();
                    return false;
                });

                hoverAlert["left"] = null;
                hoverAlert["bottom"] = null;
                hoverAlert["visible"] = true;

                this.hoverAlerts.push(hoverAlert);
                $("*[ng-view]").first().append(hoverButtonDiv);
                hoverButton.fadeIn(500);
                hoverButton.mouseover(function (e) {
                    hoverButton.fadeTo(200, 1.0);
                });
                hoverButton.mouseout(function (e) {
                    hoverButton.fadeTo(200, 0.5);
                });
            },

            /**
             * Remove a hover alert from the screen.
             * 
             * @param alertId
             *            ID set for the hover alert when it was added.
             */
            removeHoverAlert: function (alertId) {
                var hoverAlertIndex = null;
                var hoverAlertData = null;
                
                for (var index in Utility.hoverAlerts) {
                    var nextStruct = Utility.hoverAlerts[index];

                    if (nextStruct["alertId"] == alertId) {
                        hoverAlertData = nextStruct;
                        hoverAlertIndex = index;
                        break;
                    }
                }

                if (hoverAlertIndex != null && hoverAlertData != null) {
                    Utility.hoverAlerts.splice(hoverAlertIndex, 1);

                    $("#" + alertId).fadeOut(500, function () {
                        $("#" + alertId).remove();
                    });
                }
                else {
                    throw "Could not find hover alert for ID [" + alertId + "].";
                }
            },

            clearAlert: function(parentId) {
                var parentContainer = $("#" + parentId);
                parentContainer.first().children(".alert").each(function (index, element) {
                    var alertElement = $(element);
                    
                    // Remove any hover alerts
                    $("*[for='" + alertElement.prop("id") + "'].hover-alert").each(function (index, hover) {
                        Utility.removeHoverAlert($(hover).prop("id"));
                    });

                    alertElement.remove();
                });
            },

            /**
             * Cycles through a form to find AngularJS, declarative validation
             * errors. This method also returns a value of true if no errors are
             * found or false, otherwise.
             * 
             * @param $scope
             *            ngScope for the Controller of the view being
             *            displayed.
             * @param formName
             *            Name of the form to check for errors.
             * @param containerId
             *            If you want to restrict the error checking to a
             *            particular container within the form, provide that
             *            container's ID.
             * @returns {Array}
             */
            isAngularFormValid: function ($scope, formName, containerId) {
                var ngForm = $scope[formName];
                var formField = null;
                var fieldName = null;
                var form = null;
                var parentContainer = null;

                form = $("form[name='" + formName + "']");
                if (form == null) {
                    return [];
                }

                containerId = (typeof (containerId) !== 'undefined') ? containerId : null;
                if (containerId != null) {
                    parentContainer = form.find("#" + containerId).first()

                    if (parentContainer == null) {
                        return [];
                    }
                }

                for (fieldName in ngForm) {
                    // Only check our form fields (ignore Angular properties)
                    if (fieldName.charAt(0) != '$') {
                        var field = null,
            	            foundFields = null;

                        if (parentContainer != null) {
                            foundFields = parentContainer.find("*[name='" + fieldName + "']");
                            if (foundFields.length > 0) field = foundFields.first();
                        }
                        else {
                            field = form.find("*[name='" + fieldName + "']").first();
                        }

                        if (field == null) {
                            continue;
                        }

                        formField = ngForm[fieldName];

                        // Determine which errors have been identified for the
                        // field
                        for (errorKey in formField.$error) {
                            if (formField.$error[errorKey]) {
                                return false;
                            }
                        }

                    }
                }

                return true;
            },
            
            /**
             * Cycles through a form to find AngularJS, declarative validation
             * errors. This method will mark these fields as "ng-touched" so our
             * custom CSS applies the .ng-invalid.ng-touched error style to the
             * fields. This method also returns an array containing the name of
             * each erroneous field and either a generic error message or a
             * custom error message if one is provided in the errorMessages
             * array below.
             * 
             * @param formName
             *            Name of the form to check for errors.
             * @param containerId
             *            If you want to restrict the error checking to a
             *            particular container within the form, provide that
             *            container's ID.
             * @returns {Array}
             */
            getAngularFormErrors: function (formName, containerId) {
                var form = $("form[name='" + formName + "']").first();
                var ngForm = form.scope()[formName];
            	var errorMessages = new Array();
            	var fieldName = null;
            	var formField = null;
            	var fieldLabel = null;
    			var errorMessage = null;
    			var errorKey = null;
    			var parentContainer = null;

    			if (form == null) {
    			    return [];
    			}
    			
    			containerId = (typeof (containerId) !== 'undefined') ? containerId : null;
    			if (containerId != null) {
    			    parentContainer = form.find("#" + containerId).first()

    			    if (parentContainer == null) {
    			        return [];
    			    }
    			}
    			
    			for (fieldName in ngForm) {
            		// Only check our form fields (ignore Angular properties)
    			    if (fieldName.charAt(0) != '$') {
            	        var field = null,
            	            foundFields = null;

            	        if (parentContainer != null) {
            	            foundFields = parentContainer.find("*[name='" + fieldName + "']");
            	            if (foundFields.length > 0) field = foundFields.first();
            	        }
            	        else {
            	            field = form.find("*[name='" + fieldName + "']").first();
            	        }

            	        if (field == null) {
            	            continue;
            	        }
            	        
            	        formField = ngForm[fieldName];
            	        
            			// Determine which errors have been identified for the
                        // field
            			for (errorKey in formField.$error) {
            				if(formField.$error[errorKey]) {
            					// Add the ng-touched class to the erroneous
                                // field
                                field.removeClass("ng-untouched").addClass("ng-touched");
            					
            					// Attempt to identify the label associated with
                                // this field;
            					// if one cannot be found, use the field's name
                                // as the label.
            					var matchingLabels = $("label[for='" + field.attr("id") + "']");
            					if(matchingLabels.length > 0) {
            						fieldLabel = matchingLabels.first().text();
            					}
            					else {
            						fieldLabel = fieldName;
            					}
            					
            				    // Create the Error Messages
            					
                                // 1. See if we have specified a custom error
                                // message for this field
            					if(
        							typeof(Utility.errorMessages[fieldName]) !== 'undefined' &&
        							typeof(Utility.errorMessages[fieldName][errorKey]) !== 'undefined'
            					){
            					    errorMessage = Utility.errorMessages[fieldName][errorKey];
            					}
                                
                                // 2. See if ASP.NET MVC has provided an error
                                // message for us
            					else if (
                                    typeof (Utility.angularToUnobtrusiveAttrs[errorKey]) !== 'undefined' &&
                                    typeof (field.attr(Utility.angularToUnobtrusiveAttrs[errorKey])) !== 'undefined'
                                ){
            					    errorMessage = field.attr(Utility.angularToUnobtrusiveAttrs[errorKey]);
            					}
                                
                                // 3. If all else fails, try for a default error
                                // message
            					else if (typeof (Utility.defaultErrorMessages[errorKey]) !== 'undefined') {
            					    if ($.isFunction(Utility.defaultErrorMessages[errorKey])) {
            					        errorMessage = Utility.defaultErrorMessages[errorKey](field);
            					    }
            					    else {
            					        errorMessage = Utility.defaultErrorMessages[errorKey];
            					    }
            					}

            				    // 4. Abort
            					else {
            					    continue;
            					}
            					
            					errorMessages.push({"field": fieldName, "label" : fieldLabel, "error": errorKey, "message": errorMessage});
            				}
            			}
            			
            		}
        		}
    			
            	return errorMessages;
            },

            /**
             * If you're using ASP.NET MVC, this will display the error messages
             * in the error message <span></span> elements ASP.NET generates
             * for each of the form fields.
             * 
             * @param errorMessages
             *            The list of error messages generated by
             *            Utility.getAngularFormErrors().
             * @param formName
             *            Name attribute's value for the form the fields are
             *            inside of.
             */
            applyAngularErrorsToAspFields: function (errorMessages, formName) {
                var form = $("form[name='" + formName + "']").first();
                
                // Clear out all old error messages
                form.find("*[data-valmsg-for]").each(
                    function (index, element) {
                        $(element).html("");
                    }
                )

                // Apply new error messages
                for (var msg in errorMessages) {
                    var field = form.find("*[name='" + errorMessages[msg]["field"] + "']").first();
                    var messageField = form.find("*[data-valmsg-for='" + errorMessages[msg]["field"] + "']").first();

                    if (messageField !== 'undefined' && messageField != null) {
                        messageField.html("<br/><strong>" + errorMessages[msg]["label"] + "</strong> " + errorMessages[msg]["message"]);
                    }
                }
            },
            
            /**
             * Removes the ng-touched class and resets the error flags on
             * erroneous fields.
             * 
             * @param $scope
             * @param formName
             */
            resetAngularFormErrors: function($scope, formName) {
            	var ngForm = $scope[formName];
            	ngForm.$setPristine();
            	ngForm.$setUntouched();

            	var form = $("form[name='" + formName + "']");

                // Clear out all old error messages
            	form.find("*[data-valmsg-for]").each(
                    function (index, element) {
                        $(element).html("");
                    }
                )
            },

            /**
             * Retrieves Angular form errors, applys them to ASP error fields,
             * displays in the specified message container, and generates a
             * hover alert. If validation fails, returns false.
             * 
             * @param formName
             * @param messageContainerId
             * @return True if validation is successful; otherwise, false.
             */
            validateForm: function (formName, messageContainerId) {
                var errorMessages = this.getAngularFormErrors(formName);

                this.applyAngularErrorsToAspFields(errorMessages, formName);

                var errorMessage = "<ul>";
                for (var msg in errorMessages) {
                    errorMessage += "<li><strong>" + errorMessages[msg]["label"] + "</strong> " + errorMessages[msg]["message"] + "</li>";
                }
                errorMessage += "</ul>";

                this.clearAlert(messageContainerId);

                if (errorMessages.length > 0) {
                    this.showAlert(messageContainerId, "danger", false, 0, errorMessage);
                }

                return (errorMessages.length == 0);
            },

            /**
             * Displays a Bootstrap large dialog with a print preview IFRAME and
             * a print button.
             * 
             * @param url
             *            URL of the page/document to be printed.
             */
            showPrintDialogFor: function(printUrl) {
                var dialogWrapper = $("<div/>")
                                        .addClass("modal fade")
                                        .css({ "display": "block" })
                                        .prop("id", "printPreviewModal");
                    dialogWrapper.attr({
                        "aria-hidden": "true",
                        "aria-labelledby": "printPreviewModalTitle",
                        "role": "dialog"
                    });

                var dialogBlock = $("<div/>").addClass("modal-dialog modal-lg");
                var dialogContent = $("<div/>").addClass("modal-content");

                // Buid the Header
                var dialogHeader = $("<div/>").addClass("modal-header");
                        var dialogTitle = $("<h4/>")
                                                .prop("id", "printPreviewModalTitle")
                                                .addClass("modal-title")
                                                .text("Print Preview");
                        var dialogCloseButton = $("<button/>")
                                                .addClass("close")
                                                .html("<span aria-hidden='true'>&times;</span>");
                        dialogCloseButton.attr({
                            "aria-label": "Close",
                            "data-dismiss": "modal",
                            "type": "button"
                        });
                        
                        dialogHeader
                            .append(dialogCloseButton)
                            .append(dialogTitle);

                // Build the Body
                var dialogBody = $("<div/>").addClass("modal-body");
                        var iframe = $("<iframe/>")
                                        .css({
                                            "border": "none",
                                            "width": "100%",
                                            "height": "500px"
                                        })
                                        .prop("id", "printPreviewModalIframe");
                            iframe.attr({
                                "src": printUrl
                            });

                        dialogBody.append(iframe);

                // Build the Footer
                var dialogFooter = $("<div/>").addClass("modal-footer");
                var footerCloseButton = $("<button/>")
                                        .addClass("btn btn-default")
                                        .html("Close");
                footerCloseButton.attr({
                    "aria-label": "Close",
                    "data-dismiss": "modal",
                    "type": "button"
                });
                var footerPrintButton = $("<button/>")
                                        .addClass("btn btn-primary")
                                        .html("<span class='glyphicon glyphicon-print'></span>&nbsp;&nbsp;Print");
                footerPrintButton.click(function (e) {
                    $("#printPreviewModalIframe").get(0).contentWindow.focus();
                    $("#printPreviewModalIframe").get(0).contentWindow.print();
                });

                dialogFooter
                    .append(footerCloseButton)
                    .append(footerPrintButton);

                dialogContent
                    .append(dialogHeader)
                    .append(dialogBody)
                    .append(dialogFooter);

                dialogBlock.append(dialogContent);

                dialogWrapper.append(dialogBlock);

                // Add to the BODY
                $("body").first().append(dialogWrapper);

                // Remove it from the DOM when it is hidden
                $("#printPreviewModal").on("hidden.bs.modal", function (e) {
                    $("#printPreviewModal").remove();
                });

                // Show it!
                $("#printPreviewModal").modal("show");
            },

            /**
             * Submit data to the server at the specified URL via POST.
             * 
             * @param url
             *            The URL to submit the data to.
             * @param data
             *            The data to submit.
             */
            postData: function (url, data) {
                var form = $('#' + this.formId);
                
                if (form.length < 1) {
                    $('body').append($('<form/>', {
                        "id": this.formId,
                        "method": 'POST',
                        "action": url
                    }));
                }
                else {
                    form.first().attr('action', url);
                }

                form.children("*").remove("*");

                for(var i in data){
                    $('#' + this.formId).append($('<input/>', {
                        "type": 'hidden',
                        "name": i,
                        "value": data[i]
                    }));
                }

                $('#' + this.formId).submit();
            },
            formId: "hiddenForm" + Math.floor(Math.random() * 10000),
            
            /**
             * Map Angular JS error keys to jQuery Unobtrusive Validation
             * Attributes
             */
            angularToUnobtrusiveAttrs: {
                "required": "data-val-required",
                "minlength": "data-val-length",
                "maxlength": "data-val-length",
                "number": "data-val-number",
                "date": "data-val-date",
                "email": "data-val-email",
                "url": "data-val-url",
                "parse": "data-val-parse",
                "min": "data-val-number-min",
                "max": "data-val-number-max",
                "pattern": "data-val-regex",
                "passwords-match": "data-val-passwords-match",
                "password-complexity": "data-val-password-complexity"
            },


            /**
             * Map of error messages keyed by field names and error types.
             */
            errorMessages: {
            	// "State": {
            	// "maxlength": "has a maximum length of 2."
            	// },

            	// "Email": {
                // "email": "must be a valid e-mail address."
            	// }
            },
            
            /**
             * Map of default error messages.
             */
            defaultErrorMessages: {
                "required": "is a required field.",
                "pattern": "must match the specified pattern.",
                "minlength": function (field) {
                    return "must be at least " + field.attr("minlength") + " characters in length.";
                },
                "maxlength": function (field) {
                    return "must not exceed " + field.attr("maxlength") + " characters in length.";
                },
                "number": "must be a number.",
                "date": "must be a valid date.",
                "email": "must be a valid e-mail address.",
                "url": "must be a valid URL.",
                "min": function (field) {
                    return "must be at least " + field.attr("min") + ".";
                },
                "max": function (field) {
                    return "must not exceed " + field.attr("max") + ".";
                },
                "passwords-match": "must match password.",
                "password-complexity": "Password complexity requirement has not been met."
            },

            /**
             * If the specified element exists in the given array, returns the
             * index of that element; otherwise, -1 is returned.
             * 
             * @param val
             *            The value to search for.
             * @param arr
             *            The array to search.
             */
            inArray: (Array.prototype.indexOf) ?
                function (val, arr) {
                    return  arr.indexOf(val);
                } :
                function (val, arr) {
                    var i = arr.length;
                    while(i--) {
                        if(arr[i] === val) return i;
                    }
                    return -1;
                },

            /**
             * Returns true if the string provided is undefined, null, or blank.
             */
            isBlank: function(str) {
                return (typeof(str) === 'undefined' || str == null || str == '');
            },

            /**
             * DETECT BROWSER INFORMATION This method detects which browser the
             * user is using and the version of that browser. Thanks to Leo Liu
             * at Microsoft, I was able to modify the code to also test for the
             * use of Compatibility Mode.
             * 
             * @author Danail Gabenski
             * @url http://www.stackoverflow.com/questions/5916900/how-can-you-detect-the-version-of-a-browser
             * @author Leo Liu
             * @url https://social.msdn.microsoft.com/Forums/vstudio/en-US/ae715fd2-1ddd-46f7-8c26-9aed6b2103f1/how-to-detect-compatibility-mode-in-ie-any-version?forum=netfxjscript
             * @returns A "_browser" object with a property "browser" set to the
             *          lowercase abbreviation for the browser being used, a
             *          "version" property set to the FLOAT form of the version
             *          number, and a "compatibilityMode" property set to the
             *          true if the user is using IE and it is in IE 7.0
             *          Compatibility Mode.
             */
            detectBrowser: function() {
                var uagent = navigator.userAgent.toLowerCase(),
                    match = '',
                    _browser = {};

                _browser.chrome  = /webkit/.test(uagent)  && /chrome/.test(uagent);
                _browser.firefox = /mozilla/.test(uagent) && /firefox/.test(uagent);
                _browser.msie    = /msie/.test(uagent)    || /trident/.test(uagent);
                _browser.safari  = /safari/.test(uagent)  && /applewebkit/.test(uagent) && !/chrome/.test(uagent);
                _browser.opr = /mozilla/.test(uagent) && /applewebkit/.test(uagent) && /chrome/.test(uagent) && /safari/.test(uagent) && /opr/.test(uagent);
                _browser.browser = "";
                _browser.version = 0.0;
                _browser.compatibilityMode = false;

                for (x in _browser) {
                    if (_browser[x]) {
                        _browser.browser = x;
                        match = uagent.match(new RegExp("(" + x + ")( |/)([0-9]+)"));
                        if (match) {
                            _browser.version = parseFloat(match[3]);

                            // Test for Compatibility Mode
                            if(_browser.version == 7.0) {
                                if(/trident/.test(uagent)) {
                                    var tridentVersion = uagent.match(new RegExp("trident/([0-9]+)"));

                                    if(tridentVersion[1] !== 'undefined' && tridentVersion[1] != null) {
                                        tridentVersion = parseFloat(tridentVersion[1]);
                                    }
                                    else {
                                        tridentVersion = -1;
                                    }

                                    _browser.compatibilityMode = (tridentVersion >= 4.0) ? true : false;
                                }
                            }
                        } else {
                            match = uagent.match(new RegExp("rv:([0-9]+)"));
                            if (match) {
                                _browser.version = match[1];
                            }
                        }
                        break;
                    }
                }

                _browser.opera = _browser.opr;
                delete _browser.opr;
                return _browser;
            },

            testBrowserCompatibility: function () {
                var browserDetails = Utility.detectBrowser();

                if (browserDetails.browser == "msie" && (browserDetails.version < 9.0 || browserDetails.compatibilityMode)) {
                    // Show Browser Compatibility link
                    $(
                        function () {
                            var body = $("BODY");

                            var div = $("<DIV/>");
                            div.css({
                                    "border": "1px solid #FF0000",
                                    "width": "25%",
                                    "margin-right": "auto",
                                    "margin-left": "auto",
                                    "text-align": "center"
                                });

                            var link = $("<A/>");
                            link.css("font-weight", "bold");
                            link.attr("href", "BrowserCompatibility");
                            link.text("BROWSER COMPATIBILITY");

                            div.append(link);
                            body.append(div);

                            alert(
                                "\t\tINCOMPATIBLE BROWSER\n\n" +
                                "You are accessing C3S from an incompatible browser! You may be using an outdated version of IE (less than 9.0) or you may have \"Compatibility View\" mode enabled.\n\n" +
                                "BENESUGGS will not appear or function correctly until you resolve this issue.\n\n" +
                                "For more information, please follow the \"Browser Compatibility\" link found at the bottom of this page."
                            );
                        }
                    );
                }
            },
            
            /**
             * Convenience method that prepends the provided page name to the specified key and stores the value in JavaScript's
             * session storage.
             * @param pageName A unique name representing the page you wish to associate the stored value with.
             * @param key A key unique to the value you wish to store.
             * @param value Any value or object to be stored.
             */
            putPageSessionItem: function( pageName, key, value ) {
                if(value == null) {
                    console.warn("util.js: NULL is being stored for key [" + key + "].");
                }
                
                window.sessionStorage.setItem( pageName + ":" + key, JSON.stringify(value) );
            },
            
            /**
             * Convenience method that prepends the provided page name to the specified key and retrieves the value represented
             * by this combination from JavaScript's session storage.
             * @param pageName A unique name representing the page the requested value is associated with.
             * @param key A key unique to the value you wish to retrieve.
             * @returns The stored value or null if the value is not found. 
             */
            getPageSessionItem: function ( pageName, key ) {
                var value = window.sessionStorage.getItem( pageName + ":" + key );
                
                if(value == null) {
                    console.warn("util.js: No value was found in SESSION STORAGE for key [" + key + "].");
                }
                
                return JSON.parse(value);
            },
            
            /**
             * Parses the query string of the current URL into a map of key/value pairs. 
             * @returns {Object} Map of key/value pairs extracted from the current URL's query string.
             */
            getUrlQueryParams: function() {
                var search = window.location.search;
                var keyValueMap = new Object();
                
                if( search[0] == '?' ) {
                    search = search.substr( 1 );
                    
                    var keyValuePairs = search.split( '&' );
                    keyValuePairs.forEach( function( nextPair ) {
                        var nextPairSplit = nextPair.split( '=' );
                        keyValueMap[ nextPairSplit[0] ] = nextPairSplit[1];
                    });
                    
                    return keyValueMap;
                }
                
                return keyValueMap;
            }
            
        };
        
        // expose globally
        window.Utility = Utility;

        // do browser compatibility test
        Utility.testBrowserCompatibility();
    }
})(window);