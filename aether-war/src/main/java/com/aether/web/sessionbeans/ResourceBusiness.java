//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.sessionbeans
 * File: ResourceBusiness.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jan 10, 2017, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.sessionbeans;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.aether.ejb.common.Constants;
import com.aether.web.common.Utility;
import com.aether.web.common.WebConstants;
import com.aether.web.models.FileMinimized;

@Component
@Scope( scopeName = "session", proxyMode = ScopedProxyMode.TARGET_CLASS )
public class ResourceBusiness implements Serializable
{
	/**
	 * GUID for Serializable.
	 */
	private static final long serialVersionUID = -1927423339744574862L;
	
	// Pattern for testing a file name to see if it ends in an approved image
	// file extension
	private static Pattern IMAGE_FILE_NAMES_PATTERN = Pattern.compile( "^.*?(\\.)(" + String.join( "|", Constants.IMAGE_FILE_EXTENSIONS ) + ")$", Pattern.CASE_INSENSITIVE );
	
	@Autowired
	private ServletContext servletContext;
	
	private static Logger log = LogManager.getLogger( ResourceBusiness.class );
	
	public File getResourceByFileName( String fileName )
	{
		// Attempt to locate the file
		Path resourcePath = Paths.get( Constants.DIRECTORY_SYSTEM_RESOURCES.toString(), fileName );
		File resourceFile = resourcePath.toFile();
		
		if ( resourceFile.exists() )
		{
			return resourceFile;
		}
		
		return null;
	}
	
	public File getResourceThumbnailByFileName( String fileName )
	{
		// Attempt to locate the file
		Path resourcePath = Paths.get( Constants.DIRECTORY_SYSTEM_RESOURCES_THUMBNAILS.toString(), fileName );
		File resourceFile = resourcePath.toFile();
		
		if ( resourceFile.exists() )
		{
			return resourceFile;
		}
		
		return null;
	}
	
	public String addResource( String fileName, byte[] data )
	{
		String fileLocation = "";
		
		// Parse out the file name and extension
		Matcher matcher = WebConstants.FILE_NAME_PATTERN.matcher( fileName );
		
		// Generate a timestamp to ensure each file gets a unique name
		SimpleDateFormat dateFormatter = new SimpleDateFormat( "yyyyMMddkkmmss" );
		
		// If a file was provided, save it to disk
		if ( data != null && data.length > 0 && matcher.matches() )
		{
			// Generate a new file name
			String newFileName = matcher.group( 1 ) + "-" + dateFormatter.format( new Date() ) + "." + matcher.group( 2 );
			
			// Create path to resource file
			Path filePath = Paths.get( Constants.DIRECTORY_SYSTEM_RESOURCES.toString(), newFileName );
			File newFile = filePath.toFile();
			
			// Create path to thumbnail for custom lo
			Path thumbnailPath = Paths.get( Constants.DIRECTORY_SYSTEM_RESOURCES_THUMBNAILS.toString(), newFileName );
			File newThumbnail = thumbnailPath.toFile();
			
			try
			{
				// If the file doesn't exist, create it
				if ( !newFile.exists() ) newFile.createNewFile();
				
				// Write the file
				FileOutputStream fos = new FileOutputStream( newFile );
				fos.write( data, 0, data.length );
				fos.flush();
				fos.close();
				
				// If this is an image file, create the thumbnail
				if ( Utility.isImage( newFileName ) )
				{
					// If the thumbnail doesn't exist, create it
					if ( !newThumbnail.exists() ) newThumbnail.createNewFile();
					
					// Write the thumbnail
					BufferedImage newThumbnailImg = Utility.scaleImage( ImageIO.read( newFile ), WebConstants.MAX_THUMBNAIL_WIDTH, WebConstants.MAX_THUMBNAIL_HEIGHT );
					FileOutputStream thumbnailFos = new FileOutputStream( newThumbnail );
					ImageIO.write( newThumbnailImg, matcher.group( 2 ), thumbnailFos );
					fos.flush();
					fos.close();
				}
				
				fileLocation = servletContext.getContextPath() + "/Resources/" + newFileName;
			}
			catch ( IOException e )
			{
				ResourceBusiness.log.error( "An exception was encountered while trying to upload a new resource file.", e );
				return null;
			}
			
			return fileLocation;
		}
		
		return null;
	}
	
	public List<String> listImageResourceNames()
	{
		List<String> imageFileNames = null;
		
		try
		{
			// Get list of all files in resource directory
			File resourceDirectory = Constants.DIRECTORY_SYSTEM_RESOURCES.toFile();
			
			// Get a list of all image files in the Resources directory
			File[] resourceDirectoryContents = resourceDirectory.listFiles( new FileFilter() {
				
				@Override
				public boolean accept( File file )
				{
					return ResourceBusiness.IMAGE_FILE_NAMES_PATTERN.matcher( file.getName() ).matches();
				}
				
			} );
			
			// Map the array of File objects to a List of their names
			imageFileNames = Arrays.asList( resourceDirectoryContents ).stream().map( ( f ) -> {
				return f.getName();
			} ).collect( Collectors.toList() );
			
			return imageFileNames;
		}
		catch ( Exception e )
		{
			ResourceBusiness.log.error( "Exception encountered while listing images in the Resources directory.", e );
			return new ArrayList<String>( 0 );
		}
	}
	
	public List<String> listResourceNames()
	{
		List<String> fileNames = null;
		
		try
		{
			// Get list of all files in resource directory
			File resourceDirectory = Constants.DIRECTORY_SYSTEM_RESOURCES.toFile();
			
			// Get a list of all image files in the Resources directory
			File[] resourceDirectoryContents = resourceDirectory.listFiles( (FileFilter) FileFileFilter.FILE );
			
			// Map the array of File objects to a List of their names
			fileNames = Arrays.asList( resourceDirectoryContents ).stream().map( ( f ) -> {
				return f.getName();
			} ).collect( Collectors.toList() );
			
			return fileNames;
		}
		catch ( Exception e )
		{
			ResourceBusiness.log.error( "Exception encountered while listing files in the Resources directory.", e );
			return new ArrayList<String>( 0 );
		}
	}
	
	public List<FileMinimized> listResources()
	{
		List<FileMinimized> minimizedFiles = null;
		
		try
		{
			// Get list of all files in resource directory
			File resourceDirectory = Constants.DIRECTORY_SYSTEM_RESOURCES.toFile();
			
			// Get a list of all image files in the Resources directory
			File[] resourceDirectoryContents = resourceDirectory.listFiles( (FileFilter) FileFileFilter.FILE );
			
			// Map the array of File objects to a List of their names
			minimizedFiles = Arrays.asList( resourceDirectoryContents ).stream().map( ( f ) -> {
				return new FileMinimized( f );
			} ).collect( Collectors.toList() );
			
			return minimizedFiles;
		}
		catch ( Exception e )
		{
			ResourceBusiness.log.error( "Exception encountered while listing files in the Resources directory.", e );
			return new ArrayList<FileMinimized>( 0 );
		}
	}
	
	public boolean deleteResource( String fileName )
	{
		// Create path to resource file
		Path filePath = Paths.get( Constants.DIRECTORY_SYSTEM_RESOURCES.toString(), fileName );
		File file = filePath.toFile();
		
		// Create path to thumbnail of resource
		Path thumbnailPath = Paths.get( Constants.DIRECTORY_SYSTEM_RESOURCES_THUMBNAILS.toString(), fileName );
		File thumbnail = thumbnailPath.toFile();
		
		// If a file was provided, save it to disk
		if ( file != null && file.exists() )
		{
			try
			{
				if ( thumbnail.exists() ) thumbnail.delete();
				return file.delete();
			}
			catch ( SecurityException e )
			{
				ResourceBusiness.log.error( "An exception was encountered while trying to delete a resource file.", e );
				return false;
			}
		}
		
		return false;
	}
}
