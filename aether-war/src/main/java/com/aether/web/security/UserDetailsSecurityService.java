//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherEJB
 * Package: com.aether.ejb.services.security
 * File: UserDetailsSecurityService.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides DAO service for Spring authentication and authorization
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Apr 1, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.security;

import java.util.ArrayList;
import javax.ejb.EJB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.aether.ejb.beans.interfaces.UserBusinessLocal;
import com.aether.ejb.models.Permission;

@Service
public class UserDetailsSecurityService implements UserDetailsService
{
    private static Logger log = LogManager.getLogger( UserDetailsSecurityService.class );

    @EJB(mappedName = "java:global/aether-ear/aether-ejb/UserBusiness")
    private UserBusinessLocal userEjb;

    @Override
    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException
    {
        User userDetails = convertAetherUserToSpringUser( userEjb.getUserByUserName( username ) );
        if( userDetails == null ) throw new UsernameNotFoundException( "No user found for username [" + username + "]." );
        if( !( UserDetails.class.isAssignableFrom( userDetails.getClass() ) ) )
        {
            UserDetailsSecurityService.log.error( "userDetails does not implement the UserDetails interface." );
        }
        return userDetails;
    }

    public User convertAetherUserToSpringUser( com.aether.ejb.models.User user )
    {
        if( user == null ) return null;

        ArrayList<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>( user.getPermissions().size() );
        for ( Permission nextPermission : user.getPermissions() )
        {
            authorities.add( new SimpleGrantedAuthority( nextPermission.getPermissionName() ) );
        }

        //@formatter:off
        return new User( 
                user.getUserName(),
                user.getPasswordHash(),
                user.isEnabled(), 
                user.isAccountNonExpired(), 
                user.isCredentialsNonExpired(), 
                user.isAccountNonLocked(),
                authorities );
        //@formatter:on
    }

    public void setUserEjb( UserBusinessLocal userEjb )
    {
        this.userEjb = userEjb;
    }

}