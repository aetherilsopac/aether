//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.singletonbeans
 * File: AetherInitializer.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Nov 11, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.singletonbeans;

import java.io.File;
import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import com.aether.ejb.common.Constants;

@Component
@Scope(scopeName = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AetherInitializer implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = -8869539509121670395L;

    private static Logger log = LogManager.getLogger( AetherInitializer.class );

    public AetherInitializer()
    {
        init();
    }

    /**
     * Do all system initialization work.
     */
    private void init()
    {
        File indexDirectory = Constants.DIRECTORY_INDEX_STORAGE.toFile();
        if( !indexDirectory.exists() && !indexDirectory.mkdir() )
        {
            AetherInitializer.log.error( "Could not create Aether Index directory at: " + Constants.DIRECTORY_INDEX_STORAGE.toString() );
        }

        File resourceDirectory = Constants.DIRECTORY_SYSTEM_RESOURCES.toFile();
        if( !resourceDirectory.exists() && !resourceDirectory.mkdir() )
        {
            AetherInitializer.log.error( "Could not create Aether Resources directory at: " + Constants.DIRECTORY_SYSTEM_RESOURCES.toString() );
        }
        
        File resourceThumbnailsDirectory = Constants.DIRECTORY_SYSTEM_RESOURCES_THUMBNAILS.toFile();
        if( !resourceThumbnailsDirectory.exists() && !resourceThumbnailsDirectory.mkdir() )
        {
        	AetherInitializer.log.error( "Could not create Aether Resources Thumbnails directory at: " + Constants.DIRECTORY_SYSTEM_RESOURCES_THUMBNAILS.toString() );
        }
    }
}