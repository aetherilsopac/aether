//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.sessionbeans
 * File: AetherGlobalSettings.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Oct 27, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.singletonbeans;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import com.aether.ejb.beans.interfaces.SystemSettingsBusinessLocal;
import com.aether.ejb.enums.SystemSettingName;
import com.aether.ejb.models.SystemSetting;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope(scopeName = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
//@formatter:off
@JsonAutoDetect(
    creatorVisibility = Visibility.NONE,
    fieldVisibility = Visibility.NONE,
    getterVisibility = Visibility.NONE, 
    setterVisibility = Visibility.NONE, 
    isGetterVisibility = Visibility.NONE)
//@formatter:ons
public class AetherSystemSettings implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = 2439793462450641410L;

    private static Logger log = LogManager.getLogger( AetherSystemSettings.class );

    @EJB(mappedName = "java:global/aether-ear/aether-ejb/SystemSettingsBusiness")
    private SystemSettingsBusinessLocal systemSettingsEjb;

    private List<SystemSetting> systemSettings = null;

    private String systemTitle = null;
    private String googleApiKey = null;
    private Boolean useCustomLogo = null;
    private Boolean systemAvailable = null;
    private String systemUnavailableMessage = null;
    private Date lastReindexDate = null;
    private Integer lastReindexRecordCount = 0;

    @PostConstruct
    public void init()
    {
        loadFromDatabase();
    }

    public void loadFromDatabase()
    {
        systemSettings = systemSettingsEjb.getAllSettings();

        for ( SystemSetting nextSetting : systemSettings )
        {
            switch ( SystemSettingName.valueOf( nextSetting.getName() ) )
            {
                case SYSTEM_TITLE:
                    setSystemTitle( nextSetting.getValue() );
                    break;

                case GOOGLE_API_KEY:
                    if( nextSetting.getValue() != null ) setGoogleApiKey( nextSetting.getValue() );
                    break;

                case CUSTOM_LOGO:
                    setUseCustomLogo( Boolean.valueOf( nextSetting.getValue() ) );
                    break;

                case SYSTEM_AVAILABLE:
                    setSystemAvailable( Boolean.valueOf( nextSetting.getValue() ) );
                    break;

                case SYSTEM_UNAVAILABLE_MESSAGE:
                    if( nextSetting.getValue() != null ) setSystemUnavailableMessage( nextSetting.getValue() );
                    break;

                case LAST_REINDEX_DATE:
                    try
                    {
                        if( nextSetting.getValue() != null ) setLastReindexDate( Date.from( Instant.parse( nextSetting.getValue() ) ) );
                    }
                    catch ( DateTimeParseException e )
                    {
                        AetherSystemSettings.log.error( "Unable to parse LAST_REINDEX_DATE to a java.time.Instant or java.util.Date.", e );
                    }
                    break;

                case LAST_REINDEX_RECORD_COUNT:
                    setLastReindexRecordCount( Integer.valueOf( nextSetting.getValue() ) );
                    break;
            }
        }
    }

    public void replaceSettingsAndSave( AetherSystemSettings replacement )
    {
        try
        {
            BeanUtils.copyProperties( this, replacement );
            saveToDatabase();
        }
        catch ( IllegalAccessException | InvocationTargetException e )
        {
            AetherSystemSettings.log.error( "An exception was encountered while attempting to replace System Settings.", e );
        }
    }

    @SuppressWarnings("serial")
    public void saveToDatabase()
    {
        //@formatter:off
        systemSettingsEjb.saveAllSettings( new LinkedList<SystemSetting>(){{
            add( new SystemSetting( SystemSettingName.SYSTEM_TITLE.name(), getSystemTitle() ) );
            add( new SystemSetting( SystemSettingName.GOOGLE_API_KEY.name(), getGoogleApiKey() ) );
            add( new SystemSetting( SystemSettingName.CUSTOM_LOGO.name(), getUseCustomLogo().toString() ) );
            add( new SystemSetting( SystemSettingName.SYSTEM_AVAILABLE.name(), getSystemAvailable().toString() ) );
            add( new SystemSetting( SystemSettingName.LAST_REINDEX_DATE.name(), getLastReindexDate().toInstant().toString() ) );
            add( new SystemSetting( SystemSettingName.LAST_REINDEX_RECORD_COUNT.name(), getLastReindexRecordCount().toString() ) );
        }} );
        //@formatter:on
    }

    @JsonProperty
    public String getSystemTitle()
    {
        return systemTitle;
    }

    @JsonProperty
    public String getGoogleApiKey()
    {
        return googleApiKey;
    }

    @JsonProperty
    public Boolean getUseCustomLogo()
    {
        return useCustomLogo;
    }

    @JsonProperty
    public Boolean getSystemAvailable()
    {
        return systemAvailable;
    }

    @JsonProperty
    public String getSystemUnavailableMessage()
    {
        return systemUnavailableMessage;
    }

    @JsonProperty
    public Date getLastReindexDate()
    {
        return lastReindexDate;
    }

    @JsonProperty
    public Integer getLastReindexRecordCount()
    {
        return lastReindexRecordCount;
    }

    public void setSystemTitle( String systemTitle )
    {
        this.systemTitle = systemTitle;
    }

    public void setGoogleApiKey( String googleApiKey )
    {
        this.googleApiKey = googleApiKey;
    }

    public void setUseCustomLogo( Boolean useCustomLogo )
    {
        this.useCustomLogo = useCustomLogo;
    }

    public void setSystemAvailable( Boolean systemAvailable )
    {
        this.systemAvailable = systemAvailable;
    }

    public void setSystemUnavailableMessage( String systemUnavailableMessage )
    {
        this.systemUnavailableMessage = systemUnavailableMessage;
    }

    public void setLastReindexDate( Date lastReindexDate )
    {
        this.lastReindexDate = lastReindexDate;
    }

    public void setLastReindexRecordCount( Integer lastReindexRecordCount )
    {
        this.lastReindexRecordCount = lastReindexRecordCount;
    }
}
