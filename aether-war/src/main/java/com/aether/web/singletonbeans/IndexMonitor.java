//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.singletonbeans
 * File: IndexMonitor.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Monitors status of index and of reindexing operations.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 17, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.singletonbeans;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.ejb.EJB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import com.aether.ejb.indexing.IndexManagerLocal;

@Component
@Scope(scopeName = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class IndexMonitor
{
    @Autowired
    private SimpMessagingTemplate messageTemplate;

    @EJB(mappedName = "java:global/aether-ear/aether-ejb/IndexManager")
    private IndexManagerLocal indexManagerEjb;

    private AtomicBoolean isMonitoring = new AtomicBoolean( false );
    private Thread monitorThread = new Thread();

    public boolean isMonitoring()
    {
        return isMonitoring.get();
    }

    public void beginMonitoring()
    {
        synchronized ( isMonitoring )
        {
            if( !monitorThread.isAlive() && !isMonitoring.get() )
            {
                monitorThread = new Thread( new Runnable()
                {
                    @Override
                    public void run()
                    {
                        int progress = 0;

                        try
                        {
                            isMonitoring.set( true );
                            while ( isMonitoring.get() )
                            {
                                progress = indexManagerEjb.getReindexProgress();
                                if( progress == 100 || !indexManagerEjb.isReindexing() )
                                {
                                    isMonitoring.set( false );
                                }

                                // Broadcast update to listening clients
                                messageTemplate.convertAndSend( "/admin/reindexing/progress", progress );

                                Thread.sleep( 50 );
                            }
                        }
                        catch ( InterruptedException e )
                        {
                            isMonitoring.set( false );
                        }
                    }

                } );

                monitorThread.start();
            }
        }
    }

    public void stopMonitoring()
    {
        synchronized ( isMonitoring )
        {
            if( isMonitoring.get() )
            {
                isMonitoring.set( false );
            }
        }
    }
}
