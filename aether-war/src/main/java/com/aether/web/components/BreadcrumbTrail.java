//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.sessionbeans
 * File: BreadcrumbTrail.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 12, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.components;

import java.io.Serializable;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import com.aether.web.models.Breadcrumb;
import com.aether.web.models.BreadcrumbLevel;

@Component
public class BreadcrumbTrail implements Serializable
{
    /**
     * GUID ffor Serializable.
     */
    private static final long serialVersionUID = -5642767463959296233L;

    @Autowired
    private MessageSource messageSource;

    private Deque<Breadcrumb> breadcrumbs = new LinkedList<Breadcrumb>();
    private List<Breadcrumb> reversedTrail = new LinkedList<Breadcrumb>();

    @PostConstruct
    private void init()
    {
        Locale locale = LocaleContextHolder.getLocale();
        String homeLabel = messageSource.getMessage( "home.title", null, locale );
        breadcrumbs.push( new Breadcrumb( BreadcrumbLevel.LEVEL_1, homeLabel, "/" ) );
        updateReversedBreadcrumbTrail();
    }

    public List<Breadcrumb> getBreadcrumbTrail()
    {
        return reversedTrail;
    }

    public void updateReversedBreadcrumbTrail()
    {
        reversedTrail.clear();

        Iterator<Breadcrumb> reverseIter = breadcrumbs.descendingIterator();
        while ( reverseIter.hasNext() )
        {
            reversedTrail.add( reverseIter.next() );
        }
    }

    public void addBreadcrumb( Breadcrumb breadcrumb )
    {
        while ( breadcrumbs.size() > 1 )
        {
            Breadcrumb nextBreadcrumb = breadcrumbs.peek();
            if( nextBreadcrumb.getLevel().compareTo( breadcrumb.getLevel() ) < 0 ) break;
            else
                breadcrumbs.pop();
        }

        if( breadcrumb.getLevel().compareTo( BreadcrumbLevel.LEVEL_1 ) > 0 )
        {
            breadcrumbs.push( breadcrumb );
        }

        updateReversedBreadcrumbTrail();
    }
}
