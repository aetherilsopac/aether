//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.json
 * File: JsonResponseModel.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 8, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.json;

import java.io.Serializable;

/**
 * @author Clint Bush
 *
 */
public class JsonResponseModel implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = 8758545543029023059L;

    private JsonResponseStatus status = JsonResponseStatus.SUCCESS;
    private String message = "";
    private Object data = null;

    public JsonResponseModel(JsonResponseStatus status, String message, Object data)
    {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public JsonResponseStatus getStatus()
    {
        return status;
    }

    public String getMessage()
    {
        return message;
    }

    public Object getData()
    {
        return data;
    }

    public void setStatus( JsonResponseStatus status )
    {
        this.status = status;
    }

    public void setMessage( String message )
    {
        this.message = message;
    }

    public void setData( Object data )
    {
        this.data = data;
    }
}
