//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.models
 * File: Breadcrumb.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Stores data related to pages visited by the user. These are added to a Stack that represents the breadcrumb trail.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 12, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.models;

import java.io.Serializable;

public class Breadcrumb implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = 6524878100837446374L;

    private BreadcrumbLevel level = BreadcrumbLevel.LEVEL_1;
    private String label = "";
    private String url = "";

    public Breadcrumb(BreadcrumbLevel level, String label, String url)
    {
        this.level = level;
        this.label = label;
        this.url = url;
    }

    public BreadcrumbLevel getLevel()
    {
        return level;
    }

    public String getLabel()
    {
        return label;
    }

    public String getUrl()
    {
        return url;
    }

    public void setLevel( BreadcrumbLevel level )
    {
        this.level = level;
    }

    public void setLabel( String label )
    {
        this.label = label;
    }

    public void setUrl( String url )
    {
        this.url = url;
    }
}
