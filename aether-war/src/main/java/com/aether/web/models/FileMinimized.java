//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.models
 * File: FileMinimized.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jan 10, 2017, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.models;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.aether.ejb.common.Constants;

public class FileMinimized implements Serializable
{
    /**
     * GUID for Serializable.
     */
    private static final long serialVersionUID = -3508906554924657192L;

    private static Logger log = LogManager.getLogger( FileMinimized.class );

    private String name;
    private long length;
    private Date dateCreated;
    private Date lastUpdated;
    private Boolean isImage;
    private String fileExtension;

    public FileMinimized(File file)
    {
        Pattern fileNamePattern = Pattern.compile( "^(.+?)\\.([A-Za-z0-9]+)$" );
        Matcher fileNameMatcher = fileNamePattern.matcher( file.getName() );

        name = file.getName();
        length = file.length();

        try
        {
            BasicFileAttributes fileAttrs = Files.readAttributes( file.toPath(), BasicFileAttributes.class );
            dateCreated = Date.from( fileAttrs.creationTime().toInstant() );
            lastUpdated = Date.from( fileAttrs.lastModifiedTime().toInstant() );
            isImage = Boolean.FALSE;

            if( fileNameMatcher.matches() )
            {
                fileExtension = fileNameMatcher.group( 2 ).toLowerCase();
                isImage = Arrays.binarySearch( Constants.IMAGE_FILE_EXTENSIONS, fileExtension ) > -1;
            }
        }
        catch ( IOException e )
        {
            FileMinimized.log.error( "Error reading date created and date updated from file [" + file.getAbsolutePath() + "].", e );
        }
    }

    public String getName()
    {
        return name;
    }

    public long getLength()
    {
        return length;
    }

    public Date getDateCreated()
    {
        return dateCreated;
    }

    public Date getLastUpdated()
    {
        return lastUpdated;
    }

    public Boolean getIsImage()
    {
        return isImage;
    }

    public String getFileExtension()
    {
        return fileExtension;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public void setLength( long length )
    {
        this.length = length;
    }

    public void setDateCreated( Date dateCreated )
    {
        this.dateCreated = dateCreated;
    }

    public void setLastUpdated( Date lastUpdated )
    {
        this.lastUpdated = lastUpdated;
    }

    public void setIsImage( Boolean isImage )
    {
        this.isImage = isImage;
    }

    public void setFileExtension( String fileExtension )
    {
        this.fileExtension = fileExtension;
    }
}
