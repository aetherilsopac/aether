//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.viewcontrollers
 * File: CatalogueController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 11, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.viewcontrollers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ejb.EJB;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.aether.ejb.beans.interfaces.BiblioBusinessLocal;
import com.aether.ejb.beans.interfaces.ItemTypeBusinessLocal;
import com.aether.ejb.enums.IndexField;
import com.aether.ejb.models.Biblio;
import com.aether.ejb.models.ItemType;
import com.aether.ejb.models.ItemTypeField;
import com.aether.ejb.models.ItemTypeLabel;
import com.aether.ejb.models.PatronCategory;
import com.aether.web.common.WebConstants;
import com.aether.web.controllers.BaseController;
import com.aether.web.models.Breadcrumb;
import com.aether.web.models.BreadcrumbLevel;

@Controller
public class CatalogueMaintenanceController extends BaseController
{
    @SuppressWarnings("unused")
    private static Logger log = LogManager.getLogger( CatalogueMaintenanceController.class );

    @EJB(mappedName = "java:global/aether-ear/aether-ejb/ItemTypeBusiness")
    private ItemTypeBusinessLocal itemTypeEjb;

    @EJB(mappedName = "java:global/aether-ear/aether-ejb/BiblioBusiness")
    private BiblioBusinessLocal biblioEjb;

    @Secured("ROLE_CATALOGUE_MAINTENANCE")
    @RequestMapping(value = { "/Librarian/CatalogueMaintenance" }, method = RequestMethod.GET)
    public String index( ModelMap model )
    {
        List<ItemType> activeItemTypes = itemTypeEjb.getAllActiveItemTypes();

        //@formatter:off
        List<IndexField> indexFields = Stream.of( IndexField.values() )
                                             .filter( f -> { return f != IndexField.FIELD_BIBLIO_MULTI;} )
                                             .collect( Collectors.toList() );
        //@formatter:on

        model.put( "activeItemTypes", activeItemTypes );
        model.put( "indexFields", indexFields );
        model.put( "marcBiblioRecordTypeIcons", WebConstants.marcBiblioRecordTypeIcons );

        // Look-up Record Type names in our locale file
        HashMap<Character, String> marcBiblioRecordTypeLabels = new HashMap<Character, String>();
        for ( Pair<Character, String> nextPair : WebConstants.marcBiblioRecordTypes )
        {
            //@formatter:off
            marcBiblioRecordTypeLabels.put(  
                nextPair.getKey(), 
                this.getMessage( "models.biblio.record_type." + nextPair.getValue() )
            );
            //@formatter:on
        }
        model.put( "marcBiblioRecordTypeLabels", marcBiblioRecordTypeLabels );

        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_2, getMessage( "librarian.catalogue_maintenance.title" ), "/Librarian/CatalogueMaintenance" ) );

        return "views/Librarian/CatalogueMaintenance/Index";
    }

    @Secured("ROLE_CATALOGUE_MAINTENANCE")
    @RequestMapping(value = { "/Librarian/CatalogueMaintenance/Add/{itemTypeId}" }, method = RequestMethod.GET)
    public String add( ModelMap model, @PathVariable int itemTypeId )
    {
        Biblio biblio = new Biblio();
        List<PatronCategory> patronCategories = userEjb.getActivePatronCategories();
        ItemType itemType = itemTypeEjb.find( itemTypeId );
        List<ItemTypeField> fields = itemTypeEjb.getItemTypeFieldsByLocale( itemType, getLocale() );
        Map<String, ItemTypeField> fieldMap = new HashMap<String, ItemTypeField>();
        Map<String, ItemTypeLabel> labelMap = new HashMap<String, ItemTypeLabel>();

        // Create a HashMap of the fields and labels for the current locale keyed by column name
        for ( ItemTypeField field : fields )
        {
            fieldMap.put( field.getBiblioColumnName(), field );

            if( field.getLabels().size() > 0 )
            {
                labelMap.put( field.getBiblioColumnName(), field.getLabels().get( 0 ) );
            }
            else
            {
                labelMap.put( field.getBiblioColumnName(), new ItemTypeLabel( field, getLocale(), field.getBiblioColumnName(), "" ) );
            }
        }

        model.put( "biblio", biblio );
        model.put( "patronCategories", patronCategories );
        model.put( "itemType", itemType );
        model.put( "itemTypeFields", fieldMap );
        model.put( "itemTypeLabels", labelMap );

        // Look-up Record Type names in our locale file
        List<Pair<Character, String>> marcBiblioRecordTypes = new ArrayList<Pair<Character, String>>( WebConstants.marcBiblioRecordTypes.size() );
        for ( Pair<Character, String> nextPair : WebConstants.marcBiblioRecordTypes )
        {
            //@formatter:off
            marcBiblioRecordTypes.add( 
                new ImmutablePair<Character, String>( 
                    nextPair.getKey(), 
                    this.getMessage( "models.biblio.record_type." + nextPair.getValue() )
                )
            );
            //@formatter:on
        }
        model.put( "marcBiblioRecordTypes", marcBiblioRecordTypes );

        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, getMessage( "librarian.catalogue_maintenance.add.title" ), "/Librarian/CatalogueMaintenance/Add" ) );

        return "views/Librarian/CatalogueMaintenance/Add";
    }

    @Secured("ROLE_CATALOGUE_MAINTENANCE")
    @RequestMapping(value = { "/Librarian/CatalogueMaintenance/Edit/{biblioId}" }, method = RequestMethod.GET)
    public String edit( ModelMap model, @PathVariable int biblioId )
    {
        List<PatronCategory> patronCategories = userEjb.getActivePatronCategories();
        Biblio biblio = biblioEjb.find( biblioId );
        List<ItemTypeField> fields = itemTypeEjb.getItemTypeFieldsByLocale( biblio.getItemType(), getLocale() );
        Map<String, ItemTypeField> fieldMap = new HashMap<String, ItemTypeField>();
        Map<String, ItemTypeLabel> labelMap = new HashMap<String, ItemTypeLabel>();

        // Create a HashMap of the fields and labels for the current locale keyed by column name
        for ( ItemTypeField field : fields )
        {
            fieldMap.put( field.getBiblioColumnName(), field );

            if( field.getLabels().size() > 0 )
            {
                labelMap.put( field.getBiblioColumnName(), field.getLabels().get( 0 ) );
            }
            else
            {
                labelMap.put( field.getBiblioColumnName(), new ItemTypeLabel( field, getLocale(), field.getBiblioColumnName(), "" ) );
            }
        }

        model.put( "biblio", biblio );
        model.put( "patronCategories", patronCategories );
        model.put( "itemType", biblio.getItemType() );
        model.put( "itemTypeFields", fieldMap );
        model.put( "itemTypeLabels", labelMap );

        // Look-up Record Type names in our locale file
        List<Pair<Character, String>> marcBiblioRecordTypes = new ArrayList<Pair<Character, String>>( WebConstants.marcBiblioRecordTypes.size() );
        for ( Pair<Character, String> nextPair : WebConstants.marcBiblioRecordTypes )
        {
            //@formatter:off
            marcBiblioRecordTypes.add( 
                new ImmutablePair<Character, String>( 
                    nextPair.getKey(), 
                    this.getMessage( "models.biblio.record_type." + nextPair.getValue() )
                )
            );
            //@formatter:on
        }
        model.put( "marcBiblioRecordTypes", marcBiblioRecordTypes );

        //@formatter:off
        addBreadcrumb(
                new Breadcrumb( 
                        BreadcrumbLevel.LEVEL_3, 
                        getMessage( "librarian.catalogue_maintenance.edit.title" ), 
                        "/Librarian/CatalogueMaintenance/Edit/" + biblio.getId() 
                ) 
        );
        //@formatter:on

        return "views/Librarian/CatalogueMaintenance/Edit";
    }
}
