//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.viewcontrollers
 * File: BaseController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides commonly used methods to be inherited by other controllers.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 11, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;
import com.aether.ejb.beans.interfaces.UserBusinessLocal;
import com.aether.ejb.common.Constants;
import com.aether.ejb.indexing.IndexManagerLocal;
import com.aether.ejb.models.User;
import com.aether.web.components.BreadcrumbTrail;
import com.aether.web.models.AetherMessage;
import com.aether.web.models.Breadcrumb;
import com.aether.web.sessionbeans.AetherMessageStack;
import com.aether.web.singletonbeans.AetherSystemSettings;

public abstract class BaseController
{
    private static Logger log = LogManager.getLogger( BaseController.class );

    @EJB(mappedName = "java:global/aether-ear/aether-ejb/UserBusiness")
    protected UserBusinessLocal userEjb;

    @EJB(mappedName = "java:global/aether-ear/aether-ejb/IndexManager")
    protected IndexManagerLocal indexManagerEjb;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private BreadcrumbTrail breadcrumbTrail;

    @Autowired
    private AetherMessageStack messageStack;

    @Autowired
    protected AetherSystemSettings systemSettings;

    private List<String> fileExtensions;

    public BaseController()
    {
        fileExtensions = new ArrayList<String>( Constants.IMAGE_FILE_EXTENSIONS.length );
        for ( String extension : Constants.IMAGE_FILE_EXTENSIONS )
        {
            fileExtensions.add( "." + extension );
        }
    }

    @ModelAttribute("currentUser")
    public User getCurrentUser()
    {
        try
        {
            org.springframework.security.core.userdetails.User principal = ( (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal() );

            if( principal == null || principal.getUsername() == null || principal.getUsername().length() < 1 ) return null;

            User currentUser = userEjb.getUserByUserName( principal.getUsername() );

            return currentUser;
        }
        catch ( Exception e )
        {
            BaseController.log.error( "Exception encountered while trying to get current user.", e );
            return null;
        }
    }

    @ModelAttribute("isCustomLogo")
    public boolean isCustomLogo()
    {
        return systemSettings.getUseCustomLogo().booleanValue();
    }

    @ModelAttribute("systemTitle")
    public String getSystemTitle()
    {
        return systemSettings.getSystemTitle();
    }

    @ModelAttribute("systemAvailable")
    public boolean isSystemAvailable()
    {
        return systemSettings.getSystemAvailable().booleanValue();
    }

    @ModelAttribute("breadcrumbTrail")
    public List<Breadcrumb> getBreadcrumbTrail()
    {
        return breadcrumbTrail.getBreadcrumbTrail();
    }

    @ModelAttribute("messages")
    public List<AetherMessage> getAndClearAetherMessages()
    {
        return messageStack.getMessages();
    }

    @ModelAttribute("isSearchable")
    public boolean isSearchable()
    {
        return indexManagerEjb.isSearchable();
    }

    @ModelAttribute("imageFileExtensions")
    public List<String> getImageFileExtensions()
    {
        return fileExtensions;
    }

    public void addBreadcrumb( Breadcrumb breadcrumb )
    {
        breadcrumbTrail.addBreadcrumb( breadcrumb );
    }

    public void addAetherMessage( AetherMessage message )
    {
        messageStack.addMessage( message );
    }

    public String getMessage( String name )
    {
        return getMessage( name, null );
    }

    public String getMessage( String name, Object[] args )
    {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage( name, args, locale );
    }

    public String getLocale()
    {
        return LocaleContextHolder.getLocale().toString();
    }
}
