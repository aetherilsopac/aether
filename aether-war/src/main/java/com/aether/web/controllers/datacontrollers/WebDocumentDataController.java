//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.datacontrollers
 * File: WebDocumentDataController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Data Controller providing RESTful CRUD operations for working with Web Documents.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 11, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.datacontrollers;

import java.util.List;
import javax.ejb.EJB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.aether.ejb.beans.interfaces.WebDocumentBusinessLocal;
import com.aether.ejb.models.EmployeeUser;
import com.aether.ejb.models.User;
import com.aether.ejb.models.WebDocument;
import com.aether.ejb.models.minimized.WebDocumentMinimized;
import com.aether.web.controllers.BaseController;
import com.aether.web.json.JsonResponseModel;
import com.aether.web.json.JsonResponseStatus;
import com.aether.web.models.AetherMessage;
import com.aether.web.models.AetherMessageSeverity;

@Controller
public class WebDocumentDataController extends BaseController
{
    private static Logger log = LogManager.getLogger( WebDocumentDataController.class );

    @EJB(mappedName = "java:global/aether-ear/aether-ejb/WebDocumentBusiness")
    protected WebDocumentBusinessLocal webDocumentEjb;

    @Secured( "ROLE_USER" )
    @RequestMapping(value = "/Data/WebDocuments", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody JsonResponseModel listWebDocuments()
    {
        JsonResponseStatus status = JsonResponseStatus.SUCCESS;
        String message = "";
        List<WebDocumentMinimized> documents = null;

        try
        {
            documents = webDocumentEjb.getAllWebDocumentsMinimized();
        }
        catch ( Exception e )
        {
            WebDocumentDataController.log.error( e );
            status = JsonResponseStatus.EXCEPTION;
            message = getMessage( "messages.unhandled_exception" );
        }

        //@formatter:off
        return new JsonResponseModel(
            status,
            message,
            documents
        );
        //@formatter:on
    }

    @Secured( "ROLE_USER" )
    @RequestMapping(value = "/Data/WebDocuments/{id:\\d+}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody JsonResponseModel getWebDocument( @PathVariable Integer id )
    {
        JsonResponseStatus status = JsonResponseStatus.SUCCESS;
        String message = "";
        WebDocument document = null;

        try
        {
            document = webDocumentEjb.find( id );

            if( document == null )
            {
                //@formatter:off
                addAetherMessage( 
                        new AetherMessage( 
                                getMessage( "messages.error" ), 
                                getMessage( "messages.no_record_by_id", new Object[] { id } ),
                                AetherMessageSeverity.DANGER 
                        ) 
                );
                //@formatter:on
            }
        }
        catch ( Exception e )
        {
            WebDocumentDataController.log.error( e );
            status = JsonResponseStatus.EXCEPTION;
            message = getMessage( "messages.unhandled_exception" );
        }

        //@formatter:off
        return new JsonResponseModel(
            status,
            message,
            document
        );
        //@formatter:on
    }

    @Secured( "ROLE_WEB_MAINTENANCE" )
    @RequestMapping(value = "/Data/WebDocuments", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody JsonResponseModel addWebDocument( @RequestBody WebDocument document )
    {
        JsonResponseStatus status = JsonResponseStatus.SUCCESS;
        String message = "";
        User currentUser = getCurrentUser();

        try
        {
            // Confirm there are no documents where friendly URL, locale, and type all match
            Long matchingDocuments = webDocumentEjb.countMatchingWebDocuments( document );

            if( matchingDocuments == 0L )
            {
                webDocumentEjb.add( document, (EmployeeUser) currentUser );

                //@formatter:off
                addAetherMessage( 
                    new AetherMessage( 
                        getMessage("messages.success"), 
                        getMessage("messages.record_successfully_added", new Object[] { document.getTitle() }), 
                        AetherMessageSeverity.SUCCESS 
                    )
                );
                //@formatter:on
            }
            else
            {
                WebDocumentDataController.log.warn( "An attempt was made to add a non-unique Web Document." );
                status = JsonResponseStatus.INVALID;
                message = "<strong>" + getMessage( "messages.surprise" ) + "</strong> " + getMessage( "admin.web_document_maintenance.non_unique_web_document" );
            }
        }
        catch ( Exception e )
        {
            WebDocumentDataController.log.error( e );
            status = JsonResponseStatus.EXCEPTION;
            message = getMessage( "messages.unhandled_exception" );
        }

        //@formatter:off
        return new JsonResponseModel(
            status,
            message,
            document
        );
        //@formatter:on
    }

    @Secured( "ROLE_WEB_MAINTENANCE" )
    @RequestMapping(value = "/Data/WebDocuments", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody JsonResponseModel saveWebDocument( @RequestBody WebDocument document )
    {
        JsonResponseStatus status = JsonResponseStatus.SUCCESS;
        String message = "";
        User currentUser = getCurrentUser();

        try
        {
            // Confirm there are no documents where friendly URL, locale, and type all match
            Long matchingDocuments = webDocumentEjb.countMatchingWebDocuments( document );

            if( matchingDocuments == 0L )
            {
                document = webDocumentEjb.update( document, (EmployeeUser) currentUser );

                //@formatter:off
                addAetherMessage( 
                    new AetherMessage( 
                        getMessage("messages.success"), 
                        getMessage("messages.record_successfully_updated", new Object[]{ document.getTitle() }),
                        AetherMessageSeverity.SUCCESS 
                    )
                );
                //@formatter:on
            }
            else
            {
                WebDocumentDataController.log.warn( "An attempt was made to save a Web Document with non-unique properties." );
                status = JsonResponseStatus.INVALID;
                message = "<strong>" + getMessage( "messages.surprise" ) + "</strong> " + getMessage( "admin.web_document_maintenance.non_unique_web_document" );
            }
        }
        catch ( Exception e )
        {
            WebDocumentDataController.log.error( e );
            status = JsonResponseStatus.EXCEPTION;
            message = getMessage( "messages.unhandled_exception" );
        }

        //@formatter:off
        return new JsonResponseModel(
            status,
            message,
            document
        );
        //@formatter:on
    }

    @Secured( "ROLE_WEB_MAINTENANCE" )
    @RequestMapping(value = "/Data/WebDocuments/{id:\\d+}", method = RequestMethod.DELETE, produces = "application/json")
    public @ResponseBody JsonResponseModel deleteWebDocument( @PathVariable Integer id )
    {
        JsonResponseStatus status = JsonResponseStatus.SUCCESS;
        String message = "";

        try
        {
            WebDocument document = webDocumentEjb.find( id );
            if( document != null && webDocumentEjb.removeByPrimaryKey( id ) )
            {
                //@formatter:off
                addAetherMessage( 
                    new AetherMessage( 
                        getMessage("messages.success"), 
                        getMessage("messages.record_successfully_deleted", new Object[]{ document.getTitle() }),
                        AetherMessageSeverity.SUCCESS 
                    )
                );
                //@formatter:on
            }
            else
            {
                status = JsonResponseStatus.INVALID;
                message = "<strong>" + getMessage( "messages.surprise" ) + "</strong> " + getMessage( "messages.record_delete_failed", new Object[] { id } );
            }
        }
        catch ( Exception e )
        {
            WebDocumentDataController.log.error( e );
            status = JsonResponseStatus.EXCEPTION;
            message = getMessage( "messages.unhandled_exception" );
        }

        //@formatter:off
        return new JsonResponseModel(
            status,
            message,
            null
        );
        //@formatter:on
    }
}
