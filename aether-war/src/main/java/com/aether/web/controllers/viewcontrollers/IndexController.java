//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.viewcontrollers
 * File: IndexController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 22, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.viewcontrollers;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.aether.ejb.beans.interfaces.UserBusinessLocal;
import com.aether.ejb.enums.UserType;
import com.aether.web.controllers.BaseController;
import com.aether.web.models.Breadcrumb;
import com.aether.web.models.BreadcrumbLevel;
import com.aether.web.viewmodels.Login;

@Controller
public class IndexController extends BaseController
{
    @SuppressWarnings("unused")
    private static Logger log = LogManager.getLogger( IndexController.class );

    @EJB(mappedName = "java:global/aether-ear/aether-ejb/UserBusiness")
    private UserBusinessLocal userEjb;

    @RequestMapping(value = { "/", "Index" }, method = RequestMethod.GET)
    public String Index( ModelMap model )
    {
        // Remove all breadcrumbs but the Home
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_1, "", "/" ) );

        if( getCurrentUser().getUserType() == UserType.EMPLOYEE )
        {
            return "views/Employee/Index";
        }
        return "views/Patron/Index";
    }

    @RequestMapping(value = "/Login", method = RequestMethod.GET)
    public String Login( ModelMap model )
    {
        model.addAttribute( "credentials", new Login() );
        model.addAttribute( "systemUnavailableMessage", systemSettings.getSystemUnavailableMessage() );
        return "views/Login";
    }

    @RequestMapping(value = "/Register", method = RequestMethod.GET)
    public String Register( ModelMap model )
    {
        model.addAttribute( "systemUnavailableMessage", systemSettings.getSystemUnavailableMessage() );
        return "views/Register";
    }

    @RequestMapping(value = "/Logout", method = RequestMethod.GET)
    public String Logout( HttpServletRequest request, HttpServletResponse response )
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if( auth != null )
        {
            new SecurityContextLogoutHandler().logout( request, response, auth );
        }
        return "redirect:/Login";
    }
}
