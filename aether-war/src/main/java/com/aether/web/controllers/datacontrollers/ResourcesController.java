//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.datacontrollers
 * File: ResourcesController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Nov 10, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.datacontrollers;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.aether.web.controllers.BaseController;
import com.aether.web.json.JsonResponseModel;
import com.aether.web.json.JsonResponseStatus;
import com.aether.web.sessionbeans.ResourceBusiness;

@Controller
public class ResourcesController extends BaseController
{
	private static Logger log = LogManager.getLogger( ResourcesController.class );
	
	@Autowired
	private ResourceBusiness resourceBusiness;
	
	@RequestMapping( value = "/Resources/{url:.+}", method = RequestMethod.GET )
	public @ResponseBody FileSystemResource getResource( @PathVariable( "url" ) String url, HttpServletResponse httpResponse )
	{
		// Attempt to locate the file
		File resourceFile = resourceBusiness.getResourceByFileName( url );
		
		// If the resource exists, return it
		if ( resourceFile != null )
		{
			return new FileSystemResource( resourceFile );
		}
		
		// If it does not exist, set the HttpServletResponse to 404 Not Found
		try
		{
			httpResponse.sendError( HttpStatus.NOT_FOUND.value(), "Resource identified by [" + url + "] could not be found." );
			ResourcesController.log.warn( "Resource identified by [" + url + "] could not be found." );
		}
		catch ( IOException e )
		{
			ResourcesController.log.error( "Exception encountered while returning 404 HttpStatus for failure to locate resource [" + url + "].", e );
		}
		
		return null;
	}
	
	@RequestMapping( value = "/Resources/Thumbnails/{url:.+}", method = RequestMethod.GET )
	public @ResponseBody FileSystemResource getResourceThumbnail( @PathVariable( "url" ) String url, HttpServletResponse httpResponse )
	{
		// Attempt to locate the file
		File resourceFile = resourceBusiness.getResourceThumbnailByFileName( url );
		
		// If the resource exists, return it
		if ( resourceFile != null )
		{
			return new FileSystemResource( resourceFile );
		}
		
		// If it does not exist, set the HttpServletResponse to 404 Not Found
		try
		{
			httpResponse.sendError( HttpStatus.NOT_FOUND.value(), "Resource identified by [" + url + "] could not be found." );
			ResourcesController.log.warn( "Resource identified by [" + url + "] could not be found." );
		}
		catch ( IOException e )
		{
			ResourcesController.log.error( "Exception encountered while returning 404 HttpStatus for failure to locate resource [" + url + "].", e );
		}
		
		return null;
	}
	
	@Secured( { "ROLE_LIBRARIAN", "ROLE_ADMIN" } )
	@RequestMapping( value = "/Data/Resources/ImageNames", method = RequestMethod.GET )
	public @ResponseBody JsonResponseModel getImageResourceNames()
	{
		//@formatter:off
        return new JsonResponseModel(
            JsonResponseStatus.SUCCESS,
            "",
            resourceBusiness.listImageResourceNames()
        );
        //@formatter:on
	}
	
	@Secured( { "ROLE_LIBRARIAN", "ROLE_ADMIN" } )
	@RequestMapping( value = "/Data/Resources/Names", method = RequestMethod.GET )
	public @ResponseBody JsonResponseModel getResourceNames()
	{
		//@formatter:off
        return new JsonResponseModel(
            JsonResponseStatus.SUCCESS,
            "",
            resourceBusiness.listResourceNames()
        );
        //@formatter:on
	}
	
	@Secured( { "ROLE_LIBRARIAN", "ROLE_ADMIN" } )
	@RequestMapping( value = "/Data/Resources", method = RequestMethod.GET )
	public @ResponseBody JsonResponseModel getResources()
	{
		//@formatter:off
        return new JsonResponseModel(
            JsonResponseStatus.SUCCESS,
            "",
            resourceBusiness.listResources()
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_WEB_MAINTENANCE" )
	@RequestMapping( value = { "/Data/Resources" }, method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE } )
	public @ResponseBody JsonResponseModel addResource( @RequestParam( "file0" ) MultipartFile file )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		String fileLocation;
		
		try
		{
			fileLocation = resourceBusiness.addResource( file.getOriginalFilename(), file.getBytes() );
		}
		catch ( IOException e )
		{
			ResourcesController.log.error( "Exception encountered while uploading a resource.", e );
			fileLocation = null;
		}
		
		if ( fileLocation == null || fileLocation.isEmpty() )
		{
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
		}
		
		//@formatter:off
        return new JsonResponseModel( 
            status, 
            message, 
            fileLocation
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_WEB_MAINTENANCE" )
	@RequestMapping( value = { "/Data/Resources/{resourceName}" }, method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE } )
	public @ResponseBody JsonResponseModel deleteResource( @PathVariable( "resourceName" ) String resourceName )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		
		boolean isDeleted = resourceBusiness.deleteResource( resourceName );
		
		if ( isDeleted )
		{
			message = getMessage( "messages.resource_successfully_deleted", new Object[] { resourceName } );
		}
		else
		{
			status = JsonResponseStatus.INVALID;
			message = getMessage( "messages.resource_delete_failed", new Object[] { resourceName } );
		}
		
		//@formatter:off
        return new JsonResponseModel( 
            status, 
            message, 
            null
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_WEB_MAINTENANCE" )
	@RequestMapping( value = { "/Data/Resources/DeleteResources" }, method = RequestMethod.PUT, produces = { MediaType.APPLICATION_JSON_VALUE } )
	public @ResponseBody JsonResponseModel deleteResources( @RequestBody String[] resourceNames )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		boolean isDeleted = true;
		
		for ( String nextResourceName : resourceNames )
		{
			isDeleted = isDeleted && resourceBusiness.deleteResource( nextResourceName );
		}
		
		if ( isDeleted )
		{
			message = getMessage( "messages.resources_successfully_deleted" );
		}
		else
		{
			status = JsonResponseStatus.INVALID;
			message = getMessage( "messages.resources_delete_failed" );
		}
		
		//@formatter:off
        return new JsonResponseModel( 
            status, 
            message, 
            null
        );
        //@formatter:on
	}
}
