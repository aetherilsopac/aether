//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.datacontrollers
 * File: UsersController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides a RESTful interface for CRUD operations dealing with information that both Employees and Patrons have in common.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jul 30, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.datacontrollers;

import java.lang.reflect.InvocationTargetException;

import javax.ejb.EJB;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aether.ejb.beans.interfaces.UserBusinessLocal;
import com.aether.ejb.models.User;
import com.aether.ejb.models.UserPreferences;
import com.aether.web.controllers.BaseController;
import com.aether.web.json.JsonResponseModel;
import com.aether.web.json.JsonResponseStatus;
import com.aether.web.models.AetherMessage;
import com.aether.web.models.AetherMessageSeverity;

@Controller
public class UsersController extends BaseController
{
	private static Logger log = LogManager.getLogger( UsersController.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/UserBusiness" )
	protected UserBusinessLocal userEjb;
	
	@Secured( "ROLE_USER" )
	@RequestMapping( value = "/Data/Users/Preferences", method = RequestMethod.PUT, produces = "application/json" )
	public @ResponseBody JsonResponseModel updatePreferences( @RequestBody UserPreferences preferences )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User currentUser = getCurrentUser();
		
		if ( preferences.getId() != getCurrentUser().getId() )
		{
			status = JsonResponseStatus.INVALID;
			message = getMessage( "messages.lack_permission" );
		}
		else
		{
			try
			{
				BeanUtilsBean.getInstance().copyProperties( currentUser.getPreferences(), preferences );
				currentUser = userEjb.update( currentUser, currentUser );
			}
			catch ( IllegalAccessException | InvocationTargetException e )
			{
				UsersController.log.error( e );
				status = JsonResponseStatus.EXCEPTION;
				message = getMessage( "messages.unhandled_exception" );
			}
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            currentUser.getPreferences()
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_USER" )
	@RequestMapping( value = "/Data/Users/Save", method = RequestMethod.PUT, produces = "application/json" )
	public @ResponseBody JsonResponseModel saveUser( @RequestBody User user )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User currentUser = getCurrentUser();
		
		if ( user.getId() != getCurrentUser().getId() )
		{
			status = JsonResponseStatus.INVALID;
			message = getMessage( "messages.lack_permission" );
			addAetherMessage( new AetherMessage( getMessage( "messages.error" ), getMessage( "messages.lack_permission" ), AetherMessageSeverity.DANGER ) );
		}
		else
		{
			try
			{
				// Overwrite properties that an average user should not be able
				// to change
				user.setCreatedBy( currentUser.getCreatedBy() );
				user.setDateCreated( currentUser.getDateCreated() );
				user.setUpdatedBy( currentUser.getUpdatedBy() );
				user.setDateUpdated( currentUser.getDateUpdated() );
				user.setPermissions( currentUser.getPermissions() );
				user.setRole( currentUser.getRole() );
				user.setStatus( currentUser.getStatus() );
				user.setUserName( currentUser.getUserName() );
				user.setUserType( currentUser.getUserType() );
				
				if ( user.getPassword() != null )
				{
					BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
					user.setPasswordHash( encoder.encode( user.getPassword() ) );
				}
				else
				{
					user.setPasswordHash( currentUser.getPasswordHash() );
				}
				
				user.setPassword( null );
				
				BeanUtilsBean.getInstance().copyProperties( currentUser, user );
				currentUser = userEjb.update( currentUser, currentUser );
				
				addAetherMessage( new AetherMessage( getMessage( "messages.success" ), getMessage( "messages.changes_successfully_saved" ), AetherMessageSeverity.SUCCESS ) );
			}
			catch ( IllegalAccessException | InvocationTargetException e )
			{
				UsersController.log.error( e );
				status = JsonResponseStatus.EXCEPTION;
				message = getMessage( "messages.unhandled_exception" );
				
				addAetherMessage( new AetherMessage( getMessage( "messages.error" ), getMessage( "messages.changes_save_failed" ), AetherMessageSeverity.DANGER ) );
			}
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            currentUser
        );
        //@formatter:on
	}
}
