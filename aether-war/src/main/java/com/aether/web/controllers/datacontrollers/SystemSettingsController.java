//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.datacontrollers
 * File: SystemSettingsController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * DataController providing RESTful CRUD operations for working with SystemSetting and AetherSystemSettings records.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 11, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.datacontrollers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.ejb.EJB;
import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.aether.ejb.beans.interfaces.SystemSettingsBusinessLocal;
import com.aether.ejb.common.Constants;
import com.aether.ejb.indexing.IndexManagerLocal;
import com.aether.web.controllers.BaseController;
import com.aether.web.json.JsonResponseModel;
import com.aether.web.json.JsonResponseStatus;
import com.aether.web.models.AetherMessage;
import com.aether.web.models.AetherMessageSeverity;
import com.aether.web.singletonbeans.AetherSystemSettings;
import com.aether.web.singletonbeans.IndexMonitor;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class SystemSettingsController extends BaseController
{
	private static Logger log = LogManager.getLogger( SystemSettingsController.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/SystemSettingsBusiness" )
	private SystemSettingsBusinessLocal settingsEjb;
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/IndexManager" )
	private IndexManagerLocal indexManagerEjb;
	
	@Autowired
	private AetherSystemSettings systemSettings;
	
	@Autowired
	private IndexMonitor indexMonitor;
	
	@Secured( "ROLE_ADMIN" )
	@RequestMapping( value = { "/Data/SystemSettings" }, method = RequestMethod.GET )
	public @ResponseBody JsonResponseModel fetch()
	{
		//@formatter:off
        return new JsonResponseModel(
            JsonResponseStatus.SUCCESS,
            "",
            systemSettings
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_ADMIN" )
	@RequestMapping( value = { "/Data/SystemSettings" }, method = RequestMethod.POST )
	public @ResponseBody JsonResponseModel save( @RequestParam( value = "data" ) String systemSettingsJson, @RequestParam( value = "file0", required = false ) MultipartFile file )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		ObjectMapper mapper = new ObjectMapper();
		
		AetherSystemSettings systemSettings = null;
		
		try
		{
			systemSettings = mapper.readValue( systemSettingsJson, AetherSystemSettings.class );
		}
		catch ( IOException e )
		{
			SystemSettingsController.log.error( "An exception was encountered while trying to upload a custom logo.", e );
			message = getMessage( "messages.unhandled_exception" );
			status = JsonResponseStatus.EXCEPTION;
			addAetherMessage( new AetherMessage( getMessage( "messages.error" ), getMessage( "messages.unhandled_exception" ), AetherMessageSeverity.DANGER ) );
		}
		
		// If a custom logo was provided, save it to disk
		if ( status == JsonResponseStatus.SUCCESS && (file != null && file.getSize() > 0) )
		{
			// Create path to custom logo file
			Path logoPath = Paths.get( Constants.DIRECTORY_SYSTEM_RESOURCES.toString(), Constants.FILE_NAME_CUSTOM_LOGO );
			File customLogoFile = logoPath.toFile();
			
			try
			{
				// If the file doesn't exist, create it
				if ( !customLogoFile.exists() ) customLogoFile.createNewFile();
				
				// Convert to PNG
				BufferedImage bufImg = ImageIO.read( file.getInputStream() );
				
				// Write to Disk
				ImageIO.write( bufImg, "png", customLogoFile );
			}
			catch ( IOException e )
			{
				SystemSettingsController.log.error( "An exception was encounterd while trying to upload a custom logo.", e );
				message = getMessage( "messages.unhandled_exception" );
				status = JsonResponseStatus.EXCEPTION;
				addAetherMessage( new AetherMessage( getMessage( "messages.error" ), getMessage( "messages.unhandled_exception" ), AetherMessageSeverity.DANGER ) );
			}
		}
		
		if ( status == JsonResponseStatus.SUCCESS )
		{
			// Save the settings
			this.systemSettings.replaceSettingsAndSave( systemSettings );
			
			// Build success message
			addAetherMessage( new AetherMessage( getMessage( "messages.success" ), getMessage( "messages.changes_successfully_saved" ), AetherMessageSeverity.SUCCESS ) );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            this.systemSettings
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_ADMIN" )
	@RequestMapping( value = { "/Data/SystemSettings/Reindex" }, method = RequestMethod.POST )
	public @ResponseBody JsonResponseModel reindex()
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		
		// Ensure the system is not already reindexing
		if ( !indexManagerEjb.isReindexing() )
		{
			indexManagerEjb.reindex();
			indexMonitor.beginMonitoring();
			
			try
			{
				Thread.sleep( 1000 );
			}
			catch ( InterruptedException e )
			{
			}
		}
		else
		{
			SystemSettingsController.log.warn( "An attempt was made to reindex while a reindex is already in progress." );
			status = JsonResponseStatus.INVALID;
			message = getMessage( "admin.system_maintenance.reindex_already_in_progress" );
			addAetherMessage( new AetherMessage( getMessage( "messages.surprise" ), getMessage( "admin.system_maintenance.reindex_already_in_progress" ), AetherMessageSeverity.WARNING ) );
		}
		
		// Check to see if reindexing started successfully
		if ( indexManagerEjb.isReindexing() && indexMonitor.isMonitoring() && !indexManagerEjb.isSearchable() )
		{
			addAetherMessage( new AetherMessage( getMessage( "messages.notice" ), getMessage( "admin.system_maintenance.reindex_in_progress" ), AetherMessageSeverity.INFO ) );
		}
		else
		{
			SystemSettingsController.log.warn( "Reindexing failed to start." );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "admin.system_maintenance.reindex_failed_start" );
			addAetherMessage( new AetherMessage( getMessage( "messages.error" ), getMessage( "admin.system_maintenance.reindex_failed_start" ), AetherMessageSeverity.DANGER ) );
		}
		
		//@formatter:off
        return new JsonResponseModel( 
            status, 
            message, 
            null 
        );
        //@formatter:on
	}
}
