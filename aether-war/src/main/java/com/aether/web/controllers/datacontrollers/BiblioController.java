//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.datacontrollers
 * File: SruController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 17, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.datacontrollers;

import java.util.ArrayList;

import javax.ejb.EJB;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aether.ejb.beans.interfaces.BiblioBusinessLocal;
import com.aether.ejb.indexing.IndexManagerLocal;
import com.aether.ejb.models.Biblio;
import com.aether.ejb.viewmodels.BiblioQuery;
import com.aether.ejb.viewmodels.BiblioQueryResultSet;
import com.aether.ejb.viewmodels.IndexQuery;
import com.aether.ejb.viewmodels.IndexResultSet;
import com.aether.web.controllers.BaseController;
import com.aether.web.json.JsonResponseModel;
import com.aether.web.json.JsonResponseStatus;
import com.aether.web.models.AetherMessage;
import com.aether.web.models.AetherMessageSeverity;

@Controller
public class BiblioController extends BaseController
{
	private static Logger log = LogManager.getLogger( BiblioController.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/BiblioBusiness" )
	private BiblioBusinessLocal biblioEjb;
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/IndexManager" )
	private IndexManagerLocal indexManagerEjb;
	
	@Secured( "ROLE_USER" )
	@RequestMapping( value = "/Data/Biblio/{id}", method = RequestMethod.GET, produces = "application/json" )
	public @ResponseBody JsonResponseModel getBiblioById( @PathVariable Integer id )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		Biblio biblio = null;
		
		try
		{
			biblio = biblioEjb.find( id );
			
			if ( biblio == null )
			{
				//@formatter:off
                addAetherMessage( 
                        new AetherMessage( 
                                getMessage( "messages.error" ), 
                                getMessage( "messages.no_record_by_id", new Object[] { id } 
                        ),
                        AetherMessageSeverity.DANGER ) 
                );
                //@formatter.on
            }
        }
        catch ( Exception e )
        {
            BiblioController.log.error( e );
            status = JsonResponseStatus.EXCEPTION;
            message = getMessage( "messages.unhandled_exception" );
        }
        //@formatter:off
        return new JsonResponseModel(
            status,
            message,
            biblio
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_CATALOGUE_MAINTENANCE" )
	@RequestMapping( value = "/Data/Biblio/", method = RequestMethod.POST, produces = "application/json" )
	public @ResponseBody JsonResponseModel addBiblio( @RequestBody Biblio biblio )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		
		try
		{
			biblioEjb.add( biblio, getCurrentUser() );
			//@formatter:off
            addAetherMessage( 
                new AetherMessage( 
                    getMessage("messages.success"), 
                    getMessage("messages.record_successfully_added", new Object[] { biblio.getTitle() }), 
                    AetherMessageSeverity.SUCCESS 
                )
            );
            //@formatter:on
		}
		catch ( Exception e )
		{
			BiblioController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
			//@formatter:off
            addAetherMessage( 
                new AetherMessage( 
                    getMessage("messages.error"), 
                    getMessage("messages.record_add_failed"), 
                    AetherMessageSeverity.DANGER 
                ) 
            );
            //@formatter:on
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            biblio
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_CATALOGUE_MAINTENANCE" )
	@RequestMapping( value = "/Data/Biblio/", method = RequestMethod.PUT, produces = "application/json" )
	public @ResponseBody JsonResponseModel saveBiblio( @RequestBody Biblio biblio )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		
		try
		{
			biblio = biblioEjb.update( biblio, getCurrentUser() );
			//@formatter:off
            addAetherMessage( 
                new AetherMessage( 
                    getMessage("messages.success"), 
                    getMessage("messages.record_successfully_updated", new Object[]{ biblio.getTitle() }),
                    AetherMessageSeverity.SUCCESS 
                )
            );
            //@formatter:on
		}
		catch ( Exception e )
		{
			BiblioController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
			//@formatter:off
            addAetherMessage( 
                new AetherMessage( 
                    getMessage("messages.error"),
                    getMessage("messages.record_save_failed", new Object[]{ biblio.getId() }),
                    AetherMessageSeverity.DANGER 
                )
            );
            //@formatter:on
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            biblio
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_USER" )
	@RequestMapping( value = "/Data/Biblio/Search", method = RequestMethod.POST, produces = "application/json" )
	public @ResponseBody JsonResponseModel searchBiblio( @RequestBody BiblioQuery query )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		IndexQuery iQuery = new IndexQuery( query );
		IndexResultSet resultSet = null;
		BiblioQueryResultSet results = null;
		
		try
		{
			if ( query.getQuery() == null || query.getQuery().length() < 1 )
			{
				message = getMessage( "messages.no_query_specified" );
				status = JsonResponseStatus.INVALID;
			}
			else
			{
				resultSet = indexManagerEjb.queryBiblios( iQuery );
				if ( resultSet.getTotalResults() > 0 )
				{
					results = new BiblioQueryResultSet( biblioEjb.getBibliosByIds( resultSet.getResults() ), resultSet.getTotalResults() );
				}
				else
				{
					results = new BiblioQueryResultSet( new ArrayList<Biblio>( 0 ), 0L );
					//@formatter:off
                    addAetherMessage( 
                        new AetherMessage( 
                            getMessage("messages.surprise"), 
                            getMessage("messages.no_records_returned"), 
                            AetherMessageSeverity.WARNING 
                        )
                    );
                    //@formatter:on
				}
			}
		}
		catch ( Exception e )
		{
			BiblioController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
			results = new BiblioQueryResultSet( new ArrayList<Biblio>( 0 ), 0L );
			//@formatter:off
            addAetherMessage( 
                new AetherMessage( 
                    getMessage("messages.error"), 
                    getMessage("messages.unhandled_exception"), 
                    AetherMessageSeverity.DANGER 
                )
            );
            //@formatter:on
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            results
        );
        //@formatter:on
	}
}
