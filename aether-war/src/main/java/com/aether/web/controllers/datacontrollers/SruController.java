//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.datacontrollers
 * File: SruController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * 
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Sep 17, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.datacontrollers;

import java.util.List;
import javax.ejb.EJB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.aether.ejb.beans.interfaces.SruBusinessLocal;
import com.aether.ejb.models.Biblio;
import com.aether.ejb.models.SruSource;
import com.aether.ejb.viewmodels.SruQuery;
import com.aether.ejb.viewmodels.SruResponseRecordSet;
import com.aether.web.controllers.BaseController;
import com.aether.web.json.JsonResponseModel;
import com.aether.web.json.JsonResponseStatus;
import com.aether.web.models.AetherMessage;
import com.aether.web.models.AetherMessageSeverity;

@Controller
public class SruController extends BaseController
{
    private static Logger log = LogManager.getLogger( SruController.class );

    @EJB(mappedName = "java:global/aether-ear/aether-ejb/SruBusiness")
    private SruBusinessLocal sruEjb;

    @Secured( "ROLE_CATALOGUE_MAINTENANCE" )
    @RequestMapping(value = "/Data/Sru/Sources", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody JsonResponseModel getSruSources()
    {
        JsonResponseStatus status = JsonResponseStatus.SUCCESS;
        String message = "";
        List<SruSource> sruSources = null;

        try
        {
            sruSources = sruEjb.getAllActiveSruSources();
        }
        catch ( Exception e )
        {
            SruController.log.error( e );
            status = JsonResponseStatus.EXCEPTION;
            message = getMessage( "messages.unhandled_exception" );
            addAetherMessage(
                    new AetherMessage( getMessage( "messages.error" ), getMessage( "librarian.catalogue_maintenance.sru_list_retrieval_failed" ), AetherMessageSeverity.DANGER ) );
        }

        //@formatter:off
        return new JsonResponseModel(
            status,
            message,
            sruSources
        );
        //@formatter:on
    }

    @Secured( "ROLE_CATALOGUE_MAINTENANCE" )
    @RequestMapping(value = "/Data/Sru/Query", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody JsonResponseModel executeSruQuery( @RequestBody SruQuery query )
    {
        JsonResponseStatus status = JsonResponseStatus.SUCCESS;
        String message = "";
        SruResponseRecordSet recordSet = null;

        try
        {
            recordSet = sruEjb.getSruRecordSet( query );

            // Replace the short record type name (e.g. language_material, notated_music, etc.) with
            // the locale version (e.g. Books/Serials, Notated Music, etc.)
            for ( Biblio nextBiblio : recordSet.getRecords() )
            {
                nextBiblio.setRecordTypeName( getMessage( "models.biblio.record_type." + nextBiblio.getRecordTypeName() ) );
            }
        }
        catch ( Exception e )
        {
            SruController.log.error( e );
            status = JsonResponseStatus.EXCEPTION;
            message = getMessage( "messages.unhandled_exception" );

            //@formatter:off
            addAetherMessage(
                new AetherMessage( 
                        getMessage( "messages.error" ),
                        getMessage( "messages.no_record_by_id", new Object[]{ query.getBiblioIdType() + ": " + query.getBiblioId() } ),
                        AetherMessageSeverity.DANGER 
                ) 
            );
            //@formatter:on
        }

        //@formatter:off
        return new JsonResponseModel(
            status,
            message,
            recordSet
        );
        //@formatter:on
    }
}
