//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.viewcontrollers
 * File: PatronMaintenanceController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * This controller delivers views for the Patrons Maintenance screens.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jul 29, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.viewcontrollers;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aether.ejb.beans.interfaces.BranchBusinessLocal;
import com.aether.ejb.beans.interfaces.UserBusinessLocal;
import com.aether.ejb.enums.UserSex;
import com.aether.ejb.enums.UserStatus;
import com.aether.ejb.models.Branch;
import com.aether.ejb.models.PatronCategory;
import com.aether.ejb.models.PatronUser;
import com.aether.ejb.models.Role;
import com.aether.web.controllers.BaseController;
import com.aether.web.models.Breadcrumb;
import com.aether.web.models.BreadcrumbLevel;

@Controller
public class PatronMaintenanceController extends BaseController
{
	@SuppressWarnings( "unused" )
	private static Logger log = LogManager.getLogger( EmployeeMaintenanceController.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/UserBusiness" )
	private UserBusinessLocal userEjb;
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/BranchBusiness" )
	private BranchBusinessLocal branchEjb;
	
	@Secured( "ROLE_PATRON_MAINTENANCE" )
	@RequestMapping( value = { "/Librarian/PatronMaintenance" }, method = RequestMethod.GET )
	public String index( ModelMap model )
	{
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_2, getMessage( "librarian.patron_maintenance.title" ), "/Librarian/PatronMaintenance" ) );
		
		return "views/Librarian/PatronMaintenance/Index";
	}
	
	@Secured( "ROLE_PATRON_MAINTENANCE" )
	@RequestMapping( value = { "/Librarian/PatronMaintenance/Edit/{id}" }, method = RequestMethod.GET )
	public String edit( ModelMap model, @PathVariable int id )
	{
		PatronUser patron = (PatronUser) userEjb.find( id );
		List<Branch> branches = branchEjb.list();
		List<String> statuses = Arrays.stream( UserStatus.values() ).map( s -> s.name() ).collect( Collectors.toList() );
		List<PatronCategory> patronCategories = userEjb.getActivePatronCategories();
		List<String> sexes = Arrays.stream( UserSex.values() ).map( s -> s.name() ).collect( Collectors.toList() );
		
		model.put( "patron", patron );
		model.put( "branches", branches );
		model.put( "statuses", statuses );
		model.put( "patronCategories", patronCategories );
		model.put( "sexes", sexes );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, getMessage( "librarian.patron_maintenance.edit.title" ), "/Librarian/PatronMaintenance/Edit/" + id ) );
		
		return "views/Librarian/PatronMaintenance/Edit";
	}
	
	@Secured( "ROLE_PATRON_MAINTENANCE" )
	@RequestMapping( value = { "/Librarian/PatronMaintenance/Add" }, method = RequestMethod.GET )
	public String add( ModelMap model )
	{
		PatronUser user = new PatronUser();
		List<Branch> branches = branchEjb.list();
		List<Role> roles = userEjb.getAllRoles();
		List<String> statuses = Arrays.stream( UserStatus.values() ).map( s -> s.name() ).collect( Collectors.toList() );
		List<PatronCategory> patronCategories = userEjb.getActivePatronCategories();
		List<String> sexes = Arrays.stream( UserSex.values() ).map( s -> s.name() ).collect( Collectors.toList() );
		
		user.setId( -1 );
		
		//@formatter:off
        user.setRole( roles
                        .stream()
                        .filter( (r) -> { return r.getRoleName().equals( "Patron" ); } )
                        .findFirst()
                        .orElse( null ) );
        //@formatter:on
		
		user.setStatus( UserStatus.ACTIVE );
		
		model.put( "user", user );
		model.put( "branches", branches );
		model.put( "statuses", statuses );
		model.put( "patronCategories", patronCategories );
		model.put( "sexes", sexes );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, getMessage( "librarian.patron_maintenance.add.title" ), "/Librarian/PatronMaintenance/Add" ) );
		
		return "views/Librarian/PatronMaintenance/Add";
	}
}
