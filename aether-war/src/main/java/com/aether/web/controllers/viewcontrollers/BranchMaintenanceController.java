//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.viewcontrollers
 * File: BranchMaintenanceController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides branch maintenance functionality.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Feb 24, 2017, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.viewcontrollers;

import java.util.List;

import javax.ejb.EJB;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aether.ejb.beans.interfaces.BranchBusinessLocal;
import com.aether.ejb.models.Branch;
import com.aether.web.controllers.BaseController;
import com.aether.web.models.Breadcrumb;
import com.aether.web.models.BreadcrumbLevel;

@Controller
public class BranchMaintenanceController extends BaseController
{
	@SuppressWarnings( "unused" )
	private static Logger log = LogManager.getLogger( BranchMaintenanceController.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/BranchBusiness" )
	private BranchBusinessLocal branchEjb;
	
	@Secured( "ROLE_LIBRARY_MANAGEMENT" )
	@RequestMapping( value = { "/Admin/BranchMaintenance" }, method = RequestMethod.GET )
	public String index( ModelMap model )
	{
		List<Branch> documents = branchEjb.list();
		
		model.put( "branches", documents );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, getMessage( "admin.branch_maintenance.title" ), "/Admin/BranchMaintenance" ) );
		
		return "views/Admin/BranchMaintenance/Index";
	}
	
	@Secured( "ROLE_LIBRARY_MANAGEMENT" )
	@RequestMapping( value = { "/Admin/BranchMaintenance/Edit/{id}" }, method = RequestMethod.GET )
	public String edit( ModelMap model, @PathVariable int id )
	{
		Branch branch = branchEjb.find( id );
		
		model.put( "branch", branch );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_4, getMessage( "admin.branch_maintenance.edit.title" ), "/Admin/BranchMaintenance/Edit/" + id ) );
		
		return "views/Admin/BranchMaintenance/Edit";
	}
	
	@Secured( "ROLE_LIBRARY_MANAGEMENT" )
	@RequestMapping( value = { "/Admin/BranchMaintenance/Add" }, method = RequestMethod.GET )
	public String add( ModelMap model )
	{
		Branch branch = new Branch();
		
		model.put( "branch", branch );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_4, getMessage( "admin.branch_maintenance.add.title" ), "/Admin/BranchMaintenance/Add" ) );
		
		return "views/Admin/BranchMaintenance/Add";
	}
}
