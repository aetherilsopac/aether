//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.datacontrollers
 * File: PreferencesController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * DataController providing RESTful CRUD operations for working with User records.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 11, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.datacontrollers;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.ejb.EJB;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aether.ejb.beans.interfaces.UserBusinessLocal;
import com.aether.ejb.models.PatronUser;
import com.aether.ejb.models.User;
import com.aether.ejb.models.minimized.UserMinimized;
import com.aether.ejb.viewmodels.PatronQuery;
import com.aether.ejb.viewmodels.PatronResultSet;
import com.aether.web.controllers.BaseController;
import com.aether.web.json.JsonResponseModel;
import com.aether.web.json.JsonResponseStatus;
import com.aether.web.models.AetherMessage;
import com.aether.web.models.AetherMessageSeverity;

@Controller
public class PatronsController extends BaseController
{
	private static Logger log = LogManager.getLogger( PatronsController.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/UserBusiness" )
	protected UserBusinessLocal userEjb;
	
	@Secured( "ROLE_PATRON_MAINTENANCE" )
	@RequestMapping( value = "/Data/Patrons/{id:\\d+}", method = RequestMethod.GET, produces = "application/json" )
	public @ResponseBody JsonResponseModel getPatronById( @PathVariable Integer id )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User patron = null;
		
		try
		{
			patron = userEjb.find( id );
			
			if ( !(PatronUser.class.isAssignableFrom( patron.getClass() )) )
			{
				PatronsController.log.error( "An attempt was made to retrieve a Patron with an invalid ID [" + id + "]." );
				status = JsonResponseStatus.EXCEPTION;
				message = getMessage( "messages.unhandled_exception" );
				addAetherMessage( new AetherMessage( getMessage( "messages.error" ), getMessage( "messages.no_record_by_id", new Object[] { id } ), AetherMessageSeverity.DANGER ) );
				patron = null;
			}
		}
		catch ( Exception e )
		{
			PatronsController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
			addAetherMessage( new AetherMessage( getMessage( "messages.error" ), getMessage( "messages.record_retrieval_failed", new Object[] { id } ), AetherMessageSeverity.DANGER ) );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            patron
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_PATRON_MAINTENANCE" )
	@RequestMapping( value = "/Data/Patrons", method = RequestMethod.PUT, produces = "application/json" )
	public @ResponseBody JsonResponseModel savePatron( @RequestBody PatronUser patron )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User currentUser = getCurrentUser();
		PatronUser originalPatron = (PatronUser) userEjb.find( patron.getId() );
		
		if ( patron.getUserName() != null )
		{
			try
			{
				patron.setUpdatedBy( originalPatron.getUpdatedBy() );
				patron.setDateUpdated( originalPatron.getDateUpdated() );
				patron.setCreatedBy( originalPatron.getCreatedBy() );
				patron.setDateCreated( originalPatron.getDateCreated() );
				
				if ( patron.getPassword() != null )
				{
					BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
					patron.setPasswordHash( encoder.encode( patron.getPassword() ) );
				}
				else
				{
					patron.setPasswordHash( originalPatron.getPasswordHash() );
				}
				
				patron.setPassword( null );
				
				BeanUtilsBean.getInstance().copyProperties( originalPatron, patron );
				patron = (PatronUser) userEjb.update( originalPatron, currentUser );
				
				addAetherMessage( new AetherMessage( getMessage( "messages.success" ), getMessage( "messages.record_successfully_updated", new Object[] { patron.getUserName() } ), AetherMessageSeverity.SUCCESS ) );
			}
			catch ( IllegalAccessException | InvocationTargetException e )
			{
				PatronsController.log.error( e );
				status = JsonResponseStatus.EXCEPTION;
				message = getMessage( "messages.unhandled_exception" );
				addAetherMessage( new AetherMessage( getMessage( "messages.error" ), getMessage( "messages.record_save_failed", new Object[] { patron.getUserName() } ), AetherMessageSeverity.DANGER ) );
			}
		}
		else
		{
			PatronsController.log.error( getMessage( "messages.no_data_provided" ) );
			message = getMessage( "messages.no_data_provided" );
			status = JsonResponseStatus.INVALID;
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            patron
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_PATRON_MAINTENANCE" )
	@RequestMapping( value = "/Data/Patrons", method = RequestMethod.POST, produces = "application/json" )
	public @ResponseBody JsonResponseModel addPatron( @RequestBody PatronUser user )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User currentUser = getCurrentUser();
		PatronUser existingUser = (PatronUser) userEjb.find( user.getId() );
		
		if ( existingUser == null )
		{
			existingUser = (PatronUser) userEjb.getUserByUserName( user.getUserName() );
		}
		
		if ( existingUser == null )
		{
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			user.setPasswordHash( encoder.encode( user.getPassword() ) );
			
			user.setId( null );
			user.setPassword( null );
			
			userEjb.add( user, currentUser );
			
			addAetherMessage( new AetherMessage( getMessage( "messages.success" ), getMessage( "messages.record_successfully_added", new Object[] { user.getUserName() } ), AetherMessageSeverity.SUCCESS ) );
		}
		else
		{
			PatronsController.log.error( getMessage( "messages.existing_user_provided" ) );
			message = getMessage( "messages.existing_user_provided" );
			status = JsonResponseStatus.INVALID;
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            user
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_PATRON_MAINTENANCE" )
	@RequestMapping( value = "/Data/Patrons/Query", method = RequestMethod.POST, produces = "application/json" )
	public @ResponseBody JsonResponseModel findActivePatrons( @RequestBody PatronQuery query )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		long totalPatrons = 0L;
		List<UserMinimized> patrons = null;
		
		totalPatrons = userEjb.countPatrons( query );
		patrons = userEjb.queryPatrons( query );
		
		PatronResultSet resultSet = new PatronResultSet( patrons, totalPatrons );
		
		return new JsonResponseModel( status, message, resultSet );
	}
	
	@Secured( "ROLE_PATRON_MAINTENANCE" )
	@RequestMapping( value = "/Data/Patrons/QueryAll", method = RequestMethod.POST, produces = "application/json" )
	public @ResponseBody JsonResponseModel findActiveOrInactivePatrons( @RequestBody PatronQuery query )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		long totalPatrons = 0L;
		List<UserMinimized> patrons = null;
		
		totalPatrons = userEjb.countPatrons( query );
		patrons = userEjb.queryPatrons( query );
		
		PatronResultSet resultSet = new PatronResultSet( patrons, totalPatrons );
		
		return new JsonResponseModel( status, message, resultSet );
	}
}
