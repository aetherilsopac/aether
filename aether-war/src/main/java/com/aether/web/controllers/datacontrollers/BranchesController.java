//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.datacontrollers
 * File: BranchesController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Data Controller providing RESTful CRUD operations for working with Branches.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Feb 24, 2017, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.datacontrollers;

import java.util.List;

import javax.ejb.EJB;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aether.ejb.beans.interfaces.BranchBusinessLocal;
import com.aether.ejb.models.Branch;
import com.aether.ejb.models.User;
import com.aether.web.controllers.BaseController;
import com.aether.web.json.JsonResponseModel;
import com.aether.web.json.JsonResponseStatus;
import com.aether.web.models.AetherMessage;
import com.aether.web.models.AetherMessageSeverity;

@Controller
public class BranchesController extends BaseController
{
	private static Logger log = LogManager.getLogger( BranchesController.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/BranchBusiness" )
	protected BranchBusinessLocal branchEjb;
	
	@Secured( "ROLE_USER" )
	@RequestMapping( value = "/Data/Branches", method = RequestMethod.GET, produces = "application/json" )
	public @ResponseBody JsonResponseModel getBranches()
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		List<Branch> branches = null;
		
		try
		{
			branches = branchEjb.list();
		}
		catch ( Exception e )
		{
			BranchesController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            branches
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_USER" )
	@RequestMapping( value = "/Data/Branches/{id:\\d+}", method = RequestMethod.GET, produces = "application/json" )
	public @ResponseBody JsonResponseModel getBranch( @PathVariable Integer id )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		Branch branch = null;
		
		try
		{
			branch = branchEjb.find( id );
			
			if ( branch == null )
			{
				//@formatter:off
                addAetherMessage( 
                        new AetherMessage( 
                                getMessage( "messages.error" ), 
                                getMessage( "messages.no_record_by_id", new Object[] { id } ),
                                AetherMessageSeverity.DANGER 
                        ) 
                );
                //@formatter:on
			}
		}
		catch ( Exception e )
		{
			BranchesController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            branch
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_LIBRARY_MANAGEMENT" )
	@RequestMapping( value = "/Data/Branches", method = RequestMethod.POST, produces = "application/json" )
	public @ResponseBody JsonResponseModel addBranch( @RequestBody Branch branch )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User currentUser = getCurrentUser();
		
		try
		{
			branchEjb.add( branch, currentUser );
			
			//@formatter:off
            addAetherMessage( 
                new AetherMessage( 
                    getMessage("messages.success"), 
                    getMessage("messages.record_successfully_added", new Object[] { branch.getBranchName() }), 
                    AetherMessageSeverity.SUCCESS 
                )
            );
            //@formatter:on
		}
		catch ( Exception e )
		{
			BranchesController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            branch
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_LIBRARY_MANAGEMENT" )
	@RequestMapping( value = "/Data/Branches", method = RequestMethod.PUT, produces = "application/json" )
	public @ResponseBody JsonResponseModel saveBranch( @RequestBody Branch branch )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User currentUser = getCurrentUser();
		
		try
		{
			branch = branchEjb.update( branch, currentUser );
			
			//@formatter:off
            addAetherMessage( 
                new AetherMessage( 
                    getMessage("messages.success"), 
                    getMessage("messages.record_successfully_updated", new Object[]{ branch.getBranchName() }),
                    AetherMessageSeverity.SUCCESS 
                )
            );
            //@formatter:on
		}
		catch ( Exception e )
		{
			BranchesController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            branch
        );
        //@formatter:on
	}
}
