//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.viewcontrollers
 * File: EmployeeMaintenanceController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides employee maintenance functionality.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 10, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.viewcontrollers;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aether.ejb.beans.interfaces.BranchBusinessLocal;
import com.aether.ejb.beans.interfaces.UserBusinessLocal;
import com.aether.ejb.enums.UserStatus;
import com.aether.ejb.models.Branch;
import com.aether.ejb.models.EmployeeUser;
import com.aether.ejb.models.Role;
import com.aether.ejb.models.minimized.UserMinimized;
import com.aether.web.controllers.BaseController;
import com.aether.web.models.Breadcrumb;
import com.aether.web.models.BreadcrumbLevel;

@Controller
public class EmployeeMaintenanceController extends BaseController
{
	@SuppressWarnings( "unused" )
	private static Logger log = LogManager.getLogger( EmployeeMaintenanceController.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/UserBusiness" )
	private UserBusinessLocal userEjb;
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/BranchBusiness" )
	private BranchBusinessLocal branchEjb;
	
	@Secured( "ROLE_ADMIN" )
	@RequestMapping( value = { "/Admin/EmployeeMaintenance" }, method = RequestMethod.GET )
	public String index( ModelMap model )
	{
		List<UserMinimized> users = userEjb.getAllEmployeeUsersMinimized();
		
		model.put( "users", users );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, getMessage( "admin.user_maintenance.title" ), "/Admin/EmployeeMaintenance" ) );
		
		return "views/Admin/EmployeeMaintenance/Index";
	}
	
	@Secured( "ROLE_ADMIN" )
	@RequestMapping( value = { "/Admin/EmployeeMaintenance/Edit/{id}" }, method = RequestMethod.GET )
	public String edit( ModelMap model, @PathVariable int id )
	{
		EmployeeUser user = (EmployeeUser) userEjb.find( id );
		List<Branch> branches = branchEjb.list();
		List<Role> roles = userEjb.getAllRoles();
		List<String> statuses = Arrays.stream( UserStatus.values() ).map( s -> s.name() ).collect( Collectors.toList() );
		
		model.put( "user", user );
		model.put( "branches", branches );
		model.put( "roles", roles );
		model.put( "statuses", statuses );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_4, getMessage( "admin.user_maintenance.edit.title" ), "/Admin/EmployeeMaintenance/Edit/" + id ) );
		
		return "views/Admin/EmployeeMaintenance/Edit";
	}
	
	@Secured( "ROLE_ADMIN" )
	@RequestMapping( value = { "/Admin/EmployeeMaintenance/Add" }, method = RequestMethod.GET )
	public String add( ModelMap model )
	{
		EmployeeUser user = new EmployeeUser();
		List<Branch> branches = branchEjb.list();
		List<Role> roles = userEjb.getAllRoles();
		List<String> statuses = Arrays.stream( UserStatus.values() ).map( s -> s.name() ).collect( Collectors.toList() );
		
		user.setId( -1 );
		
		//@formatter:off
        user.setRole( roles
                        .stream()
                        .filter( (r) -> { return r.getRoleName().equals( "Admin" ); } )
                        .findFirst()
                        .orElse( null ) );
        //@formatter:on
		
		user.setStatus( UserStatus.ACTIVE );
		
		model.put( "user", user );
		model.put( "branches", branches );
		model.put( "roles", roles );
		model.put( "statuses", statuses );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_4, getMessage( "admin.user_maintenance.add.title" ), "/Admin/EmployeeMaintenance/Add" ) );
		
		return "views/Admin/EmployeeMaintenance/Add";
	}
}
