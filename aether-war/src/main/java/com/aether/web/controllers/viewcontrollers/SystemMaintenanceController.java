//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.viewcontrollers
 * File: SystemMaintenanceController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Controller for all System Maintenance functions.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Oct 27, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.viewcontrollers;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

import javax.ejb.EJB;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aether.ejb.common.Constants;
import com.aether.ejb.indexing.IndexManagerLocal;
import com.aether.web.controllers.BaseController;
import com.aether.web.models.Breadcrumb;
import com.aether.web.models.BreadcrumbLevel;
import com.aether.web.singletonbeans.AetherSystemSettings;

@Controller
public class SystemMaintenanceController extends BaseController
{
	@SuppressWarnings( "unused" )
	private static Logger log = LogManager.getLogger( SystemMaintenanceController.class );
	
	@Autowired
	private AetherSystemSettings systemSettings;
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/IndexManager" )
	private IndexManagerLocal indexManagerEjb;
	
	@Secured( "ROLE_ADMIN" )
	@RequestMapping( value = { "/Admin/SystemMaintenance" }, method = RequestMethod.GET )
	public String index( ModelMap model )
	{
		// Update System Settings
		systemSettings.loadFromDatabase();
		
		// Create path to custom logo file
		Path logoPath = Paths.get( Constants.DIRECTORY_SYSTEM_RESOURCES.toString(), Constants.FILE_NAME_CUSTOM_LOGO );
		File customLogo = logoPath.toFile();
		
		//@formatter:off
        String lastReindexDate = ( systemSettings.getLastReindexDate() != null ) ? 
                new SimpleDateFormat( getMessage( "formats.date_time_format" ) ).format( systemSettings.getLastReindexDate() ) : 
                getMessage( "messages.never" ).toLowerCase();
        //@formatter:on
		
		model.put( "customLogoExists", customLogo.exists() );
		model.put( "systemSettings", systemSettings );
		model.put( "isReindexing", indexManagerEjb.isReindexing() );
		model.put( "reindexProgress", indexManagerEjb.getReindexProgress() );
		model.put( "lastReindexMessage", getMessage( "messages.last_reindexing", new Object[] { lastReindexDate, systemSettings.getLastReindexRecordCount() } ) );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_2, getMessage( "admin.title" ), "/Admin" ) );
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, getMessage( "admin.system_maintenance.title" ), "/Admin/SystemMaintenance" ) );
		
		return "views/Admin/SystemMaintenance/Index";
	}
	
}
