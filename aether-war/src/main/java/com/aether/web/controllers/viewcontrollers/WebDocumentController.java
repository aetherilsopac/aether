//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.viewcontrollers
 * File: WebDocumentController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Implements logic for serving Web Documents (help, wiki, fragment, etc.).
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Dec 22, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.viewcontrollers;

import java.util.Locale;
import javax.ejb.EJB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.aether.ejb.beans.interfaces.WebDocumentBusinessLocal;
import com.aether.ejb.common.Constants;
import com.aether.ejb.enums.WebDocumentType;
import com.aether.ejb.models.WebDocument;
import com.aether.web.controllers.BaseController;
import com.aether.web.models.Breadcrumb;
import com.aether.web.models.BreadcrumbLevel;

@Controller
public class WebDocumentController extends BaseController
{
    @SuppressWarnings("unused")
    private Logger log = LogManager.getLogger( WebDocumentController.class );

    @EJB(mappedName = "java:global/aether-ear/aether-ejb/WebDocumentBusiness")
    private WebDocumentBusinessLocal webDocumentEjb;

    @Secured("ROLE_USER")
    @RequestMapping(value = "/Help", method = RequestMethod.GET)
    public String Help( ModelMap model )
    {
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_2, getMessage( "help.title" ), "/Help" ) );

        return "views/Help/Index";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/Help/{friendlyUrl}", method = RequestMethod.GET)
    public String HelpPageDefaultLocale( ModelMap model, @PathVariable String friendlyUrl )
    {
        Locale userLocale = LocaleContextHolder.getLocale();
        String localeLanguageAndCountry = userLocale.getLanguage() + "_" + userLocale.getCountry();

        //@formatter:off
        WebDocument helpDocument = webDocumentEjb.getDocumentByUrlAndLocaleWithFallbackLocale( 
                                friendlyUrl, 
                                localeLanguageAndCountry, 
                                Constants.DEFAULT_LOCALE,
                                WebDocumentType.H );
        //@formatter:on

        // If it wasn't found show the Document Not Found page
        if( helpDocument == null )
        {
            return "views/Help/DocumentNotFound";
        }

        // If it was found, show the document and generate breadcrumbs
        model.put( "helpDocument", helpDocument );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_2, getMessage( "help.title" ), "/Help" ) );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, helpDocument.getTitle(), "/Help/" + friendlyUrl ) );

        return "views/Help/HelpDocument";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/Help/{id:\\d+}", method = RequestMethod.GET)
    public String HelpPageById( ModelMap model, @PathVariable Integer id )
    {
        //@formatter:off
        WebDocument helpDocument = webDocumentEjb.getDocumentByIdAndType( id, WebDocumentType.H );
        //@formatter:on

        // If it wasn't found show the Document Not Found page
        if( helpDocument == null )
        {
            return "views/Help/DocumentNotFound";
        }

        // If it was found, show the document and generate breadcrumbs
        model.put( "helpDocument", helpDocument );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_2, getMessage( "help.title" ), "/Help" ) );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, helpDocument.getTitle(), "/Help/" + id ) );

        return "views/Help/HelpDocument";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/Help/{friendlyUrl}/{locale}", method = RequestMethod.GET)
    public String HelpPageSpecificLocale( ModelMap model, @PathVariable String friendlyUrl, @PathVariable String locale )
    {
        //@formatter:off
        WebDocument helpDocument = webDocumentEjb.getDocumentByUrlAndLocale( 
                                friendlyUrl, 
                                locale,
                                WebDocumentType.H );
        //@formatter:on

        // If it wasn't found show the Document Not Found page
        if( helpDocument == null )
        {
            return "views/Help/DocumentNotFound";
        }

        // If it was found, show the document and generate breadcrumbs
        model.put( "helpDocument", helpDocument );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_2, getMessage( "help.title" ), "/Help" ) );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, helpDocument.getTitle(), "/Help/" + friendlyUrl + "/" + locale ) );

        return "views/Help/HelpDocument";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/Wiki/{friendlyUrl}", method = RequestMethod.GET)
    public String WikiPageDefaultLocale( ModelMap model, @PathVariable String friendlyUrl )
    {
        Locale userLocale = LocaleContextHolder.getLocale();
        String localeLanguageAndCountry = userLocale.getLanguage() + "_" + userLocale.getCountry();

        //@formatter:off
        WebDocument document = webDocumentEjb.getDocumentByUrlAndLocaleWithFallbackLocale( 
                                friendlyUrl, 
                                localeLanguageAndCountry, 
                                Constants.DEFAULT_LOCALE,
                                WebDocumentType.W );
        //@formatter:on

        // If it wasn't found show the Document Not Found page
        if( document == null )
        {
            return "views/Wiki/DocumentNotFound";
        }

        // If it was found, show the document and generate breadcrumbs
        model.put( "document", document );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_2, getMessage( "help.title" ), "/Help" ) );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, document.getTitle(), "/Wiki/" + friendlyUrl ) );

        return "views/Wiki/Document";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/Wiki/{id:\\d+}", method = RequestMethod.GET)
    public String WikiPageById( ModelMap model, @PathVariable Integer id )
    {
        //@formatter:off
        WebDocument document = webDocumentEjb.getDocumentByIdAndType( id, WebDocumentType.W );
        //@formatter:on

        // If it wasn't found show the Document Not Found page
        if( document == null )
        {
            return "views/Wiki/DocumentNotFound";
        }

        // If it was found, show the document and generate breadcrumbs
        model.put( "document", document );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_2, getMessage( "wiki.home_page" ), "/Wiki/wiki_home" ) );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, document.getTitle(), "/Wiki/" + id ) );

        return "views/Wiki/Document";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/Wiki/{friendlyUrl}/{locale}", method = RequestMethod.GET)
    public String WikiPageSpecificLocale( ModelMap model, @PathVariable String friendlyUrl, @PathVariable String locale )
    {
        //@formatter:off
        WebDocument document = webDocumentEjb.getDocumentByUrlAndLocale( 
                                friendlyUrl, 
                                locale,
                                WebDocumentType.W );
        //@formatter:on

        // If it wasn't found show the Document Not Found page
        if( document == null )
        {
            return "views/Wiki/DocumentNotFound";
        }

        // If it was found, show the document and generate breadcrumbs
        model.put( "document", document );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_2, getMessage( "wiki.home_page" ), "/Wiki/wiki_home" ) );
        addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, document.getTitle(), "/Wiki/" + friendlyUrl + "/" + locale ) );

        return "views/Wiki/Document";
    }
}
