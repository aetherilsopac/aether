//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.datacontrollers
 * File: ShelvingLocationDataController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Data Controller providing RESTful CRUD operations for working with Shelving Locations.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Feb 09, 2017, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.datacontrollers;

import java.util.List;

import javax.ejb.EJB;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aether.ejb.beans.interfaces.ShelvingLocationBusinessLocal;
import com.aether.ejb.models.ShelvingLocation;
import com.aether.ejb.models.User;
import com.aether.ejb.models.minimized.ShelvingLocationMinimized;
import com.aether.web.controllers.BaseController;
import com.aether.web.json.JsonResponseModel;
import com.aether.web.json.JsonResponseStatus;
import com.aether.web.models.AetherMessage;
import com.aether.web.models.AetherMessageSeverity;

@Controller
public class ShelvingController extends BaseController
{
	private static Logger log = LogManager.getLogger( ShelvingController.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/ShelvingLocationBusiness" )
	protected ShelvingLocationBusinessLocal shelvingLocationEjb;
	
	@Secured( "ROLE_USER" )
	@RequestMapping( value = "/Data/Shelves", method = RequestMethod.GET, produces = "application/json" )
	public @ResponseBody JsonResponseModel getActiveShelvingLocations()
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		List<ShelvingLocationMinimized> locations = null;
		
		try
		{
			locations = shelvingLocationEjb.getActiveShelvingLocationsMinimized();
		}
		catch ( Exception e )
		{
			ShelvingController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            locations
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_USER" )
	@RequestMapping( value = "/Data/Shelves/{id:\\d+}", method = RequestMethod.GET, produces = "application/json" )
	public @ResponseBody JsonResponseModel getShelvingLocation( @PathVariable Integer id )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		ShelvingLocation location = null;
		
		try
		{
			location = shelvingLocationEjb.find( id );
			
			if ( location == null )
			{
				//@formatter:off
                addAetherMessage( 
                        new AetherMessage( 
                                getMessage( "messages.error" ), 
                                getMessage( "messages.no_record_by_id", new Object[] { id } ),
                                AetherMessageSeverity.DANGER 
                        ) 
                );
                //@formatter:on
			}
		}
		catch ( Exception e )
		{
			ShelvingController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            location
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_LIBRARY_MANAGEMENT" )
	@RequestMapping( value = "/Data/Shelves/All", method = RequestMethod.GET, produces = "application/json" )
	public @ResponseBody JsonResponseModel getAllShelvingLocations()
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		List<ShelvingLocationMinimized> locations = null;
		
		try
		{
			locations = shelvingLocationEjb.getAllShelvingLocationsMinimized();
		}
		catch ( Exception e )
		{
			ShelvingController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            locations
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_LIBRARY_MANAGEMENT" )
	@RequestMapping( value = "/Data/Shelves", method = RequestMethod.POST, produces = "application/json" )
	public @ResponseBody JsonResponseModel addShelvingLocation( @RequestBody ShelvingLocation location )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User currentUser = getCurrentUser();
		
		try
		{
			// Confirm there are no documents where friendly URL, locale, and type all match
			Long matchingLocations = shelvingLocationEjb.countMatchingShelvingLocations( location );
			
			if ( matchingLocations == 0L )
			{
				shelvingLocationEjb.add( location, currentUser );
				
				//@formatter:off
                addAetherMessage( 
                    new AetherMessage( 
                        getMessage("messages.success"), 
                        getMessage("messages.record_successfully_added", new Object[] { location.getName() }), 
                        AetherMessageSeverity.SUCCESS 
                    )
                );
                //@formatter:on
			}
			else
			{
				ShelvingController.log.warn( "An attempt was made to add a non-unique Shelving Location." );
				status = JsonResponseStatus.INVALID;
				message = "<strong>" + getMessage( "messages.surprise" ) + "</strong> " + getMessage( "admin.shelving_maintenance.non_unique_shelving_location" );
			}
		}
		catch ( Exception e )
		{
			ShelvingController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            location
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_LIBRARY_MANAGEMENT" )
	@RequestMapping( value = "/Data/Shelves", method = RequestMethod.PUT, produces = "application/json" )
	public @ResponseBody JsonResponseModel saveShelvingLocation( @RequestBody ShelvingLocation location )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User currentUser = getCurrentUser();
		
		try
		{
			// Confirm there are no documents where friendly URL, locale, and type all match
			Long matchingLocations = shelvingLocationEjb.countMatchingShelvingLocations( location );
			
			if ( matchingLocations == 0L )
			{
				location = shelvingLocationEjb.update( location, currentUser );
				
				//@formatter:off
                addAetherMessage( 
                    new AetherMessage( 
                        getMessage("messages.success"), 
                        getMessage("messages.record_successfully_updated", new Object[]{ location.getName() }),
                        AetherMessageSeverity.SUCCESS 
                    )
                );
                //@formatter:on
			}
			else
			{
				ShelvingController.log.warn( "An attempt was made to add a non-unique Shelving Location." );
				status = JsonResponseStatus.INVALID;
				message = "<strong>" + getMessage( "messages.surprise" ) + "</strong> " + getMessage( "admin.shelving_maintenance.non_unique_shelving_location" );
			}
		}
		catch ( Exception e )
		{
			ShelvingController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            location
        );
        //@formatter:on
	}
}
