//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.datacontrollers
 * File: PreferencesController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * DataController providing RESTful CRUD operations for working with User records.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 11, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.datacontrollers;

import java.lang.reflect.InvocationTargetException;

import javax.ejb.EJB;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aether.ejb.beans.interfaces.UserBusinessLocal;
import com.aether.ejb.models.EmployeeUser;
import com.aether.ejb.models.User;
import com.aether.web.controllers.BaseController;
import com.aether.web.json.JsonResponseModel;
import com.aether.web.json.JsonResponseStatus;
import com.aether.web.models.AetherMessage;
import com.aether.web.models.AetherMessageSeverity;

@Controller
public class EmployeesController extends BaseController
{
	private static Logger log = LogManager.getLogger( EmployeesController.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/UserBusiness" )
	protected UserBusinessLocal userEjb;
	
	@Secured( "ROLE_ADMIN" )
	@RequestMapping( value = "/Data/Employees/{id:\\d+}", method = RequestMethod.GET, produces = "application/json" )
	public @ResponseBody JsonResponseModel getEmployeeById( @PathVariable Integer id )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User employee = null;
		
		try
		{
			employee = userEjb.find( id );
			
			if ( !(EmployeeUser.class.isAssignableFrom( employee.getClass() )) )
			{
				EmployeesController.log.error( "An attempt was made to retrieve a Employee with an invalid ID [" + id + "]." );
				status = JsonResponseStatus.EXCEPTION;
				message = getMessage( "messages.unhandled_exception" );
				addAetherMessage( new AetherMessage( getMessage( "messages.error" ), getMessage( "messages.no_record_by_id", new Object[] { id } ), AetherMessageSeverity.DANGER ) );
				employee = null;
			}
		}
		catch ( Exception e )
		{
			EmployeesController.log.error( e );
			status = JsonResponseStatus.EXCEPTION;
			message = getMessage( "messages.unhandled_exception" );
			addAetherMessage( new AetherMessage( getMessage( "messages.error" ), getMessage( "messages.no_record_by_id", new Object[] { id } ), AetherMessageSeverity.DANGER ) );
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            employee
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_ADMIN" )
	@RequestMapping( value = "/Data/Employees", method = RequestMethod.PUT, produces = "application/json" )
	public @ResponseBody JsonResponseModel saveEmployee( @RequestBody EmployeeUser user )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User currentUser = getCurrentUser();
		EmployeeUser originalUser = (EmployeeUser) userEjb.find( user.getId() );
		
		if ( user.getUserName() != null )
		{
			try
			{
				user.setUpdatedBy( originalUser.getUpdatedBy() );
				user.setDateUpdated( originalUser.getDateUpdated() );
				user.setCreatedBy( originalUser.getCreatedBy() );
				user.setDateCreated( originalUser.getDateCreated() );
				
				if ( user.getPassword() != null )
				{
					BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
					user.setPasswordHash( encoder.encode( user.getPassword() ) );
				}
				else
				{
					user.setPasswordHash( originalUser.getPasswordHash() );
				}
				
				user.setPassword( null );
				
				BeanUtilsBean.getInstance().copyProperties( originalUser, user );
				user = (EmployeeUser) userEjb.update( originalUser, currentUser );
				
				addAetherMessage( new AetherMessage( getMessage( "messages.success" ), getMessage( "messages.record_successfully_updated", new Object[] { user.getUserName() } ), AetherMessageSeverity.SUCCESS ) );
			}
			catch ( IllegalAccessException | InvocationTargetException e )
			{
				EmployeesController.log.error( e );
				status = JsonResponseStatus.EXCEPTION;
				message = getMessage( "messages.unhandled_exception" );
				addAetherMessage( new AetherMessage( getMessage( "messages.error" ), getMessage( "messages.record_save_failed", new Object[] { user.getUserName() } ), AetherMessageSeverity.DANGER ) );
			}
		}
		else
		{
			EmployeesController.log.error( getMessage( "messages.no_data_provided" ) );
			message = getMessage( "messages.no_data_provided" );
			status = JsonResponseStatus.INVALID;
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            user
        );
        //@formatter:on
	}
	
	@Secured( "ROLE_ADMIN" )
	@RequestMapping( value = "/Data/Employees", method = RequestMethod.POST, produces = "application/json" )
	public @ResponseBody JsonResponseModel addEmployee( @RequestBody EmployeeUser user )
	{
		JsonResponseStatus status = JsonResponseStatus.SUCCESS;
		String message = "";
		User currentUser = getCurrentUser();
		User existingUser = userEjb.find( user.getId() );
		
		if ( existingUser == null )
		{
			existingUser = userEjb.getUserByUserName( user.getUserName() );
		}
		
		if ( existingUser == null )
		{
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			user.setPasswordHash( encoder.encode( user.getPassword() ) );
			
			user.setId( null );
			user.setPassword( null );
			
			userEjb.add( user, currentUser );
			
			addAetherMessage( new AetherMessage( getMessage( "messages.success" ), getMessage( "messages.record_successfully_added", new Object[] { user.getUserName() } ), AetherMessageSeverity.SUCCESS ) );
		}
		else
		{
			EmployeesController.log.error( getMessage( "messages.existing_user_provided" ) );
			message = getMessage( "messages.existing_user_provided" );
			status = JsonResponseStatus.INVALID;
		}
		
		//@formatter:off
        return new JsonResponseModel(
            status,
            message,
            user
        );
        //@formatter:on
	}
}
