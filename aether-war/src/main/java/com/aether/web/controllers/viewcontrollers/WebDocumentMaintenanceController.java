//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.controllers.viewcontrollers
 * File: WebDocumentMaintenanceController.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Provides web document maintenance functionality.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jun 10, 2016, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.controllers.viewcontrollers;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aether.ejb.beans.interfaces.WebDocumentBusinessLocal;
import com.aether.ejb.enums.WebDocumentType;
import com.aether.ejb.models.WebDocument;
import com.aether.ejb.models.minimized.WebDocumentMinimized;
import com.aether.web.controllers.BaseController;
import com.aether.web.models.Breadcrumb;
import com.aether.web.models.BreadcrumbLevel;

@Controller
public class WebDocumentMaintenanceController extends BaseController
{
	@SuppressWarnings( "unused" )
	private static Logger log = LogManager.getLogger( WebDocumentMaintenanceController.class );
	
	@EJB( mappedName = "java:global/aether-ear/aether-ejb/WebDocumentBusiness" )
	private WebDocumentBusinessLocal webDocumentEjb;
	
	@Secured( "ROLE_WEB_MAINTENANCE" )
	@RequestMapping( value = { "/Admin/WebDocumentMaintenance" }, method = RequestMethod.GET )
	public String index( ModelMap model )
	{
		List<WebDocumentMinimized> documents = webDocumentEjb.getAllWebDocumentsMinimized();
		Map<String, String> documentTypes = new HashMap<String, String>();
		for ( WebDocumentType documentType : WebDocumentType.values() )
		{
			documentTypes.put( documentType.name(), getMessage( "models.web_document.document_type." + documentType.name() ) );
		}
		
		model.put( "documents", documents );
		model.put( "documentTypes", documentTypes );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_3, getMessage( "admin.web_document_maintenance.title" ), "/Admin/WebDocumentMaintenance" ) );
		
		return "views/Admin/WebDocumentMaintenance/Index";
	}
	
	@Secured( "ROLE_WEB_MAINTENANCE" )
	@RequestMapping( value = { "/Admin/WebDocumentMaintenance/Edit/{id}" }, method = RequestMethod.GET )
	public String edit( ModelMap model, @PathVariable int id )
	{
		WebDocument document = webDocumentEjb.find( id );
		Map<String, String> documentTypes = new HashMap<String, String>();
		for ( WebDocumentType documentType : WebDocumentType.values() )
		{
			documentTypes.put( documentType.name(), getMessage( "models.web_document.document_type." + documentType.name() ) );
		}
		
		model.put( "document", document );
		model.put( "documentTypes", documentTypes );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_4, getMessage( "admin.web_document_maintenance.edit.title" ), "/Admin/WebDocumentMaintenance/Edit/" + id ) );
		
		return "views/Admin/WebDocumentMaintenance/Edit";
	}
	
	@Secured( "ROLE_WEB_MAINTENANCE" )
	@RequestMapping( value = { "/Admin/WebDocumentMaintenance/Add" }, method = RequestMethod.GET )
	public String add( ModelMap model )
	{
		WebDocument document = new WebDocument();
		Locale userLocale = LocaleContextHolder.getLocale();
		Map<String, String> documentTypes = new HashMap<String, String>();
		for ( WebDocumentType documentType : WebDocumentType.values() )
		{
			// No new "Fragments" are allowed, so only add the other document
			// types
			if ( documentType != WebDocumentType.F )
			{
				documentTypes.put( documentType.name(), getMessage( "models.web_document.document_type." + documentType.name() ) );
			}
		}
		
		document.setLocale( userLocale.getLanguage() + "_" + userLocale.getCountry() );
		
		model.put( "document", document );
		model.put( "documentTypes", documentTypes );
		
		addBreadcrumb( new Breadcrumb( BreadcrumbLevel.LEVEL_4, getMessage( "admin.web_document_maintenance.add.title" ), "/Admin/WebDocumentMaintenance/Add" ) );
		
		return "views/Admin/WebDocumentMaintenance/Add";
	}
}
