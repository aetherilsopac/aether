//@formatter:off
/***************************************************************************************************************************************************************
 * #######################
 * #   FILE DESCRIPTOR   #
 * #######################
 * Application: AetherWEB
 * Package: com.aether.web.common
 * File: WebConstants.java
 * 
 * #######################
 * #     MIT LICENSE     #
 * ####################### 
 * Copyright (c) 2016, Clinton Bush.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell   
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER  
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * #######################
 * #       Purpose       #
 * #######################
 * Stores commonly used constants for the Web project. Some of these may be repeats
 * of what is in the Constants folder for the EJB project due to classloading issues.
 *
 * #######################
 * #      Revision       #
 * ####################### 
 * Jan 1, 2017, Clinton Bush, 1.0.0,
 *    New file.
 * 
 *************************************************************************************************************************************************************** 
 */
//@formatter:on

package com.aether.web.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.tuple.ImmutablePair;

import com.aether.ejb.common.Constants;

public class WebConstants
{
	/**
	 * Max Width of Thumbnails in pixels.
	 */
	public static int MAX_THUMBNAIL_WIDTH = 90;
	
	/**
	 * Max Heght of Thumbnails in pixels.
	 */
	public static int MAX_THUMBNAIL_HEIGHT = 90;
	
	/**
	 * Pattern for testing a file name to see if it ends in an approved image
	 * file extension.
	 */
	public static Pattern IMAGE_FILE_NAMES_PATTERN = Pattern.compile( "^.*?(\\.)(" + String.join( "|", Constants.IMAGE_FILE_EXTENSIONS ) + ")$", Pattern.CASE_INSENSITIVE );
	
	/**
	 * Pattern for parsing out sections of a file name
	 */
	public static Pattern FILE_NAME_PATTERN = Pattern.compile( "^(.+?)\\.([A-Za-z0-9]+)$" );
	
	/**
	 * List of item types used by Aether and their corresponding character code
	 * (MARC Leader, Position 06).
	 */
	//@formatter:off
    public static final List<ImmutablePair<Character, String>> marcBiblioRecordTypes = Collections.unmodifiableList(
        Arrays.asList( 
            new ImmutablePair<Character, String>( 'a', "language_material" ),
            new ImmutablePair<Character, String>( 'c', "notated_music" ),
            new ImmutablePair<Character, String>( 'd', "manuscript_music" ),
            new ImmutablePair<Character, String>( 'e', "maps" ),
            new ImmutablePair<Character, String>( 'f', "manuscript_maps" ),
            new ImmutablePair<Character, String>( 'g', "projected_media" ),
            new ImmutablePair<Character, String>( 'i', "recorded_sound" ),
            new ImmutablePair<Character, String>( 'j', "recorded_music" ),
            new ImmutablePair<Character, String>( 'k', "2d_graphics" ),
            new ImmutablePair<Character, String>( 'm', "computer_file" ),
            new ImmutablePair<Character, String>( 'o', "kits" ),
            new ImmutablePair<Character, String>( 'p', "mixed_materials" ),
            new ImmutablePair<Character, String>( 'r', "3d_artifacts" ),
            new ImmutablePair<Character, String>( 't', "manuscripts" )
        )
    );
    //@formatter:on
	
	/**
	 * Mapping of item type character codes (MARC Leader, Position 06) to
	 * Material Design Icons.
	 */
	//@formatter:off
    public static final Map<Character, String> marcBiblioRecordTypeIcons = Collections.unmodifiableMap(
        new HashMap<Character, String>() {
            private static final long serialVersionUID = -837168619214962846L;
            {
                put( 'a', "library_books" );
                put( 'c', "queue_music" );
                put( 'd', "queue_music" );
                put( 'e', "map" );
                put( 'f', "map" );
                put( 'g', "local_movies" ); 
                put( 'i', "volume_up" );
                put( 'j', "library_music" );
                put( 'k', "photo" );
                put( 'm', "computer" );
                put( 'o', "shopping_basket" );            
                put( 'p', "collections" );
                put( 'r', "language" );
                put( 't', "subject" );      
            }
        }
    );
    //@formatter:on
}
