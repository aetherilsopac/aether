package com.aether.web.common;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.regex.Matcher;

import com.aether.ejb.common.Constants;

public class Utility
{
	public static boolean isImage( String fileName )
	{
		Matcher m = WebConstants.FILE_NAME_PATTERN.matcher( fileName );
		if ( m.matches() )
		{
			return Arrays.binarySearch( Constants.IMAGE_FILE_EXTENSIONS, m.group( 2 ) ) > -1;
		}
		return false;
	}
	
	public static BufferedImage scaleImage( BufferedImage image, int maxWidth, int maxHeight )
	{
		double scaleFactor = 0d;
		int newWidth = 0;
		int newHeight = 0;
		
		if ( image.getWidth() > maxWidth || image.getHeight() > maxHeight )
		{
			if ( image.getWidth() >= image.getHeight() )
			{
				scaleFactor = (double) maxWidth / (double) image.getWidth();
				newWidth = maxWidth;
				newHeight = (int) (image.getHeight() * scaleFactor);
			}
			else
			{
				scaleFactor = (double) maxHeight / (double) image.getHeight();
				newHeight = maxHeight;
				newWidth = (int) (image.getWidth() * scaleFactor);
			}
			
			BufferedImage returnImage = new BufferedImage( newWidth, newHeight, image.getType() );
			Graphics2D g2d = returnImage.createGraphics();
			g2d.drawImage( image, 0, 0, newWidth, newHeight, null );
			g2d.dispose();
			
			return returnImage;
		}
		else
		{
			return image;
		}
	}
}
