echo "Installing local dependencies..."
mvn install:install-file -Dfile="./dependencies/cql-java-1.12/cql-java-1.12.jar" -DgroupId=org.z3950 -DartifactId=cql-java -Dversion=1.12 -Dpackaging=jar
mvn install:install-file -Dfile="./dependencies/marc4j-2.6.5/marc4j-2.6.5.jar" -DgroupId=org.marc4j -DartifactId=marc4j -Dversion=2.6.5 -Dpackaging=jar